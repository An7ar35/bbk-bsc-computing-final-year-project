/*
 * coordinate_tool_test.hpp
 *
 *  Created on: 30 Mar 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_COORDINATE_COORDINATE_TOOL_TEST_HPP_
#define SATELLITES_COORDINATE_COORDINATE_TOOL_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/core/database/Db.hpp"
#include "../../../src/satellites/coordinate/coordinate_tool.hpp"
#include "../../../src/core/database/tables/JumpConnections.hpp"

namespace Coordinate_tool_Tests {
	//-----Helper functions------
	void setup() {
		Db testdata( "testdata.db" );
		testdata.open();
		testdata.sqlQueryPush( "DROP TABLE IF EXISTS JumpSystemsCPtest" );
		testdata.sqlQueryPush(
				"CREATE TABLE JumpSystemsCPtest( S_ID INTEGER PRIMARY KEY AUTOINCREMENT, EliteID INTEGER DEFAULT NULL, Name varchar(50) NOT NULL, X_Coordinate DOUBLE, Y_Coordinate DOUBLE, Z_Coordinate DOUBLE, UNIQUE( Name, EliteID ) )" );
		testdata.sqlQueryPush(
				"INSERT INTO JumpSystemsCPtest(S_ID,EliteID,Name) SELECT S_ID, EliteID, Name FROM JumpSystems WHERE S_ID = 1" );
		testdata.sqlQueryPush(
				"UPDATE JumpSystemsCPtest2 SET X_Coordinate=NULL, Y_Coordinate=NULL,Z_Coordinate=NULL WHERE S_ID > 4" );
		testdata.close();
	}
	Coordinate3D getCoords( Db &data, int point_id, std::string table ) {
		Table t { };
		Coordinate3D coordinates { };
		std::ostringstream oss { };
		oss << "SELECT X_Coordinate, Y_Coordinate, Z_Coordinate FROM " << table << " WHERE S_ID = "
				<< point_id;
		data.sqlQueryPull_typed( oss.str(), t );
		if( t.getRowCount() > 0 ) {
			int x_col = t.whereIsThisColumn( "X_Coordinate" );
			int y_col = t.whereIsThisColumn( "Y_Coordinate" );
			int z_col = t.whereIsThisColumn( "Z_Coordinate" );
			if( t( x_col, 0 ).getString().length() == 0 || t( y_col, 0 ).getString().length() == 0
					|| t( z_col, 0 ).getString().length() == 0 ) {
				std::cout
						<< "UNIT TEST [getCoords] Problem with coordinates from JumpSystemsCPtest."
						<< std::endl;
			}
			coordinates.x = t( x_col, 0 ).getDouble();
			coordinates.y = t( y_col, 0 ).getDouble();
			coordinates.z = t( z_col, 0 ).getDouble();
		}
		return coordinates;
	}

	void generateCoordinatesFrom__test0() { //Hardcore test - propagation one by one
		Coordinate_tool_Tests::setup();
		Db testdata( "testdata.db" );
		testdata.open();
		CoordinateTool ctool = CoordinateTool( testdata, "JumpConnections", "S_ID_A", "S_ID_B",
				"Distance", "JumpSystemsCPtest", "S_ID", "X_Coordinate", "Y_Coordinate",
				"Z_Coordinate" );

		for( unsigned int x = 1; x <= 55; x++ ) {
			std::stringstream ss { };
			ss
					<< "INSERT INTO JumpSystemsCPtest(S_ID,EliteID,Name) SELECT S_ID, EliteID, Name FROM JumpSystems WHERE S_ID = "
					<< x;
			testdata.sqlQueryPush( ss.str() );
			ctool.generateCoordinatesFrom( testdata, std::to_string( x ) );
			std::cout << "----------Point " << x << "-----------" << std::endl;
			TABLE JumpSystems { };
			testdata.sqlQueryPull( "SELECT * FROM JumpSystems", JumpSystems );
			for( unsigned int i = 0; i < x; i++ ) {
				for( unsigned int j = i; j < x; j++ ) {
					if( !( i == j ) ) {
						int a { }, b { };
						utils::stringToInt( JumpSystems[ 0 ][ i ], a );
						utils::stringToInt( JumpSystems[ 0 ][ j ], b );

						Coordinate3D A = Coordinate_tool_Tests::getCoords( testdata, a,
								"JumpSystemsCPtest" );
						Coordinate3D B = Coordinate_tool_Tests::getCoords( testdata, b,
								"JumpSystemsCPtest" );
						double c_dist = CoreTools::math::spatial::calcDistance( A, B );
						double p_dist = editTable::JumpConnections::getDistance( testdata, a, b );
						if( A.error_flag || B.error_flag ) {
							std::cout << "[" << a << "-" << b << "]" << std::endl;
							ASSERTM( "Testing generateCoordinatesFrom", false );
						} else {
							std::cout << "[" << a << "-" << b << "] Db: " << p_dist << ", Calc: "
									<< c_dist;
							if( equalish( c_dist, p_dist, 0.05 ) ) {
								std::cout << "   OK";
								ASSERTM( "Testing generateCoordinatesFrom", true );
							} else {
								std::cout << "   Not maching";
								ASSERTM( "Testing generateCoordinatesFrom", false );
							}
							std::cout << std::endl;
						}
					}
				}
			}

		}
		testdata.close();
	}

	//-------TESTS---------
	void generateCoordinatesFrom__test1() {
		Coordinate_tool_Tests::setup();
		Db testdata( "testdata.db" );
		testdata.open();
		CoordinateTool ctool = CoordinateTool( testdata, "JumpConnections", "S_ID_A", "S_ID_B",
				"Distance", "JumpSystemsCPtest", "S_ID", "X_Coordinate", "Y_Coordinate",
				"Z_Coordinate" );
		if( ctool.generateCoordinatesFrom( testdata, "1" ) ) {
			testdata.sqlQueryPush(
					"INSERT INTO JumpSystemsCPtest(S_ID,EliteID,Name) SELECT S_ID, EliteID, Name FROM JumpSystems WHERE S_ID = 2" );
			if( ctool.generateCoordinatesFrom( testdata, "2" ) ) {
				testdata.sqlQueryPush(
						"INSERT INTO JumpSystemsCPtest(S_ID,EliteID,Name) SELECT S_ID, EliteID, Name FROM JumpSystems WHERE S_ID = 3" );
				if( ctool.generateCoordinatesFrom( testdata, "3" ) ) {
					testdata.sqlQueryPush(
							"INSERT INTO JumpSystemsCPtest(S_ID,EliteID,Name) SELECT S_ID, EliteID, Name FROM JumpSystems WHERE S_ID = 4" );
					ctool.generateCoordinatesFrom( testdata, "4" );
				}
			}
		}

		Table t { };
		TABLE JumpSystems { };
		testdata.sqlQueryPull( "SELECT * FROM JumpSystems", JumpSystems );

		for( unsigned int i = 0; i < 4; i++ ) {
			for( unsigned int j = i; j < 4; j++ ) {
				if( !( i == j ) ) {
					int a { }, b { };
					utils::stringToInt( JumpSystems[ 0 ][ i ], a );
					utils::stringToInt( JumpSystems[ 0 ][ j ], b );

					Coordinate3D A = Coordinate_tool_Tests::getCoords( testdata, a,
							"JumpSystemsCPtest" );
					Coordinate3D B = Coordinate_tool_Tests::getCoords( testdata, b,
							"JumpSystemsCPtest" );
					double c_dist = CoreTools::math::spatial::calcDistance( A, B );
					double p_dist = editTable::JumpConnections::getDistance( testdata, a, b );
					if( A.error_flag || B.error_flag ) {
						std::cout << "[" << a << "-" << b << "]" << std::endl;
						ASSERTM( "Testing generateCoordinatesFrom", false );
					} else {
						std::cout << "[" << a << "-" << b << "] Db: " << p_dist << ", Calc: "
								<< c_dist;
						if( equalish( c_dist, p_dist, 0.05 ) ) {
							std::cout << "   OK";
							ASSERTM( "Testing generateCoordinatesFrom", true );
						} else {
							std::cout << "   Not maching";
							ASSERTM( "Testing generateCoordinatesFrom", false );
						}
						std::cout << std::endl;
					}
				}
			}
		}
		testdata.close();
	}

	void generateCoordinatesFrom__test2() { //Hardcore test - full on recursive propagation
		Coordinate_tool_Tests::setup();
		Db testdata( "testdata.db" );
		ASSERTM( "Testing generateCoordinatesFrom DB open", testdata.open() );
		CoordinateTool ctool = CoordinateTool( testdata, "JumpConnections", "S_ID_A", "S_ID_B",
				"Distance", "JumpSystemsCPtest2", "S_ID", "X_Coordinate", "Y_Coordinate",
				"Z_Coordinate" );
		ctool.generateCoordinatesFrom( testdata, "5" );
		TABLE JumpSystems { };
		testdata.sqlQueryPull( "SELECT * FROM JumpSystems", JumpSystems );
		for( unsigned int i = 0; i < 40; i++ ) {
			for( unsigned int j = 0; j < 40; j++ ) {
				if( !( i == j ) ) {
					int a { }, b { };
					utils::stringToInt( JumpSystems[ 0 ][ i ], a );
					utils::stringToInt( JumpSystems[ 0 ][ j ], b );

					Coordinate3D A = Coordinate_tool_Tests::getCoords( testdata, a,
							"JumpSystemsCPtest2" );
					Coordinate3D B = Coordinate_tool_Tests::getCoords( testdata, b,
							"JumpSystemsCPtest2" );
					double c_dist = CoreTools::math::spatial::calcDistance( A, B );
					double p_dist = editTable::JumpConnections::getDistance( testdata, a, b );
					if( A.error_flag || B.error_flag ) {
						std::cout << "[" << a << "-" << b << "]" << std::endl;
						ASSERTM( "Testing generateCoordinatesFrom", false );
					} else {
						std::cout << "[" << a << "-" << b << "] Db: " << p_dist << ", Calc: "
								<< c_dist;
						if( equalish( c_dist, p_dist, 0.05 ) ) {
							std::cout << "   OK";
							ASSERTM( "Testing generateCoordinatesFrom", true );
						} else {
							std::cout << "   Not maching";
							ASSERTM( "Testing generateCoordinatesFrom", false );
						}
						std::cout << std::endl;
					}
				}
			}
		}
		testdata.close();
	}

	cute::suite make_suite() {
		Coordinate_tool_Tests::setup();
		cute::suite s { };
		s.push_back( CUTE( generateCoordinatesFrom__test0 ) );
		s.push_back( CUTE( generateCoordinatesFrom__test1 ) );
		s.push_back( CUTE( generateCoordinatesFrom__test2 ) );
		return s;
	}
}

#endif /* SATELLITES_COORDINATE_COORDINATE_TOOL_TEST_HPP_ */
