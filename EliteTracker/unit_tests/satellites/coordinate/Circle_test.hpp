/*
 * Circle_test.hpp
 *
 *  Created on: 22 Mar 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_COORDINATE_CIRCLE_TEST_HPP_
#define SATELLITES_COORDINATE_CIRCLE_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>
#include "../../equalish.hpp"

#include "../../../src/satellites/coordinate/Circle.hpp"

namespace Circle_Tests {
	void Circle__test() {
		Circle circle( 1.1, 2.2, 15.12345 );
		ASSERTM( "Testing Circle 1", circle.center.x == 1.1 );
		ASSERTM( "Testing Circle 2", circle.center.y == 2.2 );
		ASSERTM( "Testing Circle 3", circle.radius == 15.12345 );
	}
	void intersect__test() {
		Circle c1( 0, 0, 10 );
		Circle c2( 0, 5, 10 );
		Coordinate2D<double> intersect1 { };
		Coordinate2D<double> intersect2 { };
		int n_intersect = c1.intersect( c2, intersect1, intersect2 );
		ASSERTM( "Testing intersect(..) 1", n_intersect == 2 );
		ASSERTM( "Testing intersect(..) 2",
				( equalish( intersect1.x, 9.682, 3 ) && intersect1.y == 2.5 ) );
		ASSERTM( "Testing intersect(..) 3",
				( equalish( intersect2.x, -9.682, 3 ) && intersect2.y == 2.5 ) );
		Circle c3( 20, 0, 2 );
		ASSERTM( "Testing intersect(..) 4", c1.intersect( c3, intersect1, intersect2 ) == 0 );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( Circle__test ) );
		s.push_back( CUTE( intersect__test ) );
		return s;
	}
}

#endif /* SATELLITES_COORDINATE_CIRCLE_TEST_HPP_ */
