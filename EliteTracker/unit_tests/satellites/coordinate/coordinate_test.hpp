/*
 * coordinate_test.hpp
 *
 *  Created on: 30 Mar 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_COORDINATE_COORDINATE_TEST_HPP_
#define SATELLITES_COORDINATE_COORDINATE_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/satellites/coordinate/coordinates.hpp"

namespace Coordinate_Tests {
	void calc1D__test() {
		Coordinate3D valid1 = Coordinate3D( 0, 0, 0 );
		Coordinate3D valid2 = Coordinate3D( 0, 0, 0 );
		Coordinate3D invalid( 5, 5, 5 );
		bool returned = coordinates::calc1D( invalid, 20, valid2 );
		ASSERTM( "Testing calc1D 1", returned == false );
		returned = coordinates::calc1D( valid1, 20, valid2 );
		ASSERTM( "Testing calc1D 2", returned == true );
		ASSERTM( "Testing calc1D 3", valid2 == Coordinate3D( 20, 0, 0 ) );
	}
	void calc2D__test() {
		Coordinate3D A = Coordinate3D( 0, 0, 0 );
		Coordinate3D B = Coordinate3D( 10, 0, 0 );
		Coordinate3D X = Coordinate3D();
		bool returned = coordinates::calc2D( A, B, 2, 2, X );
		ASSERTM( "Testing calc2D 1", returned == false );
		returned = coordinates::calc2D( A, B, 10, 10, X );
		ASSERTM( "Testing calc2D 2", returned == true );
		ASSERTM( "Testing calc2D 3",
				( equalish( X.x, 5, 0.001 ) && equalish( X.y, 8.66, 0.001 ) && X.z == 0 ) );
	}
	void calc3D__test() {
		Coordinate3D A = Coordinate3D( -30.75, 39.7188, 12.7812 ); //10
		Coordinate3D B = Coordinate3D( -22.375, 34.8438, 4 ); //20
		Coordinate3D C = Coordinate3D( -21.9688, 29.0938, -1.71875 ); //30
		Coordinate3D X { };
		bool returned = coordinates::calc3D( 13.0773, 20.0062, 8.11983, 19.6534, 15.984, 22.4274, A,
				B, C, X );
		Coordinate3D Xreal { -11.5625, 43.8125, 11.625 }; //3
		bool a_match { ( equalish( X.x, Xreal.x, 2 ) && equalish( X.y, Xreal.y, 2 )
				&& equalish( X.z, Xreal.z, 2 ) ) };
		ASSERTM( "Testing calc3D 1", returned == true );
		ASSERTM( "Testing calc3D 2", a_match );
	}
	void calcCoordinates__test() {
		Coordinate3D A = Coordinate3D( -30.75, 39.7188, 12.7812 ); //10
		Coordinate3D B = Coordinate3D( -22.375, 34.8438, 4 ); //20
		Coordinate3D C = Coordinate3D( -21.9688, 29.0938, -1.71875 ); //30
		Coordinate3D D = Coordinate3D( -17.3125, 49.5312, -1.6875 ); //40
		Coordinate3D X { };
		bool returned = coordinates::calcCoordinates( A, B, C, D, 19.6534, 15.984, 22.4274, 15.5881,
				X );
		Coordinate3D Xreal { -11.5625, 43.8125, 11.625 }; //3
		bool a_match { ( equalish( X.x, Xreal.x, 2 ) && equalish( X.y, Xreal.y, 2 )
				&& equalish( X.z, Xreal.z, 2 ) ) };
		ASSERTM( "Testing calcCoordinates 1", returned == true );
		ASSERTM( "Testing calcCoordinates 2", a_match );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( calc1D__test ) );
		s.push_back( CUTE( calc2D__test ) );
		s.push_back( CUTE( calc3D__test ) );
		s.push_back( CUTE( calcCoordinates__test ) );
		return s;
	}
}

#endif /* SATELLITES_COORDINATE_COORDINATE_TEST_HPP_ */
