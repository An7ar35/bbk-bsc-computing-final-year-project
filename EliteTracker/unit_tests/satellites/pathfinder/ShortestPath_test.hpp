/*
 * ShortestPath_test.hpp
 *
 *  Created on: 1 Mar 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_PATHFINDER_SHORTESTPATH_TEST_HPP_
#define SATELLITES_PATHFINDER_SHORTESTPATH_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/satellites/pathfinder/ShortestPath.hpp"

namespace ShortestPath_Tests {
	void findPath__test() {
		Matrix<double> m( 7, 7 );
		for( int i = 0; i < 7; i++ ) {
			m.data[ i ][ i ] = 0;
		}
		//a
		m.data[ 0 ][ 1 ] = 3;
		m.data[ 0 ][ 2 ] = 5;
		m.data[ 0 ][ 3 ] = 6;
		m.data[ 0 ][ 4 ] = -1;
		m.data[ 0 ][ 5 ] = -1;
		m.data[ 0 ][ 6 ] = -1;
		//b
		m.data[ 1 ][ 0 ] = 3;
		m.data[ 1 ][ 2 ] = -1;
		m.data[ 1 ][ 3 ] = 2;
		m.data[ 1 ][ 4 ] = -1;
		m.data[ 1 ][ 5 ] = -1;
		m.data[ 1 ][ 6 ] = -1;
		//c
		m.data[ 2 ][ 0 ] = 5;
		m.data[ 2 ][ 1 ] = -1;
		m.data[ 2 ][ 3 ] = 2;
		m.data[ 2 ][ 4 ] = 6;
		m.data[ 2 ][ 5 ] = 3;
		m.data[ 2 ][ 6 ] = 7;
		//d
		m.data[ 3 ][ 0 ] = 6;
		m.data[ 3 ][ 1 ] = 2;
		m.data[ 3 ][ 2 ] = 2;
		m.data[ 3 ][ 4 ] = -1;
		m.data[ 3 ][ 5 ] = 9;
		m.data[ 3 ][ 6 ] = 0;
		//e
		m.data[ 4 ][ 0 ] = -1;
		m.data[ 4 ][ 1 ] = -1;
		m.data[ 4 ][ 2 ] = 6;
		m.data[ 4 ][ 3 ] = -1;
		m.data[ 4 ][ 5 ] = 5;
		m.data[ 4 ][ 6 ] = 2;
		//f
		m.data[ 5 ][ 0 ] = -1;
		m.data[ 5 ][ 1 ] = -1;
		m.data[ 5 ][ 2 ] = 3;
		m.data[ 5 ][ 3 ] = 9;
		m.data[ 5 ][ 4 ] = 5;
		m.data[ 5 ][ 6 ] = 1;
		//g
		m.data[ 6 ][ 0 ] = -1;
		m.data[ 6 ][ 1 ] = -1;
		m.data[ 6 ][ 2 ] = 7;
		m.data[ 6 ][ 3 ] = 0;
		m.data[ 6 ][ 4 ] = 2;
		m.data[ 6 ][ 5 ] = 1;
		ShortestPath sp = ShortestPath( 0, 6, m );
		std::vector<int> path { };
		double dist { };
		sp.findPath( path, dist );
		ASSERTM( "Testing ShortestPath 1", path.size() == 4 );
		ASSERTM( "Testing ShortestPath 2", path[ 0 ] == 0 );
		ASSERTM( "Testing ShortestPath 3", path[ 1 ] == 2 );
		ASSERTM( "Testing ShortestPath 4", path[ 2 ] == 5 );
		ASSERTM( "Testing ShortestPath 5", path[ 3 ] == 6 );
		ASSERTM( "Testing ShortestPath dist", dist == 9 );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( findPath__test ) );
		return s;
	}
}

#endif /* SATELLITES_PATHFINDER_SHORTESTPATH_TEST_HPP_ */
