/*
 * Pathfinder_test.hpp
 *
 *  Created on: 22 Mar 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_PATHFINDER_PATHFINDER_TEST_HPP_
#define SATELLITES_PATHFINDER_PATHFINDER_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/satellites/pathfinder/Pathfinder.hpp"

namespace Pathfinder_Tests {
	void Pathfinder__SP__test1() { //current ship
		fileIO::Settings config = fileIO::Settings( "test_settings.cfg" );
		Pathfinder pf = Pathfinder( "testdata.db", config );
		bool returned = pf.findShortestPath( 1, 10 );
		Table t = pf.getPathInfo();
		ASSERTM( "Testing Pathfinder SP 1.1", returned == true );
		ASSERTM( "Testing Pathfinder SP 1.2", t.getRowCount() == 7 );
		ASSERTM( "Testing Pathfinder SP 1.3", t( 0, 0 ).getInt() == 1 );
		ASSERTM( "Testing Pathfinder SP 1.4", t( 0, 1 ).getInt() == 46 );
		ASSERTM( "Testing Pathfinder SP 1.5", t( 0, 2 ).getInt() == 23 );
		ASSERTM( "Testing Pathfinder SP 1.6", t( 0, 3 ).getInt() == 30 );
		ASSERTM( "Testing Pathfinder SP 1.7", t( 0, 4 ).getInt() == 20 );
		ASSERTM( "Testing Pathfinder SP 1.8", t( 0, 5 ).getInt() == 51 );
		ASSERTM( "Testing Pathfinder SP 1.9", t( 0, 6 ).getInt() == 10 );
	}
	void Pathfinder__SP__test2() { //other ship
		Pathfinder pf = Pathfinder( "testdata.db", 1 );
		bool returned = pf.findShortestPath( 7, 9 );
		ASSERTM( "Testing Pathfinder SP 2.1", returned == true );
		Table t = pf.getPathInfo();
		t.printToConsole();
		ASSERTM( "Testing Pathfinder SP 2.2", returned == true );
		ASSERTM( "Testing Pathfinder SP 2.3", t.getRowCount() == 2 );
		ASSERTM( "Testing Pathfinder SP 2.4", t( 0, 0 ).getInt() == 7 );
		ASSERTM( "Testing Pathfinder SP 2.5", t( 0, 1 ).getInt() == 9 );
		returned = pf.findShortestPath( 1, 10 ); //jumps too large for ship's range
		ASSERTM( "Testing Pathfinder SP 2.6", returned == false );
		pf = Pathfinder( "testdata.db", 4 ); //destroyed ship
		returned = pf.findShortestPath( 1, 10 );
		ASSERTM( "Testing Pathfinder SP 2.7", returned == false );
		pf = Pathfinder( "testdata.db", 10 ); //unknown
		returned = pf.findShortestPath( 7, 9 );
		ASSERTM( "Testing Pathfinder SP 2.8", returned == false );
	}
	void Pathfinder__SSP__test1() { //current ship
		fileIO::Settings config = fileIO::Settings( "test_settings.cfg" );
		//bool findShortestPath( int originID, std::vector<int> &places_to_go ); //Travelling salesman
		std::vector<int> tovisit { 5, 7, 10, 12, 25, 28, 50, 9, 18, 20 };
		Pathfinder pf = Pathfinder( "testdata.db", config );
		bool returned = pf.findShortestPath( 1, tovisit );
		Table t = pf.getPathInfo();
		t.printToConsole();
		ASSERTM( "Testing Pathfinder SSP 1.1", returned == true );
		ASSERTM( "Testing Pathfinder SSP 1.2", t.getRowCount() == 28 );
		ASSERTM( "Testing Pathfinder SSP 1.3", t( 0, 0 ).getInt() == 1 );
		ASSERTM( "Testing Pathfinder SSP 1.4", t( 0, 1 ).getInt() == 18 );
		ASSERTM( "Testing Pathfinder SSP 1.5", t( 0, 2 ).getInt() == 43 );
		ASSERTM( "Testing Pathfinder SSP 1.6", t( 0, 3 ).getInt() == 50 );
		ASSERTM( "Testing Pathfinder SSP 1.7", t( 0, 4 ).getInt() == 43 );
		ASSERTM( "Testing Pathfinder SSP 1.8", t( 0, 5 ).getInt() == 23 );
		ASSERTM( "Testing Pathfinder SSP 1.9", t( 0, 6 ).getInt() == 30 );
		ASSERTM( "Testing Pathfinder SSP 1.10", t( 0, 7 ).getInt() == 5 );
		ASSERTM( "Testing Pathfinder SSP 1.11", t( 0, 8 ).getInt() == 20 );
		ASSERTM( "Testing Pathfinder SSP 1.12", t( 0, 9 ).getInt() == 7 );
		ASSERTM( "Testing Pathfinder SSP 1.13", t( 0, 10 ).getInt() == 9 );
		ASSERTM( "Testing Pathfinder SSP 1.14", t( 0, 11 ).getInt() == 41 );
		ASSERTM( "Testing Pathfinder SSP 1.15", t( 0, 12 ).getInt() == 12 );
		ASSERTM( "Testing Pathfinder SSP 1.16", t( 0, 13 ).getInt() == 28 );
		ASSERTM( "Testing Pathfinder SSP 1.17", t( 0, 14 ).getInt() == 4 );
		ASSERTM( "Testing Pathfinder SSP 1.18", t( 0, 15 ).getInt() == 51 );
		ASSERTM( "Testing Pathfinder SSP 1.19", t( 0, 16 ).getInt() == 10 );
		ASSERTM( "Testing Pathfinder SSP 1.20", t( 0, 17 ).getInt() == 51 );
		ASSERTM( "Testing Pathfinder SSP 1.21", t( 0, 18 ).getInt() == 20 );
		ASSERTM( "Testing Pathfinder SSP 1.22", t( 0, 19 ).getInt() == 30 );
		ASSERTM( "Testing Pathfinder SSP 1.23", t( 0, 20 ).getInt() == 42 );
		ASSERTM( "Testing Pathfinder SSP 1.24", t( 0, 21 ).getInt() == 14 );
		ASSERTM( "Testing Pathfinder SSP 1.25", t( 0, 22 ).getInt() == 25 );
		ASSERTM( "Testing Pathfinder SSP 1.26", t( 0, 23 ).getInt() == 14 );
		ASSERTM( "Testing Pathfinder SSP 1.27", t( 0, 24 ).getInt() == 16 );
		ASSERTM( "Testing Pathfinder SSP 1.28", t( 0, 25 ).getInt() == 23 );
		ASSERTM( "Testing Pathfinder SSP 1.29", t( 0, 26 ).getInt() == 46 );
		ASSERTM( "Testing Pathfinder SSP 1.30", t( 0, 27 ).getInt() == 1 );
	}
	void Pathfinder__SSP__test2() { //other ship
		std::vector<int> tovisit { 15, 5, 10, 28, 22, 19 };
		Pathfinder pf = Pathfinder( "testdata.db", 3 ); //ASP explorer ship
		bool returned = pf.findShortestPath( 1, tovisit );
		ASSERTM( "Testing Pathfinder SSP 2.1", returned == true );
		Table t = pf.getPathInfo();
		t.printToConsole();
		ASSERTM( "Testing Pathfinder SSP 2.2", t.getRowCount() == 12 );
		ASSERTM( "Testing Pathfinder SSP 2.3", t( 0, 0 ).getInt() == 1 );
		ASSERTM( "Testing Pathfinder SSP 2.4", t( 0, 1 ).getInt() == 11 );
		ASSERTM( "Testing Pathfinder SSP 2.5", t( 0, 2 ).getInt() == 19 );
		ASSERTM( "Testing Pathfinder SSP 2.6", t( 0, 3 ).getInt() == 22 );
		ASSERTM( "Testing Pathfinder SSP 2.7", t( 0, 4 ).getInt() == 5 );
		ASSERTM( "Testing Pathfinder SSP 2.8", t( 0, 5 ).getInt() == 15 );
		ASSERTM( "Testing Pathfinder SSP 2.9", t( 0, 6 ).getInt() == 28 );
		ASSERTM( "Testing Pathfinder SSP 2.10", t( 0, 7 ).getInt() == 51 );
		ASSERTM( "Testing Pathfinder SSP 2.11", t( 0, 8 ).getInt() == 10 );
		ASSERTM( "Testing Pathfinder SSP 2.12", t( 0, 9 ).getInt() == 34 );
		ASSERTM( "Testing Pathfinder SSP 2.13", t( 0, 10 ).getInt() == 11 );
		ASSERTM( "Testing Pathfinder SSP 2.14", t( 0, 11 ).getInt() == 1 );
		pf = Pathfinder( "testdata.db", 3 );
		std::vector<int> tovisit2 { 5, 7, 10, 12, 25, 28, 50, 9, 18, 20, 2, 6 };
		returned = pf.findShortestPath( 1, tovisit2 );
		ASSERTM( "Testing Pathfinder SSP 2.15", returned == false );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( Pathfinder__SP__test1 ) );
		s.push_back( CUTE( Pathfinder__SP__test2 ) );
		s.push_back( CUTE( Pathfinder__SSP__test1 ) );
		s.push_back( CUTE( Pathfinder__SSP__test2 ) );
		return s;
	}
}

#endif /* SATELLITES_PATHFINDER_PATHFINDER_TEST_HPP_ */
