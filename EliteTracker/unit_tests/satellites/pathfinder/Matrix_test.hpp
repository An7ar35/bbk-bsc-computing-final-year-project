/*
 * Matrix_test.hpp
 *
 *  Created on: 22 Feb 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_PATHFINDER_MATRIX_TEST_HPP_
#define SATELLITES_PATHFINDER_MATRIX_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/satellites/pathfinder/Matrix.hpp"

namespace Matrix_Tests {
	void constructor__test() {
		Matrix<int> matrix = Matrix<int>( 1000, 1000 );
		int count { 0 };
		for( unsigned int i = 0; i < 1000; i++ ) {
			for( unsigned int j = 0; j < 1000; j++ ) {
				matrix.data[ i ][ j ] = count;
				count++;
			}
		}
		count = 0;
		for( unsigned int i = 0; i < 1000; i++ ) {
			for( unsigned int j = 0; j < 1000; j++ ) {
				ASSERTM( "Testing constructor(..)", matrix.data[ i ][ j ] == count );
				count++;
			}
		}
	}
	void getRows__test() {
		Matrix<int> matrix = Matrix<int>( 1000, 500 );
		ASSERTM( "Testing getRows()", matrix.getRows() == 500 );
	}
	void getCols__test() {
		Matrix<int> matrix = Matrix<int>( 1000, 500 );
		ASSERTM( "Testing getRows()", matrix.getCols() == 1000 );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( constructor__test ) );
		s.push_back( CUTE( getRows__test ) );
		s.push_back( CUTE( getCols__test ) );

		return s;
	}
}

#endif /* SATELLITES_PATHFINDER_MATRIX_TEST_HPP_ */
