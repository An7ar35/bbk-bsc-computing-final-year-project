/*
 * Node_test.hpp
 *
 *  Created on: 22 Feb 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_PATHFINDER_NODE_TEST_HPP_
#define SATELLITES_PATHFINDER_NODE_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/satellites/pathfinder/Node.hpp"

namespace Node_Tests {
	void constructor__test() {
		Node node = Node( 666, 255, 999.999 );
		ASSERTM( "Testing Node() 1", node.id == 666 );
		ASSERTM( "Testing Node() 2", node.from_id == 255 );
		ASSERTM( "Testing Node() 3", node.accumulated_distance == 999.999 );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( constructor__test ) );
		return s;
	}
}

#endif /* SATELLITES_PATHFINDER_NODE_TEST_HPP_ */
