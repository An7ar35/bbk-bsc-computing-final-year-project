/*
 * stress_tests.hpp
 *
 *  Created on: 25 Jan 2015
 *      Author: Alwlyan
 */

#ifndef STRESS_TESTS_HPP_
#define STRESS_TESTS_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../src/core/database/db.hpp"
#include "../src/core/containers/Table.hpp"

namespace Stress_Tests {
	void sql2Table__test() {
		Table table = Table();
		//Db query pull to Table
		Db test( "testdata.db" );
		test.open();
		std::chrono::high_resolution_clock::time_point t1 =
				std::chrono::high_resolution_clock::now();
		test.sqlQueryPull_typed( "SELECT * FROM StressTest", table );
		std::chrono::high_resolution_clock::time_point t2 =
				std::chrono::high_resolution_clock::now();
		test.close();
		auto duration = std::chrono::duration_cast<std::chrono::milliseconds>( t2 - t1 ).count();
		std::cout << "Db to Table transfer of 4x5 million rows of Items took: " << duration
				<< " ms" << std::endl;
		//Table sort int
		std::chrono::high_resolution_clock::time_point t3 =
				std::chrono::high_resolution_clock::now();
		table.sort( 1, Table::SortType::ASCENDING );
		std::chrono::high_resolution_clock::time_point t4 =
				std::chrono::high_resolution_clock::now();
		auto duration_sort1 =
				std::chrono::duration_cast<std::chrono::milliseconds>( t4 - t3 ).count();
		std::cout << "Sorting Integers ASC took: " << duration_sort1 << " ms"
				<< std::endl;
		//Table sort double
		std::chrono::high_resolution_clock::time_point t5 =
				std::chrono::high_resolution_clock::now();
		table.sort( 2, Table::SortType::ASCENDING );
		std::chrono::high_resolution_clock::time_point t6 =
				std::chrono::high_resolution_clock::now();
		auto duration_sort2 =
				std::chrono::duration_cast<std::chrono::milliseconds>( t6 - t5 ).count();
		std::cout << "Sorting Doubles ASC took: " << duration_sort2 << " ms"
				<< std::endl;
		//table sort string
		std::chrono::high_resolution_clock::time_point t7 =
				std::chrono::high_resolution_clock::now();
		table.sort( 3, Table::SortType::ASCENDING );
		std::chrono::high_resolution_clock::time_point t8 =
				std::chrono::high_resolution_clock::now();
		auto duration_sort3 =
				std::chrono::duration_cast<std::chrono::milliseconds>( t8 - t7 ).count();
		std::cout << "Sorting Strings ASC took: " << duration_sort3 << " ms"
				<< std::endl;

	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( sql2Table__test ) );
		return s;
	}
}

#endif /* STRESS_TESTS_HPP_ */
