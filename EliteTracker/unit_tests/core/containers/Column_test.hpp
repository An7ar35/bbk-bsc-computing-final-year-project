/*
 * Column_test.hpp
 *
 *  Created on: 21 Jan 2015
 *      Author: Alwlyan
 */

#ifndef COLUMN_TEST_HPP_
#define COLUMN_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"

#include "../src/core/containers/Column.hpp"

namespace Column_Tests {
	void getType__test() {
		Column col_null = Column( dataType::NONE, "Null column" );
		ASSERTM( "Testing getType() 1", col_null.getType() == dataType::NONE );
		Column col_int = Column( dataType::INTEGER, "Int column" );
		ASSERTM( "Testing getType() 2", col_int.getType() == dataType::INTEGER );
		Column col_double = Column( dataType::DOUBLE, "Double column" );
		ASSERTM( "Testing getType() 3", col_double.getType() == dataType::DOUBLE );
		Column col_string = Column( dataType::STRING, "String column" );
		ASSERTM( "Testing getType() 4", col_string.getType() == dataType::STRING );
	}
	void checkType__test() {
		Column col_null = Column( dataType::NONE, "Null column" );
		ASSERTM( "Testing checkType(..) 1", col_null.checkType( dataType::NONE ) );
		Column col_int = Column( dataType::INTEGER, "Int column" );
		ASSERTM( "Testing checkType(..) 2", col_int.checkType( dataType::INTEGER ) );
		Column col_double = Column( dataType::DOUBLE, "Double column" );
		ASSERTM( "Testing checkType(..) 3", col_double.checkType( dataType::DOUBLE ) );
		Column col_string = Column( dataType::STRING, "String column" );
		ASSERTM( "Testing checkType(..) 4", col_string.checkType( dataType::STRING ) );
	}
	void getHeading__test() {
		Column col_null = Column( dataType::NONE, "Null column" );
		ASSERTM( "Testing getHeading() 1", col_null.getHeading() == "Null column" );
		Column col_int = Column( dataType::INTEGER, "Int column" );
		ASSERTM( "Testing getHeading() 2", col_int.getHeading() == "Int column" );
		Column col_double = Column( dataType::DOUBLE, "Double column" );
		ASSERTM( "Testing getHeading() 3", col_double.getHeading() == "Double column" );
		Column col_string = Column( dataType::STRING, "String column" );
		ASSERTM( "Testing getHeading() 4", col_string.getHeading() == "String column" );
	}

	cute::suite make_suite() {
		cute::suite s;
		s.push_back( CUTE( getType__test ) );
		s.push_back( CUTE( checkType__test ) );
		s.push_back( CUTE( getHeading__test ) );
		return s;
	}
}

#endif /* COLUMN_TEST_HPP_ */

