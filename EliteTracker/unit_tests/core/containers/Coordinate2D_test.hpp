/*
 * Coordinate2D.hpp
 *
 *  Created on: 21 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_CONTAINERS_COORDINATE2D_TEST_HPP_
#define CORE_CONTAINERS_COORDINATE2D_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/core/containers/Coordinate2D.hpp"

namespace Coordinate2D_Tests {
	void constructor__test1() {
		Coordinate2D<int> coord = Coordinate2D<int>();
		ASSERTM( "Testing Coordinate2D()", coord.x == 0 && coord.y == 0 );
	}
	void constructor__test2() {
		Coordinate2D<int> coord = Coordinate2D<int>( 10, 50 );
		ASSERTM( "Testing Coordinate2D(..)", coord.x == 10 && coord.y == 50 );
	}
	void set__test() {
		Coordinate2D<int> coord = Coordinate2D<int>();
		coord.set( 100, 999 );
		ASSERTM( "Testing set(..)", coord.x == 100 && coord.y == 999 );
	}
	void reset__test() {
		Coordinate2D<int> coord = Coordinate2D<int>( 999, 999 );
		ASSERTM( "Testing reset() 1", coord.x == 999 && coord.y == 999 );
		coord.reset();
		ASSERTM( "Testing reset() 2", coord.x == 0 && coord.y == 0 );
	}

	void norm__test() {
		//TODO
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( constructor__test1 ) );
		s.push_back( CUTE( constructor__test2 ) );
		s.push_back( CUTE( set__test ) );
		s.push_back( CUTE( reset__test ) );
		return s;
	}
}

#endif /* CORE_CONTAINERS_COORDINATE2D_TEST_HPP_ */

