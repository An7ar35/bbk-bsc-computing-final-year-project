/*
 * Row_test.hpp
 *
 *  Created on: 21 Jan 2015
 *      Author: Alwlyan
 */

#ifndef ROW_TEST_HPP_
#define ROW_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"

#include "../src/core/containers/Row.hpp"

namespace Row_Tests {
	//Row( int size );
	void bracket_operator__test() { // '()' operator
		Row row = Row();
		row.add( 11 );
		row.add( 22 );
		row.add( 33 );
		int container { };
		Item item = row( 0 );
		item.getValue( container );
		ASSERTM( "Testing Row() '()' operator 1", container == 11 );
		item = row( 1 );
		item.getValue( container );
		ASSERTM( "Testing Row() '()' operator 2", container == 22 );
		item = row( 2 );
		item.getValue( container );
		ASSERTM( "Testing Row() '()' operator 3", container == 33 );
		std::string error_returned { };
		item = row( 3 );
		item.getValue( error_returned );
		ASSERTM( "Testing Row() '()' operator 4", error_returned == "<!exist>" );
	}
	void add__test0() { //NULL
		Row row = Row();
		row.add();
		std::string container { };
		Item item = row( 0 );
		item.getValue( container );
		ASSERTM( "Testing add() 1", item.getType() == dataType::NONE );
		ASSERTM( "Testing add() 2", container == "" );
	}
	void add__test1() { //int
		Row row = Row();
		row.add( 100 );
		int container { };
		Item item = row( 0 );
		item.getValue( container );
		ASSERTM( "Testing add(int) 1", item.getType() == dataType::INTEGER );
		ASSERTM( "Testing add(int) 2", container == 100 );
	}
	void add__test2() { //double
		Row row = Row();
		row.add( 0.00001 );
		double container { };
		Item item = row( 0 );
		item.getValue( container );
		ASSERTM( "Testing add(double) 1", item.getType() == dataType::DOUBLE );
		ASSERTM( "Testing add(double) 2", container == 0.00001 );
	}
	void add__test3() { //string
		Row row = Row();
		row.add( "coolios" );
		std::string container { };
		Item item = row( 0 );
		item.getValue( container );
		ASSERTM( "Testing add(std::string) 1", item.getType() == dataType::STRING );
		ASSERTM( "Testing add(std::string) 2", container == "coolios" );
	}
	void getItemType__test() {
		Row row = Row();
		row.add( 1 );
		row.add( 1.1 );
		row.add( "one" );
		ASSERTM( "Testing getItemType(..) 1", row.getItemType( 0 ) == dataType::INTEGER );
		ASSERTM( "Testing getItemType(..) 2", row.getItemType( 1 ) == dataType::DOUBLE );
		ASSERTM( "Testing getItemType(..) 3", row.getItemType( 2 ) == dataType::STRING );
		ASSERTM( "Testing getItemType(..) 4", row.getItemType( 3 ) == dataType::NONE ); //Error Item
	}
	void getLength__test() {
		Row row = Row();
		ASSERTM( "Testing getLength() 1", row.getLength() == 0 );
		for( int i = 0; i < 1000; i++ ) {
			row.add( i );
		}
		ASSERTM( "Testing getLength() 2", row.getLength() == 1000 );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( bracket_operator__test ) );
		s.push_back( CUTE( add__test0 ) );
		s.push_back( CUTE( add__test1 ) );
		s.push_back( CUTE( add__test2 ) );
		s.push_back( CUTE( add__test3 ) );
		s.push_back( CUTE( getItemType__test ) );
		s.push_back( CUTE( getLength__test ) );
		return s;
	}
}

#endif /* ROW_TEST_HPP_ */
