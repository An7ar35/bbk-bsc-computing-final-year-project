/*
 * Item_test.hpp
 *
 *  Created on: 21 Jan 2015
 *      Author: Alwlyan
 */

#ifndef ITEM_TEST_HPP_
#define ITEM_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"

#include <sstream>

#include "../src/core/containers/Item.hpp"

namespace Item_Tests {
	void less_operator__test() { // '<' operator
		Item null_a = Item();
		Item null_b = Item();
		Item int_a = Item( 1 );
		Item int_b = Item( 2 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( 1.00001 );
		Item string_a = Item( "aaa" );
		Item string_b = Item( "aab" );
		Item string_c = Item( "caa" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		ASSERTM( "Testing Item '<' operator 1", ( null_a < null_b ) == false );
		ASSERTM( "Testing Item '<' operator 2", ( null_b < null_a ) == false );
		//INT
		ASSERTM( "Testing Item '<' operator 3", ( int_a < int_b ) == true );
		ASSERTM( "Testing Item '<' operator 4", ( int_b < int_a ) == false );
		ASSERTM( "Testing Item '<' operator 5", ( int_a < int_a ) == false );
		//DOUBLE
		ASSERTM( "Testing Item '<' operator 6", ( double_a < double_b ) == true );
		ASSERTM( "Testing Item '<' operator 7", ( double_b < double_a ) == false );
		ASSERTM( "Testing Item '<' operator 8", ( double_a < double_a ) == false );
		//STRING
		ASSERTM( "Testing Item '<' operator 9", ( string_a < string_b ) == true );
		ASSERTM( "Testing Item '<' operator 10", ( string_b < string_a ) == false );
		ASSERTM( "Testing Item '<' operator 11", ( string_a < string_a ) == false );
		//BOOL
		ASSERTM( "Testing Item '<' operator 12", ( bool_a < bool_b ) == false );
		ASSERTM( "Testing Item '<' operator 13", ( bool_b < bool_a ) == true );
		ASSERTM( "Testing Item '<' operator 14", ( bool_a < bool_a ) == false );
	}
	void more_operator__test() { // '>' operator
		Item null_a = Item();
		Item null_b = Item();
		Item int_a = Item( 1 );
		Item int_b = Item( 2 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( 1.00001 );
		Item string_a = Item( "aaa" );
		Item string_b = Item( "aab" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		ASSERTM( "Testing Item '>' operator 1", ( null_a > null_b ) == false );
		ASSERTM( "Testing Item '>' operator 2", ( null_b > null_a ) == false );
		//INT
		ASSERTM( "Testing Item '>' operator 3", ( int_a > int_b ) == false );
		ASSERTM( "Testing Item '>' operator 4", ( int_b > int_a ) == true );
		ASSERTM( "Testing Item '>' operator 5", ( int_a > int_a ) == false );
		//DOUBLE
		ASSERTM( "Testing Item '>' operator 6", ( double_a > double_b ) == false );
		ASSERTM( "Testing Item '>' operator 7", ( double_b > double_a ) == true );
		ASSERTM( "Testing Item '>' operator 8", ( double_a > double_a ) == false );
		//STRING
		ASSERTM( "Testing Item '>' operator 9", ( string_a > string_b ) == false );
		ASSERTM( "Testing Item '>' operator 10", ( string_b > string_a ) == true );
		ASSERTM( "Testing Item '>' operator 11", ( string_a > string_a ) == false );
		//BOOL
		ASSERTM( "Testing Item '>' operator 12", ( bool_a > bool_b ) == true );
		ASSERTM( "Testing Item '>' operator 13", ( bool_b > bool_a ) == false );
		ASSERTM( "Testing Item '>' operator 14", ( bool_b > bool_b ) == false );
	}
	void less_equal_operator__test() { // '<=' operator
		Item null_a = Item();
		Item null_b = Item();
		Item int_a = Item( 1 );
		Item int_b = Item( 2 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( 1.00001 );
		Item string_a = Item( "aaa" );
		Item string_b = Item( "aab" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		ASSERTM( "Testing Item '<=' operator 1", ( null_a <= null_b ) == true );
		ASSERTM( "Testing Item '<=' operator 2", ( null_b <= null_a ) == true );
		ASSERTM( "Testing Item '<=' operator 3", ( null_a <= null_a ) == true );
		//INT
		ASSERTM( "Testing Item '<=' operator 4", ( int_a <= int_b ) == true );
		ASSERTM( "Testing Item '<=' operator 5", ( int_b <= int_a ) == false );
		ASSERTM( "Testing Item '<=' operator 6", ( int_a <= int_a ) == true );
		//DOUBLE
		ASSERTM( "Testing Item '<=' operator 7", ( double_a <= double_b ) == true );
		ASSERTM( "Testing Item '<=' operator 8", ( double_b <= double_a ) == false );
		ASSERTM( "Testing Item '<=' operator 9", ( double_a <= double_a ) == true );
		//STRING
		ASSERTM( "Testing Item '<=' operator 10", ( string_a <= string_b ) == true );
		ASSERTM( "Testing Item '<=' operator 11", ( string_b <= string_a ) == false );
		ASSERTM( "Testing Item '<=' operator 12", ( string_a <= string_a ) == true );
		//BOOL
		ASSERTM( "Testing Item '<=' operator 13", ( bool_a <= bool_b ) == false );
		ASSERTM( "Testing Item '<=' operator 14", ( bool_b <= bool_a ) == true );
		ASSERTM( "Testing Item '<=' operator 15", ( bool_b <= bool_b ) == true );
	}
	void more_equal_operator__test() { // '>=' operator
		Item null_a = Item();
		Item null_b = Item();
		Item int_a = Item( 1 );
		Item int_b = Item( 2 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( 1.00001 );
		Item string_a = Item( "aaa" );
		Item string_b = Item( "aab" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		ASSERTM( "Testing Item '>=' operator 1", ( null_a >= null_b ) == true );
		ASSERTM( "Testing Item '>=' operator 2", ( null_b >= null_a ) == true );
		ASSERTM( "Testing Item '>=' operator 3", ( null_b >= null_a ) == true );
		//INT
		ASSERTM( "Testing Item '>=' operator 4", ( int_a >= int_b ) == false );
		ASSERTM( "Testing Item '>=' operator 5", ( int_b >= int_a ) == true );
		ASSERTM( "Testing Item '>=' operator 6", ( int_a >= int_a ) == true );
		//DOUBLE
		ASSERTM( "Testing Item '>=' operator 7", ( double_a >= double_b ) == false );
		ASSERTM( "Testing Item '>=' operator 8", ( double_b >= double_a ) == true );
		ASSERTM( "Testing Item '>=' operator 9", ( double_a >= double_a ) == true );
		//STRING
		ASSERTM( "Testing Item '>=' operator 10", ( string_a >= string_b ) == false );
		ASSERTM( "Testing Item '>=' operator 11", ( string_b >= string_a ) == true );
		ASSERTM( "Testing Item '>=' operator 12", ( string_a >= string_a ) == true );
		//BOOL
		ASSERTM( "Testing Item '<=' operator 13", ( bool_a >= bool_b ) == true );
		ASSERTM( "Testing Item '<=' operator 14", ( bool_b >= bool_a ) == false );
		ASSERTM( "Testing Item '<=' operator 15", ( bool_b >= bool_b ) == true );
	}
	void same_operator__test() { // '==' operator
		Item null_a = Item();
		Item null_b = Item();
		Item int_a = Item( 1 );
		Item int_b = Item( 2 );
		Item int_c = Item( 1 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( 1.00001 );
		Item double_c = Item( 1.000001 );
		Item string_a = Item( "aaa" );
		Item string_b = Item( "aab" );
		Item string_c = Item( "aaa" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		ASSERTM( "Testing Item '==' operator 1", ( null_a == null_b ) == true );
		ASSERTM( "Testing Item '==' operator 2", ( null_b == null_a ) == true );
		ASSERTM( "Testing Item '==' operator 3", ( null_a == null_a ) == true );
		//INT
		ASSERTM( "Testing Item '==' operator 4", ( int_a == int_b ) == false );
		ASSERTM( "Testing Item '==' operator 5", ( int_b == int_a ) == false );
		ASSERTM( "Testing Item '==' operator 6", ( int_a == int_a ) == true );
		ASSERTM( "Testing Item '==' operator 7", ( int_a == int_c ) == true );
		//DOUBLE
		ASSERTM( "Testing Item '==' operator 8", ( double_a == double_b ) == false );
		ASSERTM( "Testing Item '==' operator 9", ( double_b == double_a ) == false );
		ASSERTM( "Testing Item '==' operator 10", ( double_a == double_a ) == true );
		ASSERTM( "Testing Item '==' operator 11", ( double_a == double_c ) == true );
		//STRING
		ASSERTM( "Testing Item '==' operator 12", ( string_a == string_b ) == false );
		ASSERTM( "Testing Item '==' operator 13", ( string_b == string_a ) == false );
		ASSERTM( "Testing Item '==' operator 14", ( string_a == string_a ) == true );
		ASSERTM( "Testing Item '==' operator 15", ( string_a == string_c ) == true );
		//BOOL
		ASSERTM( "Testing Item '==' operator 16", ( bool_a == bool_b ) == false );
		ASSERTM( "Testing Item '==' operator 17", ( bool_a == bool_a ) == true );
	}
	void not_same_operator__test() { // '!=' operator
		Item null_a = Item();
		Item null_b = Item();
		Item int_a = Item( 1 );
		Item int_b = Item( 2 );
		Item int_c = Item( 1 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( 1.00001 );
		Item double_c = Item( 1.000001 );
		Item string_a = Item( "aaa" );
		Item string_b = Item( "aab" );
		Item string_c = Item( "aaa" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		ASSERTM( "Testing Item '!=' operator 1", ( null_a != null_b ) == false );
		ASSERTM( "Testing Item '!=' operator 2", ( null_b != null_a ) == false );
		ASSERTM( "Testing Item '!=' operator 3", ( null_a != null_a ) == false );
		//INT
		ASSERTM( "Testing Item '!=' operator 4", ( int_a != int_b ) == true );
		ASSERTM( "Testing Item '!=' operator 5", ( int_b != int_a ) == true );
		ASSERTM( "Testing Item '!=' operator 6", ( int_a != int_a ) == false );
		ASSERTM( "Testing Item '!=' operator 7", ( int_a != int_c ) == false );
		//DOUBLE
		ASSERTM( "Testing Item '!=' operator 8", ( double_a != double_b ) == true );
		ASSERTM( "Testing Item '!=' operator 9", ( double_b != double_a ) == true );
		ASSERTM( "Testing Item '!=' operator 10", ( double_a != double_a ) == false );
		ASSERTM( "Testing Item '!=' operator 11", ( double_a != double_c ) == false );
		//STRING
		ASSERTM( "Testing Item '!=' operator 12", ( string_a != string_b ) == true );
		ASSERTM( "Testing Item '!=' operator 13", ( string_b != string_a ) == true );
		ASSERTM( "Testing Item '!=' operator 14", ( string_a != string_a ) == false );
		ASSERTM( "Testing Item '!=' operator 15", ( string_a != string_c ) == false );
		//BOOL
		ASSERTM( "Testing Item '!=' operator 16", ( bool_a != bool_b ) == true );
		ASSERTM( "Testing Item '!=' operator 17", ( bool_a != bool_a ) == false );
	}
	void Item__test1() { //Default constructor
		Item item = Item();
		std::string s { };
		item.getValue( s );
		ASSERTM( "Testing Item() constructor", s == "" );
		ASSERTM( "Testing Item() constructor", item.getType() == dataType::NONE );
	}
	void Item__test2() { //INTEGER constructor
		Item item = Item( 5 );
		int i { };
		item.getValue( i );
		ASSERTM( "Testing Item(int) constructor", i == 5 );
		ASSERTM( "Testing Item(int) constructor", item.getType() == dataType::INTEGER );
	}
	void Item__test3() { //DOUBLE constructor
		Item item = Item( 666.666 );
		double d { };
		item.getValue( d );
		ASSERTM( "Testing Item(double) constructor", d == 666.666 );
		ASSERTM( "Testing Item(double) constructor", item.getType() == dataType::DOUBLE );
	}
	void Item__test4() { //STRING constructor
		Item item = Item( "Hello world!" );
		std::string s { };
		item.getValue( s );
		ASSERTM( "Testing Item(std::string) constructor", s == "Hello world!" );
		ASSERTM( "Testing Item(std::string) constructor", item.getType() == dataType::STRING );
	}
	void Item_test5() { //BOOLEAN contructor
		Item item = Item( true );
		bool b { };
		item.getValue( b );
		ASSERTM( "Testing Item(bool) constructor", b == true );
		ASSERTM( "Testing Item(bool) constructor", item.getType() == dataType::BOOLEAN );
	}
	void stream_out_operator__test() { // '<<' operator
		std::stringstream ss { };
		Item item = Item( "this is a stream test.. testing.. uhm.. yeah." );
		ss << item;
		std::string s = ss.str();
		ASSERTM( "Testing Item(..) << stream",
				s == "this is a stream test.. testing.. uhm.. yeah." );
	}
	void equal_operator__test() { // '=' operator
		//NULL
		Item null_a = Item();
		Item null_b = null_a;
		ASSERTM( "Testing Item() '=' operator 1", null_b.getType() == dataType::NONE );
		//ITEM
		Item int_a = Item( 1 );
		Item int_b = int_a;
		int integer_value = { };
		int_b.getValue( integer_value );
		ASSERTM( "Testing Item() '=' operator 2", int_b.getType() == dataType::INTEGER );
		ASSERTM( "Testing Item() '=' operator 3", integer_value == 1 );
		//DOUBLE
		Item double_a = Item( 1.000001 );
		Item double_b = double_a;
		double double_value { };
		double_b.getValue( double_value );
		ASSERTM( "Testing Item() '=' operator 4", double_b.getType() == dataType::DOUBLE );
		ASSERTM( "Testing Item() '=' operator 5", double_value == 1.000001 );
		//STRING
		Item string_a = Item( "cool" );
		Item string_b = string_a;
		std::string string_item { };
		string_b.getValue( string_item );
		ASSERTM( "Testing Item() '=' operator 6", string_b.getType() == dataType::STRING );
		ASSERTM( "Testing Item() '=' operator 7", string_item == "cool" );
		//BOOL
		Item bool_a = Item( true );
		Item bool_b = bool_a;
		bool bool_value { };
		bool_b.getValue( bool_value );
		ASSERTM( "Testing Item() '=' operator 8", bool_b.getType() == dataType::BOOLEAN );
		ASSERTM( "Testing Item() '=' operator 9", bool_value == true );
	}
	void getValue__test1() { //int
		Item null_a = Item();
		Item int_a = Item( 1 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( -0.99 );
		Item double_c = Item( 9.49 );
		Item string_a = Item( "cool" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		int null2int_value { };
		bool returned = null_a.getValue( null2int_value );
		ASSERTM( "Testing getValue(int) returned 1", returned == true );
		ASSERTM( "Testing getValue(int) 1", null2int_value == 0 );
		//INT
		int int2int_value { };
		returned = int_a.getValue( int2int_value );
		ASSERTM( "Testing getValue(int) returned 2", returned == true );
		ASSERTM( "Testing getValue(int) 2", int2int_value == 1 );
		//DOUBLE
		int double2int_value { };
		returned = double_a.getValue( double2int_value );
		ASSERTM( "Testing getValue(int) returned 3", returned == true );
		ASSERTM( "Testing getValue(int) 3", double2int_value == 1 );
		returned = double_b.getValue( double2int_value );
		ASSERTM( "Testing getValue(int) returned 4", returned == true );
		ASSERTM( "Testing getValue(int) 4", double2int_value == -1 );
		returned = double_c.getValue( double2int_value );
		ASSERTM( "Testing getValue(int) returned 5", returned == true );
		ASSERTM( "Testing getValue(int) 5", double2int_value == 9 );
		//STRING
		int string2int_value { };
		returned = string_a.getValue( string2int_value );
		ASSERTM( "Testing getValue(int) returned 6", returned == false );
		//BOOL
		bool bool2int_value { };
		returned = bool_a.getValue( bool2int_value );
		ASSERTM( "Testing getValue(int) returned 7", returned == true );
		ASSERTM( "Testing getValue(int) 7", bool2int_value == 1 );
		returned = bool_b.getValue( bool2int_value );
		ASSERTM( "Testing getValue(int) returned 8", returned == true );
		ASSERTM( "Testing getValue(int) 8", bool2int_value == 0 );
	}
	void getValue__test2() { //double
		Item null_a = Item();
		Item int_a = Item( 1 );
		Item double_a = Item( 1.000001 );
		Item string_a = Item( "cool" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		double null2double_value { };
		bool returned = null_a.getValue( null2double_value );
		ASSERTM( "Testing getValue(double) returned 1", returned == true );
		ASSERTM( "Testing getValue(double) 1", null2double_value == 0 );
		//INT
		double int2double_value { };
		returned = int_a.getValue( int2double_value );
		ASSERTM( "Testing getValue(double) returned 2", returned == true );
		ASSERTM( "Testing getValue(double) 2", int2double_value == 1 );
		//DOUBLE
		double double2double_value { };
		returned = double_a.getValue( double2double_value );
		ASSERTM( "Testing getValue(double) returned 3", returned == true );
		ASSERTM( "Testing getValue(double) 3", double2double_value == 1.000001 );
		//STRING
		double string2double_value { };
		returned = string_a.getValue( string2double_value );
		ASSERTM( "Testing getValue(double) returned 4", returned == false );
		//BOOL
		double bool2double_value { };
		returned = bool_a.getValue( bool2double_value );
		ASSERTM( "Testing getValue(double) returned 5", returned == true );
		ASSERTM( "Testing getValue(double) 5", bool2double_value == 1 );
		returned = bool_b.getValue( bool2double_value );
		ASSERTM( "Testing getValue(double) returned 6", returned == true );
		ASSERTM( "Testing getValue(double) 6", bool2double_value == 0 );
	}
	void getValue__test3() { //std::string
		Item null_a = Item();
		Item int_a = Item( 1 );
		Item double_a = Item( 1.000001 );
		Item string_a = Item( "cool" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		std::string null2string_value { };
		bool returned = null_a.getValue( null2string_value );
		ASSERTM( "Testing getValue(string) returned 1", returned == true );
		ASSERTM( "Testing getValue(string) 1", null2string_value == "" );
		//INT
		std::string int2string_value { };
		returned = int_a.getValue( int2string_value );
		ASSERTM( "Testing getValue(string) returned 2", returned == true );
		ASSERTM( "Testing getValue(string) 2", int2string_value == "1" );
		//DOUBLE
		std::string double2string_value { };
		returned = double_a.getValue( double2string_value );
		ASSERTM( "Testing getValue(string) returned 3", returned == true );
		ASSERTM( "Testing getValue(string) 3", double2string_value == "1.000001" );
		//STRING
		std::string string2string_value { };
		returned = string_a.getValue( string2string_value );
		ASSERTM( "Testing getValue(string) returned 4", returned == true );
		ASSERTM( "Testing getValue(string) 4", string2string_value == "cool" );
		//BOOL
		std::string bool2string_value { };
		returned = bool_a.getValue( bool2string_value );
		ASSERTM( "Testing getValue(string) returned 5", returned == true );
		ASSERTM( "Testing getValue(string) 5", bool2string_value == "TRUE" );
		returned = bool_b.getValue( bool2string_value );
		ASSERTM( "Testing getValue(string) returned 6", returned == true );
		ASSERTM( "Testing getValue(string) 6", bool2string_value == "FALSE" );
	}
	void getValue__test4() { //Bool
		Item null_a = Item();
		Item int_a = Item( 5 );
		Item int_b = Item( 0 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( 0 );
		Item string_a = Item( "cool" );
		Item string_b = Item( "" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		bool null2bool_value { };
		bool returned = null_a.getValue( null2bool_value );
		ASSERTM( "Testing getValue(bool) returned 1", returned == true );
		ASSERTM( "Testing getValue(bool) 1", null2bool_value == false );
		//INT
		bool int2bool_value { };
		returned = int_a.getValue( int2bool_value );
		ASSERTM( "Testing getValue(bool) returned 2", returned == true );
		ASSERTM( "Testing getValue(bool) 2", int2bool_value == true );
		returned = int_b.getValue( int2bool_value );
		ASSERTM( "Testing getValue(bool) returned 3", returned == true );
		ASSERTM( "Testing getValue(bool) 3", int2bool_value == false );
		//DOUBLE
		bool double2bool_value { };
		returned = double_a.getValue( double2bool_value );
		ASSERTM( "Testing getValue(bool) returned 4", returned == true );
		ASSERTM( "Testing getValue(bool) 4", double2bool_value == true );
		returned = double_b.getValue( double2bool_value );
		ASSERTM( "Testing getValue(bool) returned 5", returned == true );
		ASSERTM( "Testing getValue(bool) 5", double2bool_value == false );
		//STRING
		bool string2bool_value { };
		returned = string_a.getValue( string2bool_value );
		ASSERTM( "Testing getValue(bool) returned 6", returned == true );
		ASSERTM( "Testing getValue(bool) 6", string2bool_value == true );
		returned = string_b.getValue( string2bool_value );
		ASSERTM( "Testing getValue(bool) returned 7", returned == true );
		ASSERTM( "Testing getValue(bool) 7", string2bool_value == false );
		//BOOL
		bool bool2bool_value { };
		returned = bool_a.getValue( bool2bool_value );
		ASSERTM( "Testing getValue(bool) returned 8", returned == true );
		ASSERTM( "Testing getValue(bool) 8", bool2bool_value == true );
		returned = bool_b.getValue( bool2bool_value );
		ASSERTM( "Testing getValue(bool) returned 9", returned == true );
		ASSERTM( "Testing getValue(bool) 9", bool2bool_value == false );
	}
	void getInt__Test() {
		Item null_a = Item();
		Item int_a = Item( 1 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( -0.99 );
		Item double_c = Item( 9.49 );
		Item string_a = Item( "cool" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		int null2int_value = null_a.getInt();
		ASSERTM( "Testing getInt() 1", null2int_value == 0 );
		//INT
		int int2int_value = int_a.getInt();
		ASSERTM( "Testing getInt() 2", int2int_value == 1 );
		//DOUBLE
		int double2int_value = double_a.getInt();
		ASSERTM( "Testing getInt() 3", double2int_value == 1 );
		double2int_value = double_b.getInt();
		ASSERTM( "Testing getInt() 4", double2int_value == -1 );
		double2int_value = double_c.getInt();
		ASSERTM( "Testing getInt() 5", double2int_value == 9 );
		//STRING
		int string2int_value = string_a.getInt();
		ASSERTM( "Testing getInt() 6", string2int_value == 4 );
		//BOOL
		bool bool2int_value = bool_a.getInt();
		ASSERTM( "Testing getInt() 7", bool2int_value == 1 );
		bool2int_value = bool_b.getInt();
		ASSERTM( "Testing getInt() 8", bool2int_value == 0 );
	}
	void getDouble__Test() {
		Item null_a = Item();
		Item int_a = Item( 1 );
		Item double_a = Item( 1.000001 );
		Item string_a = Item( "cool" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		double null2double_value = null_a.getDouble();
		ASSERTM( "Testing getDouble() 1", null2double_value == 0 );
		//INT
		double int2double_value = int_a.getDouble();
		ASSERTM( "Testing getDouble() 2", int2double_value == 1 );
		//DOUBLE
		double double2double_value = double_a.getDouble();
		ASSERTM( "Testing getDouble() 3", double2double_value == 1.000001 );
		//STRING
		double string2double_value = string_a.getDouble();
		ASSERTM( "Testing getDouble() 4", string2double_value == 4 );
		//BOOL
		double bool2double_value = bool_a.getDouble();
		ASSERTM( "Testing getDouble() 5", bool2double_value == 1 );
		bool2double_value = bool_b.getDouble();
		ASSERTM( "Testing getDouble() 6", bool2double_value == 0 );
	}
	void getString__Test() {
		Item null_a = Item();
		Item int_a = Item( 1 );
		Item double_a = Item( 1.000001 );
		Item string_a = Item( "cool" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		std::string null2string_value = null_a.getString();
		ASSERTM( "Testing getString() 1", null2string_value == "" );
		//INT
		std::string int2string_value = int_a.getString();
		ASSERTM( "Testing getString() 2", int2string_value == "1" );
		//DOUBLE
		std::string double2string_value = double_a.getString();
		ASSERTM( "Testing getString() 3", double2string_value == "1.000001" );
		//STRING
		std::string string2string_value = string_a.getString();
		ASSERTM( "Testing getString() 4", string2string_value == "cool" );
		//BOOL
		std::string bool2string_value = bool_a.getString();
		ASSERTM( "Testing getString() 5", bool2string_value == "TRUE" );
		bool2string_value = bool_b.getString();
		ASSERTM( "Testing getString() 6", bool2string_value == "FALSE" );
	}
	void getBool__Test() {
		Item null_a = Item();
		Item int_a = Item( 5 );
		Item int_b = Item( 0 );
		Item double_a = Item( 1.000001 );
		Item double_b = Item( 0 );
		Item string_a = Item( "cool" );
		Item string_b = Item( "" );
		Item bool_a = Item( true );
		Item bool_b = Item( false );
		//NULL
		bool null2bool_value = null_a.getBool();
		ASSERTM( "Testing getBool() 1", null2bool_value == false );
		//INT
		bool int2bool_value = int_a.getBool();
		ASSERTM( "Testing getBool() 2", int2bool_value == true );
		int2bool_value = int_b.getBool();
		ASSERTM( "Testing getBool() 3", int2bool_value == false );
		//DOUBLE
		bool double2bool_value = double_a.getBool();
		ASSERTM( "Testing getBool() 4", double2bool_value == true );
		double2bool_value = double_b.getBool();
		ASSERTM( "Testing getBool() 5", double2bool_value == false );
		//STRING
		bool string2bool_value = string_a.getBool();
		ASSERTM( "Testing getBool() 6", string2bool_value == true );
		string2bool_value = string_b.getBool();
		ASSERTM( "Testing getBool() 7", string2bool_value == false );
		//BOOL
		bool bool2bool_value = bool_a.getBool();
		ASSERTM( "Testing getBool() 8", bool2bool_value == true );
		bool2bool_value = bool_b.getBool();
		ASSERTM( "Testing getBool() 9", bool2bool_value == false );
	}
	void getType__test() {
		Item null_a = Item();
		Item int_a = Item( 1 );
		Item double_a = Item( 1.000001 );
		Item string_a = Item( "cool" );
		Item bool_a = Item( true );
		//NULL
		ASSERTM( "Testing getType() null", null_a.getType() == dataType::NONE );
		//INT
		ASSERTM( "Testing getType() int", int_a.getType() == dataType::INTEGER );
		//DOUBLE
		ASSERTM( "Testing getType() double", double_a.getType() == dataType::DOUBLE );
		//STRING
		ASSERTM( "Testing getType() std::string", string_a.getType() == dataType::STRING );
		//BOOL
		ASSERTM( "Testing getType() bool", bool_a.getType() == dataType::BOOLEAN );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( less_operator__test ) );
		s.push_back( CUTE( more_operator__test ) );
		s.push_back( CUTE( less_equal_operator__test ) );
		s.push_back( CUTE( more_equal_operator__test ) );
		s.push_back( CUTE( same_operator__test ) );
		s.push_back( CUTE( not_same_operator__test ) );
		s.push_back( CUTE( Item__test1 ) );
		s.push_back( CUTE( Item__test2 ) );
		s.push_back( CUTE( Item__test3 ) );
		s.push_back( CUTE( Item__test4 ) );
		s.push_back( CUTE( Item_test5 ) );
		s.push_back( CUTE( stream_out_operator__test ) );
		s.push_back( CUTE( equal_operator__test ) );
		s.push_back( CUTE( getValue__test1 ) );
		s.push_back( CUTE( getValue__test2 ) );
		s.push_back( CUTE( getValue__test3 ) );
		s.push_back( CUTE( getValue__test4 ) );
		s.push_back( CUTE( getInt__Test ) );
		s.push_back( CUTE( getDouble__Test ) );
		s.push_back( CUTE( getString__Test ) );
		s.push_back( CUTE( getBool__Test ) );
		s.push_back( CUTE( getType__test ) );

		return s;
	}
}

#endif /* ITEM_TEST_HPP_ */

