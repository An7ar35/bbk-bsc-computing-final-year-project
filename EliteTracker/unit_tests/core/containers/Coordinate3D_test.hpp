/*
 * Coordinate3D.hpp
 *
 *  Created on: 21 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_CONTAINERS_COORDINATE3D_TEST_HPP_
#define CORE_CONTAINERS_COORDINATE3D_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/core/containers/Coordinate3D.hpp"

namespace Coordinate3D_Tests {
	void constructor__test1() {
		Coordinate3D coord = Coordinate3D();
		ASSERTM( "Testing Coordinate3D()", coord.x == 0 && coord.y == 0 && coord.z == 0 );
	}
	void constructor__test2() {
		Coordinate3D coord = Coordinate3D( 10.01, 20.02, 30.03 );
		ASSERTM( "Testing Coordinate3D(..)",
				coord.x == 10.01 && coord.y == 20.02 && coord.z == 30.03 );
	}
	void reset__test() {
		Coordinate3D coord = Coordinate3D( 10.01, 20.02, 30.03 );
		ASSERTM( "Testing reset() 1", coord.x == 10.01 && coord.y == 20.02 && coord.z == 30.03 );
		coord.reset();
		ASSERTM( "Testing reset() 2", coord.x == 0 && coord.y == 0 && coord.z == 0 );
	}
	void isReset__test() {
		Coordinate3D coord = Coordinate3D( 999, 999, 999 );
		ASSERTM( "Testing isReset() 1", coord.x == 999 && coord.y == 999 && coord.z == 999 );
		coord.reset();
		ASSERTM( "Testing isReset() 2", coord.x == 0 && coord.y == 0 && coord.z == 0 );
	}
	void swap__test() {
		Coordinate3D a = Coordinate3D( 100, -100, 0 );
		Coordinate3D b = Coordinate3D( -50, 50, 99 );
		a.error_flag = true;
		a.swap( b );
		ASSERTM( "Testing swap(&) 1",
				( a.x == -50 && a.y == 50 && a.z == 99 && !a.error_flag ) == true );
		ASSERTM( "Testing swap(&) 2",
				( b.x == 100 && b.y == -100 && b.z == 0 && b.error_flag ) == true );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( constructor__test1 ) );
		s.push_back( CUTE( constructor__test2 ) );
		s.push_back( CUTE( reset__test ) );
		s.push_back( CUTE( isReset__test ) );
		s.push_back( CUTE( swap__test ) );
		return s;
	}
}

#endif /* CORE_CONTAINERS_COORDINATE3D_TEST_HPP_ */

