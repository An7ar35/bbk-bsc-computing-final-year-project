/*
 * Table_test.hpp
 *
 *  Created on: 21 Jan 2015
 *      Author: Alwlyan
 */

#ifndef TABLE_TEST_HPP_
#define TABLE_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../src/core/containers/Table.hpp"

namespace Table_Tests {
	void bracket_operator__test() {
		//Item & operator()( int column, int row )
		Table table = Table();
		Db test( "testdata.db" );
		test.open();
		test.sqlQueryPull_typed( "SELECT * FROM TableTest", table );
		test.close();
		int container1 { };
		int container2 { };
		std::string container3 { };
		//Row 1
		Item item_a = table( 0, 0 );
		Item item_b = table( 1, 0 );
		Item item_c = table( 2, 0 );
		item_a.getValue( container1 );
		item_b.getValue( container2 );
		item_c.getValue( container3 );
		ASSERTM( "Testing Table() '()' operator for (0,0)", container1 == 1 );
		ASSERTM( "Testing Table() '()' operator for (1,0)", container2 == 10 );
		ASSERTM( "Testing Table() '()' operator for (2,0)", container3 == "Ten" );
		//Row 2
		item_a = table( 0, 1 );
		item_b = table( 1, 1 );
		item_c = table( 2, 1 );
		item_a.getValue( container1 );
		item_b.getValue( container2 );
		item_c.getValue( container3 );
		ASSERTM( "Testing Table() '()' operator for (0,1)", container1 == 2 );
		ASSERTM( "Testing Table() '()' operator for (1,1)", container2 == 20 );
		ASSERTM( "Testing Table() '()' operator for (2,1)", container3 == "Twenty" );
		//Row 3
		item_a = table( 0, 2 );
		item_b = table( 1, 2 );
		item_c = table( 2, 2 );
		item_a.getValue( container1 );
		item_b.getValue( container2 );
		item_c.getValue( container3 );
		ASSERTM( "Testing Table() '()' operator for (0,2)", container1 == 3 );
		ASSERTM( "Testing Table() '()' operator for (1,2)", container2 == 30 );
		ASSERTM( "Testing Table() '()' operator for (2,2)", container3 == "Thirty" );
		//Row 4
		item_a = table( 0, 3 );
		item_b = table( 1, 3 );
		item_c = table( 2, 3 );
		item_a.getValue( container1 );
		item_b.getValue( container2 );
		item_c.getValue( container3 );
		ASSERTM( "Testing Table() '()' operator for (0,3)", container1 == 4 );
		ASSERTM( "Testing Table() '()' operator for (1,3)", container2 == 40 );
		ASSERTM( "Testing Table() '()' operator for (2,3)", container3 == "Forty" );
		//Row 5
		item_a = table( 0, 4 );
		item_b = table( 1, 4 );
		item_c = table( 2, 4 );
		item_a.getValue( container1 );
		item_b.getValue( container2 );
		item_c.getValue( container3 );
		ASSERTM( "Testing Table() '()' operator for (0,3)", container1 == 5 );
		ASSERTM( "Testing Table() '()' operator for (1,3)", container2 == 50 );
		ASSERTM( "Testing Table() '()' operator for (2,3)", container3 == "Fifty" );
	}
	void clear__test() {
		Table table = Table();
		Db test( "testdata.db" );
		test.open();
		test.sqlQueryPull_typed( "SELECT * FROM TableTest", table );
		test.close();
		table.clear();
		Item item = table( 0, 0 );
		std::string container { };
		item.getValue( container );
		ASSERTM( "Testing clear() 1", table.getColumnCount() == 0 );
		ASSERTM( "Testing clear() 2", table.getRowCount() == 0 );
		ASSERTM( "Testing clear() 3", container == "<!exist>" );
	}
	void clearData__test() {
		Table table = Table();
		Db test( "testdata.db" );
		test.open();
		test.sqlQueryPull_typed( "SELECT * FROM TableTest", table );
		test.close();
		table.clearData();
		Item item = table( 0, 0 );
		std::string container { };
		item.getValue( container );
		ASSERTM( "Testing clearData() 1", table.getColumnCount() == 3 );
		ASSERTM( "Testing clearData() 2", table.getHeading( 0 ) == "ID" );
		ASSERTM( "Testing clearData() 3", table.getHeading( 1 ) == "data1" );
		ASSERTM( "Testing clearData() 4", table.getHeading( 2 ) == "data2" );
		ASSERTM( "Testing clearData() 5", table.getRowCount() == 0 );
		ASSERTM( "Testing clearData() 6", container == "<!exist>" );
	}
	void createColumn__test() {
		Table table = Table();
		ASSERTM( "Testing createColumn() 1", table.getColumnCount() == 0 );
		bool returned = table.createColumn( 1, "integer" );
		ASSERTM( "Testing createColumn() returned 2", returned == true );
		ASSERTM( "Testing createColumn() 2", table.getColumnCount() == 1 );
		returned = table.createColumn( 2, "double" );
		ASSERTM( "Testing createColumn() returned 3", returned == true );
		ASSERTM( "Testing createColumn() 3", table.getColumnCount() == 2 );
		returned = table.createColumn( 3, "string" );
		ASSERTM( "Testing createColumn() returned 4", returned == true );
		ASSERTM( "Testing createColumn() 4", table.getColumnCount() == 3 );
		returned = table.createColumn( 10, "bad type" );
		ASSERTM( "Testing createColumn() returned 5", returned == false );
		ASSERTM( "Testing createColumn() 5", table.getColumnCount() == 3 );
		table.lockTableStructure();
		returned = table.createColumn( 1, "after lock" );
		ASSERTM( "Testing createColumn() returned 6", returned == false );
		ASSERTM( "Testing createColumn() 6", table.getColumnCount() == 3 );
	}
	void lockTableStructure__test() {
		Table table = Table();
		bool returned = table.createColumn( 1, "integer" );
		ASSERTM( "Testing lockTableStructure() [createColumn()] 1", returned == true );
		ASSERTM( "Testing lockTableStructure() [getColumnCount()] 1", table.getColumnCount() == 1 );
		ASSERTM( "Testing lockTableStructure() [add()] 1", table.add( 100 ) == false );
		table.lockTableStructure();
		returned = table.createColumn( 1, "new" );
		ASSERTM( "Testing lockTableStructure() [createColumn()] 2", returned == false );
		ASSERTM( "Testing lockTableStructure() [getColumnCount()] 2", table.getColumnCount() == 1 );
		ASSERTM( "Testing lockTableStructure() [add()] 2", table.add( 200 ) == true );
	}
	void add__test0() { //NULL
		Table table = Table();
		table.createColumn( 1, "dataA" );
		table.createColumn( 1, "dataB" );
		table.createColumn( 1, "dataC" );
		bool returned = table.add();
		ASSERTM( "Testing add() returned 1", returned == false );
		table.lockTableStructure();
		//Write/Read tests
		int container { };
		for( int i = 1; i <= 300; i++ ) {
			returned = table.add();
			ASSERTM( "Testing add() write successes", returned == true );
		}
		for( int i = 0, count = 1; i < 100; i++ ) { //rows
			for( int j = 0; j < 3; j++, count++ ) { //columns
				Item item = table( j, i );
				item.getValue( container );
				ASSERTM( "Testing add(int) read successes", container == 0 );
			}
		}
	}
	void add__test1() { //int
		Table table = Table();
		table.createColumn( 1, "dataA" );
		table.createColumn( 1, "dataB" );
		table.createColumn( 1, "dataC" );
		bool returned = table.add( 666 );
		ASSERTM( "Testing add(int) returned 1", returned == false );
		table.lockTableStructure();
		//Write/Read tests
		int container { };
		for( int i = 1; i <= 300; i++ ) {
			returned = table.add( i );
			ASSERTM( "Testing add(int) write successes", returned == true );
		}
		for( int i = 0, count = 1; i < 100; i++ ) { //rows
			for( int j = 0; j < 3; j++, count++ ) { //columns
				Item item = table( j, i );
				item.getValue( container );
				ASSERTM( "Testing add(int) read successes", container == count );
			}
		}
	}
	void add__test2() { //double
		Table table = Table();
		table.createColumn( 2, "dataA" );
		table.createColumn( 2, "dataB" );
		table.createColumn( 2, "dataC" );
		bool returned = table.add( 666.666 );
		ASSERTM( "Testing add(double) returned 1", returned == false );
		table.lockTableStructure();
		//Write/Read tests
		double container { };
		double value { 1.123456 };
		for( int i = 1; i <= 300; i++ ) {
			returned = table.add( value );
			ASSERTM( "Testing add(double) write successes", returned == true );
			value += 1;
		}
		double count { 1.123456 };
		for( int i = 0; i < 100; i++ ) { //rows
			for( int j = 0; j < 3; j++ ) { //columns
				Item item = table( j, i );
				item.getValue( container );
				ASSERTM( "Testing add(double) read successes", container == count );
				count += 1;
			}
		}
	}
	void add__test3() { //string
		Table table = Table();
		table.createColumn( 3, "dataA" );
		table.createColumn( 3, "dataB" );
		table.createColumn( 3, "dataC" );
		bool returned = table.add( "some data" );
		ASSERTM( "Testing add(string) returned 1", returned == false );
		table.lockTableStructure();
		//Write/Read tests
		std::string container { };
		for( int i = 1; i <= 300; i++ ) {
			returned = table.add( "some data" );
			ASSERTM( "Testing add(string) write successes", returned == true );
		}
		for( int i = 0; i < 100; i++ ) { //rows
			for( int j = 0; j < 3; j++ ) { //columns
				Item item = table( j, i );
				item.getValue( container );
				ASSERTM( "Testing add(int) read successes", container == "some data" );
			}
		}
	}
	void getHeading__test() {
		Table table = Table();
		ASSERTM( "Testing getHeading() 1", table.getHeading( 0 ) == "INVALID COL" );
		table.createColumn( 1, "integer" );
		ASSERTM( "Testing getHeading() 2", table.getHeading( 0 ) == "integer" );
		table.createColumn( 2, "double" );
		ASSERTM( "Testing getHeading() 3", table.getHeading( 1 ) == "double" );
		ASSERTM( "Testing getHeading() 4", table.getHeading( 2 ) == "INVALID COL" );
	}
	void getType__test() {
		Table table = Table();
		table.createColumn( 1, "Int_Col" );
		table.createColumn( 2, "Double_Col" );
		table.createColumn( 3, "String_Col" );
		table.createColumn( 1000, "Bool_Col" );
		ASSERTM( "Testing getType() 1 )", table.getType( 0 ) == dataType::INTEGER );
		ASSERTM( "Testing getType() 2 )", table.getType( 1 ) == dataType::DOUBLE );
		ASSERTM( "Testing getType() 3 )", table.getType( 2 ) == dataType::STRING );
		ASSERTM( "Testing getType() 5 )", table.getType( 3 ) == dataType::BOOLEAN );
		ASSERTM( "Testing getType() 6 )", table.getType( 4 ) == dataType::NONE );
	}
	void getColumnCount__test() {
		Table table = Table();
		ASSERTM( "Testing getColumnCount() 1", table.getColumnCount() == 0 );
		table.createColumn( 1, "integer" );
		ASSERTM( "Testing getColumnCount() 2", table.getColumnCount() == 1 );
		table.createColumn( 2, "double" );
		ASSERTM( "Testing getColumnCount() 3", table.getColumnCount() == 2 );
		table.createColumn( 3, "string" );
		ASSERTM( "Testing getColumnCount() 4", table.getColumnCount() == 3 );
	}
	void getRowCount__test() {
		Table table = Table();
		table.createColumn( 1, "data" );
		table.lockTableStructure();
		ASSERTM( "Testing getRowCount() 0 rows", table.getRowCount() == 0 );
		for( int i = 1; i < 1000; i++ ) {
			table.add( i );
			ASSERTM( "Testing getRowCount() 1-1000 rows", table.getRowCount() == i );
		}
		Item last = table( 0, ( table.getRowCount() - 1 ) );
		int container { };
		last.getValue( container );
		ASSERTM( "Testing getRowCount()'s last value added", container == 999 );
	}
	void checkIntegrity__test() {
		//TODO
	}
	void checkBorders__test() {
		//TODO
	}
	void sort__test() {
		//TODO
	}
	void whereIsThisColumn__test() {
		Table table = Table();
		table.createColumn( 1, "Int_Col" );
		table.createColumn( 2, "Double_Col" );
		table.createColumn( 3, "String_Col" );
		table.createColumn( 1000, "Bool_Col" );
		ASSERTM( "Testing whereIsThisColumn(..) 1", table.whereIsThisColumn( "Int_Col" ) == 0 );
		ASSERTM( "Testing whereIsThisColumn(..) 2", table.whereIsThisColumn( "Double_Col" ) == 1 );
		ASSERTM( "Testing whereIsThisColumn(..) 3", table.whereIsThisColumn( "String_Col" ) == 2 );
		ASSERTM( "Testing whereIsThisColumn(..) 4", table.whereIsThisColumn( "Bool_Col" ) == 3 );
		ASSERTM( "Testing whereIsThisColumn(..) 5", table.whereIsThisColumn( "Unknown" ) == -1 );
	}
	void searchFor__test1() {
		//TODO
	}
	void searchFor__test2() {
		//TODO
	}
	void searchFor__test3() {
		//TODO
	}
	void stress_test() {
		Table table = Table();
		table.createColumn( 1, "col1" );
		table.createColumn( 1, "col2" );
		table.createColumn( 1, "col3" );
		table.createColumn( 1, "col4" );
		table.createColumn( 1, "col5" );
		table.createColumn( 1, "col6" );
		table.createColumn( 1, "col7" );
		table.createColumn( 1, "col8" );
		table.createColumn( 1, "col9" );
		table.createColumn( 1, "col10" );
		table.lockTableStructure();
		int count { 10000000 };
		std::chrono::high_resolution_clock::time_point t1 =
				std::chrono::high_resolution_clock::now();
		for( int i = 0; i < count; i++ ) {
			table.add( i );
		}
		std::chrono::high_resolution_clock::time_point t2 =
				std::chrono::high_resolution_clock::now();
		auto duration = std::chrono::duration_cast<std::chrono::microseconds>( t2 - t1 ).count();
		Item last = table( 9, 999999 );
		int container { };
		last.getValue( container );
		ASSERTM( "Stress test with 1 million items in Table", container == 9999999 );
		std::cout << "  -> Adding " << count << " Items took: " << duration << " microseconds ("
				<< ( duration / 1000000 ) << " secs)" << std::endl;
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( bracket_operator__test ) );
		s.push_back( CUTE( clear__test ) );
		s.push_back( CUTE( clearData__test ) );
		s.push_back( CUTE( createColumn__test ) );
		s.push_back( CUTE( lockTableStructure__test ) );
		s.push_back( CUTE( add__test0 ) );
		s.push_back( CUTE( add__test1 ) );
		s.push_back( CUTE( add__test2 ) );
		s.push_back( CUTE( add__test3 ) );
		s.push_back( CUTE( getHeading__test ) );
		s.push_back( CUTE( getType__test ) );
		s.push_back( CUTE( getColumnCount__test ) );
		s.push_back( CUTE( getRowCount__test ) );

		s.push_back( CUTE( whereIsThisColumn__test ) );

		s.push_back( CUTE( stress_test ) );

		return s;
	}
}

#endif /* TABLE_TEST_HPP_ */

