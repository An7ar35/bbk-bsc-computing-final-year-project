/*
 * toolbox_test.hpp
 *
 *  Created on: 28 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_TOOLBOX_TOOLBOX_TEST_HPP_
#define CORE_TOOLBOX_TOOLBOX_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/core/toolbox/toolbox.hpp"

namespace Toolbox_Tests {
	void setPrecision__test() {
		double value { 12.123456789 };
		ASSERTM( "Testing setPrecision(..) 1", CoreTools::getPrecision( value ) == 11 );
		long double value2 { 123456789.12 };
		ASSERTM( "Testing setPrecision(..) 2", CoreTools::getPrecision( value2 ) == 11 );
		double value3 = 0;
		ASSERTM( "Testing setPrecision(..) 3", CoreTools::getPrecision( value3 ) == 0 );
		double value4 = 3;
		ASSERTM( "Testing setPrecision(..) 4", CoreTools::getPrecision( value4 ) == 1 );
	}

	void acceptably_equal__test() {
		double val1 { 1.00001 };
		double ref { 1.00002 };
		ASSERTM( "Testing acceptably_equal(..) 1",
				CoreTools::acceptably_equal( val1, ref, 0.00002 ) );
		ASSERTM( "Testing acceptably_equal(..) 1",
				!CoreTools::acceptably_equal( val1, ref, 0.00001 ) );
		ASSERTM( "Testing acceptably_equal(..) 1",
				CoreTools::acceptably_equal( val1, ref, 0.00003 ) );
	}
	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( setPrecision__test ) );
		s.push_back( CUTE( acceptably_equal__test ) );
		return s;
	}
}

#endif /* CORE_TOOLBOX_TOOLBOX_TEST_HPP_ */
