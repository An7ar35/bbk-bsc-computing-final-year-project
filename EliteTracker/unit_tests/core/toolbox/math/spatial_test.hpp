/*
 * spatial_test.hpp
 *
 *  Created on: 18 Mar 2015
 *      Author: Alwlyan
 */

#ifndef CORE_TOOLBOX_MATH_SPATIAL_TEST_HPP_
#define CORE_TOOLBOX_MATH_SPATIAL_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../equalish.hpp"
#include "../../../../src/core/toolbox/math/spatial.hpp"

namespace Spatial_Tests {
	void calcDistance__test() {
		Coordinate3D c1( 0, 0, 0 );
		Coordinate3D c2( 10, 10, 10 );
		Coordinate3D c3( -5, -50, -3 );
		ASSERTM( "Testing calcDistance 1",
				equalish( CoreTools::math::spatial::calcDistance( c1, c2 ), 17.320508075689, 5 ) );
		ASSERTM( "Testing calcDistance 2",
				equalish( CoreTools::math::spatial::calcDistance( c2, c3 ), 63.1981012373, 5 ) );
		ASSERTM( "Testing calcDistance 3",
				equalish( CoreTools::math::spatial::calcDistance( c1, c3 ), 50.338851794613, 5 ) );
	}
	void calcDistanceXYZ__test() {
		Coordinate3D c1( 0, 0, 0 );
		Coordinate3D c2( 10, 10, 10 );
		Coordinate3D c3( -5, -50, -3 );
		ASSERTM( "Testing calcDistance_XYZ 1",
				CoreTools::math::spatial::calcDistanceXYZ( c1, c2 )
						== Coordinate3D( -10, -10, -10 ) );
		ASSERTM( "Testing calcDistance_XYZ 2",
				CoreTools::math::spatial::calcDistanceXYZ( c2, c1 ) == Coordinate3D( 10, 10, 10 ) );
		ASSERTM( "Testing calcDistance_XYZ 3",
				CoreTools::math::spatial::calcDistanceXYZ( c1, c3 ) == Coordinate3D( 5, 50, 3 ) );
		ASSERTM( "Testing calcDistance_XYZ 4",
				CoreTools::math::spatial::calcDistanceXYZ( c2, c3 ) == Coordinate3D( 15, 60, 13 ) );
	}
	void calcDistanceXYZ_abs__test() {
		Coordinate3D c1( 0, 0, 0 );
		Coordinate3D c2( 10, 10, 10 );
		Coordinate3D c3( -5, -50, -3 );
		ASSERTM( "Testing calcDistance_XYZ 1",
				CoreTools::math::spatial::calcDistanceXYZ_abs( c1, c2 )
						== Coordinate3D( 10, 10, 10 ) );
		ASSERTM( "Testing calcDistance_XYZ 2",
				CoreTools::math::spatial::calcDistanceXYZ_abs( c2, c1 )
						== Coordinate3D( 10, 10, 10 ) );
		ASSERTM( "Testing calcDistance_XYZ 3",
				CoreTools::math::spatial::calcDistanceXYZ_abs( c1, c3 )
						== Coordinate3D( 5, 50, 3 ) );
		ASSERTM( "Testing calcDistance_XYZ 4",
				CoreTools::math::spatial::calcDistanceXYZ_abs( c2, c3 )
						== Coordinate3D( 15, 60, 13 ) );
	}
	void calcAngleTo__test() {
		//XY plane
		Coordinate3D coord( 5, 5, 0 );
		double angle = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::X,
				CoreTools::math::spatial::PLANE::XY, coord );
		ASSERTM( "Testing calcAngleTo X on XY 1", equalish( angle, 45, 0.01 ) );
		angle = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Y,
				CoreTools::math::spatial::PLANE::XY, coord );
		ASSERTM( "Testing calcAngleTo Y on XY 2", equalish( angle, 45, 0.01 ) );
		angle = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Z,
				CoreTools::math::spatial::PLANE::XY, coord );
		ASSERTM( "Testing calcAngleTo Z on XY 3", angle == 0 );
		coord = Coordinate3D( 1, 2, 0 );
		double angle1 = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::X,
				CoreTools::math::spatial::PLANE::XY, coord );
		ASSERTM( "Testing calcAngleTo X on XY 4", equalish( angle1, 63.4349, 2 ) );
		double angle2 = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Y,
				CoreTools::math::spatial::PLANE::XY, coord );
		ASSERTM( "Testing calcAngleTo Y on XY 5", equalish( angle2, 26.5651, 2 ) );
		ASSERTM( "Testing calcAngleTo X&Y are ~ 90", equalish( ( angle1 + angle2 ), 90, 0.01 ) );
		//XZ plane
		coord = Coordinate3D( 5, 0, 5 );
		angle = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::X,
				CoreTools::math::spatial::PLANE::XZ, coord );
		ASSERTM( "Testing calcAngleTo X on XZ 1", equalish( angle, 45, 0.01 ) );
		angle = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Z,
				CoreTools::math::spatial::PLANE::XZ, coord );
		ASSERTM( "Testing calcAngleTo Z on XZ 2", equalish( angle, 45, 0.01 ) );
		angle = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Y,
				CoreTools::math::spatial::PLANE::XZ, coord );
		ASSERTM( "Testing calcAngleTo Y on XZ 3", angle == 0 );
		coord = Coordinate3D( 1, 0, 2 );
		angle1 = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::X,
				CoreTools::math::spatial::PLANE::XZ, coord );
		ASSERTM( "Testing calcAngleTo X on XZ 4", equalish( angle1, 63.4349, 2 ) );
		angle2 = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Z,
				CoreTools::math::spatial::PLANE::XZ, coord );
		ASSERTM( "Testing calcAngleTo Z on XZ 5", equalish( angle2, 26.5651, 2 ) );
		ASSERTM( "Testing calcAngleTo X&Z are ~ 90", equalish( ( angle1 + angle2 ), 90, 0.01 ) );
		//YZ plane
		coord = Coordinate3D( 0, 5, 5 );
		angle = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Y,
				CoreTools::math::spatial::PLANE::YZ, coord );
		ASSERTM( "Testing calcAngleTo Y on YZ 1", equalish( angle, 45, 0.01 ) );
		angle = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Z,
				CoreTools::math::spatial::PLANE::YZ, coord );
		ASSERTM( "Testing calcAngleTo Z on YZ 2", equalish( angle, 45, 0.01 ) );
		angle = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::X,
				CoreTools::math::spatial::PLANE::YZ, coord );
		ASSERTM( "Testing calcAngleTo X on YZ 3", angle == 0 );
		coord = Coordinate3D( 0, 1, 2 );
		angle1 = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Y,
				CoreTools::math::spatial::PLANE::YZ, coord );
		ASSERTM( "Testing calcAngleTo Y on YZ 4", equalish( angle1, 63.4349, 2 ) );
		angle2 = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Z,
				CoreTools::math::spatial::PLANE::YZ, coord );
		ASSERTM( "Testing calcAngleTo Z on YZ 5", equalish( angle2, 26.5651, 2 ) );
		ASSERTM( "Testing calcAngleTo Y&Z are ~ 90", equalish( ( angle1 + angle2 ), 90, 0.01 ) );
	}
	void calcBounds__test() {
		Coordinate3D a = Coordinate3D( -2, 2, 2 );
		Coordinate3D b = Coordinate3D( 5, -6, -3 );
		Coordinate3D u { };
		Coordinate3D l { };
		CoreTools::math::spatial::calcBounds( a, b, l, u );
		ASSERTM( "Testing calcBounds Lower bound",
				( l.x == -2 && l.y == -6 && l.z == -3 ) == true );
		ASSERTM( "Testing calcBounds Upper bound", ( u.x == 5 && u.y == 2 && u.z == 2 ) == true );
	}
	void orderPoints__test() {
		Coordinate3D a { 10, 0, 0 };
		Coordinate3D b { -10, 0, 0 };
		Coordinate3D c { 100, 0, 0 };
		double ax = 11;
		double bx = 22;
		double cx = 33;
		CoreTools::math::spatial::orderPoints( a, b, c, ax, bx, cx );
		//order should now be b,c,a
		ASSERTM( "Testing orderPoints(..) 1", a.x == -10 );
		ASSERTM( "Testing orderPoints(..) 2", b.x == 100 );
		ASSERTM( "Testing orderPoints(..) 3", c.x == 10 );
		ASSERTM( "Testing orderPoints(..) 4", ax == 22 );
		ASSERTM( "Testing orderPoints(..) 5", bx == 33 );
		ASSERTM( "Testing orderPoints(..) 6", cx == 11 );
	}
	void transform3D_add__test() {
		Coordinate3D a { 0, 0, 0 };
		Coordinate3D b { 15, 15, 15 };
		Coordinate3D c { -15, -15, -15 };
		CoreTools::math::spatial::transform3D_add( a, b );
		ASSERTM( "Testing transform3D_add 1", a == b );
		a = Coordinate3D( 0, 0, 0 );
		CoreTools::math::spatial::transform3D_add( a, c );
		ASSERTM( "Testing transform3D_add 2", a == c );
	}
	void transform3D_substract__test() {
		Coordinate3D a { 0, 0, 0 };
		Coordinate3D b { 15, 15, 15 };
		Coordinate3D c { -15, -15, -15 };
		CoreTools::math::spatial::transform3D_substract( a, b );
		ASSERTM( "Testing transform3D_add 1", a == c );
		a = Coordinate3D( 0, 0, 0 );
		CoreTools::math::spatial::transform3D_substract( a, c );
		ASSERTM( "Testing transform3D_add 2", a == b );
	}
	void transform3D_rotate__test() {
		Coordinate3D coord( 1, 2, 5 );
		CoreTools::math::spatial::transform3D_rotate( coord, CoreTools::math::spatial::AXIS::X,
				90 );
		ASSERTM( "Testing transform3D_rotate 1",
				( equalish( coord.x, 1, 0.0001 ) && equalish( coord.y, -5, 0.0001 )
						&& equalish( coord.z, 2, 0.0001 ) ) );
		coord = Coordinate3D( 1, 2, 5 );
		CoreTools::math::spatial::transform3D_rotate( coord, CoreTools::math::spatial::AXIS::X,
				-90 );
		ASSERTM( "Testing transform3D_rotate 2",
				( equalish( coord.x, 1, 0.0001 ) && equalish( coord.y, 5, 0.0001 )
						&& equalish( coord.z, -2, 0.0001 ) ) );
		CoreTools::math::spatial::transform3D_rotate( coord, CoreTools::math::spatial::AXIS::Y,
				90 );
		ASSERTM( "Testing transform3D_rotate 3",
				( equalish( coord.x, 2, 0.0001 ) && equalish( coord.y, 5, 0.0001 )
						&& equalish( coord.z, 1, 0.0001 ) ) );
		coord = Coordinate3D( 1, 2, 5 );
		CoreTools::math::spatial::transform3D_rotate( coord, CoreTools::math::spatial::AXIS::Y,
				-90 );
		ASSERTM( "Testing transform3D_rotate 4",
				( equalish( coord.x, 5, 0.0001 ) && equalish( coord.y, 2, 0.0001 )
						&& equalish( coord.z, -1, 0.0001 ) ) );
		CoreTools::math::spatial::transform3D_rotate( coord, CoreTools::math::spatial::AXIS::Z,
				90 );
		ASSERTM( "Testing transform3D_rotate 5",
				( equalish( coord.x, -2, 0.0001 ) && equalish( coord.y, 5, 0.0001 )
						&& equalish( coord.z, -1, 0.0001 ) ) );
		coord = Coordinate3D( 1, 2, 5 );
		CoreTools::math::spatial::transform3D_rotate( coord, CoreTools::math::spatial::AXIS::Z,
				-90 );
		ASSERTM( "Testing transform3D_rotate 6",
				( equalish( coord.x, 2, 0.0001 ) && equalish( coord.y, -1, 0.0001 )
						&& equalish( coord.z, 5, 0.0001 ) ) );
	}
	void trilaterate__test1() {
		Pair<Coordinate3D> pair1 = CoreTools::math::spatial::trilaterate( 13.0773, 20.0062, 8.11983,
				19.6534, 15.984, 22.4274 );
		Pair<Coordinate3D> pair2 = CoreTools::math::spatial::trilaterate( 15, 10.77, 14.87, 16.52,
				16.11, 13.9 );
		ASSERTM( "Testing trilaterate_1 1.1",
				( equalish( pair1.a.x, 11.5385, 0.001 ) && equalish( pair1.a.y, -15.6373, 0.001 )
						&& equalish( pair1.a.z, 2.93148, 0.001 ) ) );
		ASSERTM( "Testing trilaterate_1 1.2",
				( equalish( pair1.b.x, 11.5385, 0.001 ) && equalish( pair1.b.y, -15.6373, 0.001 )
						&& equalish( pair1.b.z, -2.93148, 0.001 ) ) );
		ASSERTM( "Testing trilaterate_1 2.1",
				( equalish( pair2.a.x, 7.94594, 0.001 ) && equalish( pair2.a.y, 6.60871, 0.001 )
						&& equalish( pair2.a.z, 12.8879, 0.001 ) ) );
		ASSERTM( "Testing trilaterate_1 2.2",
				( equalish( pair2.b.x, 7.94594, 0.001 ) && equalish( pair2.b.y, 6.60871, 0.001 )
						&& equalish( pair2.b.z, -12.8879, 0.001 ) ) );
	}
	void trilaterate__test2() {
		Coordinate3D A = Coordinate3D( -30.75, 39.7188, 12.7812 ); //10
		Coordinate3D B = Coordinate3D( -22.375, 34.8438, 4 ); //20
		Coordinate3D C = Coordinate3D( -21.9688, 29.0938, -1.71875 ); //30
		Pair<Coordinate3D> pair = CoreTools::math::spatial::trilaterate( A, B, C, 19.6534, 15.984,
				22.4274 );
		Coordinate3D X { -11.5625, 43.8125, 11.625 }; //3
		bool a_match { ( equalish( pair.a.x, X.x, 2 ) && equalish( pair.a.y, X.y, 2 )
				&& equalish( pair.a.z, X.z, 2 ) ) };
		bool b_match { ( equalish( pair.b.x, X.x, 2 ) && equalish( pair.b.y, X.y, 2 )
				&& equalish( pair.b.z, X.z, 2 ) ) };
		ASSERTM( "Testing trilaterate_2", ( a_match || b_match ) );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( orderPoints__test ) );
		s.push_back( CUTE( calcBounds__test ) );
		s.push_back( CUTE( transform3D_add__test ) );
		s.push_back( CUTE( transform3D_substract__test ) );
		s.push_back( CUTE( calcDistance__test ) );
		s.push_back( CUTE( calcDistanceXYZ__test ) );
		s.push_back( CUTE( calcDistanceXYZ_abs__test ) );
		s.push_back( CUTE( calcAngleTo__test ) );
		s.push_back( CUTE( transform3D_rotate__test ) );
		s.push_back( CUTE( trilaterate__test1 ) );
		s.push_back( CUTE( trilaterate__test2 ) );
		return s;
	}
}

#endif /* CORE_TOOLBOX_MATH_SPATIAL_TEST_HPP_ */
