/*
 * trigonometry_test.hpp
 *
 *  Created on: 15 Oct 2014
 *      Author: Alwlyan
 */

#ifndef TRIGONOMETRY_TEST_HPP_
#define TRIGONOMETRY_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"

#include <iostream>
#include <math.h>
#include "../../../equalish.hpp"

#include "../../../../src/core/toolbox/math/trigonometry.hpp"

namespace trigonometry_Tests {
	void radToDegree__test() {
		double deg = CoreTools::math::trigonometry::radToDegree( 0.4 );
		ASSERTM( "Testing <radToDegree>", equalish( deg, 22.918311805, 4 ) );
	}
	void degreeToRad__test() {
		double rad = CoreTools::math::trigonometry::degreeToRad( 45 );
		ASSERTM( "Testing <degreeToRad>", equalish( rad, 0.7853981634, 4 ) );
	}
	void calcAngle_SSS() {
		double sss = CoreTools::math::trigonometry::calcAngle_SSS( 8, 6, 7 );
		ASSERTM( "Testing <calcAngle_SSS>", equalish( sss, 75.5225, 4 ) );
	}
	void calcSide_AAS() {
		double aas = CoreTools::math::trigonometry::calcSide_AAS( 35, 62, 7 );
		ASSERTM( "Testing <calcAngle_AAS>", equalish( aas, 4.5473, 4 ) );
	}
	void calcSide_SAS() {
		double sas = CoreTools::math::trigonometry::calcSide_SAS( 5, 7, 49 );
		ASSERTM( "Testing <calcAngle_SAS>", equalish( sas, 5.2987, 4 ) );
	}
	void pythagoras__test() {
		double s = CoreTools::math::trigonometry::pythagoras( 3, 5 );
		ASSERTM( "Testing <pythagoras>", s == 4 );
	}
	void pythagoras_hyp__test() {
		double h = CoreTools::math::trigonometry::pythagoras_hyp( 4, 3 );
		ASSERTM( "Testing <pythagoras_hyp>", h == 5 );
	}
	void getAreaOf_Triangle__test() {
		double area = CoreTools::math::trigonometry::getAreaOf_Triangle( 24, 30, 18 );
		ASSERTM( "Testing <getAreaOf_Triangle>", area == 216 );
	}
	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( radToDegree__test ) );
		s.push_back( CUTE( degreeToRad__test ) );
		s.push_back( CUTE( calcAngle_SSS ) );
		s.push_back( CUTE( calcSide_AAS ) );
		s.push_back( CUTE( calcSide_SAS ) );
		s.push_back( CUTE( pythagoras__test ) );
		s.push_back( CUTE( pythagoras_hyp__test ) );
		s.push_back( CUTE( getAreaOf_Triangle__test ) );
		return s;
	}

}

#endif /* TRIGONOMETRY_TEST_HPP_ */

