/*
 * Logmsg_test.hpp
 *
 *  Created on: 24 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_LOGGER_LOGMSG_TEST_HPP_
#define CORE_LOGGER_LOGMSG_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/core/logger/Logmsg.hpp"

namespace Logmsg_Tests {
	void getLocation__test() {
		std::ostringstream oss { };
		oss << "This is a test";
		LogMsg msg = LogMsg( Location::Core, EventType::Error, oss );
		ASSERTM( "Testing getLocation()", msg.getLocation() == Location::Core );
	}
	void getEventType__test() {
		std::ostringstream oss { };
		oss << "This is a test";
		LogMsg msg = LogMsg( Location::Core, EventType::Error, oss );
		ASSERTM( "Testing getEventType()", msg.getEventType() == EventType::Error );
	}
	void getInfo__test() {
		std::ostringstream oss { };
		oss << "This is a test";
		LogMsg msg = LogMsg( Location::Core, EventType::Error, oss );
		ASSERTM( "Testing getInfo()", msg.getInfo().substr( 22 ) == "[Core.Error] This is a test" );
		//dd/mm/yyyy - hh:mm:ss [Core.Error] This is a test
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( getLocation__test ) );
		s.push_back( CUTE( getEventType__test ) );
		s.push_back( CUTE( getInfo__test ) );
		return s;
	}
}

#endif /* CORE_LOGGER_LOGMSG_TEST_HPP_ */
