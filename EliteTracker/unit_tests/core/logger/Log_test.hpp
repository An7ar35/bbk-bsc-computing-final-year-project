/*
 * Log_test.hpp
 *
 *  Created on: 24 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_LOGGER_LOG_TEST_HPP_
#define CORE_LOGGER_LOG_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/core/logger/Log.hpp"

namespace Log_Tests {
	void logEvent__test() {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Trace, "Log test 1" );

	}
	void newEvent__test() {

	}
	void grabEvent__test() {

	}
	void isEmpty__test() {

	}
	void logSize__test() {

	}
	void switch_EventType__test() {

	}
	void switch_Location__test() {

	}
	void dumpToFile__test() {

	}

	cute::suite make_suite() {
		cute::suite s { };
		return s;
	}
}



#endif /* CORE_LOGGER_LOG_TEST_HPP_ */
