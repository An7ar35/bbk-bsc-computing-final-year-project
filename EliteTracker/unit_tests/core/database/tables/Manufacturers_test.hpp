/*
 * JumpConnections_test.cpp
 *
 *  Created on: 14 Feb 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_DATABASE_TABLES_MANUFACTURERS_TEST_HPP_
#define CORE_DATABASE_TABLES_MANUFACTURERS_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../../src/core/database/tables/Manufacturers.hpp"

namespace Manufacturers_Tests {

	cute::suite make_suite() {
		cute::suite s { };
		return s;
	}
}



#endif /* CORE_DATABASE_TABLES_MANUFACTURERS_TEST_HPP_ */
