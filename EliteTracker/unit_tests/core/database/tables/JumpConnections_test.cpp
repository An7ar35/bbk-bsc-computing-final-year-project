/*
 * JumpConnections_test.cpp
 *
 *  Created on: 14 Feb 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_DATABASE_TABLES_JUMPCONNECTIONS_TEST_CPP_
#define CORE_DATABASE_TABLES_JUMPCONNECTIONS_TEST_CPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../../src/core/database/tables/JumpConnections.hpp"

namespace JumpConnections_Tests {
	void getDistance__test() {
		Db testdata( "testdata.db" );
		testdata.open();
		double dist = editTable::JumpConnections::getDistance( testdata, 1, 2 );
		ASSERTM( "Testing getDistance", dist == 20.7743 );
		testdata.close();
	}
	void getAllDistancesFor__test() {
		Db testdata( "testdata.db" );
		testdata.open();


		testdata.close();
		//Table & getAllDistancesFor( Db &data, int point_id );
	}
	void add__test1() {
		Db testdata( "testdata.db" );
		testdata.open();



		testdata.close();
		//( Db &data, int pointA, int pointB );
	}
	void add__test2() {
		Db testdata( "testdata.db" );
		testdata.open();

		testdata.close();
		//( Db &data, int pointA, int pointB, double distance );
	}
	void add__test3() {
		Db testdata( "testdata.db" );
		testdata.open();

		testdata.close();
		//( Db & data, std::string pointA, std::string pointB );
	}
	void add__test4() {
		Db testdata( "testdata.db" );
		testdata.open();

		testdata.close();
		//( Db & data, std::string pointA, std::string pointB, double distance );
	}
	void edit__test() {
		Db testdata( "testdata.db" );
		testdata.open();

		testdata.close();
		//( Db &data, int pointA_id, int pointB_id, double distance );
	}
	void remove__test() {
		Db testdata( "testdata.db" );
		testdata.open();

		testdata.close();
		//( Db &data, int pointA_id, int pointB_id );
	}
	void checkStructure__test() {
		Db testdata( "testdata.db" );
		testdata.open();

		testdata.close();
		//bool checkStructure( Db &data );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( getDistance__test ) );
		return s;
	}
}

#endif /* CORE_DATABASE_TABLES_JUMPCONNECTIONS_TEST_CPP_ */
