/*
 * utils_test.hpp
 *
 *  Created on: 16 Sep 2014
 *      Author: Alwlyan
 */

#ifndef UTILS_TEST_HPP_
#define UTILS_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"

#include "../src/core/database/utils.hpp"

namespace utils_Tests {
	void stringToInt__test() {
		int i { };
		std::string s = "123456789";
		ASSERTM( "Testing <stringToInt> has converted", utils::stringToInt( s, i ) == true );
		ASSERTM( "Testing converted value is correct", 123456789 == i );
	}
	void stringToDouble__test() {
		double d { };
		std::string s = "123456.123456";
		ASSERTM( "Testing <stringToDouble> has converted", utils::stringToDouble( s, d ) == true );
		ASSERTM( "Testing converted value is correct", 123456.123456 == d );
	}
	void getXYZ__test() {
		Table t { };
		Coordinate3D coords { };
		bool returned = utils::getXYZ( t, coords );
		ASSERTM( "Testing getXYZ 1", returned == false );
		t.createColumn( 3, "X_Coordinate" );
		t.createColumn( 3, "Y_Coordinate" );
		t.createColumn( 3, "Z_Coordinate" );
		t.lockTableStructure();
		t.add( 1.01 );
		t.add( 2.02 );
		t.add( 3.03 );
		returned = utils::getXYZ( t, coords );
		ASSERTM( "Testing getXYZ 2", returned == false );
		t.clear();
		t.createColumn( 2, "X_Coordinate" );
		t.createColumn( 2, "Y_Coordinate" );
		t.createColumn( 2, "Z_Coordinate" );
		t.lockTableStructure();
		t.add( 1.01 );
		t.add( 2.02 );
		t.add( 3.03 );
		returned = utils::getXYZ( t, coords );
		ASSERTM( "Testing getXYZ 3", returned == true );
		ASSERTM( "Testing getXYZ 4", coords == Coordinate3D( 1.01, 2.02, 3.03 ) );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( stringToInt__test ) );
		s.push_back( CUTE( stringToDouble__test ) );
		s.push_back( CUTE( getXYZ__test ) );
		return s;
	}
}

#endif /* UTILS_TEST_HPP_ */

