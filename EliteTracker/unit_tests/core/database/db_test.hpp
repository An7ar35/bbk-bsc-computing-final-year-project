/*
 * db_test.hpp
 *
 *  Created on: 12 Dec 2014
 *      Author: Alwlyan
 */

#ifndef DB_TEST_HPP_
#define DB_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"

#include "../src/core/containers/table.hpp"
#include "../src/core/database/db.hpp"

namespace Db_Tests {

	///*********************************************************************
	/// Database operations
	///*********************************************************************
	void sqlQueryPull_Typed__test() {
		Table t = { };
		Db test( "testdata.db" );
		int returned = test.sqlQueryPull_typed( "SELECT * FROM Typed_Table", t );
		ASSERTM( "Testing sqlQueryPull_Typed() returned rows", returned == -1 );
		test.open();

		//adding some test data into the database
		test.sqlQueryPush( "DROP TABLE IF EXISTS Typed_Table" );
		test.sqlQueryPush(
				"CREATE TABLE Typed_Table( ID INTEGER PRIMARY KEY AUTOINCREMENT, data1 INTEGER DEFAULT NULL, data2 varchar(20) NOT NULL, data3 DOUBLE DEFAULT NULL )" );
		test.sqlQueryPush(
				"INSERT INTO Typed_Table( data1, data2, data3 ) VALUES ( 10, 'Ten', 10.00001 )" );
		test.sqlQueryPush(
				"INSERT INTO Typed_Table( data1, data2, data3 ) VALUES ( 20, 'Twenty', 20.2020 )" );
		test.sqlQueryPush( "INSERT INTO Typed_Table( data1, data2 ) VALUES ( 30, 'Thirty' )" );
		test.sqlQueryPush( "INSERT INTO Typed_Table( data2 ) VALUES ( 'Forty' )" );
		test.sqlQueryPush(
				"INSERT INTO Typed_Table( data1, data2, data3 ) VALUES ( 50, 'Fifty', 50000000.00000005 )" );

		returned = test.sqlQueryPull_typed( "SELECT * FROM Typed_Table", t );
		ASSERTM( "Testing sqlQueryPull_Typed() returned rows", returned == 5 );
		ASSERTM( "Testing sqlQueryPull_Typed() # columns", t.getColumnCount() == 4 );
		ASSERTM( "Testing sqlQueryPull_Typed() # rows", t.getRowCount() == 5 );
		ASSERTM( "Testing sqlQueryPull_Typed() headings 1", t.getHeading( 0 ) == "ID" );
		ASSERTM( "Testing sqlQueryPull_Typed() headings 2", t.getHeading( 1 ) == "data1" );
		ASSERTM( "Testing sqlQueryPull_Typed() headings 3", t.getHeading( 2 ) == "data2" );
		ASSERTM( "Testing sqlQueryPull_Typed() headings 4", t.getHeading( 3 ) == "data3" );
		ASSERTM( "Testing sqlQueryPull_Typed() types 1", t.getType( 0 ) == dataType::INTEGER );
		ASSERTM( "Testing sqlQueryPull_Typed() types 2", t.getType( 1 ) == dataType::INTEGER );
		ASSERTM( "Testing sqlQueryPull_Typed() types 3", t.getType( 2 ) == dataType::STRING );
		ASSERTM( "Testing sqlQueryPull_Typed() types 4", t.getType( 3 ) == dataType::DOUBLE );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 0,0", t( 0, 0 ).getInt() == 1 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 1,0", t( 1, 0 ).getInt() == 10 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 2,0", t( 2, 0 ).getString() == "Ten" );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 3,0", t( 3, 0 ).getDouble() == 10.00001 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 0,1", t( 0, 1 ).getInt() == 2 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 1,1", t( 1, 1 ).getInt() == 20 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 2,1", t( 2, 1 ).getString() == "Twenty" );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 3,1", t( 3, 1 ).getDouble() == 20.2020 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 0,2", t( 0, 2 ).getInt() == 3 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 1,2", t( 1, 2 ).getInt() == 30 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 2,2", t( 2, 2 ).getString() == "Thirty" );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 3,2", t( 3, 2 ).getDouble() == 0 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 0,3", t( 0, 3 ).getInt() == 4 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 1,3", t( 1, 3 ).getInt() == 0 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 2,3", t( 2, 3 ).getString() == "Forty" );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 3,3", t( 3, 3 ).getDouble() == 0 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 0,4", t( 0, 4 ).getInt() == 5 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 1,4", t( 1, 4 ).getInt() == 50 );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 2,4", t( 2, 4 ).getString() == "Fifty" );
		ASSERTM( "Testing sqlQueryPull_Typed() value @ 3,4",
				t( 3, 4 ).getDouble() == 50000000.00000005 );
		test.close();
		t.printToConsole();
	}

	void sqlQueryPull__test1() {
		Db test( "testdata.db" );
		test.open();
		int returned = test.sqlQueryPull( "SELECT data1 FROM Table2 WHERE ID = 2" );
		std::vector<std::vector<std::string>> &store = test.getStoredTABLE();
		ASSERTM( "Testing sqlQueryPull() [Internal storage] returned rows", returned == 1 );
		ASSERTM( "Testing sqlQueryPull() [Internal storage]", store[ 0 ][ 0 ] == "20" );
		returned = test.sqlQueryPull( "SELECT data1 FROM Table2 WHERE ID = 999" );
		ASSERTM( "Testing sqlQueryPull() [Internal storage] returned rows", returned == 0 );
		test.close();
		returned = test.sqlQueryPull( "SELECT data1 FROM Table2 WHERE ID = 1" );
		ASSERTM( "Testing sqlQueryPull() [Internal storage] returned rows", returned == -1 );
	}
	void sqlQueryPull__test2() {
		Db test( "testdata.db" );
		TABLE storage { };
		test.open();
		int returned = test.sqlQueryPull( "SELECT data1 FROM Table2 WHERE ID = 2", storage );
		ASSERTM( "Testing sqlQueryPull() [External storage] returned rows", returned == 1 );
		ASSERTM( "Testing sqlQueryPull() [External storage]", storage[ 0 ][ 0 ] == "20" );
		returned = test.sqlQueryPull( "SELECT data1 FROM Table2 WHERE ID = 999", storage );
		ASSERTM( "Testing sqlQueryPull() [External storage] returned rows", returned == 0 );
		test.close();
		returned = test.sqlQueryPull( "SELECT data1 FROM Table2 WHERE ID = 1", storage );
		ASSERTM( "Testing sqlQueryPull() [External storage] returned rows", returned == -1 );
	}
	void sqlQueryPull_add__test() {
		Db test( "testdata.db" );
		TABLE storage { };
		int returned = test.sqlQueryPull_add( "SELECT data1 FROM Table2 WHERE ID = 2", storage );
		ASSERTM( "Testing sqlQueryPull_add() [External storage] file not opened", returned == -1 );
		test.open();
		returned = test.sqlQueryPull_add( "SELECT data1 FROM Table2 WHERE ID = 2", storage );
		ASSERTM( "Testing sqlQueryPull_add() [External storage] returned rows", returned == 1 );
		ASSERTM( "Testing sqlQueryPull_add() [External storage]", storage[ 0 ][ 0 ] == "20" );
		ASSERTM( "Testing sqlQueryPull_add() [External storage]", storage[ 0 ].size() == 1 );
		returned = test.sqlQueryPull_add( "SELECT data1 FROM Table2 WHERE ID = 1", storage );
		ASSERTM( "Testing sqlQueryPull_add() [External storage] returned rows", returned == 1 );
		ASSERTM( "Testing sqlQueryPull_add() [External storage]", storage[ 0 ][ 1 ] == "10" );
		ASSERTM( "Testing sqlQueryPull_add() [External storage]", storage[ 0 ].size() == 2 );
		returned = test.sqlQueryPull_add( "SELECT * FROM Table2", storage );
		ASSERTM( "Testing sqlQueryPull_add() [External storage] width mismatch", returned == -2 );
	}
	void sqlQueryPush__test() {
		Db test( "testdata.db" );
		test.open();
		TABLE storage { };
		int returned = test.sqlQueryPush(
				"INSERT INTO Table2( data1, data2 ) VALUES ( 60, \"Sixty\" )" );
		ASSERTM( "Testing sqlQueryPush() returned", returned == 0 );
		returned = test.sqlQueryPull( "SELECT data1 FROM Table2 WHERE data1 = 60", storage );
		if( !storage[ 0 ].empty() )
			ASSERTM( "Testing sqlQueryPush()", storage[ 0 ][ 0 ] == "60" );
		else
			FAIL();
		test.close();
		returned = test.sqlQueryPush(
				"INSERT INTO Table2( data1, data2 ) VALUES ( 80, \"Eighty\" )" );
		ASSERTM( "Testing sqlQueryPush() returned", returned == -1 );
	}
	void sqlQueryPull_single__test1() {
		Db test( "testdata.db" );
		test.open();
		int item { 0 };
		int returned = test.sqlQueryPull_single(
				"SELECT Numbers_Int FROM Table1 WHERE Strings = 'The Number Of The Beast'", item );
		ASSERTM( "Testing sqlQueryPull_Single( ..., int ) error code", returned = 1 );
		ASSERTM( "Testing sqlQueryPull_Single( ..., int )", item == 666 );
		returned = test.sqlQueryPull_single( "SELECT * FROM Table1", item );
		ASSERTM( "Testing sqlQueryPull_Single( ..., int ) error code", returned = -2 );
		ASSERTM( "Testing sqlQueryPull_Single( ..., int )", item == 666 ); //Shouldn't have changed
		test.close();
		returned = test.sqlQueryPull_single(
				"SELECT Numbers_Int FROM Table1 WHERE Strings = 'The Number Of The Beast'", item );
		ASSERTM( "Testing sqlQueryPull_Single( ..., int ) error code", returned = -1 );
	}
	void sqlQueryPull_single__test2() {
		Db test( "testdata.db" );
		test.open();
		double item = 0;
		int returned = test.sqlQueryPull_single(
				"SELECT Numbers_Double FROM Table1 WHERE Strings = 'The Number Of The Beast'",
				item );
		ASSERTM( "Testing sqlQueryPull_Single( ..., Double ) error code", returned = 1 );
		ASSERTM( "Testing sqlQueryPull_Single( ..., Double )", item == 666.666 );
		returned = test.sqlQueryPull_single( "SELECT * FROM Table1", item );
		ASSERTM( "Testing sqlQueryPull_Single( ..., Double ) error code", returned = -2 );
		ASSERTM( "Testing sqlQueryPull_Single( ..., Double )", item == 666.666 ); //Shouldn't have changed
		test.close();
		returned = test.sqlQueryPull_single(
				"SELECT Numbers_Double FROM Table1 WHERE Strings = 'The Number Of The Beast'",
				item );
		ASSERTM( "Testing sqlQueryPull_Single( ..., Double ) error code", returned = -1 );
	}
	void sqlQueryPull_single__test3() {
		Db test( "testdata.db" );
		test.open();
		std::string item = "";
		int returned = test.sqlQueryPull_single(
				"SELECT Strings FROM Table1 WHERE Numbers_Int = 666", item );
		ASSERTM( "Testing sqlQueryPull_Single( ..., std::string ) error code", returned = 1 );
		ASSERTM( "Testing sqlQueryPull_Single( ..., std::string )",
				item == "The Number Of The Beast" );
		returned = test.sqlQueryPull_single( "SELECT * FROM Table1", item );
		ASSERTM( "Testing sqlQueryPull_Single( ..., std::string ) error code", returned = -2 );
		ASSERTM( "Testing sqlQueryPull_Single( ..., std::string )",
				item == "The Number Of The Beast" ); //Shouldn't have changed
		test.close();
		returned = test.sqlQueryPull_single( "SELECT Strings FROM Table1 WHERE Numbers_Int = 666",
				item );
		ASSERTM( "Testing sqlQueryPull_Single( ..., std::string ) error code", returned = -1 );
	}
	void getStoredTABLE__test() {
		//Indirectly tested already
		//std::vector<std::vector<std::string>> &store = test.getstoredTABLE();
	}
	///*********************************************************************
	/// File operations
	///*********************************************************************
	void open__test() {
		Db test( "testdata.db" );
		test.open();
		ASSERTM( "Testing open()", test.isFileOpened() == true );
		test.close();
		test.open();
		ASSERTM( "Testing open()", test.isFileOpened() == true );
		test.close();
	}
	void close__test() {
		Db test( "testdata.db" );
		ASSERTM( "Testing close()", test.isFileOpened() == false );
		test.open();
		test.close();
		ASSERTM( "Testing close()", test.isFileOpened() == false );
	}
	void isFileOpened__test() {
		Db test( "testdata.db" );
		ASSERTM( "Testing isFileOpened()", test.isFileOpened() == false );
		test.open();
		ASSERTM( "Testing isFileOpened()", test.isFileOpened() == true );
		test.close();
		ASSERTM( "Testing isFileOpened()", test.isFileOpened() == false );
	}
	void getFileName__test() {
		Db test( "testdata.db" );
		ASSERTM( "Testing getFileName()", test.getFileName() == "testdata.db" );
	}

	///*********************************************************************
	/// Helper functions
	///*********************************************************************
	void checkTableExists__test() {
		Db test( "testdata.db" );
		test.open();
		ASSERTM( "Testing checkTableExists( 'Table1' )",
				test.checkTableExists( "Table1" ) == true );
		ASSERTM( "Testing checkTableExists( 'NonExistantTable' )",
				test.checkTableExists( "NonExistantTable" ) == false );
		test.close();
	}
	void checkColumnExists__test() {
		Db test( "testdata.db" );
		test.open();
		ASSERTM( "Testing checkColumnExists( 'Table1', 'ID' )",
				test.checkColumnExists( "Table1", "ID" ) == true );
		ASSERTM( "Testing checkColumnExists( 'Table1', 'Numbers_Int' )",
				test.checkColumnExists( "Table1", "Numbers_Int" ) == true );
		ASSERTM( "Testing checkColumnExists( 'Table1', 'Numbers_Double' )",
				test.checkColumnExists( "Table1", "Numbers_Double" ) == true );
		ASSERTM( "Testing checkColumnExists( 'Table1', 'Strings' )",
				test.checkColumnExists( "Table1", "Strings" ) == true );
		ASSERTM( "Testing checkColumnExists( 'Table1', 'NonExistantCol' )",
				test.checkColumnExists( "Table1", "NonExistantCol" ) == false );
		ASSERTM( "Testing checkColumnExists( 'NonExistantTable', 'ID' )",
				test.checkColumnExists( "NonExistantTable", "ID" ) == false );
		ASSERTM( "Testing checkColumnExists( 'NonExistantTable', 'NonExistantCol' )",
				test.checkColumnExists( "NonExistantTable", "NonExistantCol" ) == false );
		test.close();
	}
	void checkEntryExists__test1() {
		Db test( "testdata.db" );
		test.open();
		int returned = test.checkEntryExists( "Table1", "Strings", "The Number Of The Beast" );
		ASSERTM( "Testing checkEntryExists( 'Table1', 'Strings', 'The Number Of The Beast' )",
				returned == 1 );
		returned = test.checkEntryExists( "Table1", "Strings", "NonExistantEntry" );
		ASSERTM( "Testing checkEntryExists( 'Table1', 'Strings', 'NonExistantEntry' )",
				returned == 0 );
		returned = test.checkEntryExists( "Table1", "NonExistantCol", "The Number Of The Beast" );
		ASSERTM(
				"Testing checkEntryExists( 'Table1', 'NonExistantCol', 'The Number Of The Beast' )",
				returned == 0 );
		returned = test.checkEntryExists( "NonExistantTable", "Strings",
				"The Number Of The Beast" );
		ASSERTM(
				"Testing checkEntryExists( 'NonExistantTable', 'Strings', 'The Number Of The Beast' )",
				returned == 0 );
		test.close();
	}
	void checkEntryExists__test2() {
		Db test( "testdata.db" );
		test.open();
		int returned = test.checkEntryExists( "Table1", "Numbers_Int", "666", "Strings",
				"The Number Of The Beast" );
		ASSERTM(
				"Testing checkEntryExists( 'Table1', 'Numbers_Int', '666', 'Strings', 'The Number Of The Beast' )",
				returned == 1 );
		returned = test.checkEntryExists( "Table1", "Numbers_Int", "666", "Strings",
				"NonExistantEntry" );
		ASSERTM(
				"Testing checkEntryExists( 'Table1', 'Numbers_Int', '666', 'Strings', 'NonExistantEntry' )",
				returned == 0 );
		returned = test.checkEntryExists( "NonExistantTable", "Numbers_Int", "666", "Strings",
				"The Number Of The Beast" );
		ASSERTM(
				"Testing checkEntryExists( 'NonExistantTable', 'Numbers_Int', '666', 'Strings', 'The Number Of The Beast' )",
				returned == 0 );
		test.close();
	}
	void getDataAt__test() {
		Db test( "testdata.db" );
		test.open();
		test.sqlQueryPull( "SELECT * FROM Table1" );
		std::string returned = test.getDataAt( 3, 100 );
		ASSERTM( "Testing getDataAt()", returned == "The Number Of The Beast" );
		returned = test.getDataAt( 2, 100 );
		ASSERTM( "Testing getDataAt()", returned == "666.666" );
		returned = test.getDataAt( 100, 100 );
		ASSERTM( "Testing getDataAt() Bad coordinates cushioned", returned == "" );
		returned = test.getDataAt( -1, -1 );
		ASSERTM( "Testing getDataAt() Bad coordinates cushioned", returned == "" );
		test.close();
	}
	void getColName__test1() {
		Db test( "testdata.db" );
		test.open();
		ASSERTM( "Testing getColName( 0, 'Table1' )", test.getColName( 0, "Table1" ) == "ID" );
		ASSERTM( "Testing getColName( 3, 'Table1' )", test.getColName( 3, "Table1" ) == "Strings" );
		ASSERTM( "Testing getColName( NonExistantColNumber, 'Table1' )",
				test.getColName( 10, "Table1" ) == "" );
		ASSERTM( "Testing getColName( 1, 'NonExistantTable' )",
				test.getColName( 1, "NonExistantTable" ) == "" );
		ASSERTM( "Testing getColName( NonExistantColNumber, 'NonExistantTable' )",
				test.getColName( 100, "NonExistantTable" ) == "" );
		test.close();
	}
	void getColName__test2() {
		Db test( "testdata.db" );
		test.open();
		test.sqlQueryPull( "SELECT * FROM Table1" );
		ASSERTM( "Testing getColName( 0 )", test.getColName( 0, "Table1" ) == "ID" );
		ASSERTM( "Testing getColName( 3 )", test.getColName( 3, "Table1" ) == "Strings" );
		ASSERTM( "Testing getColName( NonExistantColNumber )",
				test.getColName( 10, "Table1" ) == "" );
		test.close();
	}
	void getColType_test() {
		Db test( "testdata.db" );
		test.open();
		ASSERTM( "Testing getColType( 0, 'Table1' )", test.getColType( 0, "Table1" ) == "INTEGER" );
		ASSERTM( "Testing getColType( 1, 'Table1' )", test.getColType( 1, "Table1" ) == "INTEGER" );
		ASSERTM( "Testing getColType( 2, 'Table1' )", test.getColType( 2, "Table1" ) == "DOUBLE" );
		ASSERTM( "Testing getColType( 3, 'Table1' )",
				test.getColType( 3, "Table1" ) == "varchar(20)" );
		ASSERTM( "Testing getColType( NonExistantColNumber, 'Table1' )",
				test.getColType( 10, "Table1" ) == "" );
		ASSERTM( "Testing getColType( 0, 'NonExistantTable' )",
				test.getColType( 0, "NonExistantTable" ) == "" );
		ASSERTM( "Testing getColType( NonExistantColNumber, 'NonExistantTable' )",
				test.getColType( 10, "NonExistantTable" ) == "" );
		test.close();
	}
	void getColNumber__test() {
		Db test( "testdata.db" );
		test.open();
		ASSERTM( "Testing getColNumber( 'ID', 'Table1' )",
				test.getColNumber( "ID", "Table1" ) == 0 );
		ASSERTM( "Testing getColNumber( 'Strings', 'Table1' )",
				test.getColNumber( "Strings", "Table1" ) == 3 );
		ASSERTM( "Testing getColNumber( 'NonExistantCol', 'Table1' )",
				test.getColNumber( "NonExistantCol", "Table1" ) == -1 );
		ASSERTM( "Testing getColNumber( 'ID', 'NonExistantTable' )",
				test.getColNumber( "ID", "NonExistantTable" ) == -1 );
		ASSERTM( "Testing getColNumber( 'NonExistantCol', 'NonExistantTable' )",
				test.getColNumber( "NonExistantCol", "NonExistantTable" ) == -1 );
		test.close();
	}
	void getFullRowData__test() {
		Db test( "testdata.db" );
		test.open();
		TABLE storage { };
		bool returned = test.getFullRowData( 1, "Table2", storage );
		ASSERTM( "Testing getFullRowData( 1, 'Table2' )", returned == true );
		if( returned ) {
			ASSERTM( "Testing getFullRowData( 1, 'Table2' ) data", storage[ 0 ][ 0 ] == "1" );
			ASSERTM( "Testing getFullRowData( 1, 'Table2' ) data", storage[ 1 ][ 0 ] == "10" );
			ASSERTM( "Testing getFullRowData( 1, 'Table2' ) data", storage[ 2 ][ 0 ] == "Ten" );
		}
		returned = test.getFullRowData( 999, "Table2", storage );
		ASSERTM( "Testing getFullRowData( 1, 'Table2' )", returned == false );
		test.close();
	}
	void getRowRelation__test() {
		Db test( "testdata.db" );
		test.open();
		std::string item = "";
		bool returned = test.getRowRelation( "666", "Numbers_Int", "Table1", item, "Strings" );
		ASSERTM( "Testing getRowRelation( '666', 'Numbers_Int', 'Table1', ..., 'Strings' ) success",
				returned == true );
		ASSERTM( "Testing getRowRelation( '666', 'Numbers_Int', 'Table1', ..., 'Strings' ) value",
				item == "The Number Of The Beast" );
		item = "";
		returned = test.getRowRelation( "666", "Numbers_Int", "NonExistantTable", item, "Strings" );
		ASSERTM(
				"Testing getRowRelation( '666', 'Numbers_Int', 'NonExistantTable', ..., 'Strings' ) success",
				returned == false );
		ASSERTM(
				"Testing getRowRelation( '666', 'Numbers_Int', 'NonExistantTable', ..., 'Strings' ) value",
				item == "" );
		returned = test.getRowRelation( "666", "NonExistantCol", "Table1", item, "Strings" );
		ASSERTM(
				"Testing getRowRelation( '666', 'NonExistantCol', 'Table1', ..., 'Strings' ) success",
				returned == false );
		ASSERTM(
				"Testing getRowRelation( '666', 'NonExistantCol', 'Table1', ..., 'Strings' ) value",
				item == "" );
		returned = test.getRowRelation( "666", "Numbers_Int", "Table1", item, "NonExistantCol" );
		ASSERTM(
				"Testing getRowRelation( '666', 'Numbers_Int', 'Table1', ..., 'NonExistantCol' ) success",
				returned == false );
		ASSERTM(
				"Testing getRowRelation( '666', 'Numbers_Int', 'Table1', ..., 'NonExistantCol' ) value",
				item == "" );
		returned = test.getRowRelation( "NonExistantValue", "Numbers_Int", "Table1", item,
				"Strings" );
		ASSERTM(
				"Testing getRowRelation( 'NonExistantValue', 'Numbers_Int', 'Table1', ..., 'Strings' ) success",
				returned == false );
		ASSERTM(
				"Testing getRowRelation( 'NonExistantValue', 'Numbers_Int', 'Table1', ..., 'Strings' ) value",
				item == "" );
		returned = test.getRowRelation( "666", "Numbers_Int", "Table1", item, "Numbers_Double" );
		ASSERTM(
				"Testing getRowRelation( '666', 'Numbers_Int', 'Table1', ..., 'Numbers_Double' ) success",
				returned == true );
		ASSERTM(
				"Testing getRowRelation( '666', 'Numbers_Int', 'Table1', ..., 'Numbers_Double' ) value",
				item == "666.666" );
		test.close();
	}
	void getNumberOfRows__test() {
		Db test( "testdata.db" );
		test.open();
		ASSERTM( "Testing getNumberOfRows(..) 1", test.getNumberOfRows( "Table2" ) == 3 );
		ASSERTM( "Testing getNumberOfRows(..) 2", test.getNumberOfRows( "Table3" ) == 0 );
		test.close();
	}

	cute::suite make_suite() {
		cute::suite db_tests { };
		db_tests.push_back( CUTE( sqlQueryPull_Typed__test ) );
		db_tests.push_back( CUTE( sqlQueryPull__test1 ) );
		db_tests.push_back( CUTE( sqlQueryPull__test2 ) );
		db_tests.push_back( CUTE( sqlQueryPull_add__test ) );
		db_tests.push_back( CUTE( sqlQueryPush__test ) );
		db_tests.push_back( CUTE( sqlQueryPull_single__test1 ) );
		db_tests.push_back( CUTE( sqlQueryPull_single__test2 ) );
		db_tests.push_back( CUTE( sqlQueryPull_single__test3 ) );
		db_tests.push_back( CUTE( open__test ) );
		db_tests.push_back( CUTE( close__test ) );
		db_tests.push_back( CUTE( isFileOpened__test ) );
		db_tests.push_back( CUTE( getFileName__test ) );
		db_tests.push_back( CUTE( checkTableExists__test ) );
		db_tests.push_back( CUTE( checkColumnExists__test ) );
		db_tests.push_back( CUTE( checkEntryExists__test1 ) );
		db_tests.push_back( CUTE( checkEntryExists__test2 ) );
		db_tests.push_back( CUTE( getDataAt__test ) );
		db_tests.push_back( CUTE( getColName__test1 ) );
		db_tests.push_back( CUTE( getColName__test2 ) );
		db_tests.push_back( CUTE( getColType_test ) );
		db_tests.push_back( CUTE( getColNumber__test ) );
		db_tests.push_back( CUTE( getFullRowData__test ) );
		db_tests.push_back( CUTE( getRowRelation__test ) );
		db_tests.push_back( CUTE( getNumberOfRows__test ) );

		return db_tests;
	}
}
#endif /* DB_TEST_HPP_ */
