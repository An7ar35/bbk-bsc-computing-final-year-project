/*
 * Convert_test.hpp
 *
 *  Created on: 27 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_FILEOPS_CONVERT_TEST_HPP_
#define CORE_FILEOPS_CONVERT_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/core/fileops/Convert.hpp"

namespace Convert_Tests {
	void to_string__test() {
		ASSERTM( "Testing to_string(..) int", "666" == Convert::to_string<int>( 666 ) );
		ASSERTM( "Testing to_string(..) big int", "2147483647" == Convert::to_string<int>( 2147483647 ) );
		ASSERTM( "Testing to_string(..) double",
				"123.456" == Convert::to_string<long double>( 123.456 ) );
		ASSERTM( "Testing to_string(..) big double",
				"123.4567891012" == Convert::to_string<double>( 123.4567891012 ) );
		ASSERTM( "Testing to_string(..) big long double",
				"123.4567891012" == Convert::to_string<long double>( 123.4567891012 ) );
		ASSERTM( "Testing to_string(..) std::string",
				"Hello world" == Convert::to_string<std::string>( "Hello world" ) );
	}
	void string_to_type__test() {
		std::string s1 = "123456.123456";
		ASSERTM( "Testing string_to_type(..) double",
				123456.123456 == Convert::string_to_type<double>( s1 ) );
		std::string s2 = "666";
		ASSERTM( "Testing string_to_type(..) int", 666 == Convert::string_to_type<int>( s2 ) );
		std::string s3 = "Fail test 123456";
		ASSERTM( "Testing string_to_type(..) failed int",
				int() == Convert::string_to_type<int>( s3 ) );
		std::string s4 = "Hello world";
		ASSERTM( "Testing string_to_type(..) std::string",
				s4 == Convert::string_to_type<std::string>( s4 ) );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( to_string__test ) );
		s.push_back( CUTE( string_to_type__test ) );
		return s;
	}
}

#endif /* CORE_FILEOPS_CONVERT_TEST_HPP_ */
