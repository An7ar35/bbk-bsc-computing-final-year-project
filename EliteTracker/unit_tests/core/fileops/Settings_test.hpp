/*
 * Settings_test.hpp
 *
 *  Created on: 31 Mar 2015
 *      Author: Alwlyan
 */

#ifndef CORE_FILEOPS_SETTINGS_TEST_HPP_
#define CORE_FILEOPS_SETTINGS_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>
#include "../../equalish.hpp"

#include "../../../src/core/fileops/Settings.hpp"

namespace Settings_Tests {
	void readFile__test() {
		fileIO::Settings setting { "test_settings.cfg" };
		ASSERTM( "Testing readFile", setting.getValueOfKey<int>( "CurrentPlayerShip", 0 ) == 2 );
	}
	void writeFile__test() {
		fileIO::Settings setting { "test_settings.cfg" };
		setting.writeConfig<std::string>( "Test1", "string_test" );
		setting.writeConfig<double>( "Test2", 1.0025 );
		setting.writeConfig<int>( "Test3", 666 );
		ASSERTM( "Testing writeConfig 1", setting.getValueOfKey<int>( "Test3", 0 ) == 666 );
		ASSERTM( "Testing writeConfig 2", setting.getValueOfKey<double>( "Test2", 0.0 ) == 1.0025 );
		ASSERTM( "Testing writeConfig 3",
				setting.getValueOfKey<std::string>( "Test1", "fail" ) == "string_test" );
		setting.writeFile();
		fileIO::Settings new_setting { "test_settings.cfg" };
		ASSERTM( "Testing getValueOfKey 1",
				setting.getValueOfKey<int>( "CurrentPlayerShip", 0 ) == 2 );
		ASSERTM( "Testing getValueOfKey 2",
				setting.getValueOfKey<double>( "Test2", 0.0 ) == 1.0025 );
		ASSERTM( "Testing getValueOfKey 3", setting.getValueOfKey<int>( "Test3", 0 ) == 666 );
		ASSERTM( "Testing getValueOfKey 4",
				setting.getValueOfKey<std::string>( "Test1", "fail" ) == "string_test" );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( readFile__test ) );
		s.push_back( CUTE( writeFile__test ) );
		return s;
	}
}

#endif /* CORE_FILEOPS_SETTINGS_TEST_HPP_ */
