/*
 * fileops_utils_test.hpp
 *
 *  Created on: 20 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_FILEOPS_FILEOPS_UTILS_TEST_HPP_
#define CORE_FILEOPS_FILEOPS_UTILS_TEST_HPP_

#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"
#include <chrono>

#include "../../../src/core/fileops/fileops_utils.hpp"

namespace FileOps_Utils_Tests {
	void getNumberOfLines__test() {
		std::string file1 = "test_file1.txt";
		int lines1 = fileIO::getNumberOfLines( file1 );
		ASSERTM( "Testing getNumberOfLines(..) 1", lines1 == 100 );
		std::string file2 = "test_file2.txt";
		int lines2 = fileIO::getNumberOfLines( file2 );
		ASSERTM( "Testing getNumberOfLines(..) 2", lines2 == 0 );
	}

	cute::suite make_suite() {
		cute::suite s { };
		s.push_back( CUTE( getNumberOfLines__test ) );
		return s;
	}
}

#endif /* CORE_FILEOPS_FILEOPS_UTILS_TEST_HPP_ */
