#include "cute.h"
#include "ide_listener.h"
#include "xml_listener.h"
#include "cute_runner.h"

#include "testdata/createtestdata.hpp"
#include "../src/core/logger/log.hpp"

/** Database **/
#include "core/database/db_test.hpp"
#include "core/database/utils_test.hpp"
/** Log **/
#include "core/logger/Log_test.hpp"
#include "core/logger/Logmsg_test.hpp"
/** Containers **/
#include "core/containers/Item_test.hpp"
#include "core/containers/Row_test.hpp"
#include "core/containers/Column_test.hpp"
#include "core/containers/Coordinate2D_test.hpp"
#include "core/containers/Coordinate3D_test.hpp"
#include "core/containers/Table_test.hpp"
/** Toolbox **/
#include "core/toolbox/toolbox_test.hpp"
#include "core/toolbox/math/spatial_test.hpp"
#include "core/toolbox/math/trigonometry_test.hpp"
/** File ops **/
#include "core/fileops/Convert_test.hpp"
#include "core/fileops/fileops_utils_test.hpp"
#include "core/fileops/Settings_test.hpp"
/** Satellites **/
#include "satellites/pathfinder/Matrix_test.hpp"
#include "satellites/pathfinder/Node_test.hpp"
#include "satellites/pathfinder/ShortestPath_test.hpp"
#include "satellites/pathfinder/ShortestSetPath_test.hpp"
#include "satellites/pathfinder/Pathfinder_test.hpp"
#include "satellites/coordinate/Circle_test.hpp"
#include "satellites/coordinate/coordinate_test.hpp"
#include "satellites/coordinate/coordinate_tool_test.hpp"
/** Stress test **/
#include "stress_tests.hpp"

void runAllTests( int argc, char const *argv[] ) {
	//DATABASE
	cute::suite db_tests = Db_Tests::make_suite();
	cute::suite utils_tests = utils_Tests::make_suite();
	//LOG
	cute::suite log_tests = Log_Tests::make_suite();
	cute::suite logmsg_tests = Logmsg_Tests::make_suite();
	//CONTAINERS
	cute::suite Coordinate2D_tests = Coordinate2D_Tests::make_suite();
	cute::suite Coordinate3D_tests = Coordinate3D_Tests::make_suite();
	cute::suite Item_tests = Item_Tests::make_suite();
	cute::suite Column_tests = Column_Tests::make_suite();
	cute::suite Row_tests = Row_Tests::make_suite();
	cute::suite Table_tests = Table_Tests::make_suite();
	//TOOLBOX
	cute::suite Toolbox_tests = Toolbox_Tests::make_suite();
	cute::suite trigonometry_tests = trigonometry_Tests::make_suite();
	cute::suite Spatial_tests = Spatial_Tests::make_suite();
	//FILE OPS
	cute::suite Convert_tests = Convert_Tests::make_suite();
	cute::suite Fileops_utils_tests = FileOps_Utils_Tests::make_suite();
	cute::suite Settings_tests = Settings_Tests::make_suite();
	//SATELLITES
	cute::suite Matrix_tests = Matrix_Tests::make_suite();
	cute::suite Node_tests = Node_Tests::make_suite();
	cute::suite ShortestPath_tests = ShortestPath_Tests::make_suite();
	cute::suite ShortestSetPath_tests = ShortestSetPath_Tests::make_suite();
	cute::suite Pathfinder_tests = Pathfinder_Tests::make_suite();
	cute::suite Circle_tests = Circle_Tests::make_suite();
	cute::suite Coordinate_tests = Coordinate_Tests::make_suite();
	cute::suite Coordinate_tool_tests = Coordinate_tool_Tests::make_suite();
	//Sress Tests
	cute::suite stress_tests = Stress_Tests::make_suite();

	cute::xml_file_opener xmlfile( argc, argv );
	cute::xml_listener<cute::ide_listener<> > lis( xmlfile.out );
	cute::makeRunner( lis, argc, argv )( db_tests, "Db" );
	cute::makeRunner( lis, argc, argv )( logmsg_tests, "Logmsg" );
	cute::makeRunner( lis, argc, argv )( log_tests, "Log" );
	cute::makeRunner( lis, argc, argv )( Coordinate2D_tests, "Coordinate2D" );
	cute::makeRunner( lis, argc, argv )( Coordinate3D_tests, "Coordinate3D" );
	cute::makeRunner( lis, argc, argv )( Item_tests, "Item" );
	cute::makeRunner( lis, argc, argv )( Column_tests, "Column" );
	cute::makeRunner( lis, argc, argv )( Row_tests, "Row" );
	cute::makeRunner( lis, argc, argv )( Table_tests, "Table" );
	cute::makeRunner( lis, argc, argv )( Toolbox_tests, "Core Tools" );
	cute::makeRunner( lis, argc, argv )( trigonometry_tests, "CoreTools::math::trigonometry" );
	cute::makeRunner( lis, argc, argv )( Spatial_tests, "CoreTools::math::spatial" );
	cute::makeRunner( lis, argc, argv )( Convert_tests, "Convert" );
	cute::makeRunner( lis, argc, argv )( Fileops_utils_tests, "File Ops Utils" );
	cute::makeRunner( lis, argc, argv )( Settings_tests, "Settings" );
	cute::makeRunner( lis, argc, argv )( Matrix_tests, "Matrix" );
	cute::makeRunner( lis, argc, argv )( Node_tests, "Node" );
	cute::makeRunner( lis, argc, argv )( ShortestPath_tests, "ShortestPath" );
	cute::makeRunner( lis, argc, argv )( ShortestSetPath_tests, "ShortestSetPath" );
	cute::makeRunner( lis, argc, argv )( Pathfinder_tests, "Pathfinder" );
	cute::makeRunner( lis, argc, argv )( Circle_tests, "Pathfinder" );
	cute::makeRunner( lis, argc, argv )( Coordinate_tests, "Coordinate" );
	cute::makeRunner( lis, argc, argv )( Coordinate_tool_tests, "Coordinate_tool" );
	cute::makeRunner( lis, argc, argv )( utils_tests, "utils" );
	cute::makeRunner( lis, argc, argv )( stress_tests, "stress tests" );
}

int main( int argc, char const *argv[] ) {
	Log *log = Log::getInstance();
	log->switch_EventType( EventType::Trace, false );
	log->switch_Location( Location::SQLite, false );
	log->setFileName( "TestLog.txt" );
	log->switch_autoFileDump( true );
	TestData td = TestData();
	runAllTests( argc, argv );
	return 0;
}

