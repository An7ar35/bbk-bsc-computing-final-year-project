/*
 * createtestdata.cpp
 *
 *  Created on: 31 Dec 2014
 *      Author: Alwlyan
 */

#include "createtestdata.hpp"

TestData::TestData() {
	srand( time( NULL ) );
	this->generateTestDB();
}
TestData::~TestData() {
	/*
	 Log *log = Log::getInstance();
	 log->dumpToFile( "TestDataDB.txt" );
	 */
}
/**
 * Root call to generation of file and data
 */
void TestData::generateTestDB() {
	Db testdata( "testdata.db" );
	if( testdata.open() ) {
		this->generateTables( testdata );
	}
}
/**
 * Table generation method
 * @param testdata File name
 */
void TestData::generateTables( Db &testdata ) {
	//Table1
	testdata.sqlQueryPush( "DROP TABLE IF EXISTS Table1" );
	testdata.sqlQueryPush(
			"CREATE TABLE Table1( ID INTEGER PRIMARY KEY AUTOINCREMENT, Numbers_Int INTEGER DEFAULT NULL, Numbers_Double DOUBLE DEFAULT NULL, Strings varchar(20) NOT NULL )" );
	for( int i = 0; i < 100; i++ ) {
		std::stringstream ss;
		int someLength = this->randomInt( 2, 20 );
		ss << "INSERT INTO Table1( Numbers_Int, Numbers_Double, Strings ) VALUES ( "
				<< this->randomInt( 0, 500000 ) << "," << this->randomDouble( -500.0, 500.0 )
				<< ",'" << this->randomString( someLength ) << "' )";
		testdata.sqlQueryPush( ss.str() );
	}
	testdata.sqlQueryPush( "INSERT INTO Table1( Numbers_Int, Numbers_Double, Strings ) VALUES ( 666, 666.666, 'The Number Of The Beast' )" );
	//Table2
	testdata.sqlQueryPush( "DROP TABLE IF EXISTS Table2" );
	testdata.sqlQueryPush(
			"CREATE TABLE Table2( ID INTEGER PRIMARY KEY AUTOINCREMENT, data1 INTEGER DEFAULT NULL, data2 varchar(20) NOT NULL )" );
	testdata.sqlQueryPush( "INSERT INTO Table2( data1, data2 ) VALUES ( 10, 'Ten' )" );
	testdata.sqlQueryPush( "INSERT INTO Table2( data1, data2 ) VALUES ( 20, 'Twenty' )" );
	//Table3 (empty)
	testdata.sqlQueryPush( "DROP TABLE IF EXISTS Table3" );
	testdata.sqlQueryPush(
			"CREATE TABLE Table3( ID INTEGER PRIMARY KEY AUTOINCREMENT, data1 INTEGER DEFAULT NULL, data2 varchar(20) NOT NULL )" );
	//TableTest
	testdata.sqlQueryPush( "DROP TABLE IF EXISTS TableTest" );
	testdata.sqlQueryPush( "CREATE TABLE TableTest( ID INTEGER PRIMARY KEY AUTOINCREMENT, data1 INTEGER DEFAULT NULL, data2 varchar(20) NOT NULL )" );
	testdata.sqlQueryPush( "INSERT INTO TableTest( data1, data2 ) VALUES ( 10, 'Ten' )" );
	testdata.sqlQueryPush( "INSERT INTO TableTest( data1, data2 ) VALUES ( 20, 'Twenty' )" );
	testdata.sqlQueryPush( "INSERT INTO TableTest( data1, data2 ) VALUES ( 30, 'Thirty' )" );
	testdata.sqlQueryPush( "INSERT INTO TableTest( data1, data2 ) VALUES ( 40, 'Forty' )" );
	testdata.sqlQueryPush( "INSERT INTO TableTest( data1, data2 ) VALUES ( 50, 'Fifty' )" );
	//Stress test (10 million rows x 3 columns)
	/*
	//testdata.sqlQueryPush( "DROP TABLE IF EXISTS StressTest" );
	testdata.sqlQueryPush(
			"CREATE TABLE StressTest( ID INTEGER PRIMARY KEY AUTOINCREMENT, Numbers_Int INTEGER DEFAULT NULL, Numbers_Double DOUBLE DEFAULT NULL, Strings varchar(20) NOT NULL )" );
	for( int i = 0; i < 1000000; i++ ) {
		std::stringstream ss { };
		int someLength = this->randomInt( 2, 20 );
		ss << "INSERT INTO StressTest( Numbers_Int, Numbers_Double, Strings ) VALUES ( "
				<< this->randomInt( 0, 500000 ) << "," << this->randomDouble( -500.0, 500.0 )
				<< ",'" << this->randomString( someLength ) << "' )";
		testdata.sqlQueryPush( ss.str() );
	}
	*/

}
/**
 * Random generator of type 'Int'
 * @param min Minimum value of range
 * @param max Maximum value of range
 * @return The randomly generated integer
 */
int TestData::randomInt( int min, int max ) {
	return rand() % ( max - min ) + min;
}
/**
 * Random generator of type 'Double'
 * @param min Minimum value of range
 * @param max Maximum value of range
 * @return The randomly generated double
 */
double TestData::randomDouble( double min, double max ) {
	double f = (double) rand() / RAND_MAX;
	return min + f * ( max - min );
}
/**
 * Random generator of type 'String'
 * @param minLength Minimum size of the string
 * @param maxLength Maximum size of the string
 * @return The randomly generated string
 */
std::string TestData::randomString( int length ) {
	std::string builder = "";
	int alphaNum_length = sizeof( alphanum ) - 1;
	for( int i = 0; i < length; i++ ) {
		builder += alphanum[ this->randomInt( 0, alphaNum_length ) ];
	}
	return builder;
}

