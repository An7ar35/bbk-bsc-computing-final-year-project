/*
 * createtestdata.hpp
 *
 *  Created on: 30 Dec 2014
 *      Author: Alwlyan
 */

#ifndef CREATETESTDATA_HPP_
#define CREATETESTDATA_HPP_

#include <iostream>
#include <sstream>
#include <stdlib.h>
#include <time.h>
#include "../../src/core/database/db.hpp"
#include "../../src/core/logger/log.hpp"

static const char alphanum[] = "0123456789" "!@#$%^&*" "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";

/**
 * Creates db file of randomly created data for testing
 * Uses db.*, log.*
 */
class TestData {
	public:
		TestData();
		~TestData();
		void generateTestDB();
	private:
		void generateTables( Db &testdata );
		int randomInt( int min, int max );
		double randomDouble( double min, double max );
		std::string randomString( int length );
};

#endif /* CREATETESTDATA_HPP_ */
