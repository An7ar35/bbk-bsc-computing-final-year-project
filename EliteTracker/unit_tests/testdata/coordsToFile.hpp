/*
 * coordsToFile.hpp
 *
 *  Created on: 18 Apr 2015
 *      Author: Alwlyan
 */

#ifndef TESTDATA_COORDSTOFILE_HPP_
#define TESTDATA_COORDSTOFILE_HPP_

#include "../../src/core/containers/Coordinate3D.hpp"

class CoordToFile {
	public:
		CoordToFile( std::string filename ) {
			this->counter = 0;
			this->filename = filename;
		}
		~CoordToFile() {}

		void add( Coordinate3D &coord, std::string name_prefix ) {
			std::ofstream file { };
			file.open( this->filename.c_str(), std::ios::out | std::ios::ate | std::ios::app );
			file << name_prefix << counter << "=" << coord << "\n";
			file.close();
		}

	private:
		int counter;
		std::string filename;
};

#endif /* TESTDATA_COORDSTOFILE_HPP_ */
