/*
 * randompool.cpp
 *
 *  Created on: 31 Dec 2014
 *      Author: Alwlyan
 */

#include "randompool.hpp"

bool RandomPool::instanceFlag { false };
RandomPool *RandomPool::randompool = NULL;

/**
 * Log constructor
 */
RandomPool::RandomPool() {

}
/**
 * RandomPool destructor
 */
RandomPool::~RandomPool() {
	this->instanceFlag = false;
}

/**
 * Get the instance of the RandomPool singleton class
 * @return
 */
RandomPool* RandomPool::getInstance() {
	if( !instanceFlag ) {
		randompool = new RandomPool { };
		instanceFlag = true;
		return randompool;
	} else {
		return randompool;
	}
}

/**
 * Random generator of type 'Int'
 * @param min Minimum value of range
 * @param max Maximum value of range
 * @return The randomly generated integer
 */
int RandomPool::randomInt( int min, int max ) {
	std::mt19937 gen( ( std::random_device() )() );
	std::uniform_int_distribution<> dist( min, max );
	return dist( gen );
}
/**
 * Random generator of type 'Double'
 * @param min Minimum value of range
 * @param max Maximum value of range
 * @return The randomly generated double
 */
int RandomPool::randomDouble( double min, double max ) {
	std::mt19937 eng( ( std::random_device() )() );
	std::uniform_real_distribution<double> dist( min, max );
	return dist( eng );
}
/**
 * Random generator of type 'String'
 * @param minLength Minimum size of the string
 * @param maxLength Maximum size of the string
 * @return The randomly generated string
 */
std::string RandomPool::randomString( int length ) {
	std::string builder = "";
	int alphaNum_length = sizeof( alphanum ) - 1;
	for( int i = 0; i < length; i++ ) {
		builder += alphanum[ this->randomInt( 0, alphaNum_length ) ];
	}
	return builder;
}
