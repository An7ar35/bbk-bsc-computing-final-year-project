/*
 * randompool.hpp
 *
 *  Created on: 31 Dec 2014
 *      Author: Alwlyan
 */

#ifndef RANDOMPOOL_HPP_
#define RANDOMPOOL_HPP_

#include <iostream>
#include <random>

static const char alphanum[] = "0123456789" "!@#$%^&*" "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
		"abcdefghijklmnopqrstuvwxyz";

class RandomPool {
	public:
		~RandomPool();
		static RandomPool* getInstance();
		int randomInt( int min, int max );
		int randomDouble( double min, double max );
		std::string randomString( int length );

	private:
		RandomPool();
		static bool instanceFlag;
		static RandomPool *randompool;
};



#endif /* RANDOMPOOL_HPP_ */
