/*
 * equalish.hpp
 *
 *  Created on: 22 Mar 2015
 *      Author: Alwlyan
 */

#ifndef EQUALISH_HPP_
#define EQUALISH_HPP_

#include <iostream>
#include <iomanip>
#include <cmath>
#include <limits>

bool equalish( double a, double b, int precision ) {
	std::stringstream str1 { };
	std::stringstream str2 { };
	str1 << std::fixed << std::setprecision( precision ) << a;
	str2 << std::fixed << std::setprecision( precision ) << b;
	std::string s1 = str1.str();
	std::string s2 = str2.str();
	return s1 == s2;
}

bool equalish( double to_check, double reference, double error_margin ) {
	double top { reference + ( 0.5 * error_margin ) };
	double bottom { reference - ( 0.5 * error_margin ) };
	//std::cout << bottom << " <= " << to_check << " <= " << top << std::endl;
	return ( to_check <= top && to_check >= bottom );
}

#endif /* EQUALISH_HPP_ */
