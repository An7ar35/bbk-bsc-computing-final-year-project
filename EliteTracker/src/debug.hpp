/*
 * debug.hpp
 *
 *  Created on: 31 Aug 2014
 *      Author: Alwlyan
 */

#ifndef DEBUG_HPP_
#define DEBUG_HPP_
#define DEBUG( msg ) \
	std::cout << "DEBUG: [ " << __FILE__ << "(" << __LINE__ << ") ] \"" \
	<< __func__ << "\" " << #msg << std::endl;
#else
#define DEBUG( msg )
#endif /* DEBUG_HPP_ */

/*
 * Usage - insert in places you want messages to appear:
 * #ifdef DEBUG
 * 		DEBUG( message you want to see )
 * #endif
 */

/*
 * 31/08/2014
 * sourced from
 * http://latedev.wordpress.com/2012/08/09/c-debug-macros/
 * http://www.tutorialspoint.com/cplusplus/cpp_preprocessor.htm
 *
 */
