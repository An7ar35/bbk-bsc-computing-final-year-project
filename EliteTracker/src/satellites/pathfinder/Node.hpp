/*
 * Node.hpp
 *
 *  Created on: 10 Feb 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_PATHFINDER_NODE_HPP_
#define SATELLITES_PATHFINDER_NODE_HPP_

struct Node {
		/**
		 * Node information container for SP
		 * @param id JumpSystems S_ID
		 * @param from_id Node of origin for the distance specified
		 * @param distance Total distance from the SP's start point
		 */
		Node( int id, int from, double distance ) {
			this->id = id;
			this->from_id = from;
			this->accumulated_distance = distance;
			//this->shortest_path = false; //TODO does it need that?
		}
		int id;
		int from_id;
		double accumulated_distance;
		//bool shortest_path;
};

#endif /* SATELLITES_PATHFINDER_NODE_HPP_ */
