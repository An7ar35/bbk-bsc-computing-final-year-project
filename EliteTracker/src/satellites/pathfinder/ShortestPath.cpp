/*
 * ShortestPath.cpp
 *
 *  Created on: 11 Feb 2015
 *      Author: Es A. Davison
 */

#include "ShortestPath.hpp"

const double max_distance = std::numeric_limits<double>::infinity();

///*********************************************************************
/// Public methods
///*********************************************************************
/**
 * Constructor
 * @param origin Point of origin in the matrix
 * @param destination Point of destination in the matrix
 * @param distances Matrix of point distances
 */
ShortestPath::ShortestPath( int origin, int destination, Matrix<double> &distances ) {
	this->startPoint = origin;
	this->endPoint = destination;
	this->matrix = &distances;
}

/**
 * Destructor
 */
ShortestPath::~ShortestPath() {
}

/**
 * Find shortest path
 * @return Success
 */
bool ShortestPath::findPath( std::vector<int> &path, double &distance ) {
	std::vector<Node> nodes { };
	int n = this->matrix->getCols(); //number of nodes
	//set C(start) distance to 0 and the rest to infinity
	for( int i = 0; i < n; i++ ) {
		if( i == this->startPoint ) {
			nodes.push_back( Node( i, this->startPoint, 0 ) );
		} else {
			nodes.push_back( Node( i, this->startPoint, max_distance ) );
		}
	}
	if( explore( this->startPoint, this->endPoint, nodes ) ) {
		path = traceBack( this->startPoint, this->endPoint, nodes );
		distance = nodes[ this->endPoint ].accumulated_distance;
		dumpPathsToFile( "path_reached.txt", nodes ); //DEBUG
		ShortestPath::logPath( path, nodes, this->endPoint );
		return true;
	} else {
		//dumpPathsToFile( "path_notreached.txt", nodes ); //DEBUG
		Log *log = Log::getInstance();
		std::ostringstream msg { };
		msg << "[ShortestPath::findPath] Path between '" << this->startPoint << "' and '"
				<< this->endPoint << "' in matrix not found.";
		log->newEvent( Location::Satellite, EventType::Event, msg );
		return false;
	}
}

///*********************************************************************
/// Private methods
///*********************************************************************
/**
 * Explores all possible nodes for shortest path
 * @param start Start point
 * @param goal End point
 * @param nodes Collection of all Nodes
 * @return Success in reaching the end point
 */
bool ShortestPath::explore( int start, int goal, std::vector<Node> &nodes ) {
	if( start == goal ) {
		return true;
	}
	bool reached_the_goal { false };
	for( int n = 0; n < static_cast<int>( nodes.size() ); n++ ) {
		if( matrix->data[ start ][ n ] > 0 ) { //check if node 'n' is connected
			double distance { nodes[ start ].accumulated_distance + this->matrix->data[ start ][ n ] };
			if( nodes[ n ].accumulated_distance > distance ) { //check if node 'n' has a worse distance
				nodes[ n ].accumulated_distance = distance;
				nodes[ n ].from_id = start;
				if( explore( n, goal, nodes ) ) {
					reached_the_goal = true;
				}
			}
		}
	}
	return reached_the_goal;
}

/**
 * Traces back the path from the end node
 * @param start Start point
 * @param end End point
 * @param nodes Collection of all nodes
 * @return The shortest path in order from start to end
 */
std::vector<int> ShortestPath::traceBack( int start, int end, std::vector<Node> &nodes ) {
	std::vector<int> path { };
	int i { end };
	auto it = path.begin();
	path.insert( it, nodes[ i ].id );
	while( i != start ) {
		i = nodes[ i ].from_id;
		it = path.begin();
		path.insert( it, nodes[ i ].id );
	}
	return path;
}

/**
 * [DEBUG] Writes the list of nodes with their information to a file
 * @param fileName Name of the output file
 * @param shortest Vector of 'Node'
 */
void ShortestPath::dumpPathsToFile( std::string fileName, std::vector<Node> &shortest ) {
	std::ofstream file { };
	file.open( fileName );
	for( int i = 0; i < static_cast<int>( shortest.size() ); i++ ) {
		file << i << ";" << shortest[ i ].from_id << ";" << shortest[ i ].accumulated_distance;
		file << "\n";
	}
	file.close();
}

/**
 * Sends path to log
 * @param path Path
 */
void ShortestPath::logPath( std::vector<int> &path, std::vector<Node> &nodes, int &end ) {
	Log *log = Log::getInstance();
	std::ostringstream msg { };
	msg << "[ShortestPath::findPath] Shortest path: { ";
	for( int i = 0; i < static_cast<int>( path.size() ); i++ ) {
		msg << path[ i ] << " ";
	}
	msg << "} with distance of " << nodes[ end ].accumulated_distance;
	log->newEvent( Location::Satellite, EventType::Event, msg );
}
