/*
 * matrix.hpp
 *
 *  Created on: 30 Nov 2014
 *      Author: E. A. Davison
 */

#ifndef MATRIX_HPP_
#define MATRIX_HPP_

template<typename T> struct Matrix {
		Matrix( int cols, int rows ) {
			this->cols = cols;
			this->rows = rows;

			this->data = new T*[ cols ] { };
			for( int i = 0; i < cols; ++i ) {
				data[ i ] = new T[ rows ] { };
			}
		}
		~Matrix() {
			for( int i = 0; i < cols; ++i ) {
				delete[] data[ i ];
			}
			delete[] data;
			//free( data );
		}
		int getRows() {
			return this->rows;
		}
		int getCols() {
			return this->cols;
		}
		T **data;
	private:
		int cols, rows;
};

#endif /* MATRIX_HPP_ */
