/*
 * ShortestSetPath.hpp
 *
 *  Created on: 12 Mar 2015
 *      Author: Es A. Davison
 */

#ifndef SATELLITES_PATHFINDER_SHORTESTSETPATH_HPP_
#define SATELLITES_PATHFINDER_SHORTESTSETPATH_HPP_

#include <numeric>
#include <vector>

#include "../../core/logger/Log.hpp"
#include "ShortestPath.hpp"

class ShortestSetPath {
	public:
		ShortestSetPath();
		~ShortestSetPath();
		bool findPath( std::vector<int> &pointToVisitIndex, Matrix<double> &distances,
				std::vector<int> &path );
	private:
		struct PathOfLeastCost {
			PathOfLeastCost() {
				this->origin =  -1;
				this->destination = -1;
				this->distance = -1;
			};
			~PathOfLeastCost() {};
			int origin;
			int destination;
			std::vector<int> path;
			double distance;
		};
		int origin;
		std::vector<PathOfLeastCost> master_allpaths;

		ShortestSetPath::PathOfLeastCost findSmallestDistance( int from, std::vector<PathOfLeastCost> &allpaths );
		ShortestSetPath::PathOfLeastCost findSmallestDistance( int from, int to, std::vector<PathOfLeastCost> &allpaths );
		bool isPathRelevant( int node, int i );
		bool isPathRelevant( int node, int i, std::vector<PathOfLeastCost> &allpaths );
		void copyPath( int origin, int &destination, ShortestSetPath::PathOfLeastCost &polc, std::vector<int> &path );
};

#endif /* SATELLITES_PATHFINDER_SHORTESTSETPATH_HPP_ */
