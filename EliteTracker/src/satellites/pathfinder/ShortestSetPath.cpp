/*
 * ShortestSetPath.cpp
 *
 *  Created on: 12 Mar 2015
 *      Author: Es A. Davison
 */

#include "ShortestSetPath.hpp"

const double max_distance = std::numeric_limits<double>::infinity();

/**
 * Constructor
 * @param pointToVisitIndex List of all indices to go to (first one being the start)
 * @param distances Matrix of distances
 */
ShortestSetPath::ShortestSetPath() {
	this->origin = -1;
}

/**
 * Destructor
 */
ShortestSetPath::~ShortestSetPath() {
}

/**
 * Finds shortest path through set
 * @param pointToVisitIndex Set of points to visit
 * @param matrix Matrix of distances between nodes
 * @param path container for suggested best path
 * @return Success
 */
bool ShortestSetPath::findPath( std::vector<int> &pointToVisitIndex, Matrix<double> &matrix,
		std::vector<int> &path ) {
	Log *log = Log::getInstance();
	std::ostringstream msg { };
	msg << "[ShortestSetPath::findPath(..)] Looking for a path through: { ";
	for( unsigned int i = 0; i < pointToVisitIndex.size(); i++ ) {
		msg << pointToVisitIndex[ i ] << " ";
	}
	msg << "}.";
	log->newEvent( Location::Satellite, EventType::Event, msg );

	this->origin = pointToVisitIndex[ 0 ];
	//get shortest path between each pairs of nodes
	bool unreachable_flag { false };
	for( unsigned int i = 0; i < pointToVisitIndex.size(); i++ ) {
		for( unsigned int j = i; j < pointToVisitIndex.size(); j++ ) {
			if( i != j ) {
				ShortestPath sp = ShortestPath( pointToVisitIndex[ i ], pointToVisitIndex[ j ],
						matrix );
				PathOfLeastCost polc { };
				polc.origin = pointToVisitIndex[ i ];
				polc.destination = pointToVisitIndex[ j ];
				if( !sp.findPath( polc.path, polc.distance ) ) {
					unreachable_flag = true;
				}
				this->master_allpaths.push_back( polc );
			}
		}
	}
	if( unreachable_flag ) {
		log->logEvent( Location::Satellite, EventType::Event,
				"[ShortestSetPath::findPath(..)] Couldn't find path through all the nodes." );
		return false;
	}

	//copy of all paths to play around with
	std::vector<PathOfLeastCost> allpaths { };
	allpaths.insert( allpaths.end(), master_allpaths.begin(), master_allpaths.end() );

	unsigned int nodes_visited { 0 };
	int destination { };
	int start { this->origin };
	while( nodes_visited < pointToVisitIndex.size() ) {
		msg.clear();
		msg.str( std::string() );
		msg << "[" << nodes_visited << "/" << pointToVisitIndex.size() - 1
				<< "] Looking with start ID of " << start + 1 << "  (" << start << ")" << std::endl;
		log->newEvent( Location::Satellite, EventType::Trace, msg );

		if( nodes_visited == pointToVisitIndex.size() - 1 ) {
			PathOfLeastCost new_path = this->findSmallestDistance( start, this->origin,
					this->master_allpaths );
			this->copyPath( start, destination, new_path, path );
			path.push_back( this->origin );
			nodes_visited++;
		} else {
			PathOfLeastCost new_path = this->findSmallestDistance( start, allpaths );
			if( new_path.origin >= 0 ) {
				this->copyPath( start, destination, new_path, path );
				start = destination;
			} else {
				return false;
			}
			nodes_visited++;
		}
	}

	msg.clear();
	msg.str( std::string() );
	msg << "[ShortestSetPath::findPath(..)] Path found: { ";
	for( unsigned int i = 0; i < path.size(); i++ ) {
		msg << path[ i ] << " ";
	}
	msg << "}.";
	log->newEvent( Location::Satellite, EventType::Event, msg );
	return true;
}

/**
 * Copies a path to the path vector in the right order
 * @param origin Origin node
 * @param destination Destination node
 * @param polc Path of least cost container
 * @param path Full path container
 */
void ShortestSetPath::copyPath( int origin, int &destination,
		ShortestSetPath::PathOfLeastCost &polc, std::vector<int> &path ) {
	if( polc.origin == origin ) {
		for( unsigned int i = 0; i < polc.path.size() - 1; i++ ) {
			path.push_back( polc.path[ i ] );
		}
		destination = polc.destination;
	} else { //reverse path
		for( unsigned int i = polc.path.size() - 1; i > 0; i-- ) {
			path.push_back( polc.path[ i ] );
		}
		destination = polc.origin;
	}
}

/**
 * Finds the path of least cost from a node
 * @param from Origin node
 * @param allpaths Container for all remaining paths
 * @return Path Of Least Cost container
 */
ShortestSetPath::PathOfLeastCost ShortestSetPath::findSmallestDistance( int from,
		std::vector<PathOfLeastCost> &allpaths ) {
	PathOfLeastCost shortest { };
	std::vector<int> index_of_found { };
	double distance = max_distance;
	for( unsigned int i = 0; i < allpaths.size(); i++ ) {
		if( this->isPathRelevant( from, i, allpaths ) ) {
			index_of_found.push_back( i );
			if( allpaths[ i ].distance < distance ) {
				distance = allpaths[ i ].distance;
				shortest = allpaths[ i ];
			}
		}
	}
	for( unsigned int i = 0; i < index_of_found.size(); i++ ) {
		allpaths.erase( allpaths.begin() + index_of_found[ i ] - i );
	}
	return shortest;
}

/**
 * Finds the index of 'allPaths' that have both nodes
 * @param from Origin node
 * @param to Destination node
 * @param allpaths Container for all remaining paths
 * @return PathOfLeastCost
 */
ShortestSetPath::PathOfLeastCost ShortestSetPath::findSmallestDistance( int from, int to,
		std::vector<PathOfLeastCost> &allpaths ) {
	for( unsigned int i = 0; i < this->master_allpaths.size(); i++ ) {
		if( this->master_allpaths[ i ].origin == from
				&& this->master_allpaths[ i ].destination == to ) {
			return this->master_allpaths[ i ];
		}
		if( this->master_allpaths[ i ].origin == to
				&& this->master_allpaths[ i ].destination == from ) {
			return this->master_allpaths[ i ];
		}
	}
	Log *log = Log::getInstance();
	std::ostringstream msg { };
	msg << "[ShortestSetPath::findSmallestDistance(..)] Couldn't find a path between node '" << from
			<< "' and '" << to << " in list of remaining possible paths.";
	log->newEvent( Location::Satellite, EventType::Error, msg );
	return PathOfLeastCost { };
}

/**
 * Checks if 'master_allpaths[ i ]' involves 'node'
 * @param node Node
 * @param i Position is the 'master_allpaths' vector
 * @return Node is mentioned at that position
 * @return Success
 */
bool ShortestSetPath::isPathRelevant( int node, int i ) {
	return ( this->master_allpaths[ i ].origin == node
			|| this->master_allpaths[ i ].destination == node );
}

/**
 * Checks if 'allpaths' has a POLC with node at index provided
 * @param node Node
 * @param i Index
 * @param allpaths Container of POLCs
 * @return Success
 */
bool ShortestSetPath::isPathRelevant( int node, int i, std::vector<PathOfLeastCost> &allpaths ) {
	return ( allpaths[ i ].origin == node || allpaths[ i ].destination == node );
}
