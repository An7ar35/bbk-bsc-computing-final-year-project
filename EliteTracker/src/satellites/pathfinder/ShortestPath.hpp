/*
 * ShortestPath.hpp
 *
 *  Created on: 8 Feb 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_PATHFINDER_SHORTESTPATH_HPP_
#define SATELLITES_PATHFINDER_SHORTESTPATH_HPP_

#include <iostream>
#include <numeric>
#include <limits>
#include <vector>
#include <fstream>

#include "Node.hpp"
#include "matrix.hpp"
#include "../../core/toolbox/toolbox.hpp"
#include "../../core/logger/Log.hpp"

class ShortestPath {
	public:
		ShortestPath( int origin, int destination, Matrix<double> &distances );
		~ShortestPath();
		bool findPath( std::vector<int> &path, double &distance );

	private:
		bool explore( int start, int goal, std::vector<Node> &nodes );
		std::vector<int> traceBack( int start, int end, std::vector<Node> &nodes );
		void dumpPathsToFile( std::string fileName, std::vector<Node> &shortest );
		static void logPath( std::vector<int> &path, std::vector<Node> &nodes, int &end );
		Matrix<double> *matrix;
		int startPoint;
		int endPoint;

};

#endif /* SATELLITES_PATHFINDER_SHORTESTPATH_HPP_ */
