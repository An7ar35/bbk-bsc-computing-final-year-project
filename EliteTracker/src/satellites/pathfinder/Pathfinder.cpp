/*
 * pathfinder.cpp
 *
 *  Created on: 11 Oct 2014
 *      Author: Alwlyan
 */

#include "pathfinder.hpp"

/**
 * Constructor
 */
Pathfinder::Pathfinder( std::string db_file, fileIO::Settings &config ) {
	this->database = db_file;
	this->shipInfo.shipID = config.getValueOfKey<int>( "CurrentPlayerShip", -1 );
	this->shipInfo.fuel_scoop_capable_flag = false;
}

/**
 * Constructor
 * @param shipID ID of user's ship used for travel
 */
Pathfinder::Pathfinder( std::string db_file, int playerShipID ) {
	this->database = db_file;
	this->shipInfo.shipID = playerShipID;
	this->shipInfo.fuel_scoop_capable_flag = false;
}

/**
 * Destructor
 */
Pathfinder::~Pathfinder() {
}

/**
 * Finds the shortest path between two points
 * @param originID Start point ID
 * @param destinationID End point ID
 * @return Success
 */
bool Pathfinder::findShortestPath( int originID, int destinationID ) {
	Db data( this->database );
	if( data.open() ) {
		if( !Pathfinder::checkPointsExist( data, originID, destinationID ) ) {
			return false;
		}
		this->shortest_path.clear();
		this->datapull.clear();

		if( this->getSystemsData( data, originID, destinationID ) ) {
			int size = this->datapull.getRowCount();
			if( size < 1 ) {
				data.close();
				this->logEvent( EventType::Event,
						"[Pathfinder::findShortestPath(..)] Data pull from Db found nothing based on the points and constraints given." );
				return false;
			}

			Coordinate2D<int> origin_index = datapull.searchFor( "S_ID", Item( originID ) );
			Coordinate2D<int> destination_index = datapull.searchFor( "S_ID",
					Item( destinationID ) );
			if( !Pathfinder::checkCoordinateValidity( origin_index )
					&& !Pathfinder::checkCoordinateValidity( destination_index ) ) {
				data.close();
				this->logEvent( EventType::Error,
						"[Pathfinder::findShortestPath(..)] Could not find the point(s) in the datapull Table.)" );
				return false;
			}

			if( !getShipInfo( data ) ) {
				data.close();
				this->logEvent( EventType::Error,
						"[Pathfinder::findShortestPath(..)] Could not get the ship's information.)" );
				return false;
			}

			Matrix<double> matrix( size, size );
			if( this->createMatrix( matrix ) ) {
				//this->createMatrixFile( matrix ); //DEBUG
			}
			double shortest_path_distance { };
			ShortestPath sp = ShortestPath( origin_index.y, destination_index.y, matrix );
			return sp.findPath( this->shortest_path, shortest_path_distance );
		}
		data.close();
		return false;
	} else {
		this->logEvent( EventType::Error,
				"[Pathfinder::findShortestPath(..)] Could not open connection to database.)" );
	}
	return false;
}

/**
 * Finds the best path through given points
 * @param originID Start point ID
 * @param places_to_go IDs of points to visit
 * @return Success
 */
bool Pathfinder::findShortestPath( int originID, std::vector<int> &stopsIDs ) {
	Db data( this->database );
	if( data.open() ) {
		if( stopsIDs.size() > 10 ) {
			this->logEvent( EventType::Error,
					"[Pathfinder::findShortestPath(..)] Too many places to go to!" );
			return false;
		}
		std::vector<int> p { };
		p.push_back( originID );
		p.insert( p.end(), stopsIDs.begin(), stopsIDs.end() );
		if( !Pathfinder::checkPointsExist( data, p ) ) {
			this->shortest_path.clear();
			this->datapull.clear();
			if( this->getSystemsData( data, p ) ) {
				int size = this->datapull.getRowCount();
				if( size < 1 ) {
					this->logEvent( EventType::Event,
							"[Pathfinder::findShortestPath(..)] Data pull from Db found nothing based on the points and constraints given." );
					return false;
				}
				std::vector<int> stopsIndex { };
				for( unsigned int i = 0; i < p.size(); i++ ) {
					Coordinate2D<int> index = datapull.searchFor( "S_ID", Item( p[ i ] ) );
					if( !Pathfinder::checkCoordinateValidity( index ) ) {
						this->logEvent( EventType::Error,
								"[Pathfinder::findShortestPath(..)] Could not find the point in the datapull Table.)" );
						return false;
					}
					stopsIndex.push_back( index.y );
				}

				if( !getShipInfo( data ) ) {
					this->logEvent( EventType::Error,
							"[Pathfinder::findShortestPath(..)] Could not get the ship's information.)" );
					return false;
				}
				Matrix<double> matrix( size, size );
				if( this->createMatrix( matrix ) ) {
					//this->createMatrixFile( matrix ); //DEBUG
				}
				ShortestSetPath ssp = ShortestSetPath();
				return ssp.findPath( stopsIndex, matrix, this->shortest_path );
			}
		}
		return false;
	} else {
		this->logEvent( EventType::Error,
				"[Pathfinder::findShortestPath(..)] Could not open connection to database.)" );
	}
	return false;
}

/**
 * Gets the Full information for the shortest path
 * @return Information as a Table
 */
Table Pathfinder::getPathInfo() {

	bool error_flag { false };
	Table info { };
	info.createColumn( 1, "ID" );
	info.createColumn( 3, "System" );
	info.createColumn( 2, "Distance" );
	info.createColumn( 2, "Cumulative" );
	info.lockTableStructure();
	if( this->shortest_path.empty() ) {
		info.clearData();
		info.lockTableStructure();
		return info;
	}
	Db data( this->database );
	if( data.open() ) {
		int s_id_column = this->datapull.whereIsThisColumn( "S_ID" );
		int name_column = this->datapull.whereIsThisColumn( "Name" );
		int previous_id = this->datapull( s_id_column, this->shortest_path[ 0 ] ).getInt();
		double cumulative_distance = 0;
		for( int i = 0; i < static_cast<int>( this->shortest_path.size() ); i++ ) {
			int id = this->datapull( s_id_column, this->shortest_path[ i ] ).getInt();
			std::string name = this->datapull( name_column, this->shortest_path[ i ] ).getString();
			double distance = 0;
			if( previous_id == id ) {
				distance = 0;
			} else if( this->getDistance( data, previous_id, id, distance ) < 1 ) {
				Pathfinder::logEvent( EventType::Error,
						"[Pathfinder::getPathInfo(..)] Problem getting distance between points." );
				distance = -1;
			}
			cumulative_distance += distance;
			//Add data sequentially
			if( !info.add( id ) ) {
				error_flag = true;
			}
			if( !info.add( name ) ) {
				error_flag = true;
			}
			if( !info.add( distance ) ) {
				error_flag = true;
			}
			if( !info.add( cumulative_distance ) ) {
				error_flag = true;
			}
			previous_id = id;
		}
		if( error_flag ) {
			Pathfinder::logEvent( EventType::Error,
					"[Pathfinder::getPathInfo(..)] Error flag was up! (problem in adding data to the info table)" );
			info.clearData();
		}
	} else {
		this->logEvent( EventType::Error,
				"[Pathfinder::findShortestPath(..)] Could not open connection to database.)" );
	}
	return info;
}

///*********************************************************************
/// Private methods
///*********************************************************************
/**
 * Checks that the two point given exists in the database
 * @param a Point a
 * @param b Point b
 * @return Existence state
 */
bool Pathfinder::checkPointsExist( Db &data, int a, int b ) {
	if( data.checkEntryExists( "JumpSystems", "S_ID", std::to_string( a ) )
			&& data.checkEntryExists( "JumpSystems", "S_ID", std::to_string( b ) ) ) {
		return true;
	} else {
		Pathfinder::logEvent( EventType::Error,
				"[Pathfinder::checkPointsExists(..)] One or both of the point do not exist in the JumpSystems table." );
	}
	return false;
}

/**
 * Checks that the points given exists in the database
 * @param points List of points to check
 * @return Existence state
 */
bool Pathfinder::checkPointsExist( Db &data, std::vector<int> &points ) {
	if( points.size() >= 2 ) {
		bool unknown_flag { false };
		for( unsigned int i = 0; i < points.size(); i++ ) {
			if( !data.checkEntryExists( "JumpSystems", "S_ID", std::to_string( points[ i ] ) ) ) {
				unknown_flag = true;
				Pathfinder::logEvent( EventType::Error,
						"[Pathfinder::checkPointsExists(..)] A point do not exist in the JumpSystems table." );
			}
		}
		return unknown_flag;
	} else {
		Pathfinder::logEvent( EventType::Error,
				"[Pathfinder::checkPointsExists(..)] Not enough points given." );
	}
	return false;
}

/**
 * Checks that the coordinate given isn't set to (-1,-1) [i.e.: Error]
 * @param coordinate Coordinate2D to check
 * @return Success
 */
bool Pathfinder::checkCoordinateValidity( Coordinate2D<int> &coordinate ) {
	if( coordinate.x == -1 && coordinate.y == -1 ) {
		return false;
	}
	return true;
}

/**
 * Pulls all the systems that match the constraints of the search for SP
 * @param originID Start point ID
 * @param destinationID End point ID
 * @return Success
 */
bool Pathfinder::getSystemsData( Db &data, int originID, int destinationID ) {
	Coordinate3D o = editTable::JumpSystems::getCoordinateFor( data, originID );
	Coordinate3D d = editTable::JumpSystems::getCoordinateFor( data, destinationID );
	if( o.error_flag || d.error_flag ) {
		return false;
	}
	//calculates area of interest
	Coordinate3D l { }; //upper bound
	Coordinate3D u { }; //lower bound
	CoreTools::math::spatial::calcBounds( o, d, l, u );
	double distance = CoreTools::math::spatial::calcDistance( o, d );
	distance < 100 ? ( l.x -= 50, u.x += 50 ) : ( l.x -= distance / 2, u.x += distance / 2 );
	distance < 100 ? ( l.y -= 50, u.y += 50 ) : ( l.y -= distance / 2, u.y += distance / 2 );
	distance < 100 ? ( l.z -= 50, u.z += 50 ) : ( l.x -= distance / 2, u.z += distance / 2 );
	if( this->shipInfo.fuel_scoop_capable_flag ) {
		Pathfinder::pullSystems( data, l, u );
	} else {
		Pathfinder::pullSystemsWithStations( data, l, u );
	}
	return true;
}

/**
 * Pulls all the systems that are in the area of the points given for SSP
 * @param points IDs of the Systems to visit
 * @return Success
 */
bool Pathfinder::getSystemsData( Db &data, std::vector<int> points ) {
	/**
	 * [Lambda] Find the upper and lower bounds of the area occupied by the points
	 */
	auto findEdgesOfArea = [&]( Db &data, Coordinate3D &l, Coordinate3D &u ) {
		for( int &p : points ) {
			Coordinate3D c = editTable::JumpSystems::getCoordinateFor( data, p );
			if( !c.error_flag ) {
				if( c.x > u.x ) u.x = c.x;
				if( c.x < l.x ) l.x = c.x;
				if( c.y > u.y ) u.y = c.y;
				if( c.y < l.y ) l.y = c.y;
				if( c.z > u.z ) u.z = c.z;
				if( c.z < l.z ) l.z = c.z;
			} else {
				return false;
			}
		}
		return true;
	};
	Coordinate3D l { }; //upper bound
	Coordinate3D u { }; //lower bound
	if( !findEdgesOfArea( data, l, u ) ) {
		return false;
	}
	//calculates area of interest
	double distance = CoreTools::math::spatial::calcDistance( l, u );
	distance < 100 ? ( l.x -= 50, u.x += 50 ) : ( l.x -= distance / 2, u.x += distance / 2 );
	distance < 100 ? ( l.y -= 50, u.y += 50 ) : ( l.y -= distance / 2, u.y += distance / 2 );
	distance < 100 ? ( l.z -= 50, u.z += 50 ) : ( l.x -= distance / 2, u.z += distance / 2 );
	Pathfinder::pullSystems( data, l, u );
	return true;
}

/**
 * Creates a Matrix of the pulled data
 * @param matrix Matrix container
 * @return Success
 */
bool Pathfinder::createMatrix( Matrix<double> &matrix ) {
	//int nOperations { matrix.getCols() * matrix.getRows() }; //DEBUG
	int counter { 0 };
	//Get the location in the Table of where the Coordinate parts live in
	int x_loc = this->datapull.whereIsThisColumn( "X_Coordinate" );
	int y_loc = this->datapull.whereIsThisColumn( "Y_Coordinate" );
	int z_loc = this->datapull.whereIsThisColumn( "Z_Coordinate" );
	if( x_loc < 0 || y_loc < 0 || z_loc < 0 ) {
		return false;
	}
	//Make the matrix
	for( int i = 0; i < matrix.getCols(); i++ ) {
		for( int j = 0; j < matrix.getRows(); j++ ) {
			if( i == j ) {
				matrix.data[ i ][ j ] = 0;
			} else {
				Coordinate3D a = Coordinate3D( datapull( x_loc, i ).getDouble(),
						datapull( y_loc, i ).getDouble(), datapull( z_loc, i ).getDouble() );
				Coordinate3D b = Coordinate3D( datapull( x_loc, j ).getDouble(),
						datapull( y_loc, j ).getDouble(), datapull( z_loc, j ).getDouble() );

				double distance = CoreTools::math::spatial::calcDistance( a, b );
				//Voids any distances greater than the max jump range of the ship
				if( distance > this->shipInfo.jumpDistance ) {
					matrix.data[ i ][ j ] = -1;
				} else {
					matrix.data[ i ][ j ] = distance;
				}
			}
			counter++;
			//CoreTools::progressBar_console( counter, nOperations, 50 ); //DEBUG
		}
	}
	return true;
}

/**
 * Creates a semi-colon delimited matrix file of the distances
 * @param matrix Matrix holding the distances
 */
void Pathfinder::createMatrixFile( Matrix<double> &matrix ) {
	int nOperations { matrix.getCols() * matrix.getRows() };
	int counter { 0 };
	std::ofstream file { };
	file.open( "matrix.txt" );
	Log *log = Log::getInstance();
	log->logEvent( Location::Satellite, EventType::Event,
			"[Pathfinder::createMatrixFile] Writing matrix to file." );
	for( int i = 0; i < matrix.getCols(); i++ ) {
		for( int j = 0; j < matrix.getRows(); j++ ) {
			file << matrix.data[ i ][ j ] << ';';
			counter++;
			CoreTools::progressBar_console( counter, nOperations, 50 );
		}
		file << "\n";
	}
	file.close();
}

/**
 * Retrieves user's ship information
 * @return Success
 */
bool Pathfinder::getShipInfo( Db &data ) {
	Table table { };
	std::stringstream ss { };
	ss << "SELECT * FROM PlayerShips WHERE ID = " << this->shipInfo.shipID;
	data.sqlQueryPull_typed( ss.str(), table );
	//Does the Player's ship exist and is in one piece?
	int row = table.searchFor( "ID", Item( this->shipInfo.shipID ), "DestroyedFlag",
			Item( "FALSE" ) );
	int column_ShipID = table.whereIsThisColumn( "ShipID" );
	if( row < 0 || column_ShipID < 0 ) { //Error check
		if( row < 0 ) {
			Pathfinder::logEvent( EventType::Error,
					"[Pathfinder::getShipInfo()] Player's Ship could not be found. Either destroyed or not in list." );
		}
		if( column_ShipID < 0 ) {
			Pathfinder::logEvent( EventType::Error,
					"[Pathfinder::getShipInfo()] 'ShipID' column could not be found in Table." );
		}
		return false;
	}
	int column_fuelscoop = table.whereIsThisColumn( "FuelScoop" );
	if( column_fuelscoop < 0 ) {
		Pathfinder::logEvent( EventType::Error,
				"[Pathfinder::getShipInfo()] 'FuelScoop' column could not be found in Table. Defaulting to no fuel scoop capability." );
		this->shipInfo.fuel_scoop_capable_flag = false;
	} else {
		if( table( column_fuelscoop, row ).getString() == "TRUE" ) {
			this->shipInfo.fuel_scoop_capable_flag = true;
		} else {
			this->shipInfo.fuel_scoop_capable_flag = false;
		}
	}

	//Getting the full specs on the player's ship from the Ships database table
	std::ostringstream os { };
	os << "SELECT * FROM Ships WHERE ID = " << table( column_ShipID, row ).getInt();
	if( data.sqlQueryPull_typed( os.str(), table ) < 1 ) { //Error check

		Pathfinder::logEvent( EventType::Error,
				"[Pathfinder::getShipInfo()] Player ship not in list of known ships. (ID not found)" );
		return false;
	}
	int column_JumpRangeEmpty = table.whereIsThisColumn( "JumpRangeEmpty" );
	if( column_JumpRangeEmpty < 0 ) { //Error check

		Pathfinder::logEvent( EventType::Error,
				"[Pathfinder::getShipInfo()] 'JumpRangeEmpty' column could not be found in Table." );
		return false;
	}
	this->shipInfo.jumpDistance = table( column_JumpRangeEmpty, 0 ).getDouble();

	os.clear();
	os.str( std::string() );
	os << "[Pathfinder::getShipInfo()] ShipID: " << this->shipInfo.shipID << ", Range: "
			<< this->shipInfo.jumpDistance << ", FuelScoop: "
			<< this->shipInfo.fuel_scoop_capable_flag << ".";
	Pathfinder::logEvent( EventType::Event, std::string( os.str() ).c_str() );

	return true;
}

/**
 * Gets the distance between two points (order doesn't matter)
 * @param data Db Object
 * @param pointA First point
 * @param pointB Second point
 * @param distance Distance holder for the return value
 * @return Success - (1) Good, (0) Fail
 */
int Pathfinder::getDistance( Db &data, int pointA, int pointB, double &distance ) {
	std::stringstream ss { };
	ss << "SELECT Distance FROM JumpConnections WHERE ( S_ID_A = " << pointA << " OR S_ID_A = "
			<< pointB << " ) AND ( S_ID_B = " << pointB << " OR S_ID_B = " << pointA << " )";
	int returned = data.sqlQueryPull_single( ss.str(), distance );
	switch( returned ) {
		case -1:
			Pathfinder::logEvent( EventType::Error,
					"Trying to [Pathfinder::getDistance] - DB connection failed." );
			return 0;
			break;
		case -2:
			Pathfinder::logEvent( EventType::Error,
					"Trying to [Pathfinder::getDistance] - Too many columns fetched." );
			return 0;
			break;
		case 0:
			Pathfinder::logEvent( EventType::Error,
					"Trying to [Pathfinder::getDistance] - No results found." );
			return 0;
			break;
		case 1:
			Pathfinder::logEvent( EventType::Event, "Found distance for points." );
			return 1;
			break;
		default:
			Pathfinder::logEvent( EventType::Error,
					"Trying to [Pathfinder::getDistance] - There are > 1 matches found." );
			return 1;
	}
}

/**
 * Grabs all Systems from db that are within the bounds described
 * @param data Db Object
 * @param min_range Lower bound coordinates
 * @param max_range Higher bound coordinate
 */
void Pathfinder::pullSystems( Db &data, Coordinate3D &min_range, Coordinate3D &max_range ) {
	std::ostringstream oss { };
	oss << "SELECT DISTINCT JumpSystems.S_ID AS S_ID, " << "JumpSystems.Name AS Name, "
			<< "JumpSystems.X_Coordinate AS X_Coordinate, "
			<< "JumpSystems.Y_Coordinate AS Y_Coordinate, "
			<< "JumpSystems.Z_Coordinate AS Z_Coordinate FROM JumpSystems WHERE ( X_Coordinate >= "
			<< min_range.x << " AND X_Coordinate <= " << max_range.x << " ) AND ( Y_Coordinate >= "
			<< min_range.y << " AND Y_Coordinate <= " << max_range.y << " ) AND ( Z_Coordinate >= "
			<< min_range.z << " AND Z_Coordinate <= " << max_range.z << " )";
	data.sqlQueryPull_typed( oss.str(), this->datapull );
}

/**
 * Grabs Systems with sations (for fuel) from db that are within the bounds described
 * @param data Db Object
 * @param min_range Lower bound coordinates
 * @param max_range Higher bound coordinate
 */
void Pathfinder::pullSystemsWithStations( Db &data, Coordinate3D &min_range,
		Coordinate3D &max_range ) {
	std::ostringstream oss { };
	oss << "SELECT DISTINCT JumpSystems.S_ID AS S_ID, " << "JumpSystems.Name AS Name, "
			<< "JumpSystems.X_Coordinate AS X_Coordinate, "
			<< "JumpSystems.Y_Coordinate AS Y_Coordinate, "
			<< "JumpSystems.Z_Coordinate AS Z_Coordinate FROM JumpSystems "
			<< "JOIN Stations ON JumpSystems.S_ID = Stations.S_ID WHERE ( X_Coordinate >= "
			<< min_range.x << " AND X_Coordinate <= " << max_range.x << " ) AND ( Y_Coordinate >= "
			<< min_range.y << " AND Y_Coordinate <= " << max_range.y << " ) AND ( Z_Coordinate >= "
			<< min_range.z << " AND Z_Coordinate <= " << max_range.z << " )";
	data.sqlQueryPull_typed( oss.str(), this->datapull );
}

/**
 * Log an event
 * @param nLevel Type of event
 * @param description Description of the event
 */
void Pathfinder::logEvent( EventType nLevel, const char *description ) {
	std::ostringstream oss { };
	oss << description;
	Log *log = Log::getInstance();
	log->newEvent( Location::Satellite, nLevel, oss );
}
