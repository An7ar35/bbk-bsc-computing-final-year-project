/*
 * pathfinder.hpp
 *
 *  Created on: 11 Oct 2014
 *      Author: Alwlyan
 */

#ifndef PATHFINDER_HPP_
#define PATHFINDER_HPP_

#include <iostream>
#include <string>
#include <fstream>
#include <algorithm>
#include <iterator>
#include <initializer_list>
#include <utility>
#include "../../core/logger/Log.hpp"
#include "../../core/database/Db.hpp"
#include "../../core/containers/Table.hpp"
#include "../../core/database/tables/JumpSystems.hpp"
#include "Matrix.hpp"
#include "Node.hpp"
#include "ShortestPath.hpp"
#include "ShortestSetPath.hpp"
#include "../../core/toolbox/toolbox.hpp"
#include "../../core/toolbox/math/spatial.hpp"
#include "../../core/fileops/Settings.hpp"

class Pathfinder {
	public:
		Pathfinder( std::string db_file, fileIO::Settings &config ); //uses current ship set in config file
		Pathfinder( std::string db_file, int playerShipID ); //looks up for another of the user's ship
		~Pathfinder();
		bool findShortestPath( int originID, int destinationID ); //Shortest path
		bool findShortestPath( int originID, std::vector<int> &places_to_go ); //Travelling salesman
		Table getPathInfo();

	private:
		struct ShipInfo {
				int shipID;
				double jumpDistance;
				bool fuel_scoop_capable_flag;
		} shipInfo;
		std::string database;
		Table datapull;
		std::vector<int> shortest_path;
		bool checkPointsExist( Db &data, int a, int b );
		bool checkPointsExist( Db &data, std::vector<int> &points );
		static bool checkCoordinateValidity( Coordinate2D<int> &coordinate );
		bool getSystemsData( Db &data, int originID, int destinationID );
		bool getSystemsData( Db &data, std::vector<int> points );
		bool createMatrix( Matrix<double> &matrix );
		void createMatrixFile( Matrix<double> &matrix );
		bool getShipInfo( Db &data );
		int getDistance( Db &data, int pointA, int pointB, double &distance );
		void pullSystems( Db &data, Coordinate3D &min_range, Coordinate3D &max_range );
		void pullSystemsWithStations( Db &data, Coordinate3D &min_range, Coordinate3D &max_range );
		static void logEvent( EventType nLevel, const char *description );
};

#endif /* PATHFINDER_HPP_ */

/*
 * Source:
 * http://www.geeksforgeeks.org/greedy-algorithms-set-6-dijkstras-shortest-path-algorithm/
 */
