/*
 * coordinate_tool.cpp
 *
 *  Created on: 11 Oct 2014
 *      Author: Alwlyan
 */

#include "coordinate_tool.hpp"

///*********************************************************************
/// CoordinateTool [Public methods]
///*********************************************************************
/**
 * Constructor
 * @param data Db Object
 * @param jumpConnections Name of the table holding the node distance information
 * @param a Name of the column holding the ID for node A
 * @param b Name of the column holding the ID for node B
 * @param d Name of the column holding the distances between nodes
 * @param jumpSystems Name of the table holding the nodes' coordinate information
 * @param id Name of the column holding the ID for each nodes
 * @param x Name of the column holding the X coordinate data
 * @param y Name of the column holding the Y coordinate data
 * @param z Name of the column holding the Z coordinate data
 */
CoordinateTool::CoordinateTool( Db &data, std::string jumpConnections, std::string a, std::string b,
		std::string d, std::string jumpSystems, std::string id, std::string x, std::string y,
		std::string z ) {
	this->distance_table.table_name = jumpConnections;
	this->distance_table.id_A = a;
	this->distance_table.id_B = b;
	this->distance_table.distance = d;
	this->systems_table.table_name = jumpSystems;
	this->systems_table.system_id = id;
	this->systems_table.x_coord = x;
	this->systems_table.y_coord = y;
	this->systems_table.z_coord = z;
	this->n = 0;
	Log *log = Log::getInstance();
	std::ostringstream error_decription { };
	this->error_flag = false;
	//check that all of the information is valid
	if( !data.checkTableExists( jumpConnections ) ) {
		this->error_flag = true;
		error_decription << "'" << jumpConnections << "' table does not exist; ";
	} else {
		if( !data.checkColumnExists( jumpConnections, a ) ) {
			this->error_flag = true;
			error_decription << "'" << a << "' column doesn't exist; ";
		}
		if( !data.checkColumnExists( jumpConnections, b ) ) {
			this->error_flag = true;
			error_decription << "'" << b << "' column doesn't exist; ";
		}
		if( !data.checkColumnExists( jumpConnections, d ) ) {
			this->error_flag = true;
			error_decription << "'" << d << "' column doesn't exist; ";
		}
	}
	if( !data.checkTableExists( jumpSystems ) ) {
		this->error_flag = true;
		error_decription << "'" << jumpSystems << "' table does not exist; ";
		if( !data.checkColumnExists( jumpSystems, id ) ) {
			this->error_flag = true;
			error_decription << "'" << id << "' column doesn't exist; ";
		}
		if( !data.checkColumnExists( jumpSystems, x ) ) {
			this->error_flag = true;
			error_decription << "'" << x << "' column doesn't exist; ";
		}
		if( !data.checkColumnExists( jumpSystems, y ) ) {
			this->error_flag = true;
			error_decription << "'" << y << "' column doesn't exist; ";
		}
		if( !data.checkColumnExists( jumpSystems, z ) ) {
			this->error_flag = true;
			error_decription << "'" << z << "' column doesn't exist; ";
		}
	}
	if( this->error_flag ) {
		std::ostringstream oss { };
		oss << "[CoordinateTool(..) Constructor] " << error_decription;
		log->newEvent( Location::Satellite, EventType::Error, oss );
	}
}

/**
 * Destructor
 */
CoordinateTool::~CoordinateTool() {
}

/**
 * Propagate coordinates from given Jump System
 * (checks then calls for private function to do that)
 * @param data Db object
 * @param tableID S_ID for the JumpSystems
 * @return Success
 */
bool CoordinateTool::generateCoordinatesFrom( Db &data, std::string tableID ) {
	if( this->error_flag ) {
		return false;
	}
	Log *log = Log::getInstance();
	if( !( data.checkEntryExists( systems_table.table_name, systems_table.system_id, tableID ) ) ) {
		std::ostringstream error_description { };
		error_description << "[CoordinateTool::generateCoordinatesFrom(..)] '" << tableID
				<< "' ID (" << systems_table.system_id << ") doesn't exist in '"
				<< systems_table.table_name << "'";
		log->newEvent( Location::Satellite, EventType::Error, error_description );
		return false;
	}
	if( data.getNumberOfRows( this->systems_table.table_name ) > 1 ) {
		if( !( data.checkEntryExists( distance_table.table_name, distance_table.id_A, tableID ) )
				&& !( data.checkEntryExists( distance_table.table_name, distance_table.id_B,
						tableID ) ) ) {
			std::ostringstream error_description { };
			error_description << "[CoordinateTool::generateCoordinatesFrom(..)] '" << tableID
					<< "' ID (" << distance_table.id_A << "/" << distance_table.id_B
					<< ") doesn't exist in '" << this->distance_table.table_name << "'";
			log->newEvent( Location::Satellite, EventType::Error, error_description );
			return false;
		}
	}
	return this->generateFrom( data, tableID );
}

///*********************************************************************
/// CoordinateTool [Private methods]
///*********************************************************************
/**
 * Checks status of given system ID
 * @param data Db Object
 * @param point Point to check
 * @return Status
 */
CoordinateTool::PointStatus CoordinateTool::checkPoint( Db &data, const int &point_ID ) {
	Table t { };
	std::ostringstream oss { };
	oss << "SELECT * FROM " << this->systems_table.table_name << " WHERE "
			<< this->systems_table.system_id << " = " << point_ID;
	data.sqlQueryPull_typed( oss.str(), t );
	int x_col = t.whereIsThisColumn( this->systems_table.x_coord );
	int y_col = t.whereIsThisColumn( this->systems_table.y_coord );
	int z_col = t.whereIsThisColumn( this->systems_table.z_coord );
	Log *log = Log::getInstance();
	std::ostringstream msg { };
	log->newEvent( Location::Satellite, EventType::Event, msg );
	if( t.getRowCount() > 0 ) {
		oss.clear();
		oss.str( std::string() );
		oss << "SELECT * FROM " << this->systems_table.table_name << " WHERE "
				<< this->systems_table.system_id << " = " << point_ID << " AND "
				<< this->systems_table.x_coord << " NOT NULL AND " << this->systems_table.y_coord
				<< " NOT NULL AND " << this->systems_table.z_coord << " NOT NULL";
		data.sqlQueryPull_typed( oss.str(), t );
		if( t.getRowCount() > 0 ) {
			msg << "[CoordinateTool::checkPoint(..)] Coordinates for point '" << point_ID
					<< "' known.";
			log->newEvent( Location::Satellite, EventType::Trace, msg );
			return CoordinateTool::PointStatus::COORDINATES_KNOWN;
		} else {
			msg << "[CoordinateTool::checkPoint(..)] Coordinates for point '" << point_ID
					<< "' do not exist.";
			log->newEvent( Location::Satellite, EventType::Trace, msg );
			return CoordinateTool::PointStatus::COORDINATES_UNKNOWN;
		}
	} else {
		msg << "[CoordinateTool::checkPoint(..)] Point '" << point_ID << "' does not exist.";
		log->newEvent( Location::Satellite, EventType::Trace, msg );
		return CoordinateTool::PointStatus::NOT_EXISTS;
	}
	/*
	 t.printToConsole();
	 if( t.getRowCount() > 0
	 && ( t( x_col, 0 ).getString().length() == 0 || t( y_col, 0 ).getString().length() == 0
	 || t( z_col, 0 ).getString().length() == 0 ) ) {
	 return CoordinateTool::PointStatus::COORDINATES_UNKNOWN;
	 } else if( t.getRowCount() > 0
	 && ( t( x_col, 0 ).getString().length() != 0 && t( y_col, 0 ).getString().length() != 0
	 && t( z_col, 0 ).getString().length() != 0 ) ) {
	 return CoordinateTool::PointStatus::COORDINATES_KNOWN;
	 } else {
	 return CoordinateTool::PointStatus::NOT_EXISTS;
	 }
	 */
}

/**
 * Propagate coordinates from given Jump System
 * @param data Db object
 * @param tableID S_ID for the JumpSystems
 * @return Success
 */
bool CoordinateTool::generateFrom( Db &data, const std::string &id ) {
	Log *log = Log::getInstance();
	int newPoint = Convert::string_to_type<int>( id );
	if( CoordinateTool::checkPoint( data, newPoint )
			== CoordinateTool::PointStatus::COORDINATES_KNOWN ) {
		std::ostringstream msg { };
		msg << "CoordinateTool::generateFrom(..)] Coordinates for point '" << id
				<< "' is already known.";
		log->newEvent( Location::Satellite, EventType::Error, msg );
		return false;
	}
	int n_records = data.getNumberOfRows( this->systems_table.table_name );
	if( n_records > 4 ) { //Full 3D space
		return CoordinateTool::generate3D_2( data, newPoint );
	} else if( n_records == 4 ) { //3D - Tetrahedron
		return CoordinateTool::generate3D_1( data, newPoint );
	} else if( n_records == 3 ) { //2D - flat
		return CoordinateTool::generate2D( data, newPoint );
	} else if( n_records == 2 ) { //1D - line
		return CoordinateTool::generate1D( data, newPoint );
	} else if( n_records == 1 ) { //No dimensions, 1st record
		return CoordinateTool::generateFirst( data, newPoint );
	} else {
		log->logEvent( Location::Satellite, EventType::Error,
				"[CoordinateTool::generateFrom(..)] Number of records returned is less that 1 in database table." );
	}
	return false;
}

/**
 * Generates the 'origin' (first) coordinates of {0,0,0}
 * @param data Db Object
 * @param newPoint_ID Point A
 * @return Success
 */
bool CoordinateTool::generateFirst( Db &data, const int &newPoint_ID ) {
	Coordinate3D newPoint { 0, 0, 0 };
	editJumpSystemsTable( data, newPoint_ID, newPoint );
	std::ostringstream msg { };
	msg << "[CoordinateTool::generateFirst(..)] System '" << newPoint_ID
			<< "' coordinates calculated [" << newPoint.x << "," << newPoint.y << "," << newPoint.z
			<< "].";
	Log *log = Log::getInstance();
	log->newEvent( Location::Satellite, EventType::Event, msg );
	return true;
}

/**
 * Generates the second coordinate
 * @param data Db Object
 * @param newPoint_ID ID of 2nd point
 * @return Success
 */
bool CoordinateTool::generate1D( Db &data, const int &newPoint_ID ) {
	Table t { };
	if( CoordinateTool::pullConnectionsWithCoordinates( data, newPoint_ID, t ) > 0 ) {
		int distance_col = t.whereIsThisColumn( "Distance" );
		double distXA = t( distance_col, 0 ).getDouble();
		Coordinate3D pointA { }, newPoint { };
		int x_col = t.whereIsThisColumn( "Target_X" );
		int y_col = t.whereIsThisColumn( "Target_Y" );
		int z_col = t.whereIsThisColumn( "Target_Z" );
		pointA.x = t( x_col, 0 ).getDouble();
		pointA.y = t( y_col, 0 ).getDouble();
		pointA.z = t( z_col, 0 ).getDouble();
		if( coordinates::calc1D( pointA, distXA, newPoint ) ) {
			editJumpSystemsTable( data, newPoint_ID, newPoint );
			std::ostringstream msg { };
			msg << "[CoordinateTool::generate1D(..)] System '" << newPoint_ID
					<< "' coordinates calculated [" << newPoint.x << "," << newPoint.y << ","
					<< newPoint.z << "].";
			Log *log = Log::getInstance();
			log->newEvent( Location::Satellite, EventType::Event, msg );
			return true;
		}
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Satellite, EventType::Event,
				"[CoordinateTool::generate1D(..)] Couldn't find enough connected points to calculate coordinates." );
	}
	return false;
}

/**
 * Generate the 3rd coordinate
 * @param data Db Object
 * @param newPoint_ID ID of 3rd point
 * @return Success
 */
bool CoordinateTool::generate2D( Db &data, const int &newPoint_ID ) {
	Table t { };
	if( CoordinateTool::pullConnectionsWithCoordinates( data, newPoint_ID, t ) > 1 ) {
		int distance_col = t.whereIsThisColumn( "Distance" );
		double distXA = t( distance_col, 0 ).getDouble();
		double distXB = t( distance_col, 1 ).getDouble();
		Coordinate3D pointA { }, pointB { }, newPoint { };
		int x_col = t.whereIsThisColumn( "Target_X" );
		int y_col = t.whereIsThisColumn( "Target_Y" );
		int z_col = t.whereIsThisColumn( "Target_Z" );
		t.printToConsole();
		//search fro
		pointA.x = t( x_col, 0 ).getDouble();
		pointA.y = t( y_col, 0 ).getDouble();
		pointA.z = t( z_col, 0 ).getDouble();
		pointB.x = t( x_col, 1 ).getDouble();
		pointB.y = t( y_col, 1 ).getDouble();
		pointB.z = t( z_col, 1 ).getDouble();
		if( coordinates::calc2D( pointA, pointB, distXA, distXB, newPoint ) ) {
			editJumpSystemsTable( data, newPoint_ID, newPoint );
			std::ostringstream msg { };
			msg << "[CoordinateTool::generate2D(..)] System '" << newPoint_ID
					<< "' coordinates calculated [" << newPoint.x << "," << newPoint.y << ","
					<< newPoint.z << "].";
			Log *log = Log::getInstance();
			log->newEvent( Location::Satellite, EventType::Event, msg );
			return true;
		}
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Satellite, EventType::Event,
				"[CoordinateTool::generate2D(..)] Couldn't find enough connected points to calculate coordinates." );
	}
	return false;
}

/**
 * Generate coordinates of 3rd point
 * @param data Db Object
 * @param newPoint_ID ID of new point
 * @return Success
 */
bool CoordinateTool::generate3D_1( Db &data, const int &newPoint_ID ) {
	Table t { };
	if( CoordinateTool::pullConnectionsWithCoordinates( data, newPoint_ID, t ) > 2 ) {
		int distance_col = t.whereIsThisColumn( "Distance" );
		double distXA = t( distance_col, 0 ).getDouble();
		double distXB = t( distance_col, 1 ).getDouble();
		double distXC = t( distance_col, 2 ).getDouble();
		Coordinate3D pointA { }, pointB { }, pointC { }, newPoint { };
		int x_col = t.whereIsThisColumn( "Target_X" );
		int y_col = t.whereIsThisColumn( "Target_Y" );
		int z_col = t.whereIsThisColumn( "Target_Z" );
		pointA.x = t( x_col, 0 ).getDouble();
		pointA.y = t( y_col, 0 ).getDouble();
		pointA.z = t( z_col, 0 ).getDouble();
		pointB.x = t( x_col, 1 ).getDouble();
		pointB.y = t( y_col, 1 ).getDouble();
		pointB.z = t( z_col, 1 ).getDouble();
		pointC.x = t( x_col, 2 ).getDouble();
		pointC.y = t( y_col, 2 ).getDouble();
		pointC.z = t( z_col, 2 ).getDouble();
		int id_col = t.whereIsThisColumn( "Target" );
		double distAB = CoordinateTool::getDistanceBetween( data, t( id_col, 0 ).getInt(),
				t( id_col, 1 ).getInt() );
		double distAC = CoordinateTool::getDistanceBetween( data, t( id_col, 0 ).getInt(),
				t( id_col, 2 ).getInt() );
		double distBC = CoordinateTool::getDistanceBetween( data, t( id_col, 1 ).getInt(),
				t( id_col, 2 ).getInt() );
		if( coordinates::calc3D( distAB, distAC, distBC, distXA, distXB, distXC, pointA, pointB,
				pointC, newPoint ) ) {
			editJumpSystemsTable( data, newPoint_ID, newPoint );
			std::ostringstream msg { };
			msg << "[CoordinateTool::generate3D_1(..)] System '" << newPoint_ID
					<< "' coordinates calculated [" << newPoint.x << "," << newPoint.y << ","
					<< newPoint.z << "].";
			Log *log = Log::getInstance();
			log->newEvent( Location::Satellite, EventType::Event, msg );
			return true;
		}
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Satellite, EventType::Event,
				"[CoordinateTool::generate3D_1(..)] Couldn't find enough connected points to calculate coordinates." );
	}
	return false;
}

/**
 * Generate coordinates of point and the attached region
 * @param data Db Object
 * @param newPoint_ID ID of new point
 * @return Success
 */
bool CoordinateTool::generate3D_2( Db &data, const int &newPoint_ID ) {
	if( CoordinateTool::generateCoordinates( data, newPoint_ID ) ) {
		Table t { };
		if( CoordinateTool::pullConnectionsWithNoCoordinates( data, newPoint_ID, t ) > 0 ) {
			int x_col = t.whereIsThisColumn( "Target" );
			int rows = t.getRowCount();
			for( int i = 0; i < rows; i++ ) {
				int point = t( x_col, i ).getInt();
				if( CoordinateTool::generateCoordinates( data, point ) ) {
					Log *log = Log::getInstance();
					std::ostringstream msg { };
					msg << "[CoordinateTool::generate3D_2(..)] Point " << point
							<< "'s coordinates were generated with the help of point "
							<< newPoint_ID << ".";
					log->newEvent( Location::Satellite, EventType::Event, msg );
				}
			}
		}
		return true;
	} else {
		return false;
	}
}

/**
 * Generate coordinates of point from 4 other points
 * @param data Db Object
 * @param newPoint_ID ID of new point
 * @return Success
 */
bool CoordinateTool::generateCoordinates( Db &data, const int &newPoint_ID ) {
	Table t { };
	if( CoordinateTool::pullConnectionsWithCoordinates( data, newPoint_ID, t ) > 3 ) {
		int distance_col = t.whereIsThisColumn( "Distance" );
		double distXA = t( distance_col, 0 ).getDouble();
		double distXB = t( distance_col, 1 ).getDouble();
		double distXC = t( distance_col, 2 ).getDouble();
		double distXD = t( distance_col, 3 ).getDouble();
		Coordinate3D pointA { }, pointB { }, pointC { }, pointD { }, newPoint { };
		int id_col = t.whereIsThisColumn( "Target" );
		int x_col = t.whereIsThisColumn( "Target_X" );
		int y_col = t.whereIsThisColumn( "Target_Y" );
		int z_col = t.whereIsThisColumn( "Target_Z" );
		pointA.x = t( x_col, 0 ).getDouble();
		pointA.y = t( y_col, 0 ).getDouble();
		pointA.z = t( z_col, 0 ).getDouble();
		pointB.x = t( x_col, 1 ).getDouble();
		pointB.y = t( y_col, 1 ).getDouble();
		pointB.z = t( z_col, 1 ).getDouble();
		pointC.x = t( x_col, 2 ).getDouble();
		pointC.y = t( y_col, 2 ).getDouble();
		pointC.z = t( z_col, 2 ).getDouble();
		pointD.x = t( x_col, 3 ).getDouble();
		pointD.y = t( y_col, 3 ).getDouble();
		pointD.z = t( z_col, 3 ).getDouble();
		std::ostringstream oss { };
		oss << "[CoordinateTool::generateCoordinates(..)] Looking for system '" << newPoint_ID << "' with systems '"
				<< t( id_col, 0 ).getString() << "', '"
				<< t( id_col, 1 ).getString() << "', '"
				<< t( id_col, 2 ).getString() << "', '"
				<< t( id_col, 3 ).getString() << "'.";
		Log *log = Log::getInstance();
		log->newEvent( Location::Satellite, EventType::Trace, oss );
		if( coordinates::calcCoordinates( pointA, pointB, pointC, pointD, distXA, distXB, distXC,
				distXD, newPoint ) ) {
			this->editJumpSystemsTable( data, newPoint_ID, newPoint );
			std::ostringstream msg { };
			msg << "[CoordinateTool::generateCoordinates(..)] System '" << newPoint_ID
					<< "' coordinates calculated " << newPoint << ".";
			Log *log = Log::getInstance();
			log->newEvent( Location::Satellite, EventType::Event, msg );
			return true;
		} else {
			std::ostringstream msg { };
			msg << "[CoordinateTool::generateCoordinates(..)] System '" << newPoint_ID
					<< "' calculation failed.";
			Log *log = Log::getInstance();
			log->newEvent( Location::Satellite, EventType::Error, msg );
		}
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Satellite, EventType::Event,
				"[CoordinateTool::generateCoordinates(..)] Couldn't find enough connected points to calculate coordinates." );
	}
	return false;
}

int CoordinateTool::getNumberOfSystemsWithCoords( Db &data ) {
	Log *log = Log::getInstance();
	log->logEvent( Location::Satellite, EventType::Trace,
			"TRACE: [CoordinateTool::getNumberOfSystemsWithCoords]" );
	std::ostringstream oss { };
	oss << "SELECT " << this->systems_table.system_id << " FROM " << this->systems_table.table_name
			<< " WHERE (" << this->systems_table.x_coord << " != \"\" AND "
			<< this->systems_table.y_coord << " != \"\" AND " << this->systems_table.z_coord << ")";
	Table t { };
	return data.sqlQueryPull_typed( oss.str(), t );

}

/**
 * Pulls into given Table all JumpSystems connected to given point with coordinates
 * @param data Db Object
 * @param from_id ID of System
 * @param results Result set {[Target], [Distance], [Target_X], [Target_Y], [Target_Z]}
 * @return number of results
 */
int CoordinateTool::pullConnectionsWithCoordinates( Db &data, const int &from_id, Table &results ) {
	Log *log = Log::getInstance();
	log->logEvent( Location::Satellite, EventType::Trace,
			"TRACE: [CoordinateTool::pullConnectionsWithCoordinates]" );
	std::ostringstream oss { };
	oss << "SELECT JS." << this->systems_table.system_id << " AS Target, JC."
			<< this->distance_table.distance << " AS Distance, JS." << this->systems_table.x_coord
			<< " AS Target_X, JS." << this->systems_table.y_coord << " AS Target_Y, JS."
			<< this->systems_table.z_coord << " AS Target_Z FROM " << this->systems_table.table_name
			<< " JS INNER JOIN ( SELECT * FROM " << this->distance_table.table_name
			<< " Paths WHERE Paths." << this->distance_table.distance << " > 0 AND ( Paths."
			<< this->distance_table.id_A << " = " << from_id << " OR Paths."
			<< this->distance_table.id_B << " = " << from_id << " ) ) JC ON JS."
			<< this->systems_table.system_id << " = JC." << this->distance_table.id_A << " OR JS."
			<< this->systems_table.system_id << " = JC." << this->distance_table.id_B
			<< " WHERE ( ( JS." << this->systems_table.x_coord << " != \"\" OR JS."
			<< this->systems_table.x_coord << " NOT NULL ) AND ( JS." << this->systems_table.y_coord
			<< " != \"\" OR JS." << this->systems_table.y_coord << " NOT NULL ) AND ( JS."
			<< this->systems_table.z_coord << " != \"\" OR JS." << this->systems_table.z_coord
			<< " NOT NULL ) )";
	data.sqlQueryPull_typed( oss.str(), results );
	int distance_col = results.whereIsThisColumn( "Distance" );
	results.sort( distance_col, Table::SortType::ASCENDING );
	return results.getRowCount();
}

/**
 * Pulls into given Table all JumpSystems connected to given point without coordinates
 * @param data Db Object
 * @param from_id ID of System
 * @param results Result set {[Target], [Distance]}
 * @return number of results
 */
int CoordinateTool::pullConnectionsWithNoCoordinates( Db &data, const int &from_id,
		Table &results ) {
	Log *log = Log::getInstance();
	log->logEvent( Location::Satellite, EventType::Trace,
			"TRACE: [CoordinateTool::pullConnectionsWithNoCoordinates]" );
	std::ostringstream oss { };
	oss << "SELECT JS." << this->systems_table.system_id << " AS Target FROM "
			<< this->systems_table.table_name << " JS INNER JOIN ( SELECT * FROM "
			<< this->distance_table.table_name << " Paths WHERE Paths."
			<< this->distance_table.distance << " > 0 AND ( Paths." << this->distance_table.id_A
			<< " = " << from_id << " OR Paths." << this->distance_table.id_B << " = " << from_id
			<< " ) ) JC ON JS." << this->systems_table.system_id << " = JC."
			<< this->distance_table.id_A << " OR JS." << this->systems_table.system_id << " = JC."
			<< this->distance_table.id_B << " WHERE ( ( JS." << this->systems_table.x_coord
			<< " = \"\" OR JS." << this->systems_table.x_coord << " IS NULL ) AND ( JS."
			<< this->systems_table.y_coord << " = \"\" OR JS." << this->systems_table.y_coord
			<< " IS NULL ) AND ( JS." << this->systems_table.z_coord << " = \"\" OR JS."
			<< this->systems_table.z_coord << " IS NULL ) )";
	data.sqlQueryPull_typed( oss.str(), results );
	return results.getRowCount();
}

/**
 * Gets the distance between two points
 * @param data Db Object
 * @param pointA_id ID of 1st point
 * @param pointB_id ID of second point
 * @return The distance or (-1) if there was a problem getting it
 */
double CoordinateTool::getDistanceBetween( Db &data, int pointA_id, int pointB_id ) {
	Table t { };
	std::ostringstream oss { };
	oss << "SELECT * FROM " << this->distance_table.table_name << " WHERE ("
			<< this->distance_table.id_A << " = " << pointA_id << " AND "
			<< this->distance_table.id_B << " = " << pointB_id << ") OR ("
			<< this->distance_table.id_A << " = " << pointB_id << " AND "
			<< this->distance_table.id_B << " = " << pointA_id << ")";
	data.sqlQueryPull_typed( oss.str(), t );
	int distance_col = t.whereIsThisColumn( "Distance" );
	if( distance_col >= 0 ) {
		if( t.getRowCount() > 0 ) {
			return t( distance_col, 0 ).getDouble();
		} else {
			Log *log = Log::getInstance();
			oss.clear();
			oss.str( std::string() );
			oss << "Trying to [CoordinateTool::getDistanceBetween] - No distance found between ID '"
					<< pointA_id << "' and ID '" << pointB_id << "'.";
			log->newEvent( Location::Satellite, EventType::Error, oss );
			return -1;
		}
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Satellite, EventType::Error,
				"Trying to [CoordinateTool::getDistanceBetween] - 'Distance' column doesn't exist in table." );
		return -1;
	}
}

/**
 * Edits the JumpSystems entry
 * @param data Db object
 * @param id System ID
 * @param newPoint Coordinates
 */
void CoordinateTool::editJumpSystemsTable( Db &data, int id, const Coordinate3D &newPoint ) {
	if( data.checkEntryExists( this->systems_table.table_name, this->systems_table.system_id,
			std::to_string( id ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE " << this->systems_table.table_name << " SET " << this->systems_table.x_coord
				<< " = " << newPoint.x << ", " << this->systems_table.y_coord << " = " << newPoint.y
				<< ", " << this->systems_table.z_coord << " = " << newPoint.z << " WHERE "
				<< this->systems_table.system_id << " = " << id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [CoordinateTool::editJumpSystemsTable] - ID doesn't exist. Nothing done." );
	}
}
