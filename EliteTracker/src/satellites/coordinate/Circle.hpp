/*
 * Circle.hpp
 *
 *  Created on: 5 Mar 2015
 *      Author: Alwlyan
 */

#ifndef SATELLITES_COORDINATE_TYPES_CIRCLE_HPP_
#define SATELLITES_COORDINATE_TYPES_CIRCLE_HPP_

#include <algorithm>

#include "../../core/logger/Log.hpp"
#include "../../core/containers/Coordinate2D.hpp"

struct Circle {
		Circle( double x, double y, double r ) {
			this->center.x = x;
			this->center.y = y;
			this->radius = r;
		}
		~Circle() {
		}
		unsigned int intersect( Circle &circle, Coordinate2D<double> &intersect1,
				Coordinate2D<double> &intersect2 ) {
			Log *log = Log::getInstance();
			// distance between the centres
			double distance = Coordinate2D<double>( this->center.x - circle.center.x,
					this->center.y - circle.center.y ).calc_norm();

			// find number of solutions
			if( distance > this->radius + circle.radius ) {
				log->logEvent( Location::Satellite, EventType::Event,
						"[Circle::intersect(..)] Circles are too far apart" );
				return 0;
			} else if( distance == 0 && this->radius == circle.radius ) {
				log->logEvent( Location::Satellite, EventType::Event,
						"[Circle::intersect(..)] Circles coincide" );
				return 0;
			} else if( distance + std::min( this->radius, circle.radius )
					< std::max( this->radius, circle.radius ) ) {
				log->logEvent( Location::Satellite, EventType::Event,
						"[Circle::intersect(..)] One circle contains the other" );
				return 0;
			} else {
				double a { ( this->radius * this->radius - circle.radius * circle.radius
						+ distance * distance ) / ( 2.0 * distance ) };
				double h = sqrt( this->radius * this->radius - a * a );

				// find p2
				Coordinate2D<double> p2(
						( this->center.x + ( a * ( circle.center.x - this->center.x ) ) / distance ),
						this->center.y + ( a * ( circle.center.y - this->center.y ) ) / distance );

				// find intersection points p3
				intersect1.set( ( p2.x + ( h * ( circle.center.y - this->center.y ) / distance ) ),
						( p2.y - ( h * ( circle.center.x - this->center.x ) / distance ) ) );
				intersect2.set( ( p2.x - ( h * ( circle.center.y - this->center.y ) / distance ) ),
						( p2.y + ( h * ( circle.center.x - this->center.x ) / distance ) ) );

				if( distance == this->radius + circle.radius ) {
					return 1;
				}
				return 2;
			}
		}
		double radius;
		Coordinate2D<double> center;
};

#endif /* SATELLITES_COORDINATE_TYPES_CIRCLE_HPP_ */

/*
 * Source:
 * https://gsamaras.wordpress.com/code/determine-where-two-circles-intersect-c/
 */
