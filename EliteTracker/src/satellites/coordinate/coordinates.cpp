/*
 * coordinates.cpp
 *
 *  Created on: 21 Mar 2015
 *      Author: Es A. Davison
 */

#include "coordinates.hpp"

/**
 * Calculates the coordinate of B from the distance from A
 * @param A Point A coordinates
 * @param ab Distance ab
 * @param B Point B coordinates
 * @return Success
 */
bool coordinates::calc1D( const Coordinate3D &A, double ab, Coordinate3D &B ) {
	if( A.isReset() && B.isReset() && ab > 0 ) {
		B.x = ab;
		return true;
	}
	Log *log = Log::getInstance();
	log->logEvent( Location::Satellite, EventType::Error,
			"[coordinates::calc1D(..)] Problem in the inputs." );
	return false;
}

/**
 * Calculates the coordinate of C from the Distance from A and B
 * @param A Point A coordinates
 * @param B Point B coordinates
 * @param ax Distance ax
 * @param bx Distance bx
 * @param X New point coordinates
 * @return Success
 */
bool coordinates::calc2D( const Coordinate3D &A, const Coordinate3D &B, double ax, double bx,
		Coordinate3D &X ) {
	if( ( A.isReset() || B.isReset() ) && X.isReset() && ( ax > 0 && bx > 0 ) ) {
		Circle cA = Circle( A.x, A.y, ax );
		Circle cB = Circle( B.x, B.y, bx );
		Coordinate2D<double> i1 { }, i2 { };
		int intersects = cA.intersect( cB, i1, i2 );
		if( intersects > 0 ) {
			X.x = i1.x;
			i1.y >= 0 ? X.y = i1.y : X.y = i2.y;
			return true;
		} else {
			Log *log = Log::getInstance();
			log->logEvent( Location::Satellite, EventType::Error,
					"[coordinates::calc2D(..)] Couldn't find the intersection of the 2 circles." );
			return false;
		}
	} else {
		std::ostringstream msg { };
		msg << "(coordinates::calc2D(..)) Problem in the inputs [ A" << A << "; B" << B << "; ax = "
				<< ax << "; bx = " << bx << "]";
		Log *log = Log::getInstance();
		log->newEvent( Location::Satellite, EventType::Error, msg );
		return false;
	}
}

/**
 * Calculates the coordinate of a point X with the distances to it that are provided
 * @param ab Distance ab
 * @param ac Distance ac
 * @param bc Distance bc
 * @param ax Distance ax
 * @param bx Distance bx
 * @param cx Distance cx
 * @param A Point A coordinates
 * @param B Point B coordinates
 * @param C Point C coordinates
 * @param X New point coordinates
 * @return Success
 */
bool coordinates::calc3D( double ab, double ac, double bc, double ax, double bx, double cx,
		const Coordinate3D &A, const Coordinate3D &B, const Coordinate3D &C, Coordinate3D &X ) {
	Pair<Coordinate3D> pair = CoreTools::math::spatial::trilaterate( A, B, C, ax, bx, cx );
	pair.a.z >= 0 ? X = pair.a : X = pair.b;
	Log *log = Log::getInstance();
	std::ostringstream msg { };
	msg << "[coordinates::calc3D(..)] Got " << pair.a << " and " << pair.b
			<< " - Unknown point chosen as " << X;
	log->newEvent( Location::Satellite, EventType::Event, msg );
	return !pair.a.error_flag;
}

/**
 * Calculates the coordinate of a point X with the distances to it provided
 * @param A Point A coordinates
 * @param B Point B coordinates
 * @param C Point C coordinates
 * @param D Point D coordinates
 * @param ax Distance ax
 * @param bx Distance bx
 * @param cx Distance cx
 * @param dx Distance dx
 * @param X New point coordinates
 * @return Success
 */
bool coordinates::calcCoordinates( Coordinate3D A, Coordinate3D B, Coordinate3D C, Coordinate3D D,
		double ax, double bx, double cx, double dx, Coordinate3D &X ) {
	Pair<Coordinate3D> pair = CoreTools::math::spatial::trilaterate( A, B, C, ax, bx, cx );
	double d1 = CoreTools::math::spatial::calcDistance( D, pair.a );
	double d2 = CoreTools::math::spatial::calcDistance( D, pair.b );
	Log *log = Log::getInstance();
	if( pair.a.error_flag || pair.b.error_flag ) {
		log->logEvent( Location::Satellite, EventType::Error,
				"[coordinates::calcCoordinates(..)] Unknown point coordinates not found." );
		return false;
	}
	std::ostringstream msg { };
	msg << "[coordinates::calcCoordinates(..)] ";
	double chk_diff_a = fabs( d1 - dx );
	double chk_diff_b = fabs( d2 - dx );
	if( chk_diff_a < chk_diff_b ) {
		if( CoreTools::acceptably_equal( fabs( d1 - dx ), 0, 0.005 ) ) {
			X = pair.a;
			msg << "Got " << pair.a << " and " << pair.b << " - Unknown point calculated as " << X;
			log->newEvent( Location::Satellite, EventType::Event, msg );
			return true;
		}
	} else {
		if( CoreTools::acceptably_equal( fabs( d2 - dx ), 0, 0.005 ) ) {
			X = pair.b;
			msg << "Got " << pair.a << " and " << pair.b << " - Unknown point calculated as " << X;
			log->newEvent( Location::Satellite, EventType::Event, msg );
			return true;
		}
	}
	msg << "Failed. Got " << pair.a << " distance '" << d1 << "' and " << pair.b << " distance '"
			<< d2 << "' when stated distance is " << dx;
	log->newEvent( Location::Satellite, EventType::Error, msg );
	return false;
}

