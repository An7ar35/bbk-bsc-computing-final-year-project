/*
 * coordinate_tool.hpp
 *
 *  Created on: 11 Oct 2014
 *      Author: Alwlyan
 */

#ifndef COORDINATE_TOOL_HPP_
#define COORDINATE_TOOL_HPP_

#include <iostream>

#include "../../core/logger/Log.hpp"
#include "../../core/database/Db.hpp"
#include "../../core/containers/Table.hpp"
#include "../../core/containers/Coordinate3D.hpp"
#include "../../core/database/tables/JumpSystems.hpp"
#include "../../core/database/tables/JumpConnections.hpp"
#include "../../core/fileops/Convert.hpp"
#include "coordinates.hpp"

struct CoordinateTool {
		CoordinateTool( Db &data, std::string jumpConnections, std::string a, std::string b,
				std::string d, std::string jumpSystems, std::string id, std::string x,
				std::string y, std::string z );
		~CoordinateTool();
		bool generateCoordinatesFrom( Db &data, std::string tableID );
	private:
		enum class PointStatus {
			NOT_EXISTS, COORDINATES_KNOWN, COORDINATES_UNKNOWN
		};
		int n;
		bool error_flag;
		CoordinateTool::PointStatus checkPoint( Db &data, const int &point_ID );
		bool generateFrom( Db &data, const std::string &id );
		bool generateFirst( Db &data, const int &newPoint_ID );
		bool generate1D( Db &data, const int &newPoint_ID );
		bool generate2D( Db &data, const int &newPoint_ID );
		bool generate3D_1( Db &data, const int &newPoint_ID );
		bool generate3D_2( Db &data, const int &newPoint_ID );
		bool generateCoordinates( Db &data, const int &newPoint_ID );
		int getNumberOfSystemsWithCoords( Db &data );
		int pullConnectionsWithCoordinates( Db &data, const int &from_id, Table &results );
		int pullConnectionsWithNoCoordinates( Db &data, const int &from_id, Table &results );
		double getDistanceBetween( Db &data, int pointA_id, int pointB_id );
		/**
		 * Information container for the column names
		 * of the database table where distances between
		 * systems are stored in.
		 */
		struct Db_Distance_Table_Info {
				std::string table_name;
				std::string id_A;
				std::string id_B;
				std::string distance;
		} distance_table;
		/**
		 * Information container for the column names
		 * of the database table where systems coordinates
		 * are stored in.
		 */
		struct Db_Systems_Table_Info {
				std::string table_name;
				std::string system_id;
				std::string x_coord;
				std::string y_coord;
				std::string z_coord;
		} systems_table;
		void editJumpSystemsTable( Db &data, int id, const Coordinate3D &newPoint );
};

#endif /* COORDINATE_TOOL_HPP_ */
