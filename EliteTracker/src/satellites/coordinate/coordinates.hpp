/*
 * coordinates.hpp
 *
 *  Created on: 21 Mar 2015
 *      Author: Es A. Davison
 */

#ifndef SATELLITES_COORDINATE_COORDINATES_HPP_
#define SATELLITES_COORDINATE_COORDINATES_HPP_

#include "../../core/logger/Log.hpp"
#include "../../core/containers/Coordinate2D.hpp"
#include "../../core/containers/Coordinate3D.hpp"
#include "../../core/toolbox/toolbox.hpp"
#include "../../core/toolbox/math/trigonometry.hpp"
#include "../../core/toolbox/math/spatial.hpp"
#include "Circle.hpp"

namespace coordinates {
	bool calc1D( const Coordinate3D &A, double distanceAB, Coordinate3D &B );
	bool calc2D( const Coordinate3D &A, const Coordinate3D &B, double distanceAX, double distanceBX,
			Coordinate3D &X );
	bool calc3D( double ab, double ac, double bc, double ax, double bx, double cx,
			const Coordinate3D &A, const Coordinate3D &B, const Coordinate3D &C,
			Coordinate3D &newPoint );
	bool calcCoordinates( Coordinate3D A, Coordinate3D B, Coordinate3D C, Coordinate3D D, double ax,
			double bx, double cx, double dx, Coordinate3D &newPoint );
}

#endif /* SATELLITES_COORDINATE_COORDINATES_HPP_ */
