/*
 * main.cpp
 *
 *  Created on: 30 Aug 2014
 *      Author: Alwlyan
 */

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <iomanip>

#include "../lib/sqlite3.h"

#include "core/fileops/Settings.hpp"
#include "core/fileops/init.hpp"

#include "core/database/Db.hpp"
#include "core/database/tables/JumpSystems.hpp"
#include "core/database/tables/PlayerShips.hpp"
#include "core/logger/Log.hpp"
#include "core/containers/Table.hpp"
#include "core/toolbox/toolbox.hpp"
#include "core/toolbox/math/spatial.hpp"
#include "core/platform/windows.hpp"
#include "satellites/pathfinder/Pathfinder.hpp"
#include "satellites/coordinate/coordinate_tool.hpp"
#include "satellites/pathfinder/Matrix.hpp"

#include "../unit_tests/equalish.hpp"
/**
 * source: http://ee.usc.edu/~redekopp/cs102/L14_STL.pdf (01/09/14)
 * http://www.youtube.com/watch?v=LnAuAJfMRy8 (15/09/14)
 */

void launchCayley() {
	std::string path = "E:\\Git\\cayley_0.4.0_windows_amd64\\cayley.exe";
	platform::windows::launchapp( path );
}

int main() {
	Log *log = Log::getInstance();
	log->setFileName( "EliteTrackerLog.txt" );
	log->switch_autoFileDump( true );
	//log->switch_EventType( EventType::Event, false );
	//log->switch_EventType( EventType::Trace, false );
	//log->switch_EventType( EventType::Debug, false );
	fileIO::Settings config = fileIO::Settings( "settings.cfg" );

	Db data( "elitedata.db" );
	data.open();
	int ship_id = editTable::JumpSystems::getIdFor( data, "26 Draconis" );
	std::cout << ship_id << std::endl;

	/*
	 Coordinate3D A_temp = Coordinate3D( 5,7,8 );
	 Coordinate3D C_temp = Coordinate3D( 9,9,10 );
	 Coordinate3D B_temp = Coordinate3D( 6,10,10 );
	 double ax = 2.4494897;
	 double bx = 4.1231056;
	 double cx = 1.4142135;
	 std::cout << "Pre-order " << A_temp << " " << B_temp << " " << C_temp << std::endl;
	 //Order the points on so that ab is the hypotenuse
	 CoreTools::math::spatial::orderPoints( A_temp, B_temp, C_temp, ax, bx, cx );
	 std::cout << "Post-order " << A_temp << " " << B_temp << " " << C_temp << std::endl;
	 //Calculate distances
	 double ab = CoreTools::math::spatial::calcDistance( A_temp, B_temp );
	 double ac = CoreTools::math::spatial::calcDistance( A_temp, C_temp );
	 double bc = CoreTools::math::spatial::calcDistance( B_temp, C_temp );
	 //Transform step.1 - transform so that point A is @ (0,0,0) [i.e.: shift everything]
	 Coordinate3D shift = CoreTools::math::spatial::calcDistanceXYZ( Coordinate3D( 0, 0, 0 ),
	 A_temp );
	 CoreTools::math::spatial::transform3D_add( A_temp, shift );
	 CoreTools::math::spatial::transform3D_add( B_temp, shift );
	 CoreTools::math::spatial::transform3D_add( C_temp, shift );
	 std::cout << "1. " << A_temp << " " << B_temp << " " << C_temp << std::endl;
	 //Transform step.2 - transform so that point B is @ (ab,0,0) [i.e.: rotate on z axis and y axis]
	 double angle_z = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::X,
	 CoreTools::math::spatial::PLANE::XY, B_temp );
	 std::cout << "Z rot: " << angle_z << std::endl;
	 if( B_temp.y > 0 ) {
	 angle_z = -angle_z;
	 }
	 CoreTools::math::spatial::transform3D_rotate( B_temp, CoreTools::math::spatial::AXIS::Z,
	 angle_z );
	 CoreTools::math::spatial::transform3D_rotate( C_temp, CoreTools::math::spatial::AXIS::Z,
	 angle_z );
	 std::cout << "2.1 " << A_temp << " " << B_temp << " " << C_temp << std::endl;
	 double angle_y = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::X,
	 CoreTools::math::spatial::PLANE::XZ, B_temp );
	 std::cout << "Y rot: " << angle_y << std::endl;
	 if( B_temp.z > 0 ) {
	 angle_y = -angle_y;
	 }
	 CoreTools::math::spatial::transform3D_rotate( B_temp, CoreTools::math::spatial::AXIS::Y,
	 angle_y );
	 CoreTools::math::spatial::transform3D_rotate( C_temp, CoreTools::math::spatial::AXIS::Y,
	 angle_y );
	 std::cout << "2.2 " << A_temp << " " << B_temp << " " << C_temp << std::endl;
	 //Transform step.3 - transform so that point C is flat on the XY plane along with A & B [i.e.: rotate on X axis]
	 double angle_x = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Y,
	 CoreTools::math::spatial::PLANE::YZ, C_temp );
	 std::cout << "X rot: " << angle_x << std::endl;
	 if( C_temp.z > 0 ) {
	 angle_x = -angle_x;
	 }
	 CoreTools::math::spatial::transform3D_rotate( C_temp, CoreTools::math::spatial::AXIS::X,
	 angle_x );
	 std::cout << "3. " << A_temp << " " << B_temp << " " << C_temp << std::endl;
	 //Trilaterate to find point X
	 Pair<Coordinate3D> pair = CoreTools::math::spatial::trilaterate( ab, ac, bc, ax, bx, cx );
	 //Transform X by revering order on the steps
	 CoreTools::math::spatial::transform3D_rotate( pair.a, CoreTools::math::spatial::AXIS::X,
	 -angle_x );
	 CoreTools::math::spatial::transform3D_rotate( pair.b, CoreTools::math::spatial::AXIS::X,
	 -angle_x );
	 CoreTools::math::spatial::transform3D_rotate( pair.a, CoreTools::math::spatial::AXIS::Y,
	 -angle_y );
	 CoreTools::math::spatial::transform3D_rotate( pair.b, CoreTools::math::spatial::AXIS::Y,
	 -angle_y );
	 CoreTools::math::spatial::transform3D_rotate( pair.a, CoreTools::math::spatial::AXIS::Z,
	 -angle_z );
	 CoreTools::math::spatial::transform3D_rotate( pair.b, CoreTools::math::spatial::AXIS::Z,
	 -angle_z );
	 CoreTools::math::spatial::transform3D_substract( pair.a, shift );
	 CoreTools::math::spatial::transform3D_substract( pair.b, shift );
	 */
	/*
	 Db testdata( "coord.db" );
	 testdata.open();
	 CoordinateTool ctool = CoordinateTool( testdata, "JumpConnections", "S_ID_A", "S_ID_B",
	 "Distance", "JumpSystems", "S_ID", "X_Coordinate", "Y_Coordinate",
	 "Z_Coordinate" );
	 ctool.generateCoordinatesFrom( testdata, "5" );
	 Table t { };
	 testdata.sqlQueryPull_typed( "SELECT * FROM JumpSystems", t );
	 t.printToConsole();
	 */

	//init::fullInitialisation( config );
	/*
	 //launchCayley();
	 */
	log->logEvent( Location::Core, EventType::Trace, "Program ends." );
	std::cout << "�> There are " << log->logSize() << " log events." << std::endl;
	//log->dumpToFile( "EliteTrackerLog.txt", Log::FileOption::APPEND );
	return 0;
}

