/*
 * properties.hpp
 *
 *  Created on: 14 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_LOGGER_PROPERTIES_HPP_
#define CORE_LOGGER_PROPERTIES_HPP_

enum Location {
	Core, SQLite, IO, Satellite, GUI, OS, LOC_ENUM_SIZE
};
enum EventType {
	Error, Event, Trace, Debug, ETYPE_ENUM_SIZE
};

#endif /* CORE_LOGGER_PROPERTIES_HPP_ */
