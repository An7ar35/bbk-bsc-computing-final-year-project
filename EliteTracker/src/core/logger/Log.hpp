/*
 * log.hpp
 *
 *  Created on: 9 Sep 2014
 *      Author: Alwlyan
 */

#ifndef LOG_HPP_
#define LOG_HPP_

#include <iostream>
#include <sstream>
#include <fstream>
#include <queue>
#include "properties.hpp"
#include "Logmsg.hpp"

class Log {
	public:
		enum class FileOption {
			APPEND, OVERWRITE
		};
		~Log();
		static Log* getInstance();
		void setFileName( std::string fileName );
		static void logEvent( Location nLoc, EventType nLevel, const char *description );
		void newEvent( Location nLoc, EventType nLevel, std::ostringstream &msgText );
		std::string grabEvent( int &counter );
		bool isEmpty();
		int logSize();
		void switch_EventType( EventType nLevel, bool state );
		void switch_Location( Location nLoc, bool state );
		void switch_autoFileDump( bool state );
		bool getSwitchState( Location nLoc );
		bool getSwitchState( EventType nLevel );
		void dumpToFile( const std::string fileName, FileOption option );
	private:
		Log();
		struct LogFile {
				bool checked;
				std::string name;
		} logFile;
		static bool instanceFlag;
		static Log *log;
		bool *logFlag_Location;
		bool *logFlag_EventType;
		bool autoFileDump;
		long int log_counter;
		void addToLog( LogMsg &m );
		std::queue<LogMsg> logQueue;
		void dumpSingleToFile( FileOption option, LogMsg &msg );
};

#endif /* LOG_HPP_ */

/*
 * Sources:
 * http://stackoverflow.com/questions/511768/how-to-use-my-logging-class-like-a-std-c-stream
 * http://blog.instance-factory.com/?p=181
 * http://www.codeproject.com/Articles/288827/g-log-An-efficient-asynchronous-logger-using-Cplus#TOC_part_1
 */
