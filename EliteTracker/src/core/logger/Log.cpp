/*
 * log.cpp
 *
 *  Created on: 8 Nov 2014
 *      Author: Alwlyan
 */

#include "Log.hpp"

bool Log::instanceFlag { false };
Log* Log::log = NULL;

/**
 * Log constructor
 */
Log::Log() {
	this->logFlag_Location = new bool[ Location::LOC_ENUM_SIZE ] { };
	std::fill_n( this->logFlag_Location, Location::LOC_ENUM_SIZE, true );
	this->logFlag_EventType = new bool[ EventType::ETYPE_ENUM_SIZE ] { };
	std::fill_n( this->logFlag_EventType, EventType::ETYPE_ENUM_SIZE, true );
	this->autoFileDump = false;
	this->logFile.checked = false;
	log_counter = 0;
}

/**
 * Log destructor
 */
Log::~Log() {
	this->instanceFlag = false;
}

/**
 * Get the instance of the Log singleton class
 * @return
 */
Log* Log::getInstance() {
	if( !instanceFlag ) {
		log = new Log { };
		instanceFlag = true;
		return log;
	} else {
		return log;
	}
}

/**
 * Sets the name of the Log file
 * @param fileName File name
 */
void Log::setFileName( std::string fileName ) {
	if( fileName.length() > 0 ) {
		this->logFile.name = fileName;
		this->logFile.checked = true;
	}
}

/**
 * Log an event (Universal)
 * @param description Description of the event
 */
void Log::logEvent( Location nLoc, EventType nLevel, const char *description ) {
	std::ostringstream oss { };
	oss << description;
	Log *log = Log::getInstance();
	log->newEvent( nLoc, nLevel, oss );
}

/**
 * Creates new event in the log if the location and the level are set to true
 * @param nLoc Location of event
 * @param nLevel Type of event
 * @param msgText Description of event
 */
void Log::newEvent( Location nLoc, EventType nLevel, std::ostringstream &msgText ) {
	if( this->logFlag_EventType[ nLevel ] && this->logFlag_Location[ nLoc ] ) {
		LogMsg m( nLoc, nLevel, msgText );
		this->addToLog( m );
		if( autoFileDump && logFile.checked ) {
			log->dumpSingleToFile( Log::FileOption::APPEND, m );
		}
	}
}

/**
 * Gets event from the log queue
 * @return Event as a string
 */
std::string Log::grabEvent( int &counter ) {
	if( this->logQueue.empty() ) {
		return "No more log items found in queue..";
	} else {
		LogMsg m = this->logQueue.front();
		this->logQueue.pop();
		if( this->logFlag_Location[ m.getLocation() ]
				&& this->logFlag_EventType[ m.getEventType() ] ) {
			counter++;
			return m.getInfo() + "\n";
		} else {
			return "";
		}
	}
}

/**
 * Check if log queue is empty
 * @return Empty state
 */
bool Log::isEmpty() {
	return this->logQueue.empty();
}

/**
 * Gets the number of log events in the log queue
 * @return The number of events stored
 */
int Log::logSize() {
	return this->log_counter;
}

/**
 * Switch for the EventType logging output
 * @param nLevel Event type
 * @param state State (true/false)
 */
void Log::switch_EventType( EventType nLevel, bool state ) {
	std::cout << "Switching " << LogMsg::eventType[ nLevel ] << " to " << state << std::endl;
	this->logFlag_EventType[ nLevel ] = state;
}

/**
 * Switch for the Location logging output
 * @param nLoc Location
 * @param state State (true/false)
 */
void Log::switch_Location( Location nLoc, bool state ) {
	std::cout << "Switching " << state << " " << LogMsg::location[ nLoc ] << std::endl;
	this->logFlag_Location[ nLoc ] = state;
}

/**
 * Switch for the auto file dump functionality
 * @param state State (true/false)
 */
void Log::switch_autoFileDump( bool state ) {
	std::cout << "Switching to " << state << " for auto file dump." << std::endl;
	this->autoFileDump = state;
}

/**
 * Gets the switch state for a Location
 * @param nLoc Location
 * @return State
 */
bool Log::getSwitchState( Location nLoc ) {
	return this->logFlag_Location[ nLoc ];
}

/**
 * Gets the switch state for an EventType
 * @param nLevel EventType
 * @return State
 */
bool Log::getSwitchState( EventType nLevel ) {
	return this->logFlag_EventType[ nLevel ];
}

/**
 * Add a LogMsg to the Log
 * @param m LogMsg object
 */
void Log::addToLog( LogMsg &m ) {
	this->logQueue.emplace( m );
	this->log_counter++;
}

/**
 * Dumps the entire log queue into the log file specified
 * @param fileName Name of the log file
 * @param option File write option (append/overwrite)
 */
void Log::dumpToFile( const std::string fileName, FileOption option ) {
	int counter { 0 };
	std::ofstream logFile { };
	if( option == FileOption::APPEND ) {
		logFile.open( fileName.c_str(), std::ios::out | std::ios::ate | std::ios::app );
		while( !this->log->isEmpty() ) {
			logFile << log->grabEvent( counter );
		}
	} else if( option == FileOption::OVERWRITE ) {
		logFile.open( fileName.c_str(), std::ofstream::out | std::ofstream::trunc );
		while( !this->log->isEmpty() ) {
			logFile << log->grabEvent( counter );
		}
	}
	logFile.close();
	std::cout << "�> There were " << counter << " log events recorded in file." << std::endl;
}

/**
 * Writes a single Log entry to a file
 * @param fileName Name of the log file
 * @param option File write option (append/overwrite)
 * @param msg Log event
 */
void Log::dumpSingleToFile( FileOption option, LogMsg &m ) {
	std::ofstream logFile { };
	if( option == FileOption::APPEND ) {
		logFile.open( this->logFile.name.c_str(), std::ios::out | std::ios::ate | std::ios::app );
		logFile << m.getInfo() + "\n";
	} else if( option == FileOption::OVERWRITE ) {
		logFile.open( this->logFile.name.c_str(), std::ofstream::out | std::ofstream::trunc );
		logFile << m.getInfo() + "\n";
	}
	logFile.close();
}
