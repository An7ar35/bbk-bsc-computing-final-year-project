/*
 * logmsg.hpp
 *
 *  Created on: 4 Nov 2014
 *      Author: Alwlyan
 */

#ifndef LOGMSG_HPP_
#define LOGMSG_HPP_

#include <ctime>
#include <iostream>
#include <sstream>
#include <ctime>
#include "properties.hpp"

class LogMsg {
	public:
		LogMsg( Location nLoc, EventType nLevel, std::ostringstream &msgText );
		~LogMsg();
		friend std::ostream &operator <<( std::ostream & stream, const LogMsg &logMsg ) {
			return stream << logMsg.timeStamp << " [" << logMsg.location[ logMsg.nLoc ] << "."
					<< logMsg.eventType[ logMsg.nLevel ] << "] " << logMsg.msgText;
		}
		Location getLocation();
		EventType getEventType();
		std::string getInfo();
		static const std::string location[];
		static const std::string eventType[];
	private:
		std::string timeStamp;
		std::string msgText;
		Location nLoc;
		EventType nLevel;
		std::string getTimeStamp();
};

#endif /* LOGMSG_HPP_ */
