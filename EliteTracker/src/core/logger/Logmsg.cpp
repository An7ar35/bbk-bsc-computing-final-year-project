/*
 * logmsg.cpp
 *
 *  Created on: 5 Nov 2014
 *      Author: Alwlyan
 */

#include "logmsg.hpp"

const std::string LogMsg::location[] = { "Core", "SQLite", "IO", "Satellite", "GUI", "OS" };
const std::string LogMsg::eventType[] = { "Error", "Event", "Trace", "Debug" };

/**
 * Constructor for the 'LogMsg' class
 * @param nLoc Location of event
 * @param nLevel Type of event
 * @param msgText Information about the event
 */
LogMsg::LogMsg( Location nLoc, EventType nLevel, std::ostringstream &msgText ) {
	this->timeStamp = this->getTimeStamp();
	this->msgText = msgText.str();
	msgText.flush();
	msgText.str( std::string() );
	this->nLevel = nLevel;
	this->nLoc = nLoc;
}

/**
 * Destructor for the 'LogMsg' class
 */
LogMsg::~LogMsg() {

}

/**
 * Gets the location of the log event
 * @return The Location
 */
Location LogMsg::getLocation() {
	return this->nLoc;
}

/**
 * Gets the type of the log event
 * @return The EventType
 */
EventType LogMsg::getEventType() {
	return this->nLevel;
}

/**
 * Gives a log line with all the details of the object
 * @return Log line as a string
 */
std::string LogMsg::getInfo() {
	std::stringstream ss { };
	ss << this->timeStamp << " [" << this->location[ this->nLoc ] << "."
			<< this->eventType[ this->nLevel ] << "] " << this->msgText;
	return ss.str();
}

///*********************************************************************
/// Internal (private) functions
///*********************************************************************#
/**
 * Creates a formatted timestamp
 * @return The formatted timestamp
 */
std::string LogMsg::getTimeStamp() {
	time_t now = time( 0 );
	tm *ltm = localtime( &now );
	/**
	 * [Lambda] Pads int variable with a '0' if smaller than 10
	 */
	auto pad = []( int variable ) {
		if( variable < 10 ) {
			return "0" + std::to_string( variable );
		} else {
			return std::to_string( variable );
		}
	};
	std::stringstream ss { };
	ss << std::string( pad( ltm->tm_mday ) ) << "/" << std::string( pad( 1 + ltm->tm_mon ) ) << "/"
			<< ( 1900 + ltm->tm_year ) << " - " << std::string( pad( ltm->tm_hour ) ) << ":"
			<< std::string( pad( ltm->tm_min ) ) << ":" << std::string( pad( ltm->tm_sec ) );
	return ss.str();
}
