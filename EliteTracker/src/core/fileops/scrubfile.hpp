/*
 * scrubfile.hpp
 *
 *  Created on: 1 Oct 2014
 *      Author: Alwlyan
 */

#ifndef SCRUBFILE_HPP_
#define SCRUBFILE_HPP_

#include <iostream>
#include <vector>
#include <fstream>
#include <string>
#include <sstream>

#include "fileops_utils.hpp"
#include "../toolbox/toolbox.hpp"

#include "../database/tables/JumpSystems.hpp"
#include "../database/tables/Manufacturers.hpp"
#include "../database/tables/ShipClasses.hpp"
#include "../database/tables/Ships.hpp"
#include "../database/tables/Stations.hpp"

namespace loadBaseData {
	void load_Locations( std::string fileName, Db & data ) {
		int n = fileIO::getNumberOfLines( fileName );
		if( n < 1 ) {
			return;
		}
		std::string currentFile { };
		std::fstream file { };
		file.open( fileName.c_str(), std::ios::in );
		std::string line { };
		int lineCount { 0 };
		while( !file.eof() ) {
			std::getline( file, line );
			std::string s { };
			std::istringstream ss( line );
			std::string container[ 4 ] { };
			int i { 0 };
			while( std::getline( ss, container[ i ], ',' ) ) {
				i++;
			}
			double x = 0;
			double y = 0;
			double z = 0;
			utils::stringToDouble( container[ 1 ], x );
			utils::stringToDouble( container[ 2 ], y );
			utils::stringToDouble( container[ 3 ], z );
			editTable::JumpSystems::add( data, 0, container[ 0 ], x, y, z );
			lineCount++;
			CoreTools::progressBar_console( lineCount, n, 50 );
		}
		std::cout << "  -> found " << lineCount << " locations\n" << std::endl;
		file.close();
	}

	void load_Manufacturers( std::string fileName, Db &data ) {
		int n = fileIO::getNumberOfLines( fileName );
		if( n < 1 ) {
			return;
		}
		std::string currentFile { };
		std::fstream file { };
		file.open( fileName.c_str(), std::ios::in );
		std::string line { };
		int lineCount { 0 };
		while( !file.eof() ) {
			std::getline( file, line );
			std::string s { };
			std::istringstream ss( line );
			std::string container[ 1 ] { };
			int i { 0 };
			while( std::getline( ss, container[ i ], ',' ) ) {
				i++;
			}
			editTable::Manufacturers::add( data, container[ 0 ] );
			lineCount++;
			CoreTools::progressBar_console( lineCount, n, 50 );
		}
		std::cout << "  -> found " << lineCount << " manufacturers\n" << std::endl;
		file.close();
	}

	void load_ShipClasses( std::string fileName, Db &data ) {
		int n = fileIO::getNumberOfLines( fileName );
		if( n < 1 ) {
			return;
		}
		std::string currentFile { };
		std::fstream file { };
		file.open( fileName.c_str(), std::ios::in );
		std::string line { };
		int lineCount { 0 };
		while( !file.eof() ) {
			std::getline( file, line );
			std::string s { };
			std::istringstream ss( line );
			std::string container[ 1 ] { };
			int i { 0 };
			while( std::getline( ss, container[ i ], ',' ) ) {
				i++;
			}
			editTable::ShipClasses::add( data, container[ 0 ] );
			lineCount++;
			CoreTools::progressBar_console( lineCount, n, 50 );
		}
		std::cout << "  -> found " << lineCount << " ship classes\n" << std::endl;
		file.close();
	}

	void load_Ships( std::string fileName, Db &data ) {
		int n = fileIO::getNumberOfLines( fileName );
		if( n < 1 ) {
			return;
		}
		std::string currentFile { };
		std::fstream file { };
		file.open( fileName.c_str(), std::ios::in );
		std::string line { };
		int lineCount { 0 };
		while( !file.eof() ) {
			std::getline( file, line );
			std::string s { };
			std::istringstream ss( line );
			std::string container[ 9 ] { };
			int i { 0 };
			while( std::getline( ss, container[ i ], ',' ) ) {
				i++;
			}
			int classID { 0 };
			int manID { 0 };
			double fuelCap = 0;
			int maxCargo { 0 };
			double jumpFull = 0;
			double jumpEmpty = 0;
			int maxSpeed { 0 };
			int boost { 0 };
			utils::stringToInt( container[ 1 ], classID );
			utils::stringToInt( container[ 2 ], manID );
			utils::stringToDouble( container[ 3 ], fuelCap );
			utils::stringToInt( container[ 4 ], maxCargo );
			utils::stringToDouble( container[ 5 ], jumpFull );
			utils::stringToDouble( container[ 6 ], jumpEmpty );
			utils::stringToInt( container[ 7 ], maxSpeed );
			utils::stringToInt( container[ 8 ], boost );
			editTable::Ships::add( data, container[ 0 ], classID, manID, fuelCap, maxCargo,
					jumpFull, jumpEmpty, maxSpeed, boost );
			lineCount++;
			CoreTools::progressBar_console( lineCount, n, 50 );
		}
		std::cout << "  -> found " << lineCount << " ships\n" << std::endl;
		editTable::PlayerShips::add( data, 1 );
		std::cout << "  -> Added default player ship" << std::endl;
		file.close();
	}

	void load_Stations( std::string fileName, Db &data ) {
		int n = fileIO::getNumberOfLines( fileName );
		if( n < 1 ) {
			return;
		}
		std::string currentFile { };
		std::fstream file { };
		file.open( fileName.c_str(), std::ios::in );
		std::string line { };
		int lineCount { 0 };
		while( !file.eof() ) {
			std::getline( file, line );
			std::string s { };
			std::istringstream ss( line );
			std::string container[ 2 ] { };
			int i { 0 };
			while( std::getline( ss, container[ i ], ',' ) ) {
				i++;
			}
			int s_id { };
			utils::stringToInt( container[ 0 ], s_id );
			editTable::Stations::add( data, s_id, container[ 1 ] );
			lineCount++;
			CoreTools::progressBar_console( lineCount, n, 50 );
		}
		std::cout << "  -> found " << lineCount << " Stations\n" << std::endl;
		file.close();
	}
}

#endif /* SCRUBFILE_HPP_ */
