/*
 * fileops_utils.hpp
 *
 *  Created on: 17 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_FILEOPS_FILEOPS_UTILS_HPP_
#define CORE_FILEOPS_FILEOPS_UTILS_HPP_

#include <iostream>
#include <sstream>

#include "../logger/log.hpp"

namespace fileIO {
	/**
	 * Gets the number of lines inside a file
	 * @param fileName File to check
	 * @return Number of lines
	 */
	static int getNumberOfLines( std::string &fileName ) {
		int number_of_lines { 0 };
		std::string line { };
		std::ifstream myfile( fileName );
		while( std::getline( myfile, line ) ) {
			++number_of_lines;
		}
		return number_of_lines;
	}
}


#endif /* CORE_FILEOPS_FILEOPS_UTILS_HPP_ */
