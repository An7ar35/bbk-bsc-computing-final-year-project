/*
 \ * settings.hpp
 *
 *  Created on: 15 Feb 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_FILEOPS_SETTINGS_HPP_
#define CORE_FILEOPS_SETTINGS_HPP_

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include <map>
#include <iterator>

#include "Convert.hpp"
#include "fileops_utils.hpp"
#include "../../core/logger/Log.hpp"

namespace fileIO {
	class Settings {
		public:
			Settings( std::string configFile );
			~Settings();
			void readFile();
			void writeFile();
			/**
			 * Writes into the configuration the given key-value pair
			 * @param key Key
			 * @param value Value
			 * @return Success
			 */
			template<typename T> bool writeConfig( std::string key, T value ) {
				if( this->keyExists( key ) ) {
					this->config_content.find( key )->second = Convert::to_string( value );
				} else {
					this->config_content.insert(
							std::pair<std::string, std::string>( key,
									Convert::to_string( value ) ) );
				}
				writeFile();
				return true;
			}
			/**
			 * Gets the Value of a specified Key
			 * @param key Key
			 * @param defaultValue Default value of the key
			 * @return Value
			 */
			template<typename T> T getValueOfKey( std::string key, T defaultValue ) {
				readFile();
				if( this->keyExists( key ) ) {
					return Convert::string_to_type<T>(
							this->config_content.find( key )->second );
				} else {
					return defaultValue;
				}
			}

		private:
			std::map<std::string, std::string> config_content;
			std::string fileName;
			//Private methods
			void extractContents( const std::string &line );
			void parseLine( const std::string &line, size_t const lineNumber );
			void removeComment( std::string &line ) const;
			bool onlyWhitespace( const std::string &line ) const;
			bool validLine( const std::string &line ) const;
			/**
			 * Checks if a key exists in the config_content map
			 * @param key Key to check
			 * @return Its existence
			 */
			bool keyExists( const std::string &key ) {
				return this->config_content.find( key ) != this->config_content.end();
			}
	};
}

#endif /* CORE_FILEOPS_SETTINGS_HPP_ */

/*
 * Source
 * http://www.dreamincode.net/forums/topic/183191-create-a-simple-configuration-file-parser/
 */
