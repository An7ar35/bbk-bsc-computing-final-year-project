/*
 * Convert.hpp
 *
 *  Created on: 27 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_FILEOPS_CONVERT_HPP_
#define CORE_FILEOPS_CONVERT_HPP_

#include <iostream>
#include <sstream>
#include <typeinfo>

#include "../logger/log.hpp"
#include "../toolbox/toolbox.hpp"

struct Convert {
	public:
		/**
		 * Converts T to a string
		 * @param value T value
		 * @return The value as a string
		 */
		template<typename T> static std::string to_string( T const &value ) {
			return To_String<T>::convert( value );
		}
		/**
		 * Converts string to T
		 * @param value String value
		 * @return The value as a T type
		 */
		template<typename T> static T string_to_type( std::string const &value ) {
			return StringToType<T>::convert( value );
		}
	private:
		template<typename T> struct To_String {
				static std::string convert( T const &value ) {
					std::stringstream ss { };
					ss << value;
					return ss.str();
				}
		};
		template<typename T> struct StringToType {
				static T convert( std::string const &value ) {
					std::stringstream ss { value };
					T convertedValue { };
					if( !( ss >> convertedValue ) ) {
						std::ostringstream msg { };
						msg << "[fileIO::settings::Convert::string_to_type(..)] Not a valid "
								<< static_cast<std::string>( typeid(T).name() ) << " received.";
						Log *log = Log::getInstance();
						log->newEvent( Location::IO, EventType::Error, msg );
						return T();
					}
					return convertedValue;
				}
		};
};

/**
 * To_String Specialisations
 */
template<> struct Convert::To_String<double> {
		static std::string convert( double const &value ) {
			std::stringstream ss { };
			ss.precision( CoreTools::getPrecision( value ) );
			ss << value;
			return ss.str();
		}
};
template<> struct Convert::To_String<long double> {
		static std::string convert( long double const &value ) {
			std::stringstream ss { };
			ss.precision( CoreTools::getPrecision( value ) );
			ss << value;
			return ss.str();
		}
};
/**
 * StringToType Specialisations
 */
template<> struct Convert::StringToType<std::string> {
		static std::string convert( std::string const &value ) {
			return value;
		}
};

#endif /* CORE_FILEOPS_CONVERT_HPP_ */
