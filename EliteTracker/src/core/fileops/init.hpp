/*
 * init.hpp
 *
 *  Created on: 15 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_FILEOPS_INIT_HPP_
#define CORE_FILEOPS_INIT_HPP_

/*
 * TODO
 * init file
 * creates default database & settings.ini if not present
 *
 */
#include <iostream>

#include "../database/db.hpp"
#include "../database/editTables.hpp"
#include "../toolbox/toolbox.hpp"
#include "../logger/log.hpp"
#include "../database/utils.hpp"
#include "settings.hpp"
#include "scrubfile.hpp"

namespace init {
	void setupConfigFile( fileIO::Settings &config ) {
		config.writeConfig<int>( "CurrentPlayerShip", 1 );
	}
	void setupBaseData() {
		Db data( "elitedata.db" );
		if( data.open() ) {
			std::cout
					<< "> [INIT] Creating tables in database, Copying data from 'jumpLocations.txt' & calculating distances.."
					<< std::endl;
			editTable::createAllTables( data );
			loadBaseData::load_Locations( "resources/jumpLocations.txt", data );
			TABLE JumpSystems { };
			typedef std::vector<std::string> rows;
			data.sqlQueryPull( "SELECT Name FROM JumpSystems", JumpSystems );
			int counter { 1 };
			for( rows::size_type i = 0; i < JumpSystems[ 0 ].size(); i++ ) {
				for( rows::size_type j = i; j < JumpSystems[ 0 ].size(); j++ ) {
					CoreTools::progressBar_console( counter, 1540, 50 );
					if( !( i == j ) ) {
						std::string desc = "Applying distance between " + JumpSystems[ 0 ][ i ]
								+ " and " + JumpSystems[ 0 ][ j ];
						Log::logEvent( Location::Core, EventType::Trace, desc.c_str() );
						std::string a = JumpSystems[ 0 ][ i ];
						std::string b = JumpSystems[ 0 ][ j ];
						editTable::JumpConnections::add( data, a, b );
					}
					counter++;
				}
			}
			std::cout << "> Distance calculation finished." << std::endl;
			loadBaseData::load_Manufacturers( "resources/manufacturers.txt", data );
			loadBaseData::load_ShipClasses( "resources/shipclasses.txt", data );
			loadBaseData::load_Ships( "resources/ships.txt", data );
			loadBaseData::load_Stations( "resources/stations.txt", data );
			data.close();
			std::cout << "> Basic first-time setup finished." << std::endl;
		}
	}
	void fullInitialisation( fileIO::Settings &config ) {
		init::setupConfigFile( config );
		init::setupBaseData();
	}
}

#endif /* CORE_FILEOPS_INIT_HPP_ */
