/*
 * settings.cpp
 *
 *  Created on: 17 Feb 2015
 *      Author: Es A. Davison
 */

#include "settings.hpp"

/**
 * Constructor
 * @param configFile Name of the configuration file
 */
fileIO::Settings::Settings( std::string configFile ) {
	this->fileName = configFile;
	readFile();
}

/**
 * Destructor
 */
fileIO::Settings::~Settings() {
}

/**
 * Reads in from the configuration file and stores found key-value pairs
 */
void fileIO::Settings::readFile() {
	this->config_content.clear();
	if( !std::ifstream( this->fileName ) ) {
		std::ofstream file( this->fileName );
		if( !file ) {
			Log *log = Log::getInstance();
			log->logEvent( Location::IO, EventType::Error,
					"(fileIO::Settings::readFile) Could not create new file." );
		}
	}
	std::ifstream file { };
	file.open( this->fileName );
	if( !file ) {
		Log *log = Log::getInstance();
		std::ostringstream msg { };
		msg << "[fileIO::Settings::readConfigFile()] File '" << this->fileName
				<< "' could not be found.";
		log->newEvent( Location::IO, EventType::Error, msg );
	}
	std::string line { };
	size_t lineNumber = 0;
	while( std::getline( file, line ) ) {
		lineNumber++;
		std::string temp = line;
		if( !temp.empty() ) {
			removeComment( temp );
			if( !onlyWhitespace( temp ) ) {
				parseLine( temp, lineNumber );
			}
		}
	}
	file.close();
}

/**
 * Writes config_content to file
 */
void fileIO::Settings::writeFile() {
	std::ofstream file { };
	file.open( this->fileName );
	typedef std::map<std::string, std::string>::iterator iterator_type;
	for( iterator_type iterator = this->config_content.begin();
			iterator != this->config_content.end(); iterator++ ) {
		file << iterator->first << "=" << iterator->second;
		file << "\n";
	}
	file.close();
}

///*********************************************************************
/// Private methods
///*********************************************************************
void fileIO::Settings::extractContents( const std::string &line ) {
	/**
	 * [Lambda] Extracts the Key from a line
	 * @param delimiter Key-Value separator ('=')
	 * @param line Line
	 * @return The key
	 */
	auto extractKey = []( size_t const &delimiter,
			const std::string &line ) {
		std::string key = line.substr( 0, delimiter );
		if( key.find( '\t' ) != line.npos || key.find( ' ' ) != line.npos ) {
			key.erase( key.find_first_of( "\t " ) );
		}
		return key;
	};
	/**
	 * [Lambda] Extracts the Value from a line
	 * @param delimiter Key-Value separator ('=')
	 * @param line Line
	 * @return The value
	 */
	auto extractValue = []( size_t const &delimiter, const std::string &line ) {
		std::string value = line.substr( delimiter + 1 );
		value.erase( 0, value.find_first_not_of( "\t " ) );
		value.erase( value.find_last_not_of( "\t " ) + 1 );
		return value;
	};

	std::string temp = line;
	temp.erase( 0, temp.find_first_not_of( "\t " ) ); // Erase leading whitespace
	size_t delimiter = temp.find( '=' );

	std::string key = extractKey( delimiter, temp );
	std::string value = extractValue( delimiter, temp );

	if( !keyExists( key ) ) {
		this->config_content.insert( std::pair<std::string, std::string>( key, value ) );
	} else {
		Log *log = Log::getInstance();
		std::ostringstream msg { };
		msg << "[fileIO::Settings::extractContents(..)] Key name <" << key << "> already exists.";
		log->newEvent( Location::IO, EventType::Error, msg );
	}
}

/**
 * Parses the line
 * @param line Line
 * @param lineNumber Line number
 */
void fileIO::Settings::parseLine( const std::string &line, size_t const lineNumber ) {
	if( line.find( '=' ) == line.npos ) {
		Log *log = Log::getInstance();
		std::ostringstream msg { };
		msg << "[fileIO::Settings::parseLine(..)] '=' delimiter could not be found in line "
				<< lineNumber << ".";
		log->newEvent( Location::IO, EventType::Error, msg );
	}
	if( !validLine( line ) ) {
		Log *log = Log::getInstance();
		std::ostringstream msg { };
		msg << "[fileIO::Settings::parseLine(..)] Bad key-value pair format on line " << lineNumber
				<< ".";
		log->newEvent( Location::IO, EventType::Error, msg );
	}
	extractContents( line );
}

/**
 * Checks for ';' and removed anything after and including that
 * @param line Line
 */
void fileIO::Settings::removeComment( std::string &line ) const {
	if( line.find( '#' ) != line.npos ) {
		line.erase( line.find( '#' ) );
	}
}

/**
 * Checks if the line is populated only by whitespace
 * @param line Line
 * @return Success
 */
bool fileIO::Settings::onlyWhitespace( const std::string &line ) const {
	return ( line.find_first_not_of( ' ' ) == line.npos );
}

/**
 * Checks for correct structure of line for a configuration format (key=value)
 * @param line Line
 * @return Success
 */
bool fileIO::Settings::validLine( const std::string &line ) const {
	std::string temp = line;
	temp.erase( 0, temp.find_first_not_of( "\t " ) );
	if( temp[ 0 ] == '=' ) {
		return false;
	}
	for( size_t i = temp.find( '=' ) + 1; i < temp.length(); i++ ) {
		if( temp[ i ] != ' ' ) {
			return true;
		}
	}
	return false;
}
