/*
 * spatial.cpp
 *
 *  Created on: 17 Mar 2015
 *      Author: Es A. Davison
 */

#include "spatial.hpp"

/**
 * Calculates the distance between 2 points in 3D space
 * @param a Point A
 * @param b Point B
 * @return The distance
 */
double CoreTools::math::spatial::calcDistance( const Coordinate3D &a, const Coordinate3D &b ) {
	return sqrt( pow( b.x - a.x, 2 ) + pow( b.y - a.y, 2 ) + pow( b.z - a.z, 2 ) );
}

/**
 * Calculates the relative difference between 2 points in 3D space on their XYZ axis
 * @param a Point A
 * @param b Point B
 * @return Differences stored in a Coordinate3D object
 */
Coordinate3D CoreTools::math::spatial::calcDistanceXYZ( const Coordinate3D &a,
		const Coordinate3D &b ) {
	return Coordinate3D( ( a.x - b.x ), ( a.y - b.y ), ( a.z - b.z ) );
}

/**
 * Calculates the absolute difference between 2 point in 3D space on their XYZ axis
 * @param a Point A
 * @param b point B
 * @return Differences stored in a Coordinate3D object
 */
Coordinate3D CoreTools::math::spatial::calcDistanceXYZ_abs( const Coordinate3D &a,
		const Coordinate3D &b ) {
	return Coordinate3D( labs( a.x - b.x ), labs( a.y - b.y ), labs( a.z - b.z ) );
}

/**
 * Calculates the angle of a point to an axis on a plane
 * @param axis Axis (X/Y/Z)
 * @param plane Plane (XY/XZ/YZ)
 * @param point Point in space
 * @return The angle
 */
double CoreTools::math::spatial::calcAngleTo( AXIS axis, PLANE plane, Coordinate3D &point ) {
	double h { };
	switch( plane ) {
		case PLANE::XY:
			switch( axis ) {
				case AXIS::X:
					h = CoreTools::math::trigonometry::pythagoras_hyp( point.x, point.y );
					return CoreTools::math::trigonometry::calcAngle_SSS( point.y, h, point.x );
					break;
				case AXIS::Y:
					h = CoreTools::math::trigonometry::pythagoras_hyp( point.y, point.x );
					return CoreTools::math::trigonometry::calcAngle_SSS( point.x, point.y, h );
					break;
				default:
					break;
			}
			break;
		case PLANE::XZ:
			switch( axis ) {
				case AXIS::X:
					h = CoreTools::math::trigonometry::pythagoras_hyp( point.x, point.z );
					return CoreTools::math::trigonometry::calcAngle_SSS( point.z, h, point.x );
					break;
				case AXIS::Z:
					h = CoreTools::math::trigonometry::pythagoras_hyp( point.z, point.x );
					return CoreTools::math::trigonometry::calcAngle_SSS( point.x, point.z, h );
					break;
				default:
					break;
			}
			break;
		case PLANE::YZ:
			switch( axis ) {
				case AXIS::Y:
					h = CoreTools::math::trigonometry::pythagoras_hyp( point.y, point.z );
					return CoreTools::math::trigonometry::calcAngle_SSS( point.z, point.y, h );
					break;
				case AXIS::Z:
					h = CoreTools::math::trigonometry::pythagoras_hyp( point.z, point.y );
					return CoreTools::math::trigonometry::calcAngle_SSS( point.y, h, point.z );
					break;
				default:
					break;
			}
			break;
	}
	Log *log = Log::getInstance();
	log->logEvent( Location::Core, EventType::Error,
			"[CoreTools::math::spatial::calcAngleTo(..)] AXIS chosen does not belong to the PLANE selected." );
	return 0;
}

/**
 * Calculates the XYZ lowest and XYZ highest point in the ab spatial cube
 * @param a Point A
 * @param b Point B
 * @param l Lower bound point
 * @param u Upper bound point
 */
void CoreTools::math::spatial::calcBounds( const Coordinate3D &a, const Coordinate3D &b,
		Coordinate3D &l, Coordinate3D &u ) {
	a.x < b.x ? ( l.x = a.x, u.x = b.x ) : ( u.x = a.x, l.x = b.x );
	a.y < b.y ? ( l.y = a.y, u.y = b.y ) : ( u.y = a.y, l.y = b.y );
	a.z < b.z ? ( l.z = a.z, u.z = b.z ) : ( u.z = a.z, l.z = b.z );
}

/**
 * Orders points to form a triangle ABC where A is the at the far left end and B is at the far right end on the X axis
 * @param A Point A
 * @param B Point B
 * @param C Point C
 * @param ax Distance AX (where X is the unknown point)
 * @param bx Distance BX (where X is the unknown point)
 * @param cx Distance CX (where X is the unknown point)
 */
void CoreTools::math::spatial::orderPoints( Coordinate3D &A, Coordinate3D &B, Coordinate3D &C,
		double &ax, double &bx, double &cx ) {
	double ab = CoreTools::math::spatial::calcDistance( A, B );
	double ac = CoreTools::math::spatial::calcDistance( A, C );
	double bc = CoreTools::math::spatial::calcDistance( B, C );
	if( ac > ab && ac > bc ) { //ac is the hypotenuse
		B.swap( C );
		std::swap( bx, cx );
	} else if( bc > ab && bc > ac ) { //bc is the hypotenuse
		A.swap( B );
		std::swap( ax, bx );
		B.swap( C );
		std::swap( bx, cx );
	}
	if( B.x < A.x ) { //B is on the left
		B.swap( A );
		std::swap( bx, ax );
	}
}

/**
 * Adds the value of one set of coordinates to another
 * @param to_transform Set of coordinates to change
 * @param shift_ref Coordinate set to add
 */
void CoreTools::math::spatial::transform3D_add( Coordinate3D &to_transform,
		const Coordinate3D &shift_ref ) {
	to_transform.x += shift_ref.x;
	to_transform.y += shift_ref.y;
	to_transform.z += shift_ref.z;
}

/**
 * Subtracts the value of one set of coordinates from another
 * @param to_transform Set of coordinates to change
 * @param shift_ref Coordinate set to subtract
 */
void CoreTools::math::spatial::transform3D_substract( Coordinate3D &to_transform,
		const Coordinate3D &shift_ref ) {
	to_transform.x -= shift_ref.x;
	to_transform.y -= shift_ref.y;
	to_transform.z -= shift_ref.z;
}

/**
 * Performs a rotation on a point in 3D space
 * @param point Point in space
 * @param axis Rotation axis (X/Y/Z)
 * @param angle Angle to rotate
 */
void CoreTools::math::spatial::transform3D_rotate( Coordinate3D &point, AXIS axis, double angle ) {
	Coordinate3D temp { };
	long double rad { angle * CoreTools::math::trigonometry::pi / 180 };
	long double c = cos( rad );
	long double s = sin( rad );
	switch( axis ) {
		case AXIS::Z:
			temp.x = point.x * c - point.y * s;
			temp.y = point.x * s + point.y * c;
			temp.z = point.z;
			break;
		case AXIS::Y:
			temp.x = point.x * c - point.z * s;
			temp.y = point.y;
			temp.z = point.x * s + point.z * c;
			break;
		case AXIS::X:
			temp.x = point.x;
			temp.y = point.y * c - point.z * s;
			temp.z = point.y * s + point.z * c;
			break;
	}
	temp.error_flag = point.error_flag;
	point = temp;
}

/**
 * Works out the position a point X relative to ABC
 * Assumes that ABC is flat on the XY plane and A is at (0,0,0), B is at (ab,0,0)
 * @param ab Distance AB
 * @param ac Distance AC
 * @param bc Distance BC
 * @param ax Distance AX
 * @param bx Distance BX
 * @param cx Distance CX
 * @return Relative coordinate of point X where X.z can be either +/- its value.
 * 		   If spheres do not intersect will switch error_flag in return to true
 */
Pair<Coordinate3D> CoreTools::math::spatial::trilaterate( double ab, double ac, double bc,
		double ax, double bx, double cx ) {
	Coordinate3D X1 { };
	Coordinate3D X2 { };
	double a = CoreTools::math::trigonometry::calcAngle_SSS( bc, ac, ab );
	double rad_a = CoreTools::math::trigonometry::degreeToRad( a );
	double i { ac * cos( rad_a ) }; //x for point C
	double j { ac * sin( rad_a ) }; //y for point C

	X1.x = ( ( pow( ax, 2 ) - pow( bx, 2 ) + pow( ab, 2 ) ) / ( 2 * ab ) );
	X1.y = ( pow( ax, 2 ) - pow( cx, 2 ) + pow( j, 2 ) + pow( i, 2 )
			- ( ( i * ( pow( ax, 2 ) - pow( bx, 2 ) + pow( ab, 2 ) ) ) / ab ) ) / ( 2 * j );
	X1.z = sqrt( pow( ax, 2 ) - pow( X1.x, 2 ) - pow( X1.y, 2 ) );
	X2 = X1;
	Log *log = Log::getInstance();
	std::ostringstream msg { };
	msg << "[CoreTools::math::spatial::trilaterate( " << ab << ", " << ac << ", " << bc << ", "
			<< ax << ", " << bx << ", " << cx << ")] ";
	if( ( X1.x != X1.x ) || ( X1.y != X1.y ) || ( X1.z != X1.z ) ) {
		X1.error_flag = true;
		msg << " Cannot find intersection point.";
		log->newEvent( Location::Satellite, EventType::Error, msg );
	} else {
		X2.z = -X2.z;
		msg << " Found point X as " << X1 << " or " << X2;
		log->newEvent( Location::Satellite, EventType::Event, msg );
	}
	return Pair<Coordinate3D> { X1, X2 };
}

/**
 * Find the possible positions of point X in 3D space
 * @param A Point A
 * @param B Point B
 * @param C Point C
 * @param ax Distance AX
 * @param bx Distance BX
 * @param cx Distance CX
 * @return Pair of possible points
 */
Pair<Coordinate3D> CoreTools::math::spatial::trilaterate( const Coordinate3D &A,
		const Coordinate3D &B, const Coordinate3D &C, double ax, double bx, double cx ) {
	Coordinate3D A_temp = A;
	Coordinate3D B_temp = B;
	Coordinate3D C_temp = C;
	//Order the points on so that ab is the hypotenuse
	CoreTools::math::spatial::orderPoints( A_temp, B_temp, C_temp, ax, bx, cx );
	//Calculate distances
	double ab = CoreTools::math::spatial::calcDistance( A_temp, B_temp );
	double ac = CoreTools::math::spatial::calcDistance( A_temp, C_temp );
	double bc = CoreTools::math::spatial::calcDistance( B_temp, C_temp );
	//Transform step.1 - transform so that point A is @ (0,0,0) [i.e.: shift everything]
	Coordinate3D shift = CoreTools::math::spatial::calcDistanceXYZ( Coordinate3D( 0, 0, 0 ),
			A_temp );
	CoreTools::math::spatial::transform3D_add( A_temp, shift );
	CoreTools::math::spatial::transform3D_add( B_temp, shift );
	CoreTools::math::spatial::transform3D_add( C_temp, shift );
	//Transform step.2 - transform so that point B is @ (ab,0,0) [i.e.: rotate on z axis and y axis]
	double angle_z = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::X,
			CoreTools::math::spatial::PLANE::XY, B_temp );
	if( B_temp.y > 0 ) {
		angle_z = -angle_z;
	}
	CoreTools::math::spatial::transform3D_rotate( B_temp, CoreTools::math::spatial::AXIS::Z,
			angle_z );
	CoreTools::math::spatial::transform3D_rotate( C_temp, CoreTools::math::spatial::AXIS::Z,
			angle_z );
	double angle_y = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::X,
			CoreTools::math::spatial::PLANE::XZ, B_temp );
	if( B_temp.z > 0 ) {
		angle_y = -angle_y;
	}
	CoreTools::math::spatial::transform3D_rotate( B_temp, CoreTools::math::spatial::AXIS::Y,
			angle_y );
	CoreTools::math::spatial::transform3D_rotate( C_temp, CoreTools::math::spatial::AXIS::Y,
			angle_y );
	//Transform step.3 - transform so that point C is flat on the XY plane along with A & B [i.e.: rotate on X axis]
	double angle_x = CoreTools::math::spatial::calcAngleTo( CoreTools::math::spatial::AXIS::Y,
			CoreTools::math::spatial::PLANE::YZ, C_temp );
	if( C_temp.z > 0 ) {
		angle_x = -angle_x;
	}
	CoreTools::math::spatial::transform3D_rotate( C_temp, CoreTools::math::spatial::AXIS::X,
			angle_x );
	//Trilaterate to find point X
	Pair<Coordinate3D> pair = CoreTools::math::spatial::trilaterate( ab, ac, bc, ax, bx, cx );
	//Transform X by reversing order on the steps
	CoreTools::math::spatial::transform3D_rotate( pair.a, CoreTools::math::spatial::AXIS::X,
			-angle_x );
	CoreTools::math::spatial::transform3D_rotate( pair.b, CoreTools::math::spatial::AXIS::X,
			-angle_x );
	CoreTools::math::spatial::transform3D_rotate( pair.a, CoreTools::math::spatial::AXIS::Y,
			-angle_y );
	CoreTools::math::spatial::transform3D_rotate( pair.b, CoreTools::math::spatial::AXIS::Y,
			-angle_y );
	CoreTools::math::spatial::transform3D_rotate( pair.a, CoreTools::math::spatial::AXIS::Z,
			-angle_z );
	CoreTools::math::spatial::transform3D_rotate( pair.b, CoreTools::math::spatial::AXIS::Z,
			-angle_z );
	CoreTools::math::spatial::transform3D_substract( pair.a, shift );
	CoreTools::math::spatial::transform3D_substract( pair.b, shift );
	return pair;
}

