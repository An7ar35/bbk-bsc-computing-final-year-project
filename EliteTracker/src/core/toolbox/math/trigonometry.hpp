/*
 * trigonometry.hpp
 *
 *  Created on: 17 Mar 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_TOOLBOX_MATH_TRIGONOMETRY_HPP_
#define CORE_TOOLBOX_MATH_TRIGONOMETRY_HPP_

#include <math.h>
#include "boost/math/constants/constants.hpp"

namespace CoreTools {
	namespace math {
		namespace trigonometry {
			const double pi = boost::math::constants::pi<double>();
			double radToDegree( double angle );
			double degreeToRad( double angle );
			double cotan( double val );
			double calcAngle_SSS( double a, double b, double c );
			double calcSide_AAS( double A, double C, double c );
			double calcSide_SAS( double b, double c, double A );
			double pythagoras( double s, double h );
			double pythagoras_hyp( double s1, double s2 );
			double getAreaOf_Triangle( double a, double b, double c );
		}
	}
}

#endif /* CORE_TOOLBOX_MATH_TRIGONOMETRY_HPP_ */
