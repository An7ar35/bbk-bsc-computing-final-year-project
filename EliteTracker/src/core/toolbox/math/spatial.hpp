/*
 * spatial.hpp
 *
 *  Created on: 17 Mar 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_TOOLBOX_MATH_SPATIAL_HPP_
#define CORE_TOOLBOX_MATH_SPATIAL_HPP_

#include <iostream>
#include <math.h>

#include "../../logger/Log.hpp"
#include "../../containers/Coordinate2D.hpp"
#include "../../containers/Coordinate3D.hpp"
#include "../../containers/Pair.hpp"
#include "../../toolbox/math/trigonometry.hpp"

namespace CoreTools {
	namespace math {
		namespace spatial {
			enum class AXIS {
				X, Y, Z
			};
			enum class PLANE {
				XY, YZ, XZ
			};
			double calcDistance( const Coordinate3D &a, const Coordinate3D &b );
			Coordinate3D calcDistanceXYZ( const Coordinate3D &a, const Coordinate3D &b );
			Coordinate3D calcDistanceXYZ_abs( const Coordinate3D &a, const Coordinate3D &b );
			double calcAngleTo( AXIS axis, PLANE plane, Coordinate3D &coordinate );
			void calcBounds( const Coordinate3D &a, const Coordinate3D &b, Coordinate3D &l,
					Coordinate3D &u );
			void orderPoints( Coordinate3D &A, Coordinate3D &B, Coordinate3D &C, double &ax,
					double &bx, double &cx );
			void transform3D_add( Coordinate3D &to_transform, const Coordinate3D &shift_ref );
			void transform3D_substract( Coordinate3D &to_transform, const Coordinate3D &shift_ref );
			void transform3D_rotate( Coordinate3D &point, AXIS axis, double angle ); //TODO
			Pair<Coordinate3D> trilaterate( double ab, double ac, double bc, double ax, double bx,
					double cx );
			Pair<Coordinate3D> trilaterate( const Coordinate3D &A, const Coordinate3D &B,
					const Coordinate3D &C, double ax, double bx, double cx );

		}
	}
}

#endif /* CORE_TOOLBOX_MATH_SPATIAL_HPP_ */

/*
 * Source:
 * http://jwilson.coe.uga.edu/EMAT6680Fa05/Schultz/6690/Barn_GPS/Barn_GPS.html
 * http://embedded.olin.edu/ultrasonic/software.php
 */
