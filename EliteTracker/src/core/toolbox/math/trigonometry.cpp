/*
 * trigonometry.cpp
 *
 *  Created on: 17 Mar 2015
 *      Author: Es A. Davison
 */

#include "trigonometry.hpp"

/**
 * Converts Radians to Degrees
 * @param val Value to convert
 * @return Value in degrees
 */
double CoreTools::math::trigonometry::radToDegree( double val ) {
	return val * ( 180 / pi );
}

/**
 * Converts Degrees to Radians
 * @param val Value to convert
 * @return Value in Rads
 */
double CoreTools::math::trigonometry::degreeToRad( double val ) {
	return val * ( pi / 180 );
}

/**
 * Calculates the cotan of a value
 * @param val Value
 * @return Cotan
 */
double CoreTools::math::trigonometry::cotan( double val ) {
	return ( 1 / tan( val ) );
}

/**
 * Calculates an angle of a triangle with 3 lengths known
 * @param a Length a
 * @param b Length b
 * @param c Length c
 * @return Angle A
 */
double CoreTools::math::trigonometry::calcAngle_SSS( double a, double b, double c ) {
	return acos( ( pow( b, 2 ) + pow( c, 2 ) - pow( a, 2 ) ) / ( 2 * b * c ) ) * 180 / pi;
}

/**
 * Calculate a side (AAS)
 * @param A Angle A
 * @param C Angle C
 * @param c Length c
 * @return Length a
 */
double CoreTools::math::trigonometry::calcSide_AAS( double A, double C, double c ) {
	return ( c * sin( ( A * pi / 180 ) ) / sin( ( C * pi / 180 ) ) );
}

/**
 * Calculate a side (SAS)
 * @param b Length b
 * @param c Length c
 * @param A Angle A
 * @return Length a
 */
double CoreTools::math::trigonometry::calcSide_SAS( double b, double c, double A ) {
	return sqrt( pow( b, 2 ) + pow( c, 2 ) - 2 * b * c * cos( ( A * pi / 180 ) ) );
}

/**
 * Using Pythagoras' theorem; gets the length of a side of a right angled triangle
 * @param s Side
 * @param h Side
 * @return Length of other side
 */
double CoreTools::math::trigonometry::pythagoras( double s, double h ) {
	return sqrt( pow( h, 2 ) - pow( s, 2 ) );
}

/**
 * Using Pythagoras' theorem; gets the length of the hypotenuse of a right angled triangle
 * @param s1 Length of Side 1
 * @param s2 Length of side 2
 * @return Length of the hypotenuse
 */
double CoreTools::math::trigonometry::pythagoras_hyp( double s1, double s2 ) {
	return sqrt( pow( s1, 2 ) + pow( s2, 2 ) );
}

/**
 * Calculates the area of a triangle using Heron's Formula
 * @param a Vertex A
 * @param b Vertex B
 * @param c Vertex C
 * @return Area of triangle
 */
double CoreTools::math::trigonometry::getAreaOf_Triangle( double a, double b, double c ) {
	double p { ( a + b + c ) / 2 };
	return sqrt( ( p * ( p - a ) * ( p - b ) * ( p - c ) ) );
}
