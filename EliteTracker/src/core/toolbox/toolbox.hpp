/*
 * toolbox.hpp
 *
 *  Created on: 29 Nov 2014
 *      Author: E. A. Davison
 */

#ifndef TOOLBOX_HPP_
#define TOOLBOX_HPP_

#include <iostream>
#include <cstdio>
#include <math.h>

#include "../logger/Log.hpp"

namespace CoreTools {
	/**
	 * Console Progress bar [INLINE FUNCTION]
	 * @param current_step Current index
	 * @param total_steps Number of indices to process
	 * @param width Width of the bar
	 */
	inline void progressBar_console( const int current_step, const int total_steps,
			const int width ) {
		int position { current_step * width / total_steps };
		float percent = current_step * 100 / total_steps;
		if( current_step == 1 ) {
			std::cout << "\r";
		}
		std::cout << "[";
		for( int i = 1; i < position; i++ ) {
			std::cout << "=";
		}
		if( (int) percent == 100 ) {
			std::cout << "] " << percent << "\%" << std::endl;
		} else {
			if( position < width ) {
				std::cout << "|";
			}
			for( int i = position; i < width - 1; i++ ) {
				std::cout << " ";
			}
			std::cout << "] " << percent << "\%\r";
		}
	}

	/**
	 * Gets the precision from a double
	 * @param value Value
	 * @return The precision
	 */
	static int getPrecision( const double &value ) {
		int i { 0 };
		long double temp { value };
		while( temp >= 1 ) {
			temp *= 0.1;
			i++;
		}
		temp = value;
		int j { 0 };
		while( temp > trunc( temp ) && temp < ceil( temp ) ) {
			temp *= 10;
			j++;
		}
		return i + j;
	}

	/**
	 * Checks if value is equal to another within a specified error margin
	 * @param to_check Value to check
	 * @param reference Value to check against
	 * @param error_margin Error margin allowed
	 * @return Success
	 */
	static bool acceptably_equal( double to_check, double reference, double error_margin ) {
		double top { reference + ( 0.5 * error_margin ) };
		double bottom { reference - ( 0.5 * error_margin ) };
		return ( to_check <= top && to_check >= bottom );
	}
}

#endif /* TOOLBOX_HPP_ */
