/*
 * windows.hpp
 *
 *  Created on: 2 Dec 2014
 *      Author: Alwlyan
 */

#ifndef WINDOWS_HPP_
#define WINDOWS_HPP_

#include <windows.h>
#include <winbase.h>

namespace platform {
	//Windows specific stuff
	namespace windows {
		/**
		 * Launch a specified application
		 * @param path Full path and name of application
		 * @return Success
		 */
		int launchapp( std::string path ) {
			system( path.c_str() );

			/*
			 Log *log = Log::getInstance();
			 switch( result ) {
			 case 0:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] The operating system is out of memory or resources." );
			 return 0;
			 break;
			 case SE_ERR_FNF:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] The specified file was not found." );
			 return 0;
			 break;
			 case SE_ERR_PNF:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] The specified path was not found." );
			 return 0;
			 break;
			 case SE_ERR_ACCESSDENIED:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - The operating system denied access to the specified file." );
			 return 0;
			 break;
			 case SE_ERR_ASSOCINCOMPLETE:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - The file name association is incomplete or invalid." );
			 return 0;
			 break;
			 case SE_ERR_DDEBUSY:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - The DDE transaction could not be completed because other DDE transactions were being processed." );
			 return 0;
			 break;
			 case SE_ERR_DDEFAIL:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - The DDE transaction failed." );
			 return 0;
			 break;
			 case SE_ERR_DDETIMEOUT:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - The DDE transaction could not be completed because the request timed out." );
			 return 0;
			 break;
			 case SE_ERR_DLLNOTFOUND:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - The specified DLL was not found." );
			 return 0;
			 break;
			 case SE_ERR_NOASSOC:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - There is no application associated with the given file name extension. This error will also be returned if you attempt to print a file that is not printable." );
			 return 0;
			 break;
			 case SE_ERR_OOM:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - There was not enough memory to complete the operation." );
			 return 0;
			 break;
			 case SE_ERR_SHARE:
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - A sharing violation occurred." );
			 return 0;
			 break;
			 default:
			 if( result > (HINSTANCE) 32 ) {
			 log->logEvent( Location::OS, EventType::Event,
			 "[launchapp] - File opened without error." );
			 return 1;
			 } else {
			 log->logEvent( Location::OS, EventType::Error,
			 "[launchapp] - Error code unknown." );
			 return 0;
			 }
			 break;
			 }
			 */
		}
	}
}

#endif /* WINDOWS_HPP_ */
