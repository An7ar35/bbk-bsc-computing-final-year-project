/*
 * Column.cpp
 *
 *  Created on: 20 Jan 2015
 *      Author: Es A. Davison
 */

#include "Column.hpp"

/**
 * Column constructor
 * @param type Type of data
 */
Column::Column( dataType type, std::string heading ) {
	this->type = type;
	this->heading = heading;
}

/**
 * Column destructor
 */
Column::~Column() {
}

/**
 * Gets the content type of the column
 * @return Content type
 */
dataType Column::getType() {
	return this->type;
}

/**
 * Checks a given type against the Column's type
 * @param type Type to check against
 * @return Result of check
 */
bool Column::checkType( const dataType type ) {
	return this->type == type;
}
/**
 * Gets the heading of the column
 * @return The heading
 */
std::string Column::getHeading() {
	return this->heading;
}


