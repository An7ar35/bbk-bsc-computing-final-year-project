/*
 * Column.hpp
 *
 *  Created on: 20 Jan 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_CONTAINERS_COLUMN_HPP_
#define CORE_CONTAINERS_COLUMN_HPP_

#include "../logger/log.hpp"
#include "dataType.hpp"

/**
 * Column (properties) container
 */
struct Column {
		Column( dataType type, std::string heading );
		~Column();
		dataType getType();
		bool checkType( const dataType type );
		std::string getHeading();
	private:
		static void logEvent( EventType nLevel, const char *description );
		dataType type;
		std::string heading;
};

#endif /* CORE_CONTAINERS_COLUMN_HPP_ */
