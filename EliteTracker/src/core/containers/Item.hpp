/*
 * Item.hpp
 *
 *  Created on: 18 Jan 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_CONTAINERS_ITEM_HPP_
#define CORE_CONTAINERS_ITEM_HPP_

#include <iostream>
#include <string>
#include <cctype> //std::tolower
#include <math.h>

#include "../logger/log.hpp"
#include "dataType.hpp"

class Item {
	public:
		Item();
		Item( int value );
		Item( double value );
		Item( std::string value );
		Item( const char *value ) : Item( std::string( value ) ) { };
		Item( bool value );
		Item( const Item &item ); //copy constructor
		~Item();
		friend std::ostream & operator<<( std::ostream &output, const Item item ) {
			switch( item.itsType ) {
				case dataType::NONE:
					output << "NULL";
					break;
				case dataType::INTEGER:
					output << item.item_INT;
					break;
				case dataType::DOUBLE:
					output << item.item_DOUBLE;
					break;
				case dataType::STRING:
					output << item.item_STRING;
					break;
				case dataType::BOOLEAN:
					output << item.item_BOOL;
					break;
				default:
					output << "ERROR!";
					break;
			}
			return output;
		}
		Item & operator=( const Item &item );
		Item & operator=( const int &int_item );
		Item & operator=( const double &double_item );
		Item & operator=( const std::string &string_item );
		Item & operator=( const bool &bool_item );
		bool operator<( const Item &rhs ) const;
		bool operator>( const Item &rhs ) const;
		bool operator<=( const Item &rhs ) const;
		bool operator>=( const Item &rhs ) const;
		bool operator==( const Item& rhs ) const;
		bool operator!=( const Item &rhs ) const;

		bool getValue( int &container );
		bool getValue( double &container );
		bool getValue( std::string &container );
		bool getValue( bool &container );

		int getInt();
		double getDouble();
		std::string getString();
		bool getBool();

		dataType getType();
	private:
		static void logEvent( EventType nLevel, const char *description );
		dataType itsType;
		union {
				int item_INT;
				bool item_BOOL;
				double item_DOUBLE;
				std::string item_STRING;
		};
};

#endif /* CORE_CONTAINERS_ITEM_HPP_ */
