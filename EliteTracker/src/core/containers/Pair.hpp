/*
 * Pair.hpp
 *
 *  Created on: 21 Mar 2015
 *      Author: Alwlyan
 */

#ifndef CORE_CONTAINERS_PAIR_HPP_
#define CORE_CONTAINERS_PAIR_HPP_

template<typename T> struct Pair {
		T a;
		T b;
};

#endif /* CORE_CONTAINERS_PAIR_HPP_ */
