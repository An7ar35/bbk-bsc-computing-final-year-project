/*
 * Coordinate2D.hpp
 *
 *  Created on: 20 Jan 2015
 *      Author: Alwlyan
 */

#ifndef CORE_CONTAINERS_COORDINATE2D_HPP_
#define CORE_CONTAINERS_COORDINATE2D_HPP_

#include <iostream>
#include <math.h>

/**
 * Coordinate container (2D)
 */
template<typename T> struct Coordinate2D {
		Coordinate2D() {
			x = 0;
			y = 0;
		}
		Coordinate2D( T x_coord, T y_coord ) {
			x = x_coord;
			y = y_coord;
		}
		~Coordinate2D() {
		}
		friend std::ostream& operator<<( std::ostream& s, const Coordinate2D<T>& c ) {
			s << "(" << c.x << "," << c.y << ")";
			return s;
		}
		void set( T x, T y ) {
			this->x = x;
			this->y = y;
		}
		void reset() {
			this->x = 0;
			this->y = 0;
		}
		double calc_norm() {
			return sqrt( this->x * this->x + this->y * this->y );
		}
		T x;
		T y;
};

#endif /* CORE_CONTAINERS_COORDINATE2D_HPP_ */
