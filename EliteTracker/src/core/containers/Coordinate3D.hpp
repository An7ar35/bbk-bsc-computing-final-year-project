/*
 * Coordinate3D.hpp
 *
 *  Created on: 20 Jan 2015
 *      Author: Alwlyan
 */

#ifndef CORE_CONTAINERS_COORDINATE3D_HPP_
#define CORE_CONTAINERS_COORDINATE3D_HPP_

#include <iostream>

struct Coordinate3D {
		Coordinate3D() {
			this->x = 0;
			this->y = 0;
			this->z = 0;
			this->error_flag = false;
		}
		Coordinate3D( double x, double y, double z ) {
			this->x = x;
			this->y = y;
			this->z = z;
			this->error_flag = false;
		}
		~Coordinate3D() {

		}
		friend std::ostream& operator <<( std::ostream& s, const Coordinate3D& coord ) {
			s << "(" << coord.x << "," << coord.y << "," << coord.z << ")";
			return s;
		}
		bool operator==( const Coordinate3D& rhs ) const {
			return ( this->x == rhs.x && this->y == rhs.y && this->z == rhs.z
					&& this->error_flag == rhs.error_flag );
		}
		bool operator!=( const Coordinate3D& rhs ) const {
			return !( this->operator ==( rhs ) );
		}
		void reset() {
			this->x = 0;
			this->y = 0;
			this->z = 0;
			this->error_flag = false;
		}
		bool isReset() const {
			return ( this->x == 0 && this->y == 0 && this->z == 0 && this->error_flag == false );
		}
		void swap( Coordinate3D &coord ) {
			double temp_x { this->x };
			double temp_y { this->y };
			double temp_z { this->z };
			bool temp_flag { this->error_flag };
			this->x = coord.x;
			this->y = coord.y;
			this->z = coord.z;
			this->error_flag = coord.error_flag;
			coord.x = temp_x;
			coord.y = temp_y;
			coord.z = temp_z;
			coord.error_flag = temp_flag;
		}
		double x;
		double y;
		double z;
		bool error_flag;
};

#endif /* CORE_CONTAINERS_COORDINATE3D_HPP_ */
