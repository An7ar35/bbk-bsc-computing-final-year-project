/*
 * table.hpp
 *
 *  Created on: 11 Jan 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_CONTAINERS_TABLE_HPP_
#define CORE_CONTAINERS_TABLE_HPP_

#include <iostream>
#include <vector>
#include <string>
#include <algorithm>
#include <iterator>

#include "../logger/log.hpp"
#include "dataType.hpp"
#include "Coordinate2D.hpp"
#include "Item.hpp"
#include "Column.hpp"
#include "Row.hpp"

/**
 * Table container
 * > Multi-typed container that supports 'std::string', 'int', 'double' and 'bool'
 * > USER GUIDE: Add column > Lock Table structure > Add data
 */
struct Table {
		enum class SortType {
			ASCENDING, DESCENDING
		};
		Table();
		~Table();
		Item & operator()( const int column, const int row ) {
			if( row < static_cast<int>( this->rows.size() )
					&& column < this->rows[ row ].getLength() ) {
				if( !this->lock_flag ) {
					logEvent( EventType::Error,
							"(Table::operator()) Trying to access Item on an unlocked Table. Please lock the Table first." );
				}
				return this->rows[ row ]( column );
			} else {
				logEvent( EventType::Error,
						"(Table::operator()) Trying to access Item outside the bounds of the Table." );
				return this->error_item;
			}
		}
		void clear();
		void clearData();
		bool createColumn( const int type, const std::string heading );
		void lockTableStructure();
		//Sequential adding of data
		bool add();
		bool add( int value );
		bool add( double value );
		bool add( std::string value );
		//Getters
		std::string getHeading( const int column );
		dataType getType( const int column );
		int getColumnCount();
		int getRowCount();
		//Printing
		void printToConsole();
		//Checks
		bool checkIntegrity(); //full data types and border limits checks
		bool checkBorders(); //border limits check
		//Sort
		void sort( int column, SortType st );
		//Search
		int whereIsThisColumn( const std::string columnName );
		Coordinate2D<int> searchFor( Item item );
		Coordinate2D<int> searchFor( const std::string column, Item item );
		int searchFor( const std::string column1, const Item item1, const std::string column2,
				const Item item2 ); //order doesn't matter

	private:
		//Flags
		bool lock_flag;
		bool tableConsistency_flag;
		//Number of Rows/Columns
		int table_size_x;
		int table_size_y;
		//(x,y) coordinate of the last occupied cell
		Coordinate2D<int> cursor;
		//Containers
		std::vector<Column> columns; //Headings & Types
		std::vector<Row> rows; //Individual rows of data
		//Private methods
		void createRow();
		void resetConsistencyFlag();
		Coordinate2D<int> getNextCellFor( Coordinate2D<int> current );
		bool checkNext( Coordinate2D<int> &next, dataType type );
		bool checkLastCellPosition();
		int getColumnNumberFor( const std::string &columnName );
		//Search templates
		int find( int col, const Item value ) {
			for( int i = 0; i < static_cast<int>( this->rows.size() ); i++ ) {
				if( this->rows[ i ]( col ) == value ) {
					return i;
				}
			}
			return -1;
		}
		int findPair( const int col1, const Item value1, const int col2, const Item value2 ) {
			std::cout << col1 << ":" << value1 << ", " << col2 << ":" << value2 << std::endl;
			for( int i = 0; i < static_cast<int>( this->rows.size() ); i++ ) {
				if( this->rows[ i ]( col1 ) == value1 ) {
					if( this->rows[ i ]( col2 ) == value2 ) {
						return i;
					}
				}
				if( this->rows[ i ]( col1 ) == value2 ) {
					if( this->rows[ i ]( col2 ) == value1 ) {
						return i;
					}
				}
			}
			return -1;
		}
		//Logging
		static void logEvent( EventType nLevel, const char *description );
		Item error_item = Item( "<!exist>" );
};

#endif /* CORE_CONTAINERS_TABLE_HPP_ */
