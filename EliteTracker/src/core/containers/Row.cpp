/*
 * Row.cpp
 *
 *  Created on: 20 Jan 2015
 *      Author: Es A. Davison
 */

#include "Row.hpp"

/**
 * Row constructor
 */
Row::Row() {
}

/**
 * Row destructor
 */
Row::~Row() {

}

/**
 * Adds NULL Item to the row
 */
void Row::add() {
	this->content.push_back( Item { } );
}
/**
 * Adds value to the Row
 * @param value
 */
void Row::add( int value ) {
	this->content.push_back( Item { value } );
}

/**
 * Adds value to the Row
 * @param value
 */
void Row::add( double value ) {
	this->content.push_back( Item { value } );
}

/**
 * Adds value to the Row
 * @param value
 */
void Row::add( std::string value ) {
	this->content.push_back( Item { value } );
}

/**
 * Gets the dataType of an item in the row
 * @param column Column in which the Item belongs to
 * @return The type
 */
dataType Row::getItemType( const int column ) {
	if( column < static_cast<int>( this->content.size() ) ) {
		return this->content[ column ].getType();
	} else {
		std::stringstream ss { };
		ss << "(Row::getItemType( " << column << " )) column # is out of bounds of the row size.";
		logEvent( EventType::Error, ( ss.str() ).c_str() );
		return dataType::NONE;
	}
}

/**
 * Gets the length (width) of the Row
 * @return Length
 */
int Row::getLength() {
	return this->content.size();
}

/**
 * Log an event
 * @param nLevel Type of event
 * @param description Description of the event
 */
void Row::logEvent( EventType nLevel, const char *description ) {
	std::ostringstream oss { };
	oss << description;
	Log *log = Log::getInstance();
	log->newEvent( Location::Core, nLevel, oss );
}
