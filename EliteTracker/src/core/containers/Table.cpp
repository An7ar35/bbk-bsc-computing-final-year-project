/*
 * table.cpp
 *
 *  Created on: 13 Jan 2015
 *      Author: Es A. Davison
 */

#include "table.hpp"

///*********************************************************************
/// Table [Public methods]
///*********************************************************************
/**
 * Table constructor
 */
Table::Table() {
	this->table_size_x = 0;
	this->table_size_y = 0;
	this->lock_flag = false;
	this->tableConsistency_flag = false;
	this->cursor = Coordinate2D<int>();
	this->cursor.set( -1, -1 );
}

/**
 * Table destructor
 */
Table::~Table() {
}

/**
 * Clears everything inside the table (inc. columns)
 */
void Table::clear() {
	this->lock_flag = false;
	std::vector<Column>().swap( this->columns );
	std::vector<Row>().swap( this->rows );
	this->table_size_x = 0;
	this->table_size_y = 0;
	this->cursor.set( -1, -1 );
	this->resetConsistencyFlag();
}

/**
 * Clear the data inside the rows, leave columns alone.
 */
void Table::clearData() {
	this->lock_flag = false;
	std::vector<Row>().swap( this->rows );
	this->table_size_y = 0;
	this->cursor.set( -1, -1 );
	this->resetConsistencyFlag();
}

/**
 * Creates a new Row in the 'Table' container
 */
void Table::createRow() {
	this->resetConsistencyFlag();
	this->rows.push_back( Row { } );
	this->table_size_y++;
}

/**
 * Creates a Column in the 'Table' container
 * @param type SQLite data type or custom [1=SQLITE_INTEGER, 2=SQLITE_FLOAT, 3=SQLITE_TEXT, 5=SQLITE_NULL, 1000=Boolean]
 * @param heading Heading of the column
 * @return Success
 */
bool Table::createColumn( const int type, const std::string heading ) {
	if( this->lock_flag ) {
		Table::logEvent( EventType::Error,
				"(Table::createColumn) No more column cannot be added as Table has been locked." );
		return false;
	}
	this->resetConsistencyFlag();
	std::ostringstream msg { };
	switch( type ) {
		case 1: //SQLITE_INTEGER
			this->columns.push_back( Column { dataType::INTEGER, heading } );
			this->table_size_x++;
			break;
		case 2: //SQLITE_FLOAT
			this->columns.push_back( Column { dataType::DOUBLE, heading } );
			this->table_size_x++;
			break;
		case 3: //SQLITE_TEXT
			this->columns.push_back( Column { dataType::STRING, heading } );
			this->table_size_x++;
			break;
		case 5: //SQLITE_NULL
			msg << "(Table::createColumn) Column '" << heading << "' is created as 'NULL' type";
			Table::logEvent( EventType::Debug, ( msg.str() ).c_str() );
			this->columns.push_back( Column { dataType::STRING, heading } );
			this->table_size_x++;
			break;
		case 1000: //Non-SQlite type; Boolean
			this->columns.push_back( Column { dataType::BOOLEAN, heading } );
			this->table_size_x++;
			break;
		default:
			Table::logEvent( EventType::Error, "(Table::createColumn) Type is unsupported" );
			return false;
	}
	return true;
}

/**
 * Locks the table:
 * > No more columns may be added
 * > Data can now be added into the table
 */
void Table::lockTableStructure() {
	this->lock_flag = true;
}

/**
 * Adds a Null Item to the table sequentially
 * @return Success
 */
bool Table::add() {
	if( !this->lock_flag ) {
		Table::logEvent( EventType::Error,
				"(Table::add) Table structure isn't locked. Lock table before adding data." );
		return false;
	}
	Coordinate2D<int> next = this->getNextCellFor( this->cursor );
	if( this->checkNext( next, dataType::NONE ) ) {
		if( next.y > cursor.y ) { //create new row then push
			this->createRow();
			this->rows[ next.y ].add();
		} else { //push on current row
			this->resetConsistencyFlag();
			this->rows[ cursor.y ].add();
		}
		this->cursor = next;
		return true;
	}
	return false;
}
/**
 * Adds an 'int' Item to the table sequentially
 * @param value
 * @return Success
 */
bool Table::add( int value ) {
	if( !this->lock_flag ) {
		Table::logEvent( EventType::Error,
				"(Table::add) Table structure isn't locked. Lock table before adding data." );
		return false;
	}
	Coordinate2D<int> next = this->getNextCellFor( this->cursor );
	if( this->checkNext( next, dataType::INTEGER ) ) {
		if( next.y > cursor.y ) { //create new row then push
			this->createRow();
			this->rows[ next.y ].add( value );
		} else { //push on current row
			this->resetConsistencyFlag();
			this->rows[ cursor.y ].add( value );
		}
		this->cursor = next;
		return true;
	}
	return false;
}

/**
 * Adds a 'double' Item to the table sequentially
 * @param value
 * @return Success
 */
bool Table::add( double value ) {
	if( !this->lock_flag ) {
		Table::logEvent( EventType::Error,
				"(Table::add) Table structure isn't locked. Lock table before adding data." );
		return false;
	}
	Coordinate2D<int> next = this->getNextCellFor( this->cursor );
	if( this->checkNext( next, dataType::DOUBLE ) ) {
		if( next.y > cursor.y ) { //create new row then push
			this->createRow();
			this->rows[ next.y ].add( value );
		} else { //push on current row
			this->resetConsistencyFlag();
			this->rows[ cursor.y ].add( value );
		}
		this->cursor = next;
		return true;
	}
	return false;
}

/**
 * Adds a 'std::string' Item to the table sequentially
 * @param value
 * @return Success
 */
bool Table::add( std::string value ) {
	if( !this->lock_flag ) {
		Table::logEvent( EventType::Error,
				"(Table::add) Table structure isn't locked. Lock table before adding data." );
		return false;
	}
	Coordinate2D<int> next = this->getNextCellFor( this->cursor );
	if( this->checkNext( next, dataType::STRING ) ) {
		if( next.y > cursor.y ) { //create new row then push
			this->createRow();
			this->rows[ next.y ].add( value );
		} else { //push on current row
			this->resetConsistencyFlag();
			this->rows[ cursor.y ].add( value );
		}
		this->cursor = next;
		return true;
	}
	return false;
}

/**
 * Gets the heading of a column
 * @param column Column number
 * @return The heading
 */
std::string Table::getHeading( const int column ) {
	if( column >= this->getColumnCount() ) {
		Table::logEvent( EventType::Error, "(Table::getHeading) Column does not exist" );
		return "INVALID COL";
	} else {
		return this->columns[ column ].getHeading();
	}
}

/**
 * Gets the dataType of a column
 * @param column Column number
 * @return The dataType
 */
dataType Table::getType( const int column ) {
	if( column >= this->getColumnCount() ) {
		Table::logEvent( EventType::Error, "(Table::getType) Column does not exist" );
		return dataType::NONE;
	} else {
		return this->columns[ column ].getType();
	}
}
/**
 * Gets the number of columns in the table
 * @return The size
 */
int Table::getColumnCount() {
	return this->table_size_x;
}

/**
 * Gets the number of Rows in the table
 * @return The number of rows
 */
int Table::getRowCount() {
	if( this->rows.size() == 0 ) {
		return 0;
	}
	return this->table_size_y;
}

/**
 * Prints the Table to the console
 */
void Table::printToConsole() {
	if( !this->checkIntegrity() ) {
		std::cout << "Consistency check has fail. Possible reasons:" << std::endl
				<< "\t- Column lengths are not all the same," << std::endl << "\t- Table is empty."
				<< std::endl;
	} else {
		//Headings
		std::cout << "| ";
		for( int i = 0; i < this->getColumnCount(); i++ ) {
			std::cout << this->getHeading( i ) << " | ";
		}
		std::cout << std::endl;
		//Main data
		if( this->getRowCount() == 0 ) {
			std::cout << "No data in table..." << std::endl;
		} else {
			for( int i = 0; i < this->getRowCount(); i++ ) { //Rows
				std::cout << "| ";
				for( int j = 0; j < this->getColumnCount(); j++ ) { //Columns
					std::cout << this->rows[ i ]( j ) << " | ";
				}
				std::cout << std::endl;
			}
			std::cout << std::endl;
		}
	}
}

/**
 * Checks that all data (Item) match the column types
 * and all rows and columns are respectively the same size
 * @return Success
 */
bool Table::checkIntegrity() {
	if( this->checkBorders() ) {
		Table::logEvent( EventType::Event,
				"(Table::checkIntegrity) Checking Items are of correct type." );
		for( int i = 0; i < this->table_size_x; i++ ) {
			for( int j = 0; j < this->table_size_y; j++ ) {
				if( !( this->rows[ j ]( i ).getType() == this->columns[ i ].getType() ) ) {
					if( this->rows[ j ]( i ).getType() == dataType::NONE ) {
						Table::logEvent( EventType::Debug,
								"(Table::checkIntegrity) Item of type NULL found." );
					} else {
						Table::logEvent( EventType::Debug,
								"(Table::checkIntegrity) Item type does not match the column type." );
					}
				}
			}
		}
		Table::logEvent( EventType::Event,
				"(Table::checkIntegrity) > Items match their respective column types." );
		return true;
	} else {
		return false;
	}
}

/**
 * Checks that all rows and columns are respectively the same size
 * @return Success
 */
bool Table::checkBorders() {
	Table::logEvent( EventType::Event,
			"(Table::checkBorders) Checking Table borders are consistent." );
	this->tableConsistency_flag = false;
	//check data table
	int baseheight = this->rows.size();
	int basewidth = this->rows[ 0 ].getLength();
	//Column(s) exist
	if( this->getColumnCount() < 1 ) {
		Table::logEvent( EventType::Error, "(Table::checkBorders) Table has no Columns." );
		return false;
	}
	if( this->getRowCount() < 1 ) {
		Table::logEvent( EventType::Error, "(Table::checkBorders) Table has no Rows." );
		return false;
	}
	//Row width consistency
	for( int i = 0; i < baseheight; i++ ) {
		if( !( this->rows[ i ].getLength() == basewidth ) ) {
			Table::logEvent( EventType::Error, "(Table::checkBorders) Row(s) not the same width." );
			return false;
		}
	}
	//Data's Row width same as Column width?
	if( !( basewidth == static_cast<int>( this->columns.size() ) ) ) {
		Table::logEvent( EventType::Error,
				"(Table::checkBorders) Data width and Heading width not the same." );
		return false;
	}
	//check vector sizes with internally stored sizes
	if( !( baseheight == this->table_size_y ) ) {
		Table::logEvent( EventType::Error,
				"(Table::checkBorders) Inconsistency in table height stored." );
		return false;
	}
	if( !( basewidth == this->table_size_x ) ) {
		Table::logEvent( EventType::Error,
				"(Table::checkBorders) Inconsistency in table width stored." );
		return false;
	}
	this->tableConsistency_flag = true;
	Table::logEvent( EventType::Event,
			"(Table::checkBorders) > All rows are of consistent and correct width." );
	return this->tableConsistency_flag;
}

/**
 * Sorts the Table's rows based on the column and sort type
 * @param column Column number
 * @param st Sorting type required
 */
void Table::sort( const int column, const SortType st ) {
	//NOTE lexical sort of strings does not arrange single lettered entry followed by a space properly.
	//TODO nested sort functionality?
	if( column <= getColumnCount() ) {
		switch( st ) {
			case SortType::ASCENDING:
				std::sort( this->rows.begin(), this->rows.end(), [&]( Row &a, Row& b) {
					return a(column) < b(column);
				} );
				break;
			case SortType::DESCENDING:
				std::sort( this->rows.begin(), this->rows.end(), [&]( Row &a, Row &b) {
					return a(column) > b(column);
				} );
				break;
		}
	} else {
		Table::logEvent( EventType::Error,
				"(Table::sort) column number for sort index falls outside the Table boundaries." );
	}
}

/**
 * >> Public accessor for 'getColumnNumberFor(string)'
 * Gets the number of the column with the specified name
 * @param columnName Name of the column to find
 * @return Index of the column or (-1) if not found.
 */
int Table::whereIsThisColumn( const std::string columnName ) {
	return this->getColumnNumberFor( columnName );
}

/**
 * Searches the entire Table for Item
 * @param item Item/value
 * @return Location of the first match found as a Coordinate2D or (-1,-1) if no match is found.
 */
Coordinate2D<int> Table::searchFor( Item item ) {
	for( int x = 0; x < static_cast<int>( this->columns.size() ); x++ ) {
		if( item.getType() == this->columns[ x ].getType() ) {
			int y = this->find( x, item );
			if( y >= 0 ) {
				return Coordinate2D<int>( x, y );
			}
		}
	}
	return Coordinate2D<int>( -1, -1 );
}

/**
 * Searches the Table for Item inside specified column name
 * > Searches for the location of the column name first, then looks in that column for the Item
 * @param column Name of the column to find Item in
 * @param item Item to find
 * @return Location of the first match found as a Coordinate2D or (-1,-1) if no match is found.
 */
Coordinate2D<int> Table::searchFor( const std::string column, Item item ) {
	int x = this->getColumnNumberFor( column );
	if( x < 0 ) {
		Table::logEvent( EventType::Error,
				"(Table::searchFor) column name does not match any columns in table." );
		return Coordinate2D<int>( -1, -1 );
	}
	int y = this->find( x, item );
	if( y < 0 ) {
		return Coordinate2D<int>( -1, -1 );
	} else {
		return Coordinate2D<int>( x, y );
	}
}

/**
 * Searches for a pair of Items inside specified column names (good for key pairs)
 * @param column1 Name of the first column
 * @param item1 Item/value 1
 * @param column2 Name of the second column
 * @param item2 Item/value 2
 * @return The first match as the row number or (-1) if no match is found
 */
int Table::searchFor( const std::string column1, const Item item1, std::string column2,
		const Item item2 ) { //order doesn't matter
	int col_index1 = this->getColumnNumberFor( column1 );
	int col_index2 = this->getColumnNumberFor( column2 );
	if( col_index1 < 0 || col_index2 < 0 ) {
		Table::logEvent( EventType::Error,
				"(Table::searchFor) column name(s) does not match any columns in table." );
		return -1;
	}
	return this->findPair( col_index1, item1, col_index2, item2 );
}

///*********************************************************************
/// Table [Private methods]
///*********************************************************************
/**
 * Resets the consistency flag for the table
 */
void Table::resetConsistencyFlag() {
	this->tableConsistency_flag = false;
}

/**
 * Calculate the location of the next cell in the table
 * @param current Current cell coordinates
 * @return Next cell Coordinates
 */
Coordinate2D<int> Table::getNextCellFor( Coordinate2D<int> current ) {
	Coordinate2D<int> next { };
	if( current.x == -1 && current.y == -1 ) {
		next.set( 0, 0 );
	} else if( current.x == ( this->getColumnCount() - 1 ) ) {
		next.x = 0;
		next.y = current.y + 1;
	} else {
		next.x = current.x + 1;
		next.y = current.y;
	}
	return next;
}

/**
 * Checks the next cell after the last entry in the table and if the types matches
 * @param next Coordinate of the next cell
 * @param type Type of the value that we want to add at the end of the table
 * @return Success of the checks
 */
bool Table::checkNext( Coordinate2D<int> &next, dataType type ) {
	if( this->getColumnCount() < 1 ) {
		Table::logEvent( EventType::Error, "(Table::checkNext) No Column(s) in Table" );
		return false;
	}
	//First entry case
	if( this->cursor.x == -1 && this->cursor.y == -1 ) {
		this->createRow();
		this->cursor = next;
		if( !( type == dataType::NONE ) && !( this->columns[ cursor.x ].checkType( type ) ) ) {
			Table::logEvent( EventType::Error, "(Table::checkNext) Type mismatch on first entry." );
			return false;
		}
	} else {
		//All subsequent entries case
		if( !( type == dataType::NONE ) && !( this->columns[ next.x ].checkType( type ) ) ) {
			Table::logEvent( EventType::Error, "(Table::checkNext) Type mismatch." );
			return false;
		}
	}
	return true;
}

/**
 * Checks that the last cell position stored matches the last cell of the table
 * @return Success
 */
bool Table::checkLastCellPosition() {
	int x = this->columns.size() - 1;
	int y = this->rows.size() - 1;
	if( this->cursor.x == x && this->cursor.y == y ) {
		return true;
	}
	Table::logEvent( EventType::Error,
			"(Table::checkLastCellPosition) Last cell position not correctly recorded." );
	return false;
}

/**
 * Gets the number of the column with the specified name
 * @param columnName Name of the column to find
 * @return Index of the column or (-1) if not found.
 */
int Table::getColumnNumberFor( const std::string &columnName ) {
	for( int i = 0; i < static_cast<int>( this->columns.size() ); i++ ) {
		if( this->columns[ i ].getHeading() == columnName ) {
			return i;
		}
	}
	return -1;
}

/**
 * Log an event
 * @param nLevel Type of event
 * @param description Description of the event
 */
void Table::logEvent( EventType nLevel, const char *description ) {
	std::ostringstream oss { };
	oss << description;
	Log *log = Log::getInstance();
	log->newEvent( Location::Core, nLevel, oss );
}
