/*
 * Row.hpp
 *
 *  Created on: 20 Jan 2015
 *      Author: Alwlyan
 */

#ifndef CORE_CONTAINERS_ROW_HPP_
#define CORE_CONTAINERS_ROW_HPP_

#include "../logger/log.hpp"
#include "dataType.hpp"
#include "Item.hpp"

/**
 * Row (data) container
 */
struct Row {
		Row();
		~Row();
		Item & operator()( const int column ) {
			if( column < static_cast<int>( this->content.size() ) ) {
				return this->content[ column ];
			} else {
				std::stringstream ss { };
				ss << "(Row::operator( " << column
						<< " )) Trying to access Item outside the bounds of the Row.";
				logEvent( EventType::Error, ( ss.str() ).c_str() );
				return this->error_item;
			}
		}
		void add();
		void add( int value );
		void add( double value );
		void add( std::string value );
		dataType getItemType( const int column );
		int getLength();
	private:
		static void logEvent( EventType nLevel, const char *description );
		std::vector<Item> content;
		Item error_item = Item( "<!exist>" );

};

#endif /* CORE_CONTAINERS_ROW_HPP_ */
