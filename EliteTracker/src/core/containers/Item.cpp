/*
 * Item.cpp
 *
 *  Created on: 18 Jan 2015
 *      Author: Alwlyan
 */

#include "Item.hpp"

/**
 * Item constructor
 */
Item::Item() {
	itsType = dataType::NONE;
}
/**
 * Item constructor
 * @param value Integer value
 */
Item::Item( int value ) {
	itsType = dataType::INTEGER;
	this->item_INT = value;
}

/**
 * Item constructor
 * @param value Double value
 */
Item::Item( double value ) {
	itsType = dataType::DOUBLE;
	this->item_DOUBLE = value;
}

/**
 * Item constructor
 * @param value String value
 */
Item::Item( std::string value ) {
	this->itsType = dataType::STRING;
	new ( &this->item_STRING ) std::string { value };
}

/**
 * Item constructor
 * @param value Boolean value
 */
Item::Item( bool value ) {
	itsType = dataType::BOOLEAN;
	this->item_BOOL = value;
}

/**
 * Item Copy-Constructor
 * @param item
 */
Item::Item( const Item &item ) {
	this->itsType = item.itsType;
	switch( item.itsType ) {
		case dataType::NONE:
			//nothing to do
			break;
		case dataType::INTEGER:
			this->item_INT = item.item_INT;
			break;
		case dataType::DOUBLE:
			this->item_DOUBLE = item.item_DOUBLE;
			break;
		case dataType::STRING:
			new ( &this->item_STRING ) std::string { item.item_STRING };
			break;
		case dataType::BOOLEAN:
			this->item_BOOL = item.item_BOOL;
			break;
	}
}

/**
 * Item destructor
 */
Item::~Item() {
	if( itsType == dataType::STRING ) {
		this->item_STRING.~basic_string();
	}
}

/**
 * Operator =
 * @param item Item to copy from
 * @return The copy
 */
Item & Item::operator=( const Item &item ) {
	if( this->itsType == dataType::STRING && item.itsType == dataType::STRING ) {
		item_STRING = item.item_STRING;
		return *this;
	}
	if( this->itsType == dataType::STRING ) {
		item_STRING.~basic_string();
	}
	this->itsType = item.itsType;
	switch( this->itsType ) {
		case dataType::NONE:
			break;
		case dataType::INTEGER:
			this->item_INT = item.item_INT;
			break;
		case dataType::DOUBLE:
			this->item_DOUBLE = item.item_DOUBLE;
			break;
		case dataType::STRING:
			new ( &item_STRING ) std::string { item.item_STRING };
			break;
		case dataType::BOOLEAN:
			this->item_BOOL = item.item_BOOL;
			break;
	}
	return *this;
}

/**
 * Operator =
 * @param int_item Integer to copy from
 * @return The Item copy
 */
Item & Item::operator=( const int &int_item ) {
	switch( this->itsType ) {
		case dataType::NONE:
			this->itsType = dataType::INTEGER;
			this->item_INT = int_item;
			break;
		case dataType::INTEGER:
			this->item_INT = int_item;
			break;
		case dataType::DOUBLE:
			this->item_DOUBLE = double( int_item );
			break;
		case dataType::STRING:
			this->item_STRING.~basic_string();
			new ( &item_STRING ) std::string { std::to_string( int_item ) };
			break;
		case dataType::BOOLEAN:
			this->item_BOOL = int_item == 0 ? false : true;
			break;
	}
	return *this;
}

/**
 * Operator =
 * @param double_item Double to copy from
 * @return The Item copy
 */
Item & Item::operator=( const double &double_item ) {
	switch( this->itsType ) {
		case dataType::NONE:
			this->itsType = dataType::DOUBLE;
			this->item_INT = double_item;
			break;
		case dataType::INTEGER:
			this->item_INT = int( round( double_item ) );
			break;
		case dataType::DOUBLE:
			this->item_DOUBLE = double_item;
			break;
		case dataType::STRING:
			this->item_STRING.~basic_string();
			new ( &item_STRING ) std::string { std::to_string( double_item ) };
			break;
		case dataType::BOOLEAN:
			this->item_BOOL = double_item == 0 ? false : true;
			break;
	}
	return *this;
}

/**
 * Operator =
 * @param string_item String to copy from
 * @return The Item copy
 */
Item & Item::operator=( const std::string &string_item ) {
	switch( this->itsType ) {
		case dataType::STRING:
			this->item_STRING.~basic_string();
			new ( &item_STRING ) std::string { string_item };
			break;
		case dataType::NONE:
			this->itsType = dataType::STRING;
			new ( &item_STRING ) std::string { string_item };
			break;
		default:
			Item::logEvent( EventType::Trace,
					"Item 'Operator=' [Item<>String] used but Item was not of compatible type so nothing was done." );
			break;
	}
	return *this;
}

/**
 * Operator =
 * @param bool_item item Boolean to copy from
 * @return The Item copy
 */
Item & Item::operator=( const bool &bool_item ) {
	switch( this->itsType ) {
		case dataType::NONE:
			this->itsType = dataType::BOOLEAN;
			this->item_BOOL = bool_item;
			break;
		case dataType::INTEGER:
			this->item_INT = bool_item ? 1 : 0;
			break;
		case dataType::DOUBLE:
			item_DOUBLE = bool_item ? 1 : 0;
			break;
		case dataType::STRING:
			this->item_STRING.~basic_string();
			new ( &item_STRING ) std::string { ( bool_item ? "TRUE" : "FALSE" ) };
			break;
		case dataType::BOOLEAN:
			this->item_BOOL = bool_item;
			break;
	}
	return *this;
}

/**
 * Operator <
 * @param rhs Item to compare to
 * @return Boolean result
 */
bool Item::operator<( const Item &rhs ) const {
	if( this->itsType == rhs.itsType ) {
		switch( this->itsType ) {
			case dataType::NONE:
				return false;
			case dataType::INTEGER:
				return ( this->item_INT < rhs.item_INT );
			case dataType::DOUBLE:
				return ( this->item_DOUBLE < rhs.item_DOUBLE );
			case dataType::STRING:
				return ( this->item_STRING < rhs.item_STRING );
			case dataType::BOOLEAN:
				return ( this->item_BOOL < rhs.item_BOOL );
		}
	}
	return false;
}

/**
 * Operator >
 * @param rhs Item to compare to
 * @return Boolean result
 */
bool Item::operator>( const Item &rhs ) const {
	return !( this->operator <( rhs ) || this->operator ==( rhs ) );
}

/**
 * Operator <=
 * @param rhs Item to compare to
 * @return Boolean result
 */
bool Item::operator<=( const Item &rhs ) const {
	return !( this->operator >( rhs ) );
}

/**
 * Operator >=
 * @param rhs Item to compare to
 * @return Boolean result
 */
bool Item::operator>=( const Item &rhs ) const {
	return !( this->operator <( rhs ) );
}

/**
 * Operator ==
 * @param rhs Item to compare to
 * @return Boolean result
 */
bool Item::operator==( const Item& rhs ) const {
	if( this->itsType == rhs.itsType ) {
		switch( this->itsType ) {
			case dataType::NONE:
				return true;
			case dataType::INTEGER:
				return ( this->item_INT == rhs.item_INT );
			case dataType::DOUBLE:
				return ( this->item_DOUBLE == rhs.item_DOUBLE );
			case dataType::STRING:
				return ( this->item_STRING == rhs.item_STRING );
			case dataType::BOOLEAN:
				return ( this->item_BOOL == rhs.item_BOOL );
		}
	}
	return false;
}

/**
 * Operator !=
 * @param rhs Item to compare to
 * @return Boolean result
 */
bool Item::operator!=( const Item &rhs ) const {
	return !( this->operator ==( rhs ) );
}

/**
 * Gets the value stored
 * @param container Container to put the value into
 * @return Success
 */
bool Item::getValue( int &container ) {
	switch( this->itsType ) {
		case dataType::NONE:
			container = 0;
			return true;
		case dataType::INTEGER:
			container = this->item_INT;
			return true;
		case dataType::DOUBLE:
			container = int( round( this->item_DOUBLE ) );
			return true;
		case dataType::STRING:
			return false;
		case dataType::BOOLEAN:
			container = this->item_BOOL ? 1 : 0;
			return true;
	}
	return false;
}

/**
 * Gets the value stored
 * @param container Container to put the value into
 * @return Success
 */
bool Item::getValue( double &container ) {
	switch( this->itsType ) {
		case dataType::NONE:
			container = 0;
			return true;
		case dataType::INTEGER:
			container = double( this->item_INT );
			return true;
		case dataType::DOUBLE:
			container = this->item_DOUBLE;
			return true;
		case dataType::STRING:
			return false;
		case dataType::BOOLEAN:
			container = this->item_BOOL ? 1 : 0;
			return true;
	}
	return false;
}

/**
 * Gets the value stored
 * @param container Container to put the value into
 * @return Success
 */
bool Item::getValue( std::string &container ) {
	switch( this->itsType ) {
		case dataType::NONE:
			container = "";
			return true;
		case dataType::INTEGER:
			container = std::to_string( this->item_INT );
			return true;
		case dataType::DOUBLE:
			container = std::to_string( this->item_DOUBLE );
			return true;
		case dataType::STRING:
			container = this->item_STRING;
			return true;
		case dataType::BOOLEAN:
			container = this->item_BOOL ? "TRUE" : "FALSE";
			return true;
	}
	return false;
}

/**
 * Gets the value stored
 * @param container Container to put the value into
 * @return Success
 */
bool Item::getValue( bool &container ) {
	switch( this->itsType ) {
		case dataType::NONE:
			container = false;
			return true;
		case dataType::INTEGER:
			container = ( this->item_INT == 0 ? false : true );
			return true;
		case dataType::DOUBLE:
			container = ( this->item_DOUBLE == 0 ? false : true );
			return true;
		case dataType::STRING:
			container = ( this->item_STRING.length() > 0 ? true : false );
			return true;
		case dataType::BOOLEAN:
			container = this->item_BOOL;
			return true;
	}
	return false;
}
/**
 * Returns the Item as an int
 * > if item is std::string, returns its length
 * @return Item as integer
 */
int Item::getInt() {
	switch( this->itsType ) {
		case dataType::NONE:
			return 0;
			break;
		case dataType::INTEGER:
			return this->item_INT;
			break;
		case dataType::DOUBLE:
			return int( round( this->item_DOUBLE ) );
			break;
		case dataType::STRING:
			return this->item_STRING.length();
			break;
		case dataType::BOOLEAN:
			return ( this->item_BOOL ? 1 : 0 );
			break;
		default:
			return 0;
	}
}

/**
 * Returns the Item as an int
 * > if item is std::string, returns its length
 * @return Item as double
 */
double Item::getDouble() {
	switch( this->itsType ) {
		case dataType::NONE:
			return 0;
			break;
		case dataType::INTEGER:
			return double( this->item_INT );
			break;
		case dataType::DOUBLE:
			return this->item_DOUBLE;
			break;
		case dataType::STRING:
			return double( this->item_STRING.length() );
			break;
		case dataType::BOOLEAN:
			return ( this->item_BOOL ? 1 : 0 );
			break;
		default:
			return 0;
	}
}

/**
 * Returns the Item as a string
 * @return Item as std::string
 */
std::string Item::getString() {
	switch( this->itsType ) {
		case dataType::NONE:
			return "";
			break;
		case dataType::INTEGER:
			return std::to_string( this->item_INT );
			break;
		case dataType::DOUBLE:
			return std::to_string( this->item_DOUBLE );
			break;
		case dataType::STRING:
			return this->item_STRING;
			break;
		case dataType::BOOLEAN:
			return ( this->item_BOOL ? "TRUE" : "FALSE" );
			break;
		default:
			return "";
	}
}

/**
 * Returns the Item as a boolean
 * @return Item as bool
 */
bool Item::getBool() {
	switch( this->itsType ) {
		case dataType::NONE:
			return false;
			break;
		case dataType::INTEGER:
			return ( this->item_INT == 0 ? false : true );
			break;
		case dataType::DOUBLE:
			return ( this->item_DOUBLE == 0 ? false : true );
			break;
		case dataType::STRING:
			return ( this->item_STRING.length() > 0 ? true : false );
			break;
		case dataType::BOOLEAN:
			return this->item_BOOL;
			break;
		default:
			return false;
	}
}

/**
 * Gets the type of the Item
 * @return The type
 */
dataType Item::getType() {
	return this->itsType;
}

/**
 * Log an event
 * @param nLevel Type of event
 * @param description Description of the event
 */
void Item::logEvent( EventType nLevel, const char *description ) {
	std::ostringstream oss { };
	oss << description;
	Log *log = Log::getInstance();
	log->newEvent( Location::Core, nLevel, oss );
}

