/*
 * dataType.hpp
 *
 *  Created on: 18 Jan 2015
 *      Author: Alwlyan
 */

#ifndef CORE_CONTAINERS_DATATYPE_HPP_
#define CORE_CONTAINERS_DATATYPE_HPP_

/**
 * Supported Data types from SQLite (
 */
enum class dataType {
	NONE, INTEGER, DOUBLE, STRING, BOOLEAN
};

#endif /* CORE_CONTAINERS_DATATYPE_HPP_ */
