/*
 * utils.hpp
 *
 *  Created on: 11 Sep 2014
 *      Author: Alwlyan
 */

#ifndef UTILS_HPP_
#define UTILS_HPP_

#include <math.h>
#include <ctime>
#include <locale>
#include <boost/spirit/include/qi_parse.hpp>
#include <boost/spirit/include/qi_numeric.hpp>

#include "../logger/Log.hpp"
#include "../containers/Coordinate3D.hpp"
#include "../containers/Table.hpp"

namespace utils {
	bool stringToInt( const std::string &, int & );
	bool stringToDouble( const std::string &, double & );

	bool getXYZ( Table &t, Coordinate3D &xyz );

	std::string getTime( tm *ltm );
	std::string getDate( tm *ltm );
	std::string getTimeStamp( tm *ltm );
} //namespace utils

#endif /* UTILS_HPP_ */

/**
 * Sources:
 * http://forums.frontier.co.uk/showthread.php?t=34824
 */
