/*
 * editTables.cpp
 *
 *  Created on: 9 Sep 2014
 *      Author: Es A. Davison
 */

#include "editTables.hpp"
/*
 Database tables:
 ----------------
 JumpSystems - id, eliteID, name, x, y, z
 JumpConnections - distances between nodes - S_ID_A, S_ID_B, Distance
 Objects - id, eliteID, name, typeID, description
 ObjectTypes - id, type(moon, planet, STATION, ...)
 SystemsObjects - <Systems>, <Object>
 PlayerShips - id, ShipID<Ships>
 Ships - id, name, <ShipClass>, <Manufacturers>, fuelCapacity, cargo, jumpRangeFull, jumpRangeEmpty, maxSpeed, boostSpeed
 ShipClass - ID, ShipClass
 ShipLog - EntryNumber, Date, Time, From_ID<JumpSystems>, To_ID<JumpSystems>, Description
 Manufacturers - Man_ID, Name, ...Cats maybe?
 */

#include <string>

///*********************************************************************
/// Setup operations
///*********************************************************************
/**
 * Creates all tables required
 * If table exist then checks structural integrity
 * @param data Db Object
 */
void editTable::createAllTables( Db &data ) {
	if( !data.checkTableExists( "JumpSystems" ) ) {
		//JumpSystems - id, eliteID, name, x, y, z
		data.sqlQueryPush(
				"CREATE TABLE JumpSystems( S_ID INTEGER PRIMARY KEY AUTOINCREMENT, EliteID INTEGER DEFAULT NULL, Name varchar(50) NOT NULL, X_Coordinate DOUBLE, Y_Coordinate DOUBLE, Z_Coordinate DOUBLE, UNIQUE( Name, EliteID ) )" );
	} else {
		if( !editTable::JumpSystems::checkStructure( data ) ) {
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error, "\'JumpSystems\' table is fucked." );
		}
	}
	if( !data.checkTableExists( "JumpConnections" ) ) {
		//JumpConnections - distances between nodes - S_ID_A<Systems>, S_ID_B<Systems>, Distance
		data.sqlQueryPush(
				"CREATE TABLE JumpConnections( S_ID_A INTEGER NOT NULL, S_ID_B INTEGER NOT NULL, Distance DOUBLE NOT NULL, PRIMARY KEY( S_ID_A, S_ID_B ) )" );
	} else {
		if( !editTable::JumpConnections::checkStructure( data ) ) {
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error,
					"\'JumpConnections\' table is fucked." );
		}
	}
	if( !data.checkTableExists( "Stations" ) ) {
		//Stations - S_ID<JumpSystems>, eliteID, name, description
		data.sqlQueryPush(
				"CREATE TABLE Stations( S_ID INTEGER REFERENCES JumpSystems(S_ID), EliteID INTEGER DEFAULT NULL, Name varchar(50) NOT NULL, Description TEXT, UNIQUE( S_ID, EliteID, Name ) )" );
	} else {
		if( !editTable::Stations::checkStructure( data ) ) {
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error, "\'Stations\' table is fucked." );
		}
	}
	if( !data.checkTableExists( "PlayerShips" ) ) {
		//PlayerShips - id, ShipID<Ships>
		data.sqlQueryPush(
				"CREATE TABLE PlayerShips( ID INTEGER PRIMARY KEY AUTOINCREMENT, ShipID INTEGER REFERENCES Ships(ID) NOT NULL, FuelScoop BOOLEAN DEFAULT FALSE, DestroyedFlag BOOLEAN DEFAULT FALSE )" );
	} else {
		if( !editTable::PlayerShips::checkStructure( data ) ) {
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error, "\'PlayerShips\' table is fucked." );
		}
	}
	if( !data.checkTableExists( "Manufacturers" ) ) {
		//Manufacturer - ID, Manufacturer
		data.sqlQueryPush(
				"CREATE TABLE Manufacturers( ID INTEGER PRIMARY KEY AUTOINCREMENT, Manufacturer VARCHAR(50) NOT NULL, UNIQUE( Manufacturer ) )" );
	} else {
		if( !editTable::Ships::checkStructure( data ) ) {
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error, "\'Manufacturers\' table is fucked." );
		}
	}
	if( !data.checkTableExists( "ShipClasses" ) ) {
		//ShipClass - ID, ShipClass
		data.sqlQueryPush(
				"CREATE TABLE ShipClasses( ID INTEGER PRIMARY KEY AUTOINCREMENT, ShipClass VARCHAR(50) NOT NULL, UNIQUE( ShipClass ) )" );
	} else {
		if( !editTable::ShipClasses::checkStructure( data ) ) {
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error, "\'ShipClasses\' table is fucked." );
		}
	}
	if( !data.checkTableExists( "ShipLog" ) ) {
		//ShipLog - EntryNumber, Date, Time, From_ID<JumpSystems>, To_ID<JumpSystems>, Description
		data.sqlQueryPush(
				"CREATE TABLE ShipLog( EntryNumber INTEGER PRIMARY KEY AUTOINCREMENT, Date VARCHAR(12), Time VARCHAR(10), From_ID INTEGER REFERENCES JumpSystems(S_ID), To_ID INTEGER REFERENCES JumpSystems(S_ID), Description TEXT NOT NULL, UNIQUE( EntryNumber ) )" );
	} else {
		if( !editTable::ShipLog::checkStructure( data ) ) {
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error, "\'ShipLog\' table is fucked." );
		}
	}
	if( !data.checkTableExists( "Ships" ) ) {
		//Ships - ID, Name, ShipClassID<ShipClass>, ManufacturerID<Manufacturers>, FuelCapacity, MaxCargo, JumpRangeFull, JumpRangeEmpty, MaxSpeed, BoostSpeed
		data.sqlQueryPush(
				"CREATE TABLE Ships( ID INTEGER PRIMARY KEY AUTOINCREMENT, Name VARCHAR(50) NOT NULL, ShipClassID INTEGER NOT NULL REFERENCES ShipClasses(ID), ManufacturerID INTEGER NOT NULL REFERENCES Manufacturers(ID), FuelCapacity DOUBLE NOT NULL DEFAULT 0, MaxCargo DOUBLE NOT NULL, JumpRangeFull DOUBLE NOT NULL, JumpRangeEmpty DOUBLE NOT NULL, MaxSpeed DOUBLE NOT NULL, BoostSpeed DOUBLE NOT NULL, UNIQUE( Name ) )" );
	} else {
		if( !editTable::Ships::checkStructure( data ) ) {
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error, "\'Ships\' table is fucked." );
		}
	}
}

/**
 * Check table structures
 * @param data Db object
 * @return Success
 */
bool editTable::checkTables( Db &data ) {
	if( !data.checkTableExists( "JumpSystems" ) ) {
		return false;
	}
	if( !editTable::JumpSystems::checkStructure( data ) ) {
		return false;
	}
	if( !data.checkTableExists( "JumpConnections" ) ) {
		return false;
	}
	if( !editTable::JumpConnections::checkStructure( data ) ) {
		return false;
	}
	if( !data.checkTableExists( "Stations" ) ) {
		return false;
	}
	if( !editTable::Stations::checkStructure( data ) ) {
		return false;
	}
	if( !data.checkTableExists( "PlayerShips" ) ) {
		return false;
	}
	if( !editTable::PlayerShips::checkStructure( data ) ) {
		return false;
	}
	if( !data.checkTableExists( "Manufacturers" ) ) {
		return false;
	}
	if( !editTable::Ships::checkStructure( data ) ) {
		return false;
	}
	if( !data.checkTableExists( "ShipClasses" ) ) {
		return false;
	}
	if( !editTable::ShipClasses::checkStructure( data ) ) {
		return false;
	}
	if( !data.checkTableExists( "ShipLog" ) ) {
		return false;
	}
	if( !editTable::ShipLog::checkStructure( data ) ) {
		return false;
	}
	if( !data.checkTableExists( "Ships" ) ) {
		return false;
	}
	if( !editTable::Ships::checkStructure( data ) ) {
		return false;
	}
	return true;
}
/**
 * Sources:
 * http://www.kumobius.com/2013/08/c-string-to-int/
 */
