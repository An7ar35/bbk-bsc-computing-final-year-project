/*
 * utils.cpp
 *
 *  Created on: 11 Sep 2014
 *      Author: Alwlyan
 */

#include "utils.hpp"

/**
 * Converts a string to an integer
 * @param str string to be converted
 * @param result integer holder
 * @return success of conversion as a boolean value
 */
bool utils::stringToInt( const std::string &str, int &result ) {
	std::string::const_iterator i = str.begin();
	if( !boost::spirit::qi::parse( i, str.end(), boost::spirit::int_, result ) ) {
		return false;
	}
	return i == str.end(); // ensure the whole string was parsed
}

/**
 * Converts a string to a double
 * @param str string to be converted
 * @param result double holder
 * @return success of conversion as a boolean value
 */
bool utils::stringToDouble( const std::string &str, double &result ) {
	std::string::const_iterator i = str.begin();
	if( !boost::spirit::qi::parse( i, str.end(), boost::spirit::double_, result ) ) {
		return false;
	}
	return i == str.end(); // ensures the whole string was parsed
}

/**
 * Extracts XYZ coordinates from Table set
 * @param t TABLE result data from query
 * @param xyz Coordinate3D container
 * @return success as boolean
 */
bool utils::getXYZ( Table &t, Coordinate3D &xyz ) {
	int x_col = t.whereIsThisColumn( "X_Coordinate" );
	int y_col = t.whereIsThisColumn( "Y_Coordinate" );
	int z_col = t.whereIsThisColumn( "Z_Coordinate" );
	if( x_col < 0 || y_col < 0 || z_col < 0 ) {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"[utils::getXYZ()] Could not find 1 or more of the XYZ coordinate columns in Table." );
		return false;
	}
	if( t.getType( x_col ) != dataType::DOUBLE || t.getType( y_col ) != dataType::DOUBLE
			|| t.getType( z_col ) != dataType::DOUBLE ) {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"[utils::getXYZ()] Coordinate data from Table is not fully of type 'double'." );
		return false;
	}
	xyz.x = t( x_col, 0 ).getDouble();
	xyz.y = t( y_col, 0 ).getDouble();
	xyz.z = t( z_col, 0 ).getDouble();
	return true;
}

/**
 * Gets the time from a time struct pointer
 * @param ltm tm struct pointer
 * @return Time formatted as "hh:mm:ss"
 */
std::string utils::getTime( tm *ltm ) {
	/**
	 * [Lambda] Pads int variable with a '0' if smaller than 10
	 */
	auto pad = []( int variable ) {
		if( variable < 10 ) {
			return "0" + std::to_string( variable );
		} else {
			return std::to_string( variable );
		}
	};
	std::stringstream ss { };
	ss << std::string( pad( ltm->tm_hour ) ) << ":" << std::string( pad( ltm->tm_min ) ) << ":"
			<< std::string( pad( ltm->tm_sec ) );
	return ss.str();
}

/**
 * Gets the date from a time struct pointer
 * @param ltm tm struct pointer
 * @return Date formatted as "dd/mm/yyyy"
 */
std::string utils::getDate( tm *ltm ) {
	/**
	 * [Lambda] Pads int variable with a '0' if smaller than 10
	 */
	auto pad = []( int variable ) {
		if( variable < 10 ) {
			return "0" + std::to_string( variable );
		} else {
			return std::to_string( variable );
		}
	};
	std::stringstream ss { };
	ss << std::string( pad( ltm->tm_mday ) ) << "/" << std::string( pad( 1 + ltm->tm_mon ) ) << "/"
			<< ( 1900 + ltm->tm_year );
	return ss.str();
}

/**
 * Gets a full time stamp from a time struct pointer
 * @param ltm tm struct pointer
 * @return Time stamp formatted as "dd/mm/yyyy - hh:mm:ss"
 */
std::string utils::getTimeStamp( tm *ltm ) {
	std::stringstream ss { };
	ss << utils::getDate( ltm ) << " - " << utils::getTime( ltm );
	return ss.str();
}

