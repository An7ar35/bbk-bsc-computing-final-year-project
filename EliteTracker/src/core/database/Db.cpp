/*
 * db.cpp
 *
 *  Created on: 31 Aug 2014
 *      Author: Alwlyan
 */

#include "Db.hpp"

/**
 * Constructor for the 'Db' class
 * @param fileName The name of the database file to use
 */
Db::Db( const std::string fileName ) {
	this->fileName = fileName;
	this->database = NULL;
	this->opened_flag = false;
}

/**
 * Destructor of the 'Db' class
 */
Db::~Db() {
	this->close();
}

/**
 * Streams a description of the Db object
 * @param stream Output stream
 * @param db Db object
 * @return the description
 */
std::ostream &operator <<( std::ostream &stream, Db &db ) {
	std::string state = "closed";
	if( db.isFileOpened() ) {
		state = "opened";
	}
	return stream << "Db( " << db.getFileName() << " : " << state << " )";
}

///*********************************************************************
/// Database operations
///*********************************************************************
/**
 * Stores the result of given query into a typed 'Table' container object
 * ->Currently supports type SQLITE_TEXT, SQLITE_INTEGER, SQLITE_FLOAT
 * @param query SQL query as string
 * @param table Typed container for the result of the query
 * @return number of rows returned by query or negative integer for error:
 * 	(-1) Database file was closed
 * 	(-2) Unsupported type found in database query pull
 */
int Db::sqlQueryPull_typed( const std::string query, Table &table ) {
	return this->sqlQueryPull_typed( query, table, SQLITE_NULL );
}

/**
 * Stores the result of given query into a typed 'Table' container object
 * ->Currently supports type SQLITE_TEXT, SQLITE_INTEGER, SQLITE_FLOAT
 * @param query SQL query as string
 * @param table Typed container for the result of the query
 * @param null_override Type replacement for NULL [1=SQLITE_INTEGER, 2=SQLITE_FLOAT, 3=SQLITE_TEXT, 5=SQLITE_NULL, 1000=Boolean]
 * @return number of rows returned by query or negative integer for error:
 * 	(-1) Database file was closed
 * 	(-2) Unsupported type found in database query pull
 */
int Db::sqlQueryPull_typed( const std::string query, Table &table, int null_override ) {
	if( !this->isFileOpened() ) {
		Db::logEvent( EventType::Error, "(Db::sqlQueryPull_typed) Database file not opened." );
		return -1;
	}
	int rowCounter { 0 };
	sqlite3_stmt *statement { };
	sqlMsgCode( sqlite3_prepare( this->database, query.c_str(), -1, &statement, NULL ), query );
	int cols = sqlite3_column_count( statement ); //get number of columns from statement
	sqlMsgCode( sqlite3_step( statement ), query ); //execute statement

	table.clear(); //Making sure container is empty

	//Getting the headings & types
	for( int i = 0; i < cols; i++ ) {
		int type = sqlite3_column_type( statement, i );
		if( type == SQLITE_NULL ) { //NULL type
			type = null_override;
		}
		table.createColumn( type, std::string( sqlite3_column_name( statement, i ) ) );
	}
	//Columns set; Ready to add
	table.lockTableStructure();
	//Retrieving the data
	while( sqlite3_column_text( statement, 0 ) ) { //reading the 1st column of result
		for( int i = 0; i < cols; i++ ) {
			bool success { false };
			if( sqlite3_column_type( statement, i ) == SQLITE_TEXT ) {
				if( sqlite3_column_text( statement, i ) == nullptr ) {
					success = table.add( "" );
				} else {
					success = table.add(
							std::string( (char*) ( sqlite3_column_text( statement, i ) ) ) );
				}
			} else if( sqlite3_column_type( statement, i ) == SQLITE_INTEGER ) {
				success = table.add( sqlite3_column_int( statement, i ) );
			} else if( sqlite3_column_type( statement, i ) == SQLITE_FLOAT ) {
				success = table.add( sqlite3_column_double( statement, i ) );
			} else if( sqlite3_column_type( statement, i ) == SQLITE_NULL ) {
				switch( null_override ) {
					case SQLITE_INTEGER:
						success = table.add( 0 );
						break;
					case SQLITE_FLOAT:
						success = table.add( 0.0 );
						break;
					case SQLITE_TEXT:
						success = table.add( "" );
						break;
					case 1000:
						success = table.add( 0 );
						break;
					default:
						success = table.add();
						break;
				}
			} else {
				sqlMsgCode( sqlite3_finalize( statement ), query );
				Db::logEvent( EventType::Error,
						"(Db::sqlQueryPull_typed) Type unsupported (SQLITE_BLOB)." );
				table.clear();
				return -2;
			}
			if( !success ) {
				sqlMsgCode( sqlite3_finalize( statement ), query );
				Db::logEvent( EventType::Error,
						"(Db::sqlQueryPull_typed) Problem adding data to table. Process aborted." );
				//table.clear();
				return -2;
			}
		}
		rowCounter++;
		sqlMsgCode( sqlite3_step( statement ), query );
	}
	sqlMsgCode( sqlite3_finalize( statement ), query );
	return rowCounter;
}

/**
 * Stores the result of given query into 'tableOutput' and the column names in 'tableHeadings'
 * @param query SQL query as string
 * @return number of rows returned by query or -1 if file not opened
 */
int Db::sqlQueryPull( const std::string query ) {
	if( !this->isFileOpened() ) {
		Db::logEvent( EventType::Error, "(Db::sqlQueryPull) Database file not opened." );
		return -1;
	}
	int rowCounter { 0 };
	sqlite3_stmt *statement { };
	sqlMsgCode( sqlite3_prepare( this->database, query.c_str(), -1, &statement, NULL ) );
	int cols = sqlite3_column_count( statement ); //get number of columns from statement
	sqlMsgCode( sqlite3_step( statement ) ); //execute statement

//Prepare 'tableHeadings' and get the column names
	std::vector<std::string>().swap( this->tableHeadings );
	for( int i = 0; i < cols; i++ ) {
		this->tableHeadings.push_back( sqlite3_column_name( statement, i ) );
	}

//Prepare 'tableOutput' and get the data
	Db::clearTABLE( this->tableOutput );
	for( int i = 0; i < cols; i++ ) {
		this->tableOutput.push_back( std::vector<std::string>() );
	}
	while( sqlite3_column_text( statement, 0 ) ) { //reading the 1st column of result
		for( int i = 0; i < cols; i++ ) {
			if( sqlite3_column_text( statement, i ) == nullptr ) {
				this->tableOutput[ i ].push_back( "" );
			} else {
				this->tableOutput[ i ].push_back(
						std::string( (char *) sqlite3_column_text( statement, i ) ) );
			}
		}
		rowCounter++;
		sqlMsgCode( sqlite3_step( statement ) );
	}
	sqlMsgCode( sqlite3_finalize( statement ) );
	return rowCounter; //TODO sort out error control
}

/**
 * Stores the result of given query into a given table (no headings)
 * @param query SQL query as string
 * @param table reference to 2D vector for storing result
 * @return number of rows returned by query or (-1) if file not opened
 */
int Db::sqlQueryPull( const std::string query, TABLE &table ) {
	if( !this->isFileOpened() ) {
		Db::logEvent( EventType::Error, "(Db::sqlQueryPull) Database file not opened." );
		return -1;
	}
	int rowCounter { 0 };
	sqlite3_stmt *statement { };

	sqlMsgCode( sqlite3_prepare( this->database, query.c_str(), -1, &statement, nullptr ) ); //prepare statement
	int cols = sqlite3_column_count( statement ); //get number of columns from statement
	sqlMsgCode( sqlite3_step( statement ) ); //execute statement

	//Prepare 'table' and get the data
	Db::clearTABLE( table );
	for( int i = 0; i < cols; i++ ) {
		table.push_back( std::vector<std::string>() );
	}
	while( sqlite3_column_text( statement, 0 ) ) { //reading the 1st column of result
		for( int i = 0; i < cols; i++ ) {
			if( sqlite3_column_text( statement, i ) == nullptr ) {
				table[ i ].push_back( "" );
			} else {
				table[ i ].push_back( std::string( (char *) sqlite3_column_text( statement, i ) ) );
			}
		}
		rowCounter++;
		sqlMsgCode( sqlite3_step( statement ) );
	}
	sqlMsgCode( sqlite3_finalize( statement ) );
	return rowCounter;
}

/**
 * Adds the result of given query into a given table (no headings)
 * -> Doesn't clear, just adds new sets of data
 * -> If given TABLE is empty then the width is determined by the query.
 * -> WARNING: Does NOT check if the new data matches the previously recorded sets in given TABLE
 * @param query SQL query as string
 * @param table reference to 2D vector for storing result
 * @return number of rows returned by query or (-1) if file not opened / (-2) if table width mismatch
 */
int Db::sqlQueryPull_add( const std::string query, TABLE &table ) {
	if( !this->isFileOpened() ) {
		Db::logEvent( EventType::Error, "(Db::sqlQueryPull_add) Database file not opened." );
		return -1;
	}
	int rowCounter { 0 };
	sqlite3_stmt *statement { };
	sqlMsgCode( sqlite3_prepare( this->database, query.c_str(), -1, &statement, nullptr ) ); //prepare statement
	int cols = sqlite3_column_count( statement ); //get number of columns from statement
	sqlMsgCode( sqlite3_step( statement ) ); //execute statement

	if( table.empty() ) {
		for( int i = 0; i < cols; i++ ) {
			table.push_back( std::vector<std::string>() );
		}
	} else if( table.size() != static_cast<unsigned int>( cols ) ) { //Checks table width match statement columns
		sqlMsgCode( sqlite3_finalize( statement ) );
		Db::logEvent( EventType::Error,
				"(Db::sqlQueryPull_add) Mismatch between storage width and query columns." );
		return -2;
	}
	while( sqlite3_column_text( statement, 0 ) ) { //reading the 1st column of result
		for( int i = 0; i < cols; i++ ) {
			if( sqlite3_column_text( statement, i ) == nullptr ) {
				table[ i ].push_back( "" );
			} else {
				table[ i ].push_back( std::string( (char *) sqlite3_column_text( statement, i ) ) );
			}
		}
		rowCounter++;
		sqlMsgCode( sqlite3_step( statement ) );
	}
	sqlMsgCode( sqlite3_finalize( statement ) );
	return rowCounter;
}

/**
 * Straight Query, nothing stored\n
 * Note: good for queries where nothing is expected back
 * @param query SQL query as string
 * @return number of rows returned by query (0) or (-1) if file not opened
 */
int Db::sqlQueryPush( std::string query ) {
	if( !this->isFileOpened() ) {
		Db::logEvent( EventType::Error, "(Db::sqlQueryPush) Database file not opened." );
		return -1;
	}
	sqlite3_stmt *statement { };
	sqlMsgCode( sqlite3_prepare_v2( this->database, query.c_str(), -1, &statement, nullptr ), query ); //prepare statement
	sqlMsgCode( sqlite3_step( statement ), query ); //execute statement
	sqlMsgCode( sqlite3_finalize( statement ), query );
	return 0;
}

/**
 * Grabs a single item of type INT based on a query
 * @param query SQL query as string
 * @param item Integer container for the result
 * @return number of rows returned by query; (-1) File not opened, (-2) Too many columns
 */
int Db::sqlQueryPull_single( const std::string query, int &item ) {
	if( !this->isFileOpened() ) {
		Db::logEvent( EventType::Error, "(Db::sqlQueryPull_single) Database file not opened." );
		return -1;
	}
	int rowCounter { 0 };
	sqlite3_stmt *statement { };

	sqlMsgCode( sqlite3_prepare( this->database, query.c_str(), -1, &statement, nullptr ) ); //prepare statement
	int cols = sqlite3_column_count( statement ); //get number of columns from statement
	if( cols > 1 ) {
		sqlMsgCode( sqlite3_finalize( statement ) );
		Db::logEvent( EventType::Error,
				"(Db::sqlQueryPull_single) Query returns more than one column." );
		return -2;
	}
	sqlMsgCode( sqlite3_step( statement ) ); //execute statement
	while( sqlite3_column_int( statement, 0 ) ) { //reading the 1st column of result
		if( sqlite3_column_int( statement, 0 ) == 0 ) {
			Db::logEvent( EventType::Error, "(Db::sqlQueryPull_single) Query returns nothing." );
		} else {
			item = sqlite3_column_int( statement, 0 );
			rowCounter++;
		}
		sqlMsgCode( sqlite3_step( statement ) );
	}
	sqlMsgCode( sqlite3_finalize( statement ) );
	return rowCounter;
}

/**
 * Grabs a single item of type DOUBLE based on a query
 * @param query SQL query as string
 * @param item Double container for the result
 * @return number of rows returned by query; (-1) File not opened, (-2) Too many columns
 */
int Db::sqlQueryPull_single( const std::string query, double &item ) {
	if( !this->isFileOpened() ) {
		Db::logEvent( EventType::Error, "(Db::sqlQueryPull_single) Database file not opened." );
		return -1;
	}
	int rowCounter { 0 };
	sqlite3_stmt *statement { };

	sqlMsgCode( sqlite3_prepare( this->database, query.c_str(), -1, &statement, nullptr ) ); //prepare statement
	int cols = sqlite3_column_count( statement ); //get number of columns from statement
	if( cols > 1 ) {
		sqlMsgCode( sqlite3_finalize( statement ) );
		Db::logEvent( EventType::Error,
				"(Db::sqlQueryPull_single) Query returns more than one column." );
		return -2;
	}
	sqlMsgCode( sqlite3_step( statement ) ); //execute statement
	while( sqlite3_column_double( statement, 0 ) ) { //reading the 1st column of result
		if( sqlite3_column_double( statement, 0 ) == 0.0 ) {
			Db::logEvent( EventType::Error, "(Db::sqlQueryPull_single) Query returns nothing." );
		} else {
			item = sqlite3_column_double( statement, 0 );
		}
		rowCounter++;
		sqlMsgCode( sqlite3_step( statement ) );
	}
	sqlMsgCode( sqlite3_finalize( statement ) );
	return rowCounter;
}

/**
 * Grabs a single item of type TEXT based on a query
 * @param query SQL query as string
 * @param item String container for the result
 * @return number of rows returned by query; (-1) File not opened, (-2) Too many columns
 */
int Db::sqlQueryPull_single( const std::string query, std::string &item ) {
	if( !this->isFileOpened() ) {
		Db::logEvent( EventType::Error, "(Db::sqlQueryPull_single) Database file not opened." );
		return -1;
	}
	int rowCounter { 0 };
	sqlite3_stmt *statement { };

	sqlMsgCode( sqlite3_prepare( this->database, query.c_str(), -1, &statement, nullptr ) ); //prepare statement
	int cols = sqlite3_column_count( statement ); //get number of columns from statement
	if( cols > 1 ) {
		sqlMsgCode( sqlite3_finalize( statement ) );
		Db::logEvent( EventType::Error,
				"(Db::sqlQueryPull_single) Query returns more than one column." );
		return -2;
	}
	sqlMsgCode( sqlite3_step( statement ) ); //execute statement
	while( sqlite3_column_text( statement, 0 ) ) { //reading the 1st column of result
		if( sqlite3_column_text( statement, 0 ) == nullptr ) {
			Db::logEvent( EventType::Error, "(Db::sqlQueryPull_single) Query returns nothing." );
		} else {
			item = std::string( (char *) sqlite3_column_text( statement, 0 ) );
			rowCounter++;
		}
		sqlMsgCode( sqlite3_step( statement ) );
	}
	sqlMsgCode( sqlite3_finalize( statement ) );
	return rowCounter;
}

/**
 * Gives access to the 'tableOutput' storage
 * @return pointer to the TABLE
 */
TABLE & Db::getStoredTABLE() {
	return this->tableOutput;
}

///*********************************************************************
/// File operations
///*********************************************************************
/**
 * Opens the database file
 * @return the open state as a boolean
 */
bool Db::open() {
	const char * ptr_fileName = fileName.c_str();
	bool problemExists = sqlMsgCode( sqlite3_open( ptr_fileName, &database ) );
	if( problemExists ) {
		this->opened_flag = false;
	} else {
		Db::logEvent( EventType::Event, "(Db::open) Db connection opened." );
		this->opened_flag = true;
	}
	return this->opened_flag;
}

/**
 * closes the database
 */
void Db::close() {
	if( this->opened_flag ) {
		bool problemExists = sqlMsgCode( sqlite3_close( database ) );
		if( problemExists == 0 ) {
			Db::logEvent( EventType::Event, "(Db::close) Db connection closed." );
			this->opened_flag = false;
		}
	}
}

/**
 * Answers 'Is the db file opened?'
 * @return the opened state of the connection
 */
bool Db::isFileOpened() {
	return this->opened_flag;
}

/**
 * Gives the name of the database file
 * @return the filename as a string
 */
std::string Db::getFileName() {
	return this->fileName;
}

///*********************************************************************
/// Print operations [Console]
///*********************************************************************
/**
 * DEPRECIATED - Prints the result of given query directly to console
 * @param query SQL query as string
 * @return success as boolean
 */
bool Db::sqlQueryPrint( const std::string query ) {
	if( !this->isFileOpened() ) {
		return false;
	}
	/**
	 * Lambda callback method for 'sqlite3_exec'
	 */
	auto callback = []( void * ptr_data, int nCount, char** pValue, char** pName ) {
		std::string s {};
		for( int i = 0; i < nCount; i++ ) {
			s += pName[ i ];
			s += ": ";
			s += pValue[ i ];
			s += "\n";
		}
		std::cout << s << std::endl;
		return 0;
	};
	char *errMsg { };
	bool flag = sqlite3_exec( this->database, query.c_str(), callback, 0, &errMsg );
	if( flag ) {
		std::cout << "SQL Error: " << errMsg << std::endl;
	}
	return flag;
}

/**
 * Prints the data in 'tableOutput' to console
 */
void Db::printTableOutput() {
	typedef std::vector<std::vector<std::string>> columns;
	typedef std::vector<std::string> rows;

	for( columns::size_type i = 0; i < tableOutput.size(); i++ ) {
		std::cout << tableHeadings[ i ] << " >> ";
		for( rows::size_type j = 0; j < tableOutput[ i ].size(); j++ ) {
			std::cout << tableOutput[ i ][ j ] << ", ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

/**
 * Prints the data in given TABLE to console
 * @param table 'TABLE' container
 */
void Db::printThis( const TABLE & table ) {
	typedef std::vector<std::vector<std::string>> columns;
	typedef std::vector<std::string> rows;

	for( columns::size_type i = 0; i < table.size(); i++ ) {
		for( rows::size_type j = 0; j < table[ i ].size(); j++ ) {
			std::cout << table[ i ][ j ] << ", ";
		}
		std::cout << std::endl;
	}
	std::cout << std::endl;
}

/**
 * Print metadata of specified table
 * @param tableName Name of table
 */
void Db::printTableMetadata( std::string tableName ) {
	TABLE result { };
	std::stringstream ss { };
	ss << "PRAGMA table_info('" << tableName << "');";
	Db::sqlQueryPull( ss.str(), result );
	Db::printThis( result );
}

///*********************************************************************
/// Helper functions
///*********************************************************************
/**
 * Checks if given table exists in connected database
 * @param table Table to check
 * @return Its existence
 */
bool Db::checkTableExists( std::string table ) {
	TABLE t { };
	int rows_returned { 0 };
	std::stringstream ss { };
	ss << "SELECT name FROM sqlite_master WHERE type='table' AND name='" << table << "';";
	rows_returned = this->sqlQueryPull( ss.str(), t );
	if( rows_returned > 0 ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Checks if given column name exists in a given table
 * @param table Table to check
 * @param column Column to check
 * @return Its existence
 */
bool Db::checkColumnExists( std::string table, std::string column ) {
	TABLE t { };
	if( this->checkTableExists( table ) ) {
		this->getTableMetadata( table, t );
		if( Db::checkTable( t ) && Db::checkTableBoundary( t, 1, 0 ) ) {
			auto result = std::find( t[ 1 ].begin(), t[ 1 ].end(), column );
			if( result != std::end( t[ 1 ] ) ) {
				return true;
			}
		}
	}
	return false;
}

/**
 * Checks if an entry exists
 * @param table Table to check
 * @param column Column to check
 * @param entry String to find a match for
 * @return the number of occurrences for the search query
 */
int Db::checkEntryExists( std::string table, std::string column, std::string entry ) {
	TABLE t { };
	int rows_returned { 0 };
	std::stringstream ss { };
	ss << "SELECT * FROM " << table << " WHERE " << column << " LIKE '" << entry << "'";
	rows_returned = this->sqlQueryPull( ss.str(), t );
	return rows_returned;
}

/**
 * Checks if an 2 entries exist in the same row\n
 * -> good for two valued keys
 * @param table Table to check
 * @param column1 First column to check
 * @param entry1 String to find a match for inside the first column
 * @param column2 Second column to check
 * @param entry2 String to find a match for inside the second column
 * @return the number of occurrences for the search query
 */
int Db::checkEntryExists( std::string table, std::string column1, std::string entry1,
		std::string column2, std::string entry2 ) {
	TABLE t { };
	int rows_returned { 0 };
	std::stringstream ss { };
	ss << "SELECT * FROM " << table << " WHERE " << column1 << " LIKE '" << entry1 << "' AND "
			<< column2 << " LIKE '" << entry2 << "'";
	rows_returned = this->sqlQueryPull( ss.str(), t );
	return rows_returned;
}

/**
 * Gives the data held in a vector coordinate
 * @param col Column of the vector
 * @param row Row of the vector
 * @return The data
 */
std::string Db::getDataAt( const int col, const int row ) {
	if( Db::checkTableBoundary( this->tableOutput, col, row ) ) {
		return this->tableOutput[ col ][ row ];
	} else {
		return "";
	}
}

/**
 * Get the name of the specified column belonging to 'tableOutput'
 * @param col Column number
 * @return The name of the column
 */
std::string Db::getColName( const int col ) {
	if( static_cast<unsigned int>( col ) < this->tableHeadings.size() && col > 0 ) {
		return this->tableHeadings[ col ];
	} else {
		return "";
	}
}

/**
 * Get the name of the specified column from the database
 * @param col Column number
 * @param tableName Table name where the column is
 * @return Type as a string (empty if column doesn't exist)
 */
std::string Db::getColName( const int col, const std::string tableName ) {
	TABLE metadata { };
	this->getTableMetadata( tableName, metadata );
	if( Db::checkTable( metadata ) && Db::checkTableBoundary( metadata, col, 1 ) ) {
		return metadata[ 1 ][ col ];
	}
	return "";
}

/**
 * Gets the type a column [checks from the database]
 * @param column Column number
 * @param tableName Table name where the column belongs to
 * @return Type as a string (empty if column doesn't exist)
 */
std::string Db::getColType( const int column, const std::string tableName ) {
	TABLE metadata { };
	this->getTableMetadata( tableName, metadata );
	if( Db::checkTable( metadata ) && Db::checkTableBoundary( metadata, 0, column ) ) {
		return metadata[ 2 ][ column ];
	}
	return "";
}

/**
 * Gets the number of a column from its name [checks from the database]\n
 * Note: the name is case sensitive
 * @param name Name of the column
 * @param tableName Name of the table to look into
 * @return Number of the column (-1 if no match)
 */
int Db::getColNumber( const std::string colName, const std::string tableName ) {
	TABLE metadata { };
	this->getTableMetadata( tableName, metadata );
	if( Db::checkTable( metadata ) ) {
		for( unsigned int i = 0; i < metadata[ 1 ].size(); i++ ) {
			if( metadata[ 1 ][ i ] == colName ) {
				return i;
			}
		}
	}
	return -1;
}

/**
 * Gets a full row of data from a given table
 * @param rowNumber Row number to grab data from
 * @param tableName Name of the table
 * @param holder Where the row data gets stored in
 * @return Success
 */
bool Db::getFullRowData( const int rowNumber, const std::string tableName, TABLE & results ) {
	std::stringstream ss { };
	ss << "SELECT * FROM " << tableName << " WHERE ROWID = " << rowNumber;
	int rowsFound = this->sqlQueryPull( ss.str(), results );
	return ( rowsFound > 0 );
}

/**
 * Gets the value in a given column in the same tuple of a given value of another column
 * @param value Row identifying value
 * @param column Column where the Row identifying value lives in
 * @param table Table to search in
 * @param relationVal Holder for value to return
 * @param relationCol The column where the relationVal lives in
 * @return Success
 */
bool Db::getRowRelation( const std::string colValue, const std::string colName,
		const std::string tableName, std::string &relationValue,
		const std::string relationColName ) {
	TABLE results { };
	std::stringstream ss { };
	ss << "SELECT * FROM " << tableName << " WHERE " << colName << " = '" << colValue << "'";
	int rowsFound = this->sqlQueryPull( ss.str(), results );
	if( rowsFound > 0 ) {
		int colNumber = this->getColNumber( relationColName, tableName );
		if( colNumber < 0 ) { //Checking the col_name column exists
			Db::logEvent( EventType::Error, "(Db::getRowRelation) Column in table may not exist." );
			return false;
		}
		int relationColNumber = this->getColNumber( relationColName, tableName );
		if( relationColNumber < 0 ) { //Checking the relationCol column exists
			Db::logEvent( EventType::Error, "(Db::getRowRelation) Column in table may not exist." );
			return false;
		}
		relationValue = results[ relationColNumber ][ 0 ];
		return true;
	}
	Db::logEvent( EventType::Error,
			"(Db::getRowRelation) Nothing was found with specified arguments" );
//TODO check the pointA_rows & pointB_rows for type of message and send to error
	return false;
}

/**
 * Gets the number of rows in a table
 * @param table Table in the database
 * @return Number of rows or (-1) if table doesn't exist
 */
int Db::getNumberOfRows( std::string table ) {
	std::stringstream ss { };
	if( Db::checkTableExists( table ) ) {
		Table t { };
		ss << "SELECT COUNT(*) FROM " << table;
		Db::sqlQueryPull_typed( ss.str(), t );
		//COUNT(*)
		return t( 0, 0 ).getInt();
	} else {
		return -1;
	}
}

///*********************************************************************
/// Internal (private) functions
///*********************************************************************
/**
 * Gets metadata for a specified table
 * @param tableName Name of the table
 * @param metadata storage for the metadata
 */
void Db::getTableMetadata( const std::string tableName, TABLE &metadata ) {
	std::stringstream ss { };
	ss << "PRAGMA table_info('" << tableName << "');";
	Db::sqlQueryPull( ss.str(), metadata );
}

/**
 * Checks if the x-y coordinate given falls into the table boundaries
 * @param table Table to check
 * @param x Column coordinate
 * @param y Row coordinate
 * @return Validity of coordinate as boolean
 */
bool Db::checkTableBoundary( TABLE &table, const int &x, const int &y ) {
	unsigned int cols = table.size();
	unsigned int rows = table[ x ].size();
	if( static_cast<unsigned int>( x ) < cols && x >= 0 && static_cast<unsigned int>( y ) < rows
			&& y >= 0 ) {
		return true;
	} else {
		return false;
	}
}

/**
 * Check if the given table has something in it
 * @param table Table to check
 * @return result of check as boolean
 */
bool Db::checkTable( TABLE &table ) {
	if( table.empty() ) {
		sqlMsgCode( SQLITE_EMPTY_TABLE );
		return false;
	} else {
		return true;
	}
}

/**
 * Forces out-of-scope to clear memory on given TABLE
 * @param table TABLE to be cleared
 */
void Db::clearTABLE( TABLE & table ) {
	TABLE().swap( table );
}

/**
 * Log an event
 * @param nLevel Type of event
 * @param description Description of the event
 */
void Db::logEvent( EventType nLevel, const char *description ) {
	std::ostringstream oss { };
	oss << description;
	Log *log = Log::getInstance();
	log->newEvent( Location::Core, nLevel, oss );
}
