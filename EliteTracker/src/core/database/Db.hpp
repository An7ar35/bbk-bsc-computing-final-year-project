/*
 * db.hpp
 *
 *  Created on: 31 Aug 2014
 *      Author: Alwlyan
 */

#ifndef DB_HPP_
#define DB_HPP_

#include <iostream>
#include <vector>
#include <algorithm>
#include <iterator>
#include <stdlib.h>
#include "../../../lib/sqlite3.h"
#include "../logger/Log.hpp"
#include "../containers/Table.hpp"
#include "utils.hpp"
#include "sqlmsg.hpp"

typedef std::vector<std::vector<std::string>> TABLE;

/**
 * The Db class is a SQLite3 API wrapper for use with single target database file per instance
 */
class Db {
	public:
		Db( const std::string filename );
		~Db();
		friend std::ostream & operator <<( std::ostream &stream, const Db &db );

		///*********************************************************************
		/// Database operations
		///*********************************************************************
		int sqlQueryPull_typed( const std::string query, Table &table );
		int sqlQueryPull_typed( const std::string query, Table &table, int null_override );
		int sqlQueryPull( const std::string query ); //stores result in 'tableOutput'
		int sqlQueryPull( const std::string query, TABLE &table );
		int sqlQueryPull_add( const std::string query, TABLE &table );
		int sqlQueryPush( const std::string query ); //stores nothing
		int sqlQueryPull_single( const std::string query, int &item );
		int sqlQueryPull_single( const std::string query, double &item );
		int sqlQueryPull_single( const std::string query, std::string &item );
		TABLE & getStoredTABLE();

		///*********************************************************************
		/// File operations
		///*********************************************************************
		bool open();
		void close();
		bool isFileOpened();
		std::string getFileName();

		///*********************************************************************
		/// Print operations [Console]
		///*********************************************************************
		bool sqlQueryPrint( const std::string query );
		void printTableOutput(); //Depreciated
		void printThis( const TABLE &table );
		void printTableMetadata( const std::string tableName );

		///*********************************************************************
		/// Helper functions
		///*********************************************************************
		bool checkTableExists( std::string table );
		bool checkColumnExists( std::string table, std::string column );
		int checkEntryExists( std::string table, std::string column, std::string entry );
		int checkEntryExists( std::string table, std::string column1, std::string entry1,
				std::string column2, std::string entry2 );
		std::string getDataAt( const int col, const int row );
		std::string getColName( const int col );
		std::string getColName( const int col, const std::string tableName );
		std::string getColType( const int column, const std::string tableName );
		int getColNumber( const std::string colName, const std::string tableName );
		bool getFullRowData( const int rowNumber, const std::string tableName, TABLE &results );
		bool getRowRelation( const std::string colValue, const std::string colName,
				const std::string tableName, std::string &relationValue,
				const std::string relationColName );
		int getNumberOfRows( std::string table );

	private:
		bool opened_flag;
		std::string fileName;
		sqlite3 *database;
		std::vector<std::string> tableHeadings;
		TABLE tableOutput;

		///*********************************************************************
		/// Internal (private) functions
		///*********************************************************************
		void getTableMetadata( const std::string tableName, TABLE &metaData );
		static bool checkTableBoundary( TABLE &table, const int &x, const int &y );
		static bool checkTable( TABLE &table );
		static void clearTABLE( TABLE &table );
		static void logEvent( EventType nLevel, const char *description );
};

#endif /* DB_HPP_ */

/*
 * Sources:
 * http://www.sqlite.org/c3ref/funclist.html
 * http://stackoverflow.com/questions/18839799/retrieve-sqlite-table-data-in-c
 * http://stackoverflow.com/questions/11931978/c-sqlite-select-statement-and-store-as-vector
 * http://www.cplusplus.com/reference/vector/vector/clear/
 * http://stackoverflow.com/questions/1549930/c-equivalent-of-java-tostring
 *
 */
