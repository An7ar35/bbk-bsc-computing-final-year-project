/*
 * PlayerShips.hpp
 *
 *  Created on: 7 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_DATABASE_TABLES_PLAYERSHIPS_HPP_
#define CORE_DATABASE_TABLES_PLAYERSHIPS_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "../utils.hpp"
#include "../db.hpp"

namespace editTable {
	namespace PlayerShips {
		///*********************************************************************
		/// Custom getters
		///*********************************************************************
		bool getPlayerShipInfo( Db &data, int id, Table &table );
		bool getPlayerShipsInfo( Db &data, Table &table );
		bool getPlayerShips_graveyard( Db &data, Table &table );
		bool getPlayerShips_current( Db &data, Table &table );
		int getPlayerShip_ShipID( Db &data, int id );
		///*********************************************************************
		/// Add operations
		///*********************************************************************
		void add( Db &data, int shipID );
		void add( Db &data, int shipID, bool fuelscoop );
		///*********************************************************************
		/// Edit operations
		///*********************************************************************
		void edit( Db &data, int id, bool destroyed );
		void edit( Db &data, int id, bool fuelscoop, bool destroyed );
		///*********************************************************************
		/// Remove operations
		///*********************************************************************
		void remove( Db &data, int id );
		///*********************************************************************
		/// Check operations
		///*********************************************************************
		bool checkStructure( Db &data );
	}
}

#endif /* CORE_DATABASE_TABLES_PLAYERSHIPS_HPP_ */
