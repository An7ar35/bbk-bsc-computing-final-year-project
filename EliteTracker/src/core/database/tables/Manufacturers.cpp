/*
 * Manufacturers.cpp
 *
 *  Created on: 7 Feb 2015
 *      Author: Alwlyan
 */

#include "Manufacturers.hpp"

///*********************************************************************
/// Add operations
///*********************************************************************
/**
 * Adds entry
 * @param data Db object
 * @param manufacturer Manufacturer
 */
void editTable::Manufacturers::add( Db &data, std::string manufacturer ) {
	std::stringstream ss { };
	ss << "INSERT INTO Manufacturers( Manufacturer ) VALUES ( \"" << manufacturer << "\" )";
	data.sqlQueryPush( ss.str() );
}

///*********************************************************************
/// Edit operations
///*********************************************************************
/**
 * Edits/Updates the PlayerShip table
 * @param data Db object
 * @param id Manufacturer ID
 * @param manufacturer Manufacturer
 */
void editTable::Manufacturers::edit( Db &data, int id, std::string manufacturer ) {
	if( data.checkEntryExists( "Manufacturers", "ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE Manufacturers SET Manufacturer = \"" << manufacturer << "\" WHERE ID = " << id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - ID doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Remove operations
///*********************************************************************
/**
 * Removes entry
 * @param data Db object
 * @param id ID
 */
void editTable::Manufacturers::remove( Db &data, int id ) {
	if( data.checkEntryExists( "Manufacturers", "ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM Manufacturers WHERE ID = " << id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - ID doesn't exist. Nothing done." );
	}
}

/**
 * Removes entry
 * @param data Db object
 * @param manufacturer Manufacturer
 */
void editTable::Manufacturers::remove( Db &data, std::string manufacturer ) {
	if( data.checkEntryExists( "Manufacturers", "Manufacturer", manufacturer ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM Manufacturers WHERE Manufacturer = \"" << manufacturer << "\"";
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - Manufacturer doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Check operations
///*********************************************************************
bool editTable::Manufacturers::checkStructure( Db &data ) {
	bool correctStructureFlag { true };
	if( !data.checkColumnExists( "Manufacturers", "ID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Manufacturers\' table \'ID\' column missing." );
	}
	if( !data.checkColumnExists( "Manufacturers", "Manufacturer" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Manufacturers\' table \'Manufacturer\' column missing." );
	}
	return correctStructureFlag;
}

