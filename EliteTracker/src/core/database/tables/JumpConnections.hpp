/*
 * JumpConnections.hpp
 *
 *  Created on: 30 Sep 2014
 *      Author: Alwlyan
 */

#ifndef JUMPCONNECTIONS_HPP_
#define JUMPCONNECTIONS_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "../utils.hpp"
#include "../db.hpp"
#include "../../containers/Coordinate3D.hpp"
#include "../../toolbox/math/spatial.hpp"

namespace editTable {
	namespace JumpConnections {
		///*********************************************************************
		/// Custom getters
		///*********************************************************************
		double getDistance( Db &data, int pointA, int pointB );
		Table & getAllDistancesFor( Db &data, int point_id );
		///*********************************************************************
		/// Add operations
		///*********************************************************************
		void add( Db &data, int pointA, int pointB );
		void add( Db &data, int pointA, int pointB, double distance );
		void add( Db & data, std::string pointA, std::string pointB );
		void add( Db & data, std::string pointA, std::string pointB, double distance );
		///*********************************************************************
		/// Edit operations
		///*********************************************************************
		void edit( Db &data, int pointA_id, int pointB_id, double distance );
		///*********************************************************************
		/// Remove operations
		///*********************************************************************
		void remove( Db &data, int pointA_id, int pointB_id );
		///*********************************************************************
		/// Check operations
		///*********************************************************************
		bool checkStructure( Db &data );
	}
}

#endif /* JUMPCONNECTIONS_HPP_ */
