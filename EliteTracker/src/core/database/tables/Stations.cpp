/*
 * Stations.cpp
 *
 *  Created on: 12 Mar 2015
 *      Author: Es A. Davison
 */

#include "Stations.hpp"

///*********************************************************************
/// Add operations
///*********************************************************************
/**
 * Add to 'Stations' table
 * @param data Db Object
 * @param s_id System ID
 * @param name Name of station
 */
void editTable::Stations::add( Db &data, int s_id, std::string name ) {
	std::stringstream ss { };
	ss << "INSERT INTO Stations( S_ID, Name ) VALUES ( " << s_id << ", \"" << name << "\" )";
	data.sqlQueryPush( ss.str() );
}

/**
 * Add to 'Stations' table
 * @param data Db Object
 * @param s_id System ID
 * @param eID Internal Elite ID for station
 * @param name Name of station
 */
void editTable::Stations::add( Db &data, int s_id, int eID, std::string name ) {
	std::stringstream ss { };
	ss << "INSERT INTO Stations( S_ID, EliteID, Name ) VALUES ( " << s_id << ", " << eID << ",\""
			<< name << "\" )";
	data.sqlQueryPush( ss.str() );
}

/**
 * Add to 'Stations' table
 * @param data Db Object
 * @param s_id System ID
 * @param name Name of station
 * @param description Description of station
 */
void editTable::Stations::add( Db &data, int s_id, std::string name, std::string description ) {
	std::stringstream ss { };
	ss << "INSERT INTO Stations( S_ID, Name, Description ) VALUES ( " << s_id << ", \"" << name
			<< "\", \"" << description << "\" )";
	data.sqlQueryPush( ss.str() );
}

/**
 * Add to 'Stations' table
 * @param data Db Object
 * @param s_id System ID
 * @param eID Internal Elite ID for station
 * @param name Name of station
 * @param description Description of station
 */
void editTable::Stations::add( Db &data, int s_id, int eID, std::string name,
		std::string description ) {
	std::stringstream ss { };
	ss << "INSERT INTO Objects( S_ID, EliteID, Name, Description ) VALUES ( " << s_id << ", " << eID
			<< ",\"" << name << "\", \"" << description << "\" )";
	data.sqlQueryPush( ss.str() );
}
///*********************************************************************
/// Edit operations
///*********************************************************************
/**
 * Edits/Updates the 'Stations' table
 * @param data Db Object
 * @param s_id System ID
 * @param eID Internal Elite ID for station
 */
void editTable::Stations::edit( Db &data, int s_id, int eID ) {
	if( data.checkEntryExists( "Stations", "S_ID", std::to_string( s_id ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE Stations SET EliteID = " << eID << " WHERE S_ID = " << s_id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - Station id doesn't exist. Nothing done." );
	}
}

/**
 * Edits/Updates the 'Stations' table
 * @param data Db Object
 * @param s_id System ID
 * @param description Description of station
 */
void editTable::Stations::edit( Db &data, int s_id, std::string description ) {
	if( data.checkEntryExists( "Stations", "S_ID", std::to_string( s_id ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE Stations SET Description = \"" << description << "\" WHERE S_ID = " << s_id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - Station id doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Remove operations
///*********************************************************************
/**
 * Removes an entry
 * @param data Db Object
 * @param eID Internal Elite ID for station
 */
void editTable::Stations::remove( Db &data, int eID ) {
if( data.checkEntryExists( "Stations", "EliteID", std::to_string( eID ) ) ) {
	std::stringstream ss { };
	ss << "DELETE FROM Stations WHERE EliteID = " << eID;
	data.sqlQueryPush( ss.str() );
} else {
	Log *log = Log::getInstance();
	log->logEvent( Location::Core, EventType::Error,
			"Trying to [remove] - EliteID doesn't exist. Nothing done." );
}
}

/**
 * Removes an entry
 * @param data Db Object
 * @param name Name of station
 */
void editTable::Stations::remove( Db &data, std::string name ) {
if( data.checkEntryExists( "Stations", "Name", name ) ) {
	std::stringstream ss { };
	ss << "DELETE FROM Stations WHERE Name = " << name;
	data.sqlQueryPush( ss.str() );
} else {
	Log *log = Log::getInstance();
	log->logEvent( Location::Core, EventType::Error,
			"Trying to [remove] - Name doesn't exist. Nothing done." );
}
}

///*********************************************************************
/// Check operations
///*********************************************************************
/**
 * Check 'Objects' table structure
 * @param data Db Object
 * @return Structure integrity state
 */
bool editTable::Stations::checkStructure( Db &data ) {
bool correctStructureFlag { true };
if( !data.checkColumnExists( "Stations", "S_ID" ) ) {
	correctStructureFlag = false;
	Log *log = Log::getInstance();
	log->logEvent( Location::Core, EventType::Error,
			"\'Stations\' table \'S_ID\' column missing." );
}
if( !data.checkColumnExists( "Stations", "EliteID" ) ) {
	correctStructureFlag = false;
	Log *log = Log::getInstance();
	log->logEvent( Location::Core, EventType::Error,
			"\'Stations\' table \'EliteID\' column missing." );
}
if( !data.checkColumnExists( "Stations", "Name" ) ) {
	correctStructureFlag = false;
	Log *log = Log::getInstance();
	log->logEvent( Location::Core, EventType::Error,
			"\'Stations\' table \'Name\' column missing." );
}
if( !data.checkColumnExists( "Stations", "Description" ) ) {
	correctStructureFlag = false;
	Log *log = Log::getInstance();
	log->logEvent( Location::Core, EventType::Error,
			"\'Stations\' table \'Description\' column missing." );
}
return correctStructureFlag;
}
