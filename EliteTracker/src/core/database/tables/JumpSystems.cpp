/*
 * jumpsystems.cpp
 *
 *  Created on: 30 Sep 2014
 *      Author: Alwlyan
 */

#include "JumpSystems.hpp"

///*********************************************************************
/// Custom getters
///*********************************************************************
Coordinate3D editTable::JumpSystems::getCoordinateFor( Db &data, int point_id ) {
	Table t { };
	Coordinate3D coordinates { };
	std::ostringstream oss { };
	oss << "SELECT X_Coordinate, Y_Coordinate, Z_Coordinate FROM JumpSystems WHERE S_ID = "
			<< point_id;
	data.sqlQueryPull_typed( oss.str(), t );
	if( t.getRowCount() > 0 ) {
		int x_col = t.whereIsThisColumn( "X_Coordinate" );
		int y_col = t.whereIsThisColumn( "Y_Coordinate" );
		int z_col = t.whereIsThisColumn( "Z_Coordinate" );
		if( t( x_col, 0 ).getString().length() == 0 || t( y_col, 0 ).getString().length() == 0
				|| t( z_col, 0 ).getString().length() == 0 ) {
			coordinates.error_flag = true;
		}
		coordinates.x = t( x_col, 0 ).getDouble();
		coordinates.y = t( y_col, 0 ).getDouble();
		coordinates.z = t( z_col, 0 ).getDouble();
	} else {
		coordinates.error_flag = true;
	}
	if( coordinates.error_flag ) {
		oss.clear();
		oss.str( std::string() );
		oss
				<< "[editTable::JumpSystems::getCoordinateFor(..)] Couldn't find/get coordinates for point: '"
				<< point_id << "'.";
		Log *log = Log::getInstance();
		log->newEvent( Location::Core, EventType::Error, oss );
	}
	return coordinates;
}
/**
 * Gets the S_ID of a specified System
 * @param data Db object
 * @param name Name of the system
 * @return S_ID or (-1) of not found.
 */
int editTable::JumpSystems::getIdFor( Db &data, std::string name ) {
	std::stringstream ss { };
	Table t { };
	ss << "SELECT S_ID FROM JumpSystems WHERE Name = \"" << name << "\"";
	data.sqlQueryPull_typed( ss.str(), t );
	int id_col = t.whereIsThisColumn( "S_ID" );
	if( t.getRowCount() > 0 && id_col >= 0 ) {
		return t( id_col, 0 ).getInt();
	}
	return -1;
}
///*********************************************************************
/// Add operations
///*********************************************************************
/**
 * Add to 'JumpSystems' table exc. coordinates
 * @param data Db Object
 * @param name Name of the system
 */
void editTable::JumpSystems::add( Db &data, std::string name ) {
	std::stringstream ss { };
	ss << "INSERT INTO JumpSystems( Name ) VALUES ( \"" << name << "\" )";
	data.sqlQueryPush( ss.str() );
}

/**
 * Add to 'JumpSystems' table exc. coordinates
 * @param data Db Object
 * @param eID Elite's system ID (use '0' for unknown)
 * @param name Name of the system
 */
void editTable::JumpSystems::add( Db &data, int eID, std::string name ) {
	std::stringstream ss { };
	if( eID <= 0 ) {
		ss << "INSERT INTO JumpSystems( Name ) VALUES ( \"" << name << "\" )";
	} else {
		ss << "INSERT INTO JumpSystems( Name ) VALUES ( " << eID << ",\"" << name << "\" )";
	}
	data.sqlQueryPush( ss.str() );
}

/**
 * Add to 'JumpSystems' table inc. coordinates
 * @param data Db Object
 * @param eID Elite's system ID
 * @param name Name of the system
 * @param x X coordinate
 * @param y Y coordinate
 * @param z Z coordinate
 */
void editTable::JumpSystems::add( Db &data, int eID, std::string name, double x, double y,
		double z ) {
	std::stringstream ss { };
	ss
			<< "INSERT INTO JumpSystems( EliteID, Name, X_Coordinate, Y_Coordinate, Z_Coordinate ) VALUES ( "
			<< eID << ",\"" << name << "\"," << x << "," << y << "," << z << " )";
	data.sqlQueryPush( ss.str() );
}

///*********************************************************************
/// Edit operations
///*********************************************************************
/**
 * Edits/Updates the JumpSystems table
 * @param data Db Object
 * @param id Row ID
 * @param eID Elite's system ID
 */
void editTable::JumpSystems::edit( Db &data, int id, int eID ) {
	//TODO edit jumpsystems
	if( data.checkEntryExists( "JumpSystems", "S_ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE JumpSystems SET EliteID = " << eID << " WHERE S_ID = " << id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - ID doesn't exist. Nothing done." );
	}
}

/**
 * Edit/Updates the JumpSystems table
 * @param data Db Object
 * @param id Row ID
 * @param name Name of the system
 */
void editTable::JumpSystems::edit( Db &data, int id, std::string name ) {
	if( data.checkEntryExists( "JumpSystems", "S_ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE JumpSystems SET Name = \"" << name << "\" WHERE S_ID = " << id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - ID doesn't exist. Nothing done." );
	}
}

/**
 * Edit/Updates the JumpSystems table
 * @param data Db Object
 * @param id Row ID
 * @param x X coordinate
 * @param y Y coordinate
 * @param z Z coordinate
 */
void editTable::JumpSystems::edit( Db &data, int id, double x, double y, double z ) {
	if( data.checkEntryExists( "JumpSystems", "S_ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE JumpSystems SET X_Coordinate = " << x << ", Y_Coordinate = " << y
				<< ", Z_Coordinate = " << z << " WHERE S_ID = " << id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - ID doesn't exist. Nothing done." );
	}
}
///*********************************************************************
/// Remove operations
///*********************************************************************
/**
 * Removes an entry
 * @param data Db object
 * @param id S_ID of the entry
 */
void editTable::JumpSystems::remove( Db &data, int id ) {
	if( data.checkEntryExists( "JumpSystems", "S_ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM JumpSystems WHERE S_ID = " << id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - ID doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Check operations
///*********************************************************************
/**
 * Check 'JumpSystems' table structure
 * @param data Db Object
 * @return Structure integrity state
 */
bool editTable::JumpSystems::checkStructure( Db &data ) {
	bool correctStructureFlag { true };
	if( !data.checkColumnExists( "JumpSystems", "S_ID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'JumpSystems\' table \'S_ID\' column missing." );
	}
	if( !data.checkColumnExists( "JumpSystems", "EliteID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'JumpSystems\' table \'EliteID\' column missing." );
	}
	if( !data.checkColumnExists( "JumpSystems", "Name" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'JumpSystems\' table \'Name\' column missing." );
	}
	if( !data.checkColumnExists( "JumpSystems", "X_Coordinate" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'JumpSystems\' table \'X_Coordinate\' column missing." );
	}
	if( !data.checkColumnExists( "JumpSystems", "Y_Coordinate" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'JumpSystems\' table \'Y_Coordinate\' column missing." );
	}
	if( !data.checkColumnExists( "JumpSystems", "Z_Coordinate" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'JumpSystems\' table \'Z_Coordinate\' column missing." );
	}
	return correctStructureFlag;
}

