/*
 * ShipClasses.cpp
 *
 *  Created on: 7 Feb 2015
 *      Author: Es A. Davison
 */

#include "ShipClasses.hpp"

///*********************************************************************
/// Add operations
///*********************************************************************
/**
 * Adds an entry
 * @param data Db object
 * @param ship_class Ship class
 */
void editTable::ShipClasses::add( Db &data, std::string ship_class ) {
	std::stringstream ss { };
	ss << "INSERT INTO ShipClasses( ShipClass ) VALUES ( \"" << ship_class << "\" )";
	data.sqlQueryPush( ss.str() );
}

///*********************************************************************
/// Edit operations
///*********************************************************************
/**
 * Edits/Updates the PlayerShip table
 * @param data Db object
 * @param id
 * @param ship_class
 */
void editTable::ShipClasses::edit( Db &data, int id, std::string ship_class ) {
	if( data.checkEntryExists( "ShipClasses", "ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE ShipClasses SET ShipClass = \"" << ship_class << "\" WHERE ID = " << id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - ID doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Remove operations
///*********************************************************************
/**
 * Removes an entry
 * @param data Db object
 * @param classID Ship class ID
 */
void editTable::ShipClasses::remove( Db &data, int classID ) {
	if( data.checkEntryExists( "ShipClasses", "ID", std::to_string( classID ) ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM ShipClasses WHERE ID = " << classID;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - ID doesn't exist. Nothing done." );
	}
}
/**
 * Removes an entry
 * @param data Db object
 * @param ship_class Ship class
 */
void editTable::ShipClasses::remove( Db &data, std::string ship_class ) {
	if( data.checkEntryExists( "ShipClasses", "ShipClass", ship_class ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM PlayerShips WHERE ShipClass = '" << ship_class << "'";
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - ShipClass doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Check operations
///*********************************************************************
/**
 * Check 'ShipClasses' table structure
 * @param data Db object
 * @return Structure integrity state
 */
bool editTable::ShipClasses::checkStructure( Db &data ) {
	bool correctStructureFlag { true };
	if( !data.checkColumnExists( "ShipClasses", "ID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'ShipClasses\' table \'ID\' column missing." );
	}
	if( !data.checkColumnExists( "ShipClasses", "ShipClass" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'ShipClasses\' table \'ShipClass\' column missing." );
	}
	return correctStructureFlag;
}
