/*
 * PlayerShip.cpp
 *
 *  Created on: 7 Feb 2015
 *      Author: Es A. Davison
 */

#include "PlayerShips.hpp"

///*********************************************************************
/// Custom Getters
///*********************************************************************
/**
 * Get complete specs for player ship id provided
 * @param data Db object
 * @param id Player ship ID
 * @param table Container
 * @return Success
 */
bool editTable::PlayerShips::getPlayerShipInfo( Db &data, int id, Table &table ) {
	std::ostringstream oss { };
	oss
			<< "SELECT PlayerShips.ID ID, p1.Name Name, p2.ShipClass Class, p3.Manufacturer Manufacturer, PlayerShips.FuelScoop \"Fuel Scoop\", p1.MaxCargo \"Max Cargo (T)\", p1.JumpRangeFull \"Jump Range Full (Ly)\", p1.JumpRangeEmpty \"Jump Range Empty (Ly)\", p1.MaxSpeed \"Max Speed (m/s)\", p1.BoostSpeed \"Boost Speed (m/s)\" FROM PlayerShips LEFT JOIN Ships p1 ON p1.ID = PlayerShips.ShipID LEFT JOIN ShipClasses p2 ON p2.ID = p1.ShipClassID LEFT JOIN Manufacturers p3 ON p3.ID = p1.ManufacturerID  WHERE PlayerShips.ID = "
			<< id;
	int returned = data.sqlQueryPull_typed( oss.str(), table );
	return ( returned >= 0 );
}
/**
 * Get complete all player's ships (inc. specs)
 * @param data Db object
 * @param table Container
 * @return Success
 */
bool editTable::PlayerShips::getPlayerShipsInfo( Db &data, Table &table ) {
	std::string query =
			"SELECT PlayerShips.ID ID, p1.Name Name, p2.ShipClass Class, p3.Manufacturer Manufacturer, PlayerShips.FuelScoop \"Fuel Scoop\", p1.MaxCargo \"Max Cargo (T)\", p1.JumpRangeFull \"Jump Range Full (Ly)\", p1.JumpRangeEmpty \"Jump Range Empty (Ly)\", p1.MaxSpeed \"Max Speed (m/s)\", p1.BoostSpeed \"Boost Speed (m/s)\" FROM PlayerShips LEFT JOIN Ships p1 ON p1.ID = PlayerShips.ShipID LEFT JOIN ShipClasses p2 ON p2.ID = p1.ShipClassID LEFT JOIN Manufacturers p3 ON p3.ID = p1.ManufacturerID";
	int returned = data.sqlQueryPull_typed( query, table );
	return ( returned >= 0 );
}
/**
 * Get complete all player's ships destroyed
 * @param data Db object
 * @param table Container
 * @return Success
 */
bool editTable::PlayerShips::getPlayerShips_graveyard( Db &data, Table &table ) {
	std::string query =
			"SELECT PlayerShips.ID ID, p1.Name Name, p2.ShipClass Class, p3.Manufacturer Manufacturer, PlayerShips.FuelScoop \"Fuel Scoop\", p1.MaxCargo \"Max Cargo (T)\", p1.JumpRangeFull \"Jump Range Full (Ly)\", p1.JumpRangeEmpty \"Jump Range Empty (Ly)\", p1.MaxSpeed \"Max Speed (m/s)\", p1.BoostSpeed \"Boost Speed (m/s)\" FROM PlayerShips LEFT JOIN Ships p1 ON p1.ID = PlayerShips.ShipID LEFT JOIN ShipClasses p2 ON p2.ID = p1.ShipClassID LEFT JOIN Manufacturers p3 ON p3.ID = p1.ManufacturerID WHERE PlayerShips.DestroyedFlag = \"TRUE\"";
	int returned = data.sqlQueryPull_typed( query, table );
	return ( returned >= 0 );
}
/**
 * Get complete all player's ships not destroyed
 * @param data Db object
 * @param table Container
 * @return Success
 */
bool editTable::PlayerShips::getPlayerShips_current( Db &data, Table &table ) {
	std::string query =
			"SELECT PlayerShips.ID ID, p1.Name Name, p2.ShipClass Class, p3.Manufacturer Manufacturer, PlayerShips.FuelScoop \"Fuel Scoop\", p1.MaxCargo \"Max Cargo (T)\", p1.JumpRangeFull \"Jump Range Full (Ly)\", p1.JumpRangeEmpty \"Jump Range Empty (Ly)\", p1.MaxSpeed \"Max Speed (m/s)\", p1.BoostSpeed \"Boost Speed (m/s)\" FROM PlayerShips LEFT JOIN Ships p1 ON p1.ID = PlayerShips.ShipID LEFT JOIN ShipClasses p2 ON p2.ID = p1.ShipClassID LEFT JOIN Manufacturers p3 ON p3.ID = p1.ManufacturerID WHERE PlayerShips.DestroyedFlag = \"FALSE\"";
	int returned = data.sqlQueryPull_typed( query, table );
	return ( returned >= 0 );
}
/**
 * Get the base ship ID of the player ship
 * @param data Db object
 * @param id Player ship ID
 * @return Ship ID (-1 if fail)
 */
int editTable::PlayerShips::getPlayerShip_ShipID( Db &data, int id ) {
	std::ostringstream query { };
	query
			<< "SELECT p1.ID FROM Ships p1 LEFT JOIN PlayerShips p2 ON p2.ShipID = p1.ID WHERE p2.ID = "
			<< id;
	Table t { };
	int returned = data.sqlQueryPull_typed( query.str(), t );
	if( returned > 0 ) {
		int col = t.whereIsThisColumn( "ID" );
		if( t.getRowCount() > 0 ) {
			return t( col, 0 ).getInt();
		}
	}
	return -1;
}
///*********************************************************************
/// Add operations
///*********************************************************************
/**
 * Add an entry
 * @param data Db Object
 * @param shipID Ship ID
 */
void editTable::PlayerShips::add( Db &data, int shipID ) {
	std::stringstream ss { };
	ss << "INSERT INTO PlayerShips( ShipID ) VALUES ( " << shipID << " )";
	data.sqlQueryPush( ss.str() );
}

/**
 * Add an entry
 * @param data Db Object
 * @param shipID Ship ID
 * @param fuelscoop Whether or not there is fuel scoop capability
 */
void editTable::PlayerShips::add( Db &data, int shipID, bool fuelscoop ) {
	std::stringstream ss { };
	if( fuelscoop ) {
		ss << "INSERT INTO PlayerShips( ShipID, FuelScoop ) VALUES ( " << shipID << ", \"TRUE\" )";
	} else {
		ss << "INSERT INTO PlayerShips( ShipID, FuelScoop ) VALUES ( " << shipID << ", \"FALSE\" )";
	}
	data.sqlQueryPush( ss.str() );
}
///*********************************************************************
/// Edit operations
///*********************************************************************
/**
 * Edits/Updates the PlayerShip table
 * @param data Db Object
 * @param id ID of the entry
 * @param destroyed DestroyedFlag
 */
void editTable::PlayerShips::edit( Db &data, int id, bool destroyed ) {
	if( data.checkEntryExists( "PlayerShips", "ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		if( destroyed ) {
			ss << "UPDATE PlayerShips SET DestroyedFlag = \"TRUE\" WHERE ID = " << id;
		} else {
			ss << "UPDATE PlayerShips SET DestroyedFlag = \"FALSE\" WHERE ID = " << id;
		}
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - ID doesn't exist. Nothing done." );
	}
}

/**
 * Edits/Updates the PlayerShip table
 * @param data Db Object
 * @param id ID of the entry
 * @param fuelscoop Fuel scoop capability
 */
void editTable::PlayerShips::edit( Db &data, int id, bool fuelscoop, bool destroyed ) {
	if( data.checkEntryExists( "PlayerShips", "ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		std::string T = "TRUE";
		std::string F = "FALSE";
		ss << "UPDATE PlayerShips SET FuelScoop = ";
		if( fuelscoop ) {
			ss << "\"" << T << "\"";
		} else {
			ss << "\"" << F << "\"";
		}
		if( destroyed ) {
			ss << ", DestroyedFlag = \"" << T << "\" WHERE ID = " << id;
		} else {
			ss << ", DestroyedFlag = \"" << F << "\" WHERE ID = " << id;
		}
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - ID doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Remove operations
///*********************************************************************
/**
 * Removes an entry
 * @param data Db object
 * @param id ID of the entry
 */
void editTable::PlayerShips::remove( Db &data, int id ) {
	if( data.checkEntryExists( "PlayerShips", "ID", std::to_string( id ) ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM PlayerShips WHERE ID = " << id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - ID doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Check operations
///*********************************************************************
/**
 * Check 'PlayerShips' table structure
 * @param data Db object
 * @return Structure integrity state
 */
bool editTable::PlayerShips::checkStructure( Db &data ) {
	bool correctStructureFlag { true };
	if( !data.checkColumnExists( "PlayerShips", "ID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'PlayerShips\' table \'ID\' column missing." );
	}
	if( !data.checkColumnExists( "PlayerShips", "ShipID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'PlayerShips\' table \'ShipID\' column missing." );
	}
	if( !data.checkColumnExists( "PlayerShips", "FuelScoop" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'PlayerShips\' table \'FuelScoop\' column missing." );
	}
	if( !data.checkColumnExists( "PlayerShips", "DestroyedFlag" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'PlayerShips\' table \'DestroyedFlag\' column missing." );
	}
	return correctStructureFlag;
}

