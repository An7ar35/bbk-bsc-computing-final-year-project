/*
 * Manufacturers.hpp
 *
 *  Created on: 7 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_DATABASE_TABLES_MANUFACTURERS_HPP_
#define CORE_DATABASE_TABLES_MANUFACTURERS_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "../utils.hpp"
#include "../db.hpp"

namespace editTable {
	namespace Manufacturers {
		///*********************************************************************
		/// Add operations
		///*********************************************************************
		void add( Db &data, std::string manufacturer );
		///*********************************************************************
		/// Edit operations
		///*********************************************************************
		void edit( Db &data, int id, std::string manufacturer );
		///*********************************************************************
		/// Remove operations
		///*********************************************************************
		void remove( Db &data, int id );
		void remove( Db &data, std::string manufacturer );
		///*********************************************************************
		/// Check operations
		///*********************************************************************
		bool checkStructure( Db &data );
	}
}

#endif /* CORE_DATABASE_TABLES_MANUFACTURERS_HPP_ */
