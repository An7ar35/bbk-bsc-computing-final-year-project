/*
 * Ships.hpp
 *
 *  Created on: 7 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_DATABASE_TABLES_SHIPS_HPP_
#define CORE_DATABASE_TABLES_SHIPS_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "../utils.hpp"
#include "../db.hpp"

namespace editTable {
	namespace Ships {
		///*********************************************************************
		/// Custom getters
		///*********************************************************************
		bool getShipsInfo( Db &data, Table &table );
		///*********************************************************************
		/// Add operations
		///*********************************************************************
		void add( Db &data, std::string name, int shipClassID, int manufacturersID,
				double fuelCap, int maxCargo, double jumpRangeFull, double jumpRangeEmpty,
				int maxSpeed, int boostSpeed );
		///*********************************************************************
		/// Edit operations
		///*********************************************************************
		//Not available
		///*********************************************************************
		/// Remove operations
		///*********************************************************************
		void remove( Db &data, int shipID );
		void remove( Db &data, std::string name );
		///*********************************************************************
		/// Check operations
		///*********************************************************************
		bool checkStructure( Db &data );
	}
}

//Ships - ID, Name, ShipClassID<ShipClass>, ManufacturerID<Manufacturers>, FuelCapacity, MaxCargo, JumpRangeFull, JumpRangeEmpty, MaxSpeed, BoostSpeed

#endif /* CORE_DATABASE_TABLES_SHIPS_HPP_ */
