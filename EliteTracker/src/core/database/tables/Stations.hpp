/*
 * Stations.hpp
 *
 *  Created on: 12 Mar 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_DATABASE_TABLES_STATIONS_HPP_
#define CORE_DATABASE_TABLES_STATIONS_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "../utils.hpp"
#include "../db.hpp"

namespace editTable {
	namespace Stations {
		///*********************************************************************
		/// Add operations
		///*********************************************************************
		//S_ID , EliteID, Name, Description
		void add( Db &data, int s_id, std::string name );
		void add( Db &data, int s_id, int eID, std::string name );
		void add( Db &data, int s_id, std::string name, std::string description );
		void add( Db &data, int s_id, int eID, std::string name, std::string description );
		///*********************************************************************
		/// Edit operations
		///*********************************************************************
		void edit( Db &data, int s_id, int eID );
		void edit( Db &data, int s_id, std::string description );
		///*********************************************************************
		/// Remove operations
		///*********************************************************************
		void remove( Db &data, int eID );
		void remove( Db &data, std::string name );
		///*********************************************************************
		/// Check operations
		///*********************************************************************
		bool checkStructure( Db &data );
	}
}

#endif /* CORE_DATABASE_TABLES_STATIONS_HPP_ */
