/*
 * ShipClasses.hpp
 *
 *  Created on: 7 Feb 2015
 *      Author: Alwlyan
 */

#ifndef CORE_DATABASE_TABLES_SHIPCLASSES_HPP_
#define CORE_DATABASE_TABLES_SHIPCLASSES_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "../utils.hpp"
#include "../db.hpp"

namespace editTable {
	namespace ShipClasses {
		///*********************************************************************
		/// Add operations
		///*********************************************************************
		void add( Db &data, std::string ship_class );
		///*********************************************************************
		/// Edit operations
		///*********************************************************************
		void edit( Db &data, int classID, std::string ship_class );
		///*********************************************************************
		/// Remove operations
		///*********************************************************************
		void remove( Db &data, int classID );
		void remove( Db &data, std::string ship_class );
		///*********************************************************************
		/// Check operations
		///*********************************************************************
		bool checkStructure( Db &data );
	}
}

#endif /* CORE_DATABASE_TABLES_SHIPCLASSES_HPP_ */
