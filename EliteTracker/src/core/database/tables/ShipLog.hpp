/*
 * ShipLog.hpp
 *
 *  Created on: 24 Mar 2015
 *      Author: Es A. Davison
 */

#ifndef CORE_DATABASE_TABLES_SHIPLOG_HPP_
#define CORE_DATABASE_TABLES_SHIPLOG_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "../utils.hpp"
#include "../db.hpp"

namespace editTable {
	namespace ShipLog {
		///*********************************************************************
		/// Custom getters
		///*********************************************************************
		bool getShipLogData( Db &data, Table &table );
		///*********************************************************************
		/// Add operations
		///*********************************************************************
		void add( Db &data, std::string date, int from, int to );
		void add( Db &data, std::string date, std::string time, int from, int to );
		void add( Db &data, std::string date, int from, int to, std::string description );
		void add( Db &data, std::string date, std::string time, int from, int to, std::string description );
		///*********************************************************************
		/// Edit operations
		///*********************************************************************
		void edit( Db &data, int entry_number, std::string date, std::string time );
		void edit( Db &data, int entry_number, int from, int to );
		void edit( Db &data, int entry_number, std::string description );
		void edit( Db &data, int entry_number, std::string date, std::string time, int from, int to, std::string description );
		///*********************************************************************
		/// Remove operations
		///*********************************************************************
		void remove( Db &data, int entry_number );
		///*********************************************************************
		/// Check operations
		///*********************************************************************
		bool checkStructure( Db &data );
	}
}

//ShipLog - EntryNumber, Date, Time, From_ID<JumpSystems>, To_ID<JumpSystems>, Description

#endif /* CORE_DATABASE_TABLES_SHIPLOG_HPP_ */
