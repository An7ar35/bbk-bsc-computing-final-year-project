/*
 * jumpsystems.hpp
 *
 *  Created on: 30 Sep 2014
 *      Author: Alwlyan
 */

#ifndef JUMPSYSTEMS_HPP_
#define JUMPSYSTEMS_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "../utils.hpp"
#include "../db.hpp"
#include "../../containers/Coordinate3D.hpp"

namespace editTable {
	namespace JumpSystems {
		///*********************************************************************
		/// Custom getters
		///*********************************************************************
		Coordinate3D getCoordinateFor( Db &data, int point_id );
		int getIdFor( Db &data, std::string name );
		///*********************************************************************
		/// Add operations
		///*********************************************************************
		void add( Db &data, std::string name );
		void add( Db &data, int eID, std::string name );
		void add( Db &data, int eID, std::string name, double x, double y, double z );
		///*********************************************************************
		/// Edit operations
		///*********************************************************************
		void edit( Db &data, int id, int eID );
		void edit( Db &data, int id, std::string name );
		void edit( Db &data, int id, double x, double y, double z );
		///*********************************************************************
		/// Remove operations
		///*********************************************************************
		void remove( Db &data, int id );
		///*********************************************************************
		/// Check operations
		///*********************************************************************
		bool checkStructure( Db &data );
	}
}

#endif /* JUMPSYSTEMS_HPP_ */
