/*
 * ShipLog.cpp
 *
 *  Created on: 24 Mar 2015
 *      Author: Es A. Davison
 */

#include "ShipLog.hpp"

///*********************************************************************
/// Custom getters
///*********************************************************************
/**
 * Get the Ship log data
 * @param data Db object
 * @param table Container
 * @return Success
 */
bool editTable::ShipLog::getShipLogData( Db &data, Table &table ) {
	std::string query = "SELECT ShipLog.EntryNumber Entry, ShipLog.Date Date, ShipLog.Time Time, p1.Name Origin, p2.Name Destination, ShipLog.Description Description, ShipLog.From_ID o_id, ShipLog.To_ID d_id FROM ShipLog LEFT JOIN JumpSystems p1 ON ( ShipLog.From_ID = p1.S_ID ) LEFT JOIN JumpSystems p2 ON ( ShipLog.To_ID = p2.S_ID )";
	int returned = data.sqlQueryPull_typed( query, table );
	return ( returned >= 0 );
}
///*********************************************************************
/// Add operations
///*********************************************************************
/**
 * Adds to 'ShipLog' table
 * @param data Db object
 * @param date Date
 * @param from From 'JumpSystems' ID
 * @param to To 'JumpSystems' ID
 */
void editTable::ShipLog::add( Db &data, std::string date, int from, int to ) {
	std::stringstream ss { };
	ss << "INSERT INTO ShipLog( Date, From_ID, To_ID ) VALUES ( \"" << date << "\", " << from << ", "
			<< to << " )";
	data.sqlQueryPush( ss.str() );
}
/**
 * Adds to 'ShipLog' table
 * @param data Db object
 * @param date Date
 * @param time Time
 * @param from From 'JumpSystems' ID
 * @param to To 'JumpSystems' ID
 */
void editTable::ShipLog::add( Db &data, std::string date, std::string time, int from, int to ) {
	std::stringstream ss { };
	ss << "INSERT INTO ShipLog( Date, Time, From_ID, To_ID ) VALUES ( \"" << date << "\", \"" << time
			<< "\", " << from << ", " << to << " )";
	data.sqlQueryPush( ss.str() );
}
/**
 * Adds to 'ShipLog' table
 * @param data Db object
 * @param date Date
 * @param from From 'JumpSystems' ID
 * @param to To 'JumpSystems' ID
 * @param description Description of entry
 */
void editTable::ShipLog::add( Db &data, std::string date, int from, int to,
		std::string description ) {
	std::stringstream ss { };
	ss << "INSERT INTO ShipLog( Date, From_ID, To_ID, Description ) VALUES ( \"" << date << "\", "
			<< from << ", " << to << ", \"" << description << "\" )";
	data.sqlQueryPush( ss.str() );
}
/**
 * Adds to 'ShipLog' table
 * @param data Db object
 * @param date Date
 * @param time Time
 * @param from From 'JumpSystems' ID
 * @param to To 'JumpSystems' ID
 * @param description Description of entry
 */
void editTable::ShipLog::add( Db &data, std::string date, std::string time, int from, int to,
		std::string description ) {
	std::stringstream ss { };
	ss << "INSERT INTO ShipLog( Date, Time, From_ID, To_ID, Description ) VALUES ( \"" << date
			<< "\", \"" << time << "\", " << from << ", " << to << ", \"" << description << "\" )";
	data.sqlQueryPush( ss.str() );
}
///*********************************************************************
/// Edit operations
///*********************************************************************
/**
 * Edits/Updates the 'ShipLog' table
 * @param data Db object
 * @param entry_number Entry ID
 * @param date Date of event
 * @param time Time of event
 */
void editTable::ShipLog::edit( Db &data, int entry_number, std::string date, std::string time ) {
	if( data.checkEntryExists( "ShipLog", "EntryNumber", std::to_string( entry_number ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE ShipLog SET Date = \"" << date << "\", Time = \"" << time
				<< "\" WHERE EntryNumber = " << entry_number;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - EntryNumber doesn't exist. Nothing done." );
	}
}

/**
 * Edits/Updates the 'ShipLog' table
 * @param data Db object
 * @param entry_number Entry ID
 * @param from Origin system ID
 * @param to Destination system ID
 */
void editTable::ShipLog::edit( Db &data, int entry_number, int from, int to ) {
	if( data.checkEntryExists( "ShipLog", "EntryNumber", std::to_string( entry_number ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE ShipLog SET From_ID = " << from << ", To_ID = " << to
				<< " WHERE EntryNumber = " << entry_number;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - EntryNumber doesn't exist. Nothing done." );
	}
}

/**
 * Edits/Updates the 'ShipLog' table
 * @param data Db object
 * @param entry_number Entry ID
 * @param description Description of event
 */
void editTable::ShipLog::edit( Db &data, int entry_number, std::string description ) {
	if( data.checkEntryExists( "ShipLog", "EntryNumber", std::to_string( entry_number ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE ShipLog SET Description = \"" << description << "\" WHERE EntryNumber = "
				<< entry_number;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - EntryNumber doesn't exist. Nothing done." );
	}
}

/**
 * Edits/Updates the 'ShipLog' table
 * @param data Db object
 * @param entry_number Entry ID
 * @param date Date of event
 * @param time Time of event
 * @param from Origin system ID
 * @param to Destination system ID
 * @param description Description of event
 */
void editTable::ShipLog::edit( Db &data, int entry_number, std::string date, std::string time, int from, int to,
		std::string description ) {
	if( data.checkEntryExists( "ShipLog", "EntryNumber", std::to_string( entry_number ) ) ) {
		std::stringstream ss { };
		ss << "UPDATE ShipLog SET Date = \"" << date << "\", Time = \"" << time << "\", From_ID = "
				<< from << ", To_ID = " << to << ", Description = \"" << description
				<< "\" WHERE EntryNumber = " << entry_number;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [edit] - EntryNumber doesn't exist. Nothing done." );
	}
}
///*********************************************************************
/// Remove operations
///*********************************************************************
/**
 * Removes an entry
 * @param data Db object
 * @param entry_number Log entry number
 */
void editTable::ShipLog::remove( Db &data, int entry_number ) {
	if( data.checkEntryExists( "ShipLog", "EntryNumber", std::to_string( entry_number ) ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM ShipLog WHERE EntryNumber = " << entry_number;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - EntryNumber doesn't exist. Nothing done." );
	}
}
///*********************************************************************
/// Check operations
///*********************************************************************
/**
 * Check 'ShipLog' table structure
 * @param data Db object
 * @return Structure integrity state
 */
bool editTable::ShipLog::checkStructure( Db &data ) {
	bool correctStructureFlag { true };
	if( !data.checkColumnExists( "ShipLog", "EntryNumber" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'ShipLog\' table \'ID\' column missing." );
	}
	if( !data.checkColumnExists( "ShipLog", "Date" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'ShipLog\' table \'Date\' column missing." );
	}
	if( !data.checkColumnExists( "ShipLog", "Time" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'ShipLog\' table \'Time\' column missing." );
	}
	if( !data.checkColumnExists( "ShipLog", "From_ID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'ShipLog\' table \'From_ID\' column missing." );
	}
	if( !data.checkColumnExists( "ShipLog", "To_ID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'ShipLog\' table \'To_ID\' column missing." );
	}
	if( !data.checkColumnExists( "ShipLog", "Description" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'ShipLog\' table \'Description\' column missing." );
	}
	return correctStructureFlag;
}
