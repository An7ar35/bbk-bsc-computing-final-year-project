/*
 * Ships.cpp
 *
 *  Created on: 7 Feb 2015
 *      Author: Alwlyan
 */

#include "Ships.hpp"
///*********************************************************************
/// Custom getters
///*********************************************************************
/**
 * Gets all the game ships with their information
 * @param data Db object
 * @param table Container
 * @return Success
 */
bool editTable::Ships::getShipsInfo( Db &data, Table &table ) {
	std::string query =
			"SELECT Ships.ID ID, Ships.Name Name, p2.ShipClass Class, p3.Manufacturer Manufacturer, Ships.MaxCargo [Max Cargo (T)], Ships.JumpRangeFull [Jump Range Full (Ly)], Ships.JumpRangeEmpty [Jump Range Empty (Ly)], Ships.MaxSpeed [Max Speed (m/s)], Ships.BoostSpeed [Boost Speed (m/s)] FROM Ships LEFT JOIN ShipClasses p2 ON p2.ID = Ships.ShipClassID LEFT JOIN Manufacturers p3 ON p3.ID = Ships.ManufacturerID";
	int returned = data.sqlQueryPull_typed( query, table );
	return ( returned > 0 );
}

///*********************************************************************
/// Add operations
///*********************************************************************
/**
 * Add an entry to 'Ships' table
 * @param data Db object
 * @param name Name
 * @param shipClassID ShipClassID
 * @param manufacturerID ManufacturerID
 * @param fuelCap FuelCapacity
 * @param maxCargo MaxCargo
 * @param jumpRangeFull JumpRangeFull
 * @param jumpRangeEmpty JumpRangeEmpty
 * @param maxSpeed MaxSpeed
 * @param boostSpeed BoostSpeed
 */
void editTable::Ships::add( Db &data, std::string name, int shipClassID, int manufacturerID,
		double fuelCap, int maxCargo, double jumpRangeFull, double jumpRangeEmpty, int maxSpeed,
		int boostSpeed ) {
	std::stringstream ss { };
	ss
			<< "INSERT INTO Ships( Name, ShipClassID, ManufacturerID, FuelCapacity, MaxCargo, JumpRangeFull, JumpRangeEmpty, MaxSpeed, BoostSpeed ) VALUES ( \""
			<< name << "\", " << shipClassID << ", " << manufacturerID << ", " << fuelCap << ", "
			<< maxCargo << ", " << jumpRangeFull << ", " << jumpRangeEmpty << ", " << maxSpeed
			<< ", " << boostSpeed << " )";
	data.sqlQueryPush( ss.str() );
}

///*********************************************************************
/// Edit operations
///*********************************************************************
//Not available
///*********************************************************************
/// Remove operations
///*********************************************************************
/**
 * Remove an entry
 * @param data Db object
 * @param shipID ID
 */
void editTable::Ships::remove( Db &data, int shipID ) {
	if( data.checkEntryExists( "Ships", "ID", std::to_string( shipID ) ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM Ships WHERE ID = " << shipID;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - ID doesn't exist. Nothing done." );
	}
}
/**
 * Remove an entry
 * @param data Db object
 * @param name Name
 */
void editTable::Ships::remove( Db &data, std::string name ) {
	if( data.checkEntryExists( "Ships", "Name", name ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM Ships WHERE Name = " << name;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - Name doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Check operations
///*********************************************************************
/**
 * Check 'Ships' table structure
 * @param data Db object
 * @return Structure integrity state
 */
bool editTable::Ships::checkStructure( Db &data ) {
	bool correctStructureFlag { true };
	if( !data.checkColumnExists( "Ships", "ID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error, "\'Ships\' table \'ID\' column missing." );
	}
	if( !data.checkColumnExists( "Ships", "Name" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Ships\' table \'Name\' column missing." );
	}
	if( !data.checkColumnExists( "Ships", "ShipClassID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Ships\' table \'ShipClassID\' column missing." );
	}
	if( !data.checkColumnExists( "Ships", "ManufacturerID" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Ships\' table \'ManufacturerID\' column missing." );
	}
	if( !data.checkColumnExists( "Ships", "FuelCapacity" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Ships\' table \'FuelCapacity\' column missing." );
	}
	if( !data.checkColumnExists( "Ships", "MaxCargo" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Ships\' table \'MaxCargo\' column missing." );
	}
	if( !data.checkColumnExists( "Ships", "JumpRangeFull" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Ships\' table \'JumpRangeFull\' column missing." );
	}
	if( !data.checkColumnExists( "Ships", "JumpRangeEmpty" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Ships\' table \'JumpRangeEmpty\' column missing." );
	}
	if( !data.checkColumnExists( "Ships", "MaxSpeed" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Ships\' table \'MaxSpeed\' column missing." );
	}
	if( !data.checkColumnExists( "Ships", "BoostSpeed" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'Ships\' table \'BoostSpeed\' column missing." );
	}
	return correctStructureFlag;
}
