/*
 * JumpConnections.cpp
 *
 *  Created on: 30 Sep 2014
 *      Author: Alwlyan
 */

#include "JumpConnections.hpp"

///*********************************************************************
/// Custom getters
///*********************************************************************
/**
 * Gets the distance between two points
 * @param data Db Object
 * @param pointA Point A
 * @param pointB Point B
 * @return The distance or (-1) if there was a problem getting it
 */
double editTable::JumpConnections::getDistance( Db &data, int pointA, int pointB ) {
	Table t { };
	std::ostringstream oss { };
	oss << "SELECT * FROM JumpConnections WHERE (S_ID_A = " << pointA << " AND S_ID_B = " << pointB
			<< ") OR (S_ID_A = " << pointB << " AND S_ID_B = " << pointA << ")";
	data.sqlQueryPull_typed( oss.str(), t );
	int distance_col = t.whereIsThisColumn( "Distance" );
	if( distance_col >= 0 ) {
		if( t.getRowCount() > 0 ) {
			return t( distance_col, 0 ).getDouble();
		} else {
			Log *log = Log::getInstance();
			oss.clear();
			oss.str( std::string() );
			oss << "Trying to [getDistance] - No distance found between ID '" << pointA
					<< "' and ID '" << pointB << "'.";
			log->newEvent( Location::Core, EventType::Error, oss );
			return -1;
		}
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [getDistance] - 'Distance' column doesn't exist in table." );
		return -1;
	}
}

/**
 * Gets all entries from JumpConnections with the specified ID
 * @param data Db Object
 * @param point_id Point ID
 * @return Table of results
 */
Table & editTable::JumpConnections::getAllDistancesFor( Db &data, int point_id ) {
	Table t { };
	std::ostringstream oss { };
	oss << "SELECT * FROM JumpConnections WHERE (S_ID_A = " << point_id << ") OR (S_ID_B = " << point_id << ")";
	data.sqlQueryPull_typed( oss.str(), t );
	return t;
}

///*********************************************************************
/// Add operations
///*********************************************************************
/**
 * Add to 'JumpsConnection' table by ID with unknown distance
 * @param data Db Object
 * @param pointA Origin system ID
 * @param pointB Destination system ID
 */
void editTable::JumpConnections::add( Db &data, int pointA, int pointB ) {
	if( pointA == pointB ) {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [add] - Origin and Destination points are the same!" );
		return;
	}
	std::stringstream ss { };
	if( pointA > pointB ) {
		int temp { pointA };
		pointA = pointB;
		pointB = temp;
	}
	int numOfColsExpected { 3 };

	//Get point A information from database
	Table pointA_info { }, pointB_info { };
	ss << "SELECT X_Coordinate, Y_Coordinate, Z_Coordinate FROM JumpSystems WHERE S_ID = "
			<< pointA;
	if( data.sqlQueryPull_typed( ss.str(), pointA_info ) < 1 ) {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [add] - (#1) One of the points do not exist in the 'JumpSystems' table" );
		return;
	}
	ss.str( std::string() ); //empty the stream
	ss.clear(); //clear any errors

	//Get point B information from database
	ss << "SELECT X_Coordinate, Y_Coordinate, Z_Coordinate FROM JumpSystems WHERE S_ID = "
			<< pointB;
	if( data.sqlQueryPull_typed( ss.str(), pointB_info ) < 1 ) {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [add] - (#2) One of the points do not exist in the 'JumpSystems' table" );
		return;
	}

	if( pointA_info.getColumnCount() == numOfColsExpected
			&& pointB_info.getColumnCount() == numOfColsExpected ) { //Have we got expected number of columns (3)?
		Coordinate3D a = Coordinate3D();
		Coordinate3D b = Coordinate3D();
		if( utils::getXYZ( pointA_info, a ) && utils::getXYZ( pointB_info, b ) ) { //Validate & get XYZ coordinates
			double distance = CoreTools::math::spatial::calcDistance( a, b );
			ss.str( std::string() ); //empty the stream
			ss.clear(); //clear any errors
			ss << "INSERT INTO JumpConnections( S_ID_A, S_ID_B, Distance ) VALUES ( " << pointA
					<< "," << pointB << "," << distance << " )";
			data.sqlQueryPush( ss.str() );
		} else {
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error,
					"Trying to [add] - Problem in converting Table coordinates to a Coordinate3D object." );
		}
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [add] - Result from the query of 'JumpSystems' table does not match expected number of columns" );
	}
}

/**
 * Add to 'JumpsConnection' table by ID
 * @param data Db Object
 * @param pointA Origin system ID
 * @param pointB Destination system ID
 * @param distance Jump distance
 */
void editTable::JumpConnections::add( Db &data, int pointA, int pointB, double distance ) {
	if( pointA == pointB ) {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [add] - Origin and Destination points are the same, nothing done." );
		return;
	}
	if( distance > 0 ) {
		std::stringstream ss { };
		if( pointA > pointB ) {
			int temp { pointA };
			pointA = pointB;
			pointB = temp;
		}
		ss << "INSERT INTO JumpConnections( S_ID_A, S_ID_B, Distance ) VALUES ( " << pointA << ","
				<< pointB << "," << distance << " )";
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [add] - Distance not valid, nothing done." );
	}
}

/**
 * Add to 'JumpsConnection' table by Name
 * @param data Db Object
 * @param pointA Origin system name
 * @param pointB Destination system name
 */
void editTable::JumpConnections::add( Db & data, std::string pointA, std::string pointB ) {
	editTable::JumpConnections::add( data, pointA, pointB, -1 );
}

/**
 * Add to 'JumpsConnection' table by Name
 * @param data Db Object
 * @param pointA Origin system name
 * @param pointB Destination system name
 * @param distance Distance between points
 */
void editTable::JumpConnections::add( Db & data, std::string pointA, std::string pointB,
		double distance ) {
	int pointA_id { }, pointB_id { };
	TABLE pointA_match { }, pointB_match { };

	std::stringstream ss { };
	ss << "SELECT * FROM JumpSystems WHERE Name LIKE \"" << pointA << "\"";
	int pointA_rows = data.sqlQueryPull( ss.str(), pointA_match );
	ss.str( std::string() ); //empty the stream
	ss.clear(); //clear any errors
	ss << "SELECT * FROM JumpSystems WHERE Name LIKE \"" << pointB << "\"";
	int pointB_rows = data.sqlQueryPull( ss.str(), pointB_match );

	if( pointA_rows > 0 && pointB_rows > 0 ) {
		int colNumber = data.getColNumber( "Name", "JumpSystems" );
		if( colNumber < 0 ) { //Checking the 'Name' column exists
			Log *log = Log::getInstance();
			log->logEvent( Location::Core, EventType::Error,
					"Trying to [add] - Table 'JumpSystems' may not exist..." );
			return;
		}
		if( pointA_match[ 2 ][ 0 ] == pointA ) {
			std::string s = pointA_match[ 0 ][ 0 ];
			if( utils::stringToInt( s, pointA_id ) == false ) {
				Log *log = Log::getInstance();
				log->logEvent( Location::Core, EventType::Error,
						"Trying to [add] - Failed string to int conversion (A) -> possible issue with the string.." );
			}
		}
		if( pointB_match[ 2 ][ 0 ] == pointB ) {
			std::string s = pointB_match[ 0 ][ 0 ];
			if( utils::stringToInt( s, pointB_id ) == false ) {
				Log *log = Log::getInstance();
				log->logEvent( Location::Core, EventType::Error,
						"Trying to [add] - Failed string to int conversion (B) -> possible issue with the string.." );
			}
		}
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [add] - One or more of the parameters are wrong" );
		return;
	}
	if( distance > 0 ) {
		editTable::JumpConnections::add( data, pointA_id, pointB_id, distance );
	} else {
		editTable::JumpConnections::add( data, pointA_id, pointB_id );
	}
}
///*********************************************************************
/// Edit operations
///*********************************************************************
void editTable::JumpConnections::edit( Db &data, int pointA_id, int pointB_id, double distance ) {
//TODO
}
///*********************************************************************
/// Remove operations
///*********************************************************************
void editTable::JumpConnections::remove( Db &data, int pointA_id, int pointB_id ) {
	if( data.checkEntryExists( "JumpConnections", "S_ID_A", std::to_string( pointA_id ), "S_ID_B",
			std::to_string( pointB_id ) ) ) {
		std::stringstream ss { };
		ss << "DELETE FROM JumpConnections WHERE S_ID_A = " << pointA_id << " AND S_ID_B = "
				<< pointB_id;
		data.sqlQueryPush( ss.str() );
	} else {
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"Trying to [remove] - Key doesn't exist. Nothing done." );
	}
}

///*********************************************************************
/// Check operations
///*********************************************************************
/**
 * Check 'JumpConnections' table structure
 * @param data Db Object
 * @return Structure integrity state
 */
bool editTable::JumpConnections::checkStructure( Db &data ) {
	bool correctStructureFlag { true };
	if( !data.checkColumnExists( "JumpConnections", "S_ID_A" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'JumpConnections\' table \'S_ID_A\' column missing." );
	}
	if( !data.checkColumnExists( "JumpConnections", "S_ID_B" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'JumpConnections\' table \'S_ID_B\' column missing." );
	}
	if( !data.checkColumnExists( "JumpConnections", "Distance" ) ) {
		correctStructureFlag = false;
		Log *log = Log::getInstance();
		log->logEvent( Location::Core, EventType::Error,
				"\'JumpConnections\' table \'Distance\' column missing." );
	}
	return correctStructureFlag;
}
