/*
 * sqlmsg.cpp
 *
 *  Created on: 14 Sep 2014
 *      Author: Alwlyan
 */

#include "sqlmsg.hpp"

/**
 * SQLite3 Error code handling\n
 * (All codes are from the official manual)\n
 * SQlite message codes are forwarded here for logging purposes.
 * If the code matches an error then the function returns 'true' on the existence of a problem.
 * Source: http://www.sqlite.org/c3ref/c_abort.html
 * @param code Error code produced
 * @param query Query used if any
 * @return Problem existence ( 0 = no problems )
 */
bool sqlMsgCode( int code, std::string &query ) {
	std::ostringstream oss { };
	Log *log = Log::getInstance();

	switch( code ) {
		case SQLITE_OK: /* Successful result */
			oss << "SQLite: OK";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Trace, oss );
			return 0;
		case SQLITE_ERROR: /* SQL error or missing database */
			oss << "SQL error or missing database.";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_INTERNAL: /* Internal logic error in SQLite */
			oss << "Internal logic error in SQLite";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_PERM: /* Access permission denied */
			oss << "Access permission denied";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_ABORT: /* Callback routine requested an abort */
			oss << "Callback routine requested an abort";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_BUSY: /* The database file is locked */
			oss << "The database file is locked";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_LOCKED: /* A table in the database is locked */
			oss << "A table in the database is locked";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_NOMEM: /* A malloc() failed */
			oss << "A malloc() failed";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_READONLY: /* Attempt to write a readonly database */
			oss << "Attempt to write a readonly database";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_INTERRUPT: /* Operation terminated by sqlite3_interrupt()*/
			oss << "Operation terminated by sqlite3_interrupt()";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_IOERR: /* Some kind of disk I/O error occurred */
			oss << "Some kind of disk I/O error occurred";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_CORRUPT: /* The database disk image is malformed */
			oss << "The database disk image is malformed";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_NOTFOUND: /* Unknown opcode in sqlite3_file_control() */
			oss << "Unknown opcode in sqlite3_file_control()";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_FULL: /* Insertion failed because database is full */
			oss << "Insertion failed because database is full";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_CANTOPEN: /* Unable to open the database file */
			oss << "Unable to open the database file";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_PROTOCOL: /* Database lock protocol error */
			oss << "Database lock protocol error";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_EMPTY: /* Database is empty */
			oss << "Database is empty";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_SCHEMA: /* The database schema changed */
			oss << "The database schema changed";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_TOOBIG: /* String or BLOB exceeds size limit */
			oss << "String or BLOB exceeds size limit";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_CONSTRAINT: /* Abort due to constraint violation */
			oss << "Abort due to constraint violation";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_MISMATCH: /* Data type mismatch */
			oss << "Data type mismatch";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_MISUSE: /* Library used incorrectly */
			oss << "Library used incorrectly";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_NOLFS: /* Uses OS features not supported on host */
			oss << "Uses OS features not supported on host";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_AUTH: /* Authorisation denied */
			oss << "Authorisation denied";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_FORMAT: /* Auxiliary database format error */
			oss << "Auxiliary database format error";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_RANGE: /* 2nd parameter to sqlite3_bind out of range */
			oss << "2nd parameter to sqlite3_bind out of range";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_NOTADB: /* File opened that is not a database file */
			oss << "File opened that is not a database file";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_NOTICE: /* Notifications from sqlite3_log() */
			oss << "Notifications from sqlite3_log()";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		case SQLITE_WARNING: /* Warnings from sqlite3_log() */
			oss << "Warnings from sqlite3_log()";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Debug, oss );
			return 1;
		case SQLITE_ROW: /* sqlite3_step() has another row ready */
			oss << "sqlite3_step() has another row ready";
			log->newEvent( Location::SQLite, EventType::Debug, oss );
			return 0;
		case SQLITE_DONE: /* sqlite3_step() has finished executing */
			oss << "sqlite3_step() has finished executing";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Debug, oss );
			return 0;
		case SQLITE_EMPTY_TABLE:
			oss << "Table specified does not exist or is empty";
			if( query.size() > 0 ) {
				oss << "\n     Query: " << query;
			}
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
		default:
			oss << "Weird unrecognised SQlite3 error code: " << code;
			log->newEvent( Location::SQLite, EventType::Error, oss );
			return 1;
	}
}

bool sqlMsgCode( int code ) {
	std::string s = "";
	return sqlMsgCode( code, s );
}

bool sqlMsgCode( int code, const std::string &query ) {
	std::string s = query;
	return sqlMsgCode( code, s );
}
