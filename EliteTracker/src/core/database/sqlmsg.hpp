/*
 * sqlmsg.hpp
 *
 *  Created on: 14 Sep 2014
 *      Author: Alwlyan
 */

#ifndef SQLMSG_HPP_
#define SQLMSG_HPP_

#include <iostream>
#include <sstream>
#include "../logger/log.hpp"
#include "../../../lib/sqlite3.h"

constexpr int SQLITE_EMPTY_TABLE { 1001 };

bool sqlMsgCode( int );
bool sqlMsgCode( int, std::string & );
bool sqlMsgCode( int, const std::string & );

#endif /* SQLMSG_HPP_ */
