/*
 * editTables.hpp
 *
 *  Created on: 9 Sep 2014
 *      Author: Alwlyan
 */

#ifndef EDITTABLES_HPP_
#define EDITTABLES_HPP_

#include <iostream>
#include <sstream>
#include <string>
#include "../logger/log.hpp"
#include "utils.hpp"
#include "db.hpp"

#include "tables/JumpConnections.hpp"
#include "tables/JumpSystems.hpp"
#include "tables/Manufacturers.hpp"
#include "tables/PlayerShips.hpp"
#include "tables/ShipClasses.hpp"
#include "tables/Ships.hpp"
#include "tables/Stations.hpp"
#include "tables/ShipLog.hpp"

namespace editTable {
	///*********************************************************************
	/// Setup operations
	///*********************************************************************
	void createAllTables( Db & );
	bool checkTables( Db & );
}

#endif /* EDITTABLES_HPP_ */
