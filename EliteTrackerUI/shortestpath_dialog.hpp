#ifndef SHORTESTPATH_DIALOG_HPP
#define SHORTESTPATH_DIALOG_HPP

#include <QDialog>
#include <QtCore>
#include <QtGUI>
#include <access.hpp>
#include "../../EliteTracker/src/core/containers/Table.hpp"
#include "../../EliteTracker/src/satellites/pathfinder/Pathfinder.hpp"

namespace Ui {
  class shortestpath_dialog;
}

class shortestpath_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit shortestpath_dialog(QWidget *parent = 0);
    ~shortestpath_dialog();
    void refresh();

  private slots:
    void on_close_clicked();

    void on_go_clicked();

  private:
    Ui::shortestpath_dialog *ui;
    QStandardItemModel *sp_model;
    Table sp_results;
    Pathfinder *pf;

};

#endif // SHORTESTPATH_DIALOG_HPP
