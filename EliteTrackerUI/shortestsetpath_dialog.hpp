#ifndef SHORTESTSETPATH_DIALOG_HPP
#define SHORTESTSETPATH_DIALOG_HPP

#include <QDialog>
#include <QtCore>
#include <QtGUI>
#include <QMessageBox>
#include <access.hpp>
#include "../../EliteTracker/src/core/containers/Table.hpp"
#include "../../EliteTracker/src/satellites/pathfinder/Pathfinder.hpp"

namespace Ui {
  class shortestsetpath_dialog;
}

class shortestsetpath_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit shortestsetpath_dialog(QWidget *parent = 0);
    ~shortestsetpath_dialog();
    void refresh_table();

  private slots:
    void on_close_clicked();
    void on_clea_list_clicked();
    void on_go_clicked();
    void on_add_clicked();

    void on_from_comboBox_activated(int index);

  private:
    Ui::shortestsetpath_dialog *ui;
    QStandardItemModel *ssp_table_model;
    QStandardItemModel *ssp_list_model;
    Table ssp_results;
    std::vector<int> set;
    Pathfinder *pf;
};

#endif // SHORTESTSETPATH_DIALOG_HPP
