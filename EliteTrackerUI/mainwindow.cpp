#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "aboutdialog.hpp"
#include "edit_shiplog_dialog.hpp"
#include "add_shiplog_dialog.hpp"
#include "search_shiplog_dialog.hpp"
#include "edit_playership_dialog.hpp"
#include "shortestpath_dialog.hpp"
#include "shortestsetpath_dialog.hpp"
#include "log_dialog.hpp"
#include "add_jumpconnection_dialog.hpp"
#include "add_jumpsystem_dialog.hpp"
#include "add_manufacturer_dialog.hpp"
#include "add_station_dialog.hpp"

#include "../../EliteTracker/src/core/database/editTables.hpp"
#include "../../EliteTracker/src/core/containers/Coordinate2D.hpp"
#include "../../EliteTracker/src/core/containers/Table.hpp"
#include "../../EliteTracker/src/core/database/Db.hpp"
#include "../../EliteTracker/src/core/database/tables/PlayerShips.hpp"
#include "../../EliteTracker/src/core/database/tables/ShipLog.hpp"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    Log *log = Log::getInstance();
    log->setFileName( "EliteTrackerLog.txt");
    log->switch_autoFileDump(true);
    log->switch_EventType( EventType::Debug, false );
    ui->setupUi(this);
    ui->playership_tableView->setSelectionMode(QAbstractItemView::NoSelection);
    ui->playership_tableView->horizontalHeader()->hide();
    ui->playership_tableView->verticalHeader()->setSelectionMode(QAbstractItemView::NoSelection);
    refresh_PlayerShip();
    ui->shipLog_tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
    ui->shipLog_tableView->verticalHeader()->hide();
    ui->shipLog_tableView->setAlternatingRowColors(true);
    refresh_ShipLog();
}

MainWindow::~MainWindow()
{
  delete model;
  delete ship_model;
  delete ui;
}

void MainWindow::refresh_PlayerShip() {
  Access *access = Access::getInstance();
  Db data = access->getDb();
  fileIO::Settings config = access->getConfig();
  current_ship = config.getValueOfKey<int>("CurrentPlayerShip", 1);
  bool gotInfo = editTable::PlayerShips::getPlayerShipInfo( data, current_ship, playership );
  if( gotInfo && playership.getRowCount() > 0 ) {
    this->ship_model = new QStandardItemModel(playership.getColumnCount(), playership.getRowCount(), this);
    for( int col = 0; col < playership.getColumnCount(); col++) {
      this->ship_model->setVerticalHeaderItem( col, new QStandardItem( QString::fromStdString( playership.getHeading( col )) ));
    }
    for( int row = 0; row < playership.getRowCount(); row++ ) {
      for( int col = 0; col < playership.getColumnCount(); col++ ) {
        QStandardItem *item = new QStandardItem( QString::fromStdString( playership(col,row).getString()) );
        item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
        ship_model->setItem(col,row,item);
      }
    }
  }
  ui->playership_tableView->setModel(ship_model);
  ui->playership_tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
  ui->playership_tableView->clearSelection();
  refresh_ShipPic();
}

void MainWindow::refresh_ShipLog() {
  Access *access = Access::getInstance();
  Db data = access->getDb();
  editTable::ShipLog::getShipLogData( data, shiplog );
  if( shiplog.getRowCount() > 0 ) {
    this->model = new QStandardItemModel(shiplog.getRowCount(),shiplog.getColumnCount()-2,this);
    for( int col = 0; col < shiplog.getColumnCount()-2; col++ ) {
      this->model->setHorizontalHeaderItem( col, new QStandardItem( QString::fromStdString( shiplog.getHeading( col )) ));
    }
    for( int row = 0; row < shiplog.getRowCount(); row++ ) {
      for( int col = 0; col < shiplog.getColumnCount()-2; col++ ) {
          QStandardItem *item = new QStandardItem( QString::fromStdString( shiplog(col,row).getString()) );
          item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          if( col < 4 ) {
            item->setData(Qt::AlignVCenter, Qt::TextAlignmentRole);
          }
          model->setItem(row,col,item);
      }
    }
  } else {
    this->model = new QStandardItemModel( 0, empty_shiplog_headings.size(),this);
    for( unsigned int col = 0; col < empty_shiplog_headings.size(); col++ ) {
      this->model->setHorizontalHeaderItem( col, new QStandardItem( QString::fromStdString( empty_shiplog_headings.at( col ) ) ));
    }
  }
  ui->shipLog_tableView->setModel(model);
  ui->shipLog_tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
  ui->shipLog_tableView->resizeColumnToContents(0);
  ui->shipLog_tableView->horizontalHeader()->setSectionResizeMode(5, QHeaderView::Stretch);
  ui->shipLog_tableView->clearSelection();
  ui->shipLog_tableView->sortByColumn(0, Qt::SortOrder(Qt::DescendingOrder) );
}

void MainWindow::refresh_ShipPic() {
  Access *access = Access::getInstance();
  Db data = access->getDb();
  int ship_id = editTable::PlayerShips::getPlayerShip_ShipID( data, current_ship );
  if( ship_id > 0 ) {
    QString path = qApp->applicationDirPath() + QString("/resources/ships/%1.jpg").arg(ship_id);
    QPixmap ship(path);
    ui->ship_pic->setScaledContents(true);
    ui->ship_pic->setPixmap(ship);
  }
}

void MainWindow::on_actionExit_triggered()
{
    this->close();
}

void MainWindow::on_actionAbout_triggered()
{
  AboutDialog about_dialog;
  about_dialog.setModal(true);
  about_dialog.exec();
}

void MainWindow::on_editCurrentShip_clicked()
{
  edit_playership_dialog *edit_playership = new edit_playership_dialog();
  edit_playership->setModal(true);
  connect( edit_playership, SIGNAL( hasUpdated() ), this, SLOT( refresh_PlayerShip() ) );
  edit_playership->show();
}

void MainWindow::on_add_ShipLog_entry_clicked()
{
  Access *access = Access::getInstance();
  Db data = access->getDb();

  Table systems { };
  data.sqlQueryPull_typed( "SELECT S_ID, Name FROM JumpSystems", systems );
  int to_col = shiplog.whereIsThisColumn( "d_id" );
  int d_id = -1;
  if( shiplog.getRowCount() > 0 ) {
    int last = shiplog.getRowCount() - 1;
    d_id = shiplog(to_col,last).getInt();
  }
  add_shiplog_dialog *add_shiplog = new add_shiplog_dialog();
  add_shiplog->setData( d_id );
  add_shiplog->setModal(true);
  connect( add_shiplog, SIGNAL( hasValidated() ), this, SLOT( refresh_ShipLog() ) );
  add_shiplog->show();
}

void MainWindow::on_edit_ShipLog_entry_clicked()
{
  Access *access = Access::getInstance();
  Db data = access->getDb();

  int i = ui->shipLog_tableView->selectionModel()->currentIndex().row();
  if( i >= 0 ) {
    int entry_id = model->data(model->index(i, 0), Qt::DisplayRole).toInt();
    Coordinate2D<int> table_i = shiplog.searchFor( "Entry", entry_id );
    Table systems { };
    data.sqlQueryPull_typed( "SELECT S_ID, Name FROM JumpSystems", systems );
    int from_col = shiplog.whereIsThisColumn( "o_id" );
    int to_col = shiplog.whereIsThisColumn( "d_id" );
    int date_col = shiplog.whereIsThisColumn( "Date" );
    int time_col = shiplog.whereIsThisColumn( "Time" );
    int desc_col = shiplog.whereIsThisColumn( "Description" );

    int o_id { -1 };
    int d_id { -1 };
    if( i < shiplog.getRowCount() ) {
      o_id = shiplog(from_col, table_i.y).getInt();
      d_id = shiplog(to_col, table_i.y).getInt();
    }
    std::string date = shiplog(date_col, table_i.y).getString();
    std::string time = shiplog(time_col, table_i.y).getString();
    QString description = QString::fromStdString( shiplog(desc_col, table_i.y).getString() );

    Edit_ShipLog_Dialog *edit_shiplog = new Edit_ShipLog_Dialog();
    edit_shiplog->setData( entry_id, date, time, o_id, d_id, description );
    edit_shiplog->setModal(true);
    connect( edit_shiplog, SIGNAL( hasUpdated() ), this, SLOT( refresh_ShipLog() ) );
    edit_shiplog->show();
  }
}

void MainWindow::on_delete_ShipLog_entry_clicked()
{
  Access *access = Access::getInstance();
  Db data = access->getDb();

  int i = ui->shipLog_tableView->selectionModel()->currentIndex().row();
  if( i >= 0 ) {
    int id = ui->shipLog_tableView->model()->data(ui->shipLog_tableView->model()->index(i,0)).toInt();
    editTable::ShipLog::remove( data, id );
    refresh_ShipLog();
  }
}

void MainWindow::on_actionPath_Finder_triggered()
{
    shortestpath_dialog *sp = new shortestpath_dialog();
    sp->setModal(true);
    sp->show();
}

void MainWindow::on_actionShortest_path_multi_systems_triggered()
{
    shortestsetpath_dialog * ssp = new shortestsetpath_dialog();
    ssp->setModal(true);
    ssp->show();
}

void MainWindow::on_actionLog_triggered()
{
  log_dialog *log = new log_dialog();
  log->setModal(true);
  log->show();
}

void MainWindow::on_search_pushButton_clicked()
{
  refresh_ShipLog();
  search_shiplog_dialog *search = new search_shiplog_dialog();
  search->setData( shiplog );
  connect( search, SIGNAL( find(int,QString) ), this, SLOT( search_ShipLog(int,QString) ) );
  search->show();
}

void MainWindow::search_ShipLog( int col, QString str ) {
  QMessageBox *msg = new QMessageBox();
  msg->setWindowTitle(QString("Search Result"));
  msg->addButton(QString("Close"),QMessageBox::AcceptRole);
  QString row_text;
  if( shiplog.getRowCount() < 1 ) {
    row_text = "Nothing in the shiplog.";
  } else {
    std::string col_heading = shiplog.getHeading( col );
    Item item { };
    switch( shiplog.getType( col ) ) {
      case dataType::INTEGER:
        item = Item( str.toInt() );
        break;
      case dataType::DOUBLE:
        item = Item( str.toDouble() );
        break;
      default:
        item = Item( str.toStdString() );
        break;
    }
    Coordinate2D<int> match = shiplog.searchFor( col_heading, item );
    if( match.x >= 0 && match.y >= 0 ) {
      row_text = "<b>" + QString::fromStdString( shiplog.getHeading(0) ) + ":</b> " + QString::fromStdString( shiplog( 0, match.y ).getString() ) + "<br>";
      row_text += "<b>" + QString::fromStdString( shiplog.getHeading(1) ) + ":</b> " + QString::fromStdString( shiplog( 1, match.y ).getString() ) + "<br>";
      row_text += "<b>" + QString::fromStdString( shiplog.getHeading(2) ) + ":</b> " + QString::fromStdString( shiplog( 2, match.y ).getString() ) + "<br>";
      row_text += "<b>" + QString::fromStdString( shiplog.getHeading(3) ) + ":</b> " + QString::fromStdString( shiplog( 3, match.y ).getString() ) + "<br>";
      row_text += "<b>" + QString::fromStdString( shiplog.getHeading(4) ) + ":</b> " + QString::fromStdString( shiplog( 4, match.y ).getString() ) + "<br>";
      row_text += "<b>" + QString::fromStdString( shiplog.getHeading(5) ) + ":</b> " + QString::fromStdString( shiplog( 5, match.y ).getString() ) + "<br>";
      msg->setText( row_text );
    } else {
      row_text = "No match found.";
    }
  }
  msg->setText( row_text );
  msg->show();
}

void MainWindow::on_actionAdd_Stations_triggered()
{
  add_Station_dialog *station = new add_Station_dialog();
  station->setModal(true);
  station->show();
}

void MainWindow::on_actionAdd_Jump_System_triggered()
{
  add_JumpSystem_dialog *js = new add_JumpSystem_dialog();
  js->setModal(true);
  js->show();
}

void MainWindow::on_actionAdd_Jump_Connection_triggered()
{
  add_JumpConnection_dialog *jc = new add_JumpConnection_dialog();
  jc->setModal(true);
  jc->show();
}
