#ifndef VIEW_PLAYERSHIP_GRAVEYARD_DIALOG_HPP
#define VIEW_PLAYERSHIP_GRAVEYARD_DIALOG_HPP

#include <QDialog>
#include <QtCore>
#include <QtGUI>
#include <QListWidgetItem>
#include "access.hpp"
#include "../../EliteTracker/src/core/database/Db.hpp"
#include "../../EliteTracker/src/core/containers/Table.hpp"

namespace Ui {
  class view_playership_graveyard_dialog;
}

class view_playership_graveyard_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit view_playership_graveyard_dialog(QWidget *parent = 0);
    ~view_playership_graveyard_dialog();
    void refreshList();
    void showSpecs(int row );

  signals:
    void hasClosed();

  private slots:
    void on_resurect_clicked();
    void on_close_clicked();
    void on_graveyard_listWidget_itemClicked(QListWidgetItem *item);

  private:
    Ui::view_playership_graveyard_dialog *ui;
    Table graveyard;
    QStandardItemModel *ship_model;
};

#endif // VIEW_PLAYERSHIP_GRAVEYARD_DIALOG_HPP
