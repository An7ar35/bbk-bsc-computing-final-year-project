#-------------------------------------------------
#
# Project created by QtCreator 2015-02-15T16:42:12
#
#-------------------------------------------------

QT       += core gui
CONFIG   += c++14
INCLUDEPATH += D:\Development\Libraries\boost_1_56_0
CONFIG   += static
QMAKE_CXXFLAGS += -std=c++1y

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = EliteTrackerUI
TEMPLATE = app

SOURCES += main.cpp\
    mainwindow.cpp \
    aboutdialog.cpp \
    ../../EliteTracker/src/core/containers/Column.cpp \
    ../../EliteTracker/src/core/containers/Item.cpp \
    ../../EliteTracker/src/core/containers/Row.cpp \
    ../../EliteTracker/src/core/containers/Table.cpp \
    ../../EliteTracker/src/core/database/tables/JumpConnections.cpp \
    ../../EliteTracker/src/core/database/tables/JumpSystems.cpp \
    ../../EliteTracker/src/core/database/tables/Manufacturers.cpp \
    ../../EliteTracker/src/core/database/tables/PlayerShips.cpp \
    ../../EliteTracker/src/core/database/tables/ShipClasses.cpp \
    ../../EliteTracker/src/core/database/tables/ShipLog.cpp \
    ../../EliteTracker/src/core/database/tables/Ships.cpp \
    ../../EliteTracker/src/core/database/tables/Stations.cpp \
    ../../EliteTracker/src/core/database/Db.cpp \
    ../../EliteTracker/src/core/database/editTables.cpp \
    ../../EliteTracker/src/core/database/sqlmsg.cpp \
    ../../EliteTracker/src/core/database/utils.cpp \
    ../../EliteTracker/src/core/fileops/Settings.cpp \
    ../../EliteTracker/src/core/logger/Log.cpp \
    ../../EliteTracker/src/core/logger/Logmsg.cpp \
    ../../EliteTracker/src/core/platform/windows.cpp \
    ../../EliteTracker/src/core/toolbox/math/spatial.cpp \
    ../../EliteTracker/src/core/toolbox/math/trigonometry.cpp \
    ../../EliteTracker/src/satellites/coordinate/coordinate_tool.cpp \
    ../../EliteTracker/src/satellites/coordinate/coordinates.cpp \
    ../../EliteTracker/src/satellites/pathfinder/Pathfinder.cpp \
    ../../EliteTracker/src/satellites/pathfinder/ShortestPath.cpp \
    ../../EliteTracker/src/satellites/pathfinder/ShortestSetPath.cpp \
    ../../EliteTracker/lib/sqlite3.c \
    edit_shiplog_dialog.cpp \
    access.cpp \
    add_shiplog_dialog.cpp \
    edit_playership_dialog.cpp \
    view_playership_graveyard_dialog.cpp \
    add_playership_dialog.cpp \
    shortestpath_dialog.cpp \
    shortestsetpath_dialog.cpp \
    log_dialog.cpp \
    add_jumpsystem_dialog.cpp \
    add_jumpconnection_dialog.cpp \
    add_manufacturer_dialog.cpp \
    add_station_dialog.cpp \
    search_shiplog_dialog.cpp \
    edit_jumpsystems_dialog.cpp

HEADERS  += mainwindow.h \
    mainwindow.h \
    aboutdialog.hpp \
    ../../EliteTracker/lib/sqlite3.h \
    ../../EliteTracker/src/core/containers/Column.hpp \
    ../../EliteTracker/src/core/containers/Coordinate2D.hpp \
    ../../EliteTracker/src/core/containers/Coordinate3D.hpp \
    ../../EliteTracker/src/core/containers/dataType.hpp \
    ../../EliteTracker/src/core/containers/Item.hpp \
    ../../EliteTracker/src/core/containers/Pair.hpp \
    ../../EliteTracker/src/core/containers/Row.hpp \
    ../../EliteTracker/src/core/containers/Table.hpp \
    ../../EliteTracker/src/core/database/tables/JumpConnections.hpp \
    ../../EliteTracker/src/core/database/tables/JumpSystems.hpp \
    ../../EliteTracker/src/core/database/tables/Manufacturers.hpp \
    ../../EliteTracker/src/core/database/tables/PlayerShips.hpp \
    ../../EliteTracker/src/core/database/tables/ShipClasses.hpp \
    ../../EliteTracker/src/core/database/tables/ShipLog.hpp \
    ../../EliteTracker/src/core/database/tables/Ships.hpp \
    ../../EliteTracker/src/core/database/tables/Stations.hpp \
    ../../EliteTracker/src/core/database/Db.hpp \
    ../../EliteTracker/src/core/database/editTables.hpp \
    ../../EliteTracker/src/core/database/sqlmsg.hpp \
    ../../EliteTracker/src/core/database/utils.hpp \
    ../../EliteTracker/src/core/fileops/Convert.hpp \
    ../../EliteTracker/src/core/fileops/fileops_utils.hpp \
    ../../EliteTracker/src/core/fileops/init.hpp \
    ../../EliteTracker/src/core/fileops/scrubfile.hpp \
    ../../EliteTracker/src/core/fileops/Settings.hpp \
    ../../EliteTracker/src/core/logger/Log.hpp \
    ../../EliteTracker/src/core/logger/Logmsg.hpp \
    ../../EliteTracker/src/core/logger/properties.hpp \
    ../../EliteTracker/src/core/platform/windows.hpp \
    ../../EliteTracker/src/core/toolbox/math/spatial.hpp \
    ../../EliteTracker/src/core/toolbox/math/trigonometry.hpp \
    ../../EliteTracker/src/core/toolbox/toolbox.hpp \
    ../../EliteTracker/src/satellites/coordinate/Circle.hpp \
    ../../EliteTracker/src/satellites/coordinate/coordinate_tool.hpp \
    ../../EliteTracker/src/satellites/coordinate/coordinates.hpp \
    ../../EliteTracker/src/satellites/pathfinder/Matrix.hpp \
    ../../EliteTracker/src/satellites/pathfinder/Node.hpp \
    ../../EliteTracker/src/satellites/pathfinder/Pathfinder.hpp \
    ../../EliteTracker/src/satellites/pathfinder/ShortestPath.hpp \
    ../../EliteTracker/src/satellites/pathfinder/ShortestSetPath.hpp \
    edit_shiplog_dialog.hpp \
    access.hpp \
    add_shiplog_dialog.hpp \
    edit_playership_dialog.hpp \
    view_playership_graveyard_dialog.hpp \
    add_playership_dialog.hpp \
    shortestpath_dialog.hpp \
    shortestsetpath_dialog.hpp \
    log_dialog.hpp \
    add_jumpsystem_dialog.hpp \
    add_jumpconnection_dialog.hpp \
    add_manufacturer_dialog.hpp \
    add_station_dialog.hpp \
    search_shiplog_dialog.hpp \
    edit_jumpsystems_dialog.hpp


FORMS    += mainwindow.ui \
    aboutdialog.ui \
    edit_shiplog_dialog.ui \
    add_shiplog_dialog.ui \
    edit_playership_dialog.ui \
    view_playership_graveyard_dialog.ui \
    add_playership_dialog.ui \
    shortestpath_dialog.ui \
    shortestsetpath_dialog.ui \
    log_dialog.ui \
    add_jumpsystem_dialog.ui \
    add_jumpconnection_dialog.ui \
    add_manufacturer_dialog.ui \
    add_station_dialog.ui \
    search_shiplog_dialog.ui \
    edit_jumpsystems_dialog.ui


