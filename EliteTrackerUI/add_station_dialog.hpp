#ifndef ADD_STATION_DIALOG_HPP
#define ADD_STATION_DIALOG_HPP

#include <QDialog>
#include <QMessageBox>
#include "access.hpp"
#include "../../EliteTracker/src/core/containers/Table.hpp"
#include "../../EliteTracker/src/core/database/tables/Stations.hpp"

namespace Ui {
  class add_Station_dialog;
}

class add_Station_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit add_Station_dialog(QWidget *parent = 0);
    ~add_Station_dialog();

  private slots:
    void writeToDb( int id, std::string name );
    void on_close_button_clicked();

    void on_add_button_clicked();

  private:
    Ui::add_Station_dialog *ui;
};

#endif // ADD_STATION_DIALOG_HPP
