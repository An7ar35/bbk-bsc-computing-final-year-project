#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QAction>
#include <QDialog>
#include <QtCore>
#include <QtGUI>
#include <QProcess>
#include <QPicture>
#include "access.hpp"

namespace Ui {
  class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

  public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
  protected:

  public slots:
    void refresh_ShipLog();
    void refresh_PlayerShip();
    void refresh_ShipPic();

  private:
    Ui::MainWindow *ui;
    QStandardItemModel *model;
    QStandardItemModel *ship_model;
    Table shiplog { };
    Table playership { };
    int current_ship;
    std::vector<std::string> empty_shiplog_headings { "Entry", "Date", "Time", "Origin", "Destination", "Description" };

  private slots:
    void on_actionAbout_triggered();
    void on_editCurrentShip_clicked();
    void on_edit_ShipLog_entry_clicked();
    void on_add_ShipLog_entry_clicked();
    void on_actionExit_triggered();
    void on_delete_ShipLog_entry_clicked();
    void on_actionPath_Finder_triggered();
    void on_actionShortest_path_multi_systems_triggered();
    void on_actionLog_triggered();
    void on_actionAdd_Jump_System_triggered();
    void on_actionAdd_Jump_Connection_triggered();
    void on_actionAdd_Stations_triggered();
    void on_search_pushButton_clicked();
    void search_ShipLog( int col, QString str );
};

#endif // MAINWINDOW_H
