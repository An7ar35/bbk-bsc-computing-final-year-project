#include "add_shiplog_dialog.hpp"
#include "ui_add_shiplog_dialog.h"

add_shiplog_dialog::add_shiplog_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::add_shiplog_dialog)
{
  ui->setupUi(this);
  QTime timeNow = QTime::currentTime();
  ui->timeEdit->setTime( timeNow );
  QDate dateNow = QDate::currentDate();
  ui->dateEdit->setDate(dateNow);
  Access *access = Access::getInstance();
  Db data = access->getDb();
  Table systems_table { };
  std::string shiplog_query = "SELECT S_ID, Name FROM JumpSystems;";
  data.sqlQueryPull_typed( shiplog_query, systems_table );
  for( int i = 0; i < systems_table.getRowCount(); i++ ) {
    ui->originList->addItem(QString::fromStdString( systems_table(1,i).getString() ), systems_table(0,i).getInt() );
    ui->destinationList->addItem(QString::fromStdString( systems_table(1,i).getString() ), systems_table(0,i).getInt() );
  }
}

add_shiplog_dialog::~add_shiplog_dialog()
{
  delete ui;
}

void add_shiplog_dialog::setData( int &origin ) {
  int o_index = ui->originList->findData(origin);
  if ( o_index != -1 ) {
     ui->originList->setCurrentIndex(o_index);
  }
}

void add_shiplog_dialog::on_buttonBox_accepted()
{

    //check entries
    //if cool add to database
    //else highlight the fucked ones
  Access *access = Access::getInstance();
  Db data = access->getDb();
  int from = ui->originList->itemData(ui->originList->currentIndex()).toInt();
  int to = ui->destinationList->itemData(ui->destinationList->currentIndex()).toInt();
  std::string str_desc = (ui->descriptionEdit->toPlainText()).toStdString();
  std::string str_date = (ui->dateEdit->text()).toStdString();
  std::string str_time = (ui->timeEdit->text()).toStdString();
  editTable::ShipLog::add(data, str_date, str_time, from, to, str_desc );
  emit hasValidated();
}
