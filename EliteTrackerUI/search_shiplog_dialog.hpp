#ifndef SEARCH_SHIPLOG_DIALOG_HPP
#define SEARCH_SHIPLOG_DIALOG_HPP

#include <QDialog>
#include "../../EliteTracker/src/core/containers/Table.hpp"

namespace Ui {
  class search_shiplog_dialog;
}

class search_shiplog_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit search_shiplog_dialog(QWidget *parent = 0);
    ~search_shiplog_dialog();
    void setData( Table &shiplog );

  signals:
    void find( int col, QString text );

  private slots:
    void on_search_pushButton_clicked();

  private:
    Ui::search_shiplog_dialog *ui;

};

#endif // SEARCH_SHIPLOG_DIALOG_HPP
