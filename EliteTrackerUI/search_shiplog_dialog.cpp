#include "search_shiplog_dialog.hpp"
#include "ui_search_shiplog_dialog.h"

search_shiplog_dialog::search_shiplog_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::search_shiplog_dialog)
{
  ui->setupUi(this);
}

search_shiplog_dialog::~search_shiplog_dialog()
{
  delete ui;
}

void search_shiplog_dialog::setData( Table &shiplog ) {
  for( int i = 0; i < shiplog.getColumnCount()-2; i++ ) {
    QString str = QString::fromStdString( shiplog.getHeading( i ) );
    ui->comboBox->addItem(str, i );
  }
  ui->comboBox->setCurrentIndex(0);
}

void search_shiplog_dialog::on_search_pushButton_clicked()
{
  QString str = ui->textEdit->toPlainText();
  int col = ui->comboBox->currentIndex();
  emit find( col, str );
  this->close();
}
