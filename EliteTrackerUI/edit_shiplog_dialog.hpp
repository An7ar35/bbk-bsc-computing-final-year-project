#ifndef EDIT_SHIPLOG_DIALOG_HPP
#define EDIT_SHIPLOG_DIALOG_HPP

#include <QDialog>
#include <QMessageBox>
#include "access.hpp"
#include "../../EliteTracker/src/core/database/tables/ShipLog.hpp"

namespace Ui {
  class Edit_ShipLog_Dialog;
}

class Edit_ShipLog_Dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit Edit_ShipLog_Dialog(QWidget *parent = 0);
    void setData(int &id, std::string &date, std::string &time, int &origin, int &destination, QString &desc );
    ~Edit_ShipLog_Dialog();

  private slots:
    void on_buttonBox_accepted();

  signals:
    void hasUpdated();

  private:
    Ui::Edit_ShipLog_Dialog *ui;
    int logEntry_number;
};

#endif // EDIT_SHIPLOG_DIALOG_HPP
