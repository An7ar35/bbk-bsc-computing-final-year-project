#ifndef ADD_MANUFACTURER_DIALOG_HPP
#define ADD_MANUFACTURER_DIALOG_HPP

#include <QDialog>

namespace Ui {
  class add_manufacturer_dialog;
}

class add_manufacturer_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit add_manufacturer_dialog(QWidget *parent = 0);
    ~add_manufacturer_dialog();

  private slots:
    void on_close_button_clicked();

  private:
    Ui::add_manufacturer_dialog *ui;
};

#endif // ADD_MANUFACTURER_DIALOG_HPP
