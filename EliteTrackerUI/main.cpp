#include "mainwindow.h"
#include <QApplication>
#include <QtWidgets>
#include <QMessageBox>
#include "../../EliteTracker/src/core/fileops/init.hpp"
#include "../../EliteTracker/src/core/database/editTables.hpp"
#include "../../EliteTracker/src/core/database/Db.hpp"

bool checkBaseFiles() {
  Access *access = Access::getInstance();
  Db data = access->getDb();
  if( editTable::checkTables( data ) &&
      data.getNumberOfRows( "Ships" ) >= 15 &&
      data.getNumberOfRows( "ShipClasses" ) >= 4 &&
      data.getNumberOfRows( "Stations" ) >= 110 &&
      data.getNumberOfRows( "Manufacturers" ) >= 6 &&
      data.getNumberOfRows( "JumpSystems" ) >= 55 ) { //Check base data
    return true;
  }
  return false;
}

int main(int argc, char *argv[])
{
  QApplication a(argc, argv);
  Log *log = Log::getInstance();
  log->setFileName( "EliteTrackerLog.txt");
  log->switch_autoFileDump(true);
  log->switch_EventType( EventType::Event, false );
  log->switch_EventType( EventType::Trace, false );
  Access *access = Access::getInstance();
  if( !checkBaseFiles() ) {
    init::setupBaseData();
    if( !checkBaseFiles() ) {
      log->logEvent( Location::IO, EventType::Error, "INIT: EliteTracker has encountered a problem loading player data.");
      QMessageBox errorBox;
      errorBox.critical(0,"Error","EliteTracker has encountered a problem loading player data.");
      return 1;
    }
  }
  fileIO::Settings config = access->getConfig();
  if( config.getValueOfKey<int>("CurrentPlayerShip", -1) == -1 ) {
    if( !config.writeConfig<int>("CurrentPlayerShip", 1 ) ) { //try add default
      log->logEvent( Location::IO, EventType::Error, "INIT: EliteTracker has encountered a problem with the settings file." );
      QMessageBox errorBox;
      errorBox.critical(0,"Error","EliteTracker has encountered a problem with the settings file.");
      return 1;
    }
  } else {
    Access *access = Access::getInstance();
    Db data = access->getDb();
    int ship_id = config.getValueOfKey<int>("CurrentPlayerShip", -1);
    Table t { };
    editTable::PlayerShips::getPlayerShipInfo( data, ship_id, t );
    if( t.getRowCount() < 1 ) {
      config.writeConfig<int>("CurrentPlayerShip", 1);
      log->logEvent( Location::IO, EventType::Error, "INIT: Current player ship doesn't exists in database. Default set. " );
    } else {
      log->logEvent( Location::IO, EventType::Event, "INIT: Current player ship exists in database." );
    }
  }
  MainWindow w;
  w.show();

  return a.exec();
}
