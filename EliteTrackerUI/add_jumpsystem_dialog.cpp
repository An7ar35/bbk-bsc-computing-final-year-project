#include "add_jumpsystem_dialog.hpp"
#include "ui_add_jumpsystem_dialog.h"
#include "edit_jumpsystems_dialog.hpp"
#include "../../EliteTracker/src/core/database/tables/JumpSystems.hpp"

add_JumpSystem_dialog::add_JumpSystem_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::add_JumpSystem_dialog)
{
  ui->setupUi(this);
  Access *access = Access::getInstance();
  Db data = access->getDb();
  ct = new CoordinateTool( data, "JumpConnections", "S_ID_A", "S_ID_B", "Distance", "JumpSystems", "S_ID", "X_Coordinate", "Y_Coordinate", "Z_Coordinate" );
  ui->tableView->setSelectionBehavior(QAbstractItemView::SelectRows);
  ui->tableView->verticalHeader()->hide();
  ui->tableView->setAlternatingRowColors(true);
  refreshList();
}

add_JumpSystem_dialog::~add_JumpSystem_dialog()
{
  delete ui;
  delete model;
}

void add_JumpSystem_dialog::on_close_button_clicked()
{
  this->close();
}

void add_JumpSystem_dialog::refreshList() {
  Access *access = Access::getInstance();
  Db data = access->getDb();
  data.sqlQueryPull_typed( "SELECT * FROM JumpSystems", jumpsystems );

  if( jumpsystems.getRowCount() > 0 ) {
    this->model = new QStandardItemModel(jumpsystems.getRowCount(),jumpsystems.getColumnCount(),this);
    for( int col = 0; col < jumpsystems.getColumnCount(); col++ ) {
      this->model->setHorizontalHeaderItem( col, new QStandardItem( QString::fromStdString( jumpsystems.getHeading( col )) ));
    }
    for( int row = 0; row < jumpsystems.getRowCount(); row++ ) {
      for( int col = 0; col < jumpsystems.getColumnCount(); col++ ) {
          QStandardItem *item = new QStandardItem( QString::fromStdString( jumpsystems(col,row).getString()) );
          item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          model->setItem(row,col,item);
      }
    }
    ui->tableView->setModel(model);
    ui->tableView->verticalHeader()->setSectionResizeMode(QHeaderView::ResizeToContents);
    ui->tableView->resizeColumnToContents(0);
    ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    ui->tableView->clearSelection();
    ui->tableView->sortByColumn(2, Qt::SortOrder(Qt::AscendingOrder) );
  }
}

void add_JumpSystem_dialog::on_add_button_clicked()
{
  Access *access = Access::getInstance();
  Db data = access->getDb();
  std::string name = (ui->jsname_plainTextEdit->toPlainText()).toStdString();
  if( name.size() > 3 ) {
    editTable::JumpSystems::add(data, name );
    int new_id = editTable::JumpSystems::getIdFor( data, "name" );
    if( new_id > 0 ) {
       ct->generateCoordinatesFrom( data, std::to_string( new_id ) );
    }
    ui->jsname_plainTextEdit->clear();
    refreshList();
  } else {
    QMessageBox errorBox;
    errorBox.critical(0,"Error","Name must be more than 3 characters long.");
  }
}

void add_JumpSystem_dialog::on_edit_button_clicked()
{
  int i = ui->tableView->selectionModel()->currentIndex().row();
  if( i >= 0 ) {
    int s_id = model->data(model->index(i, 0), Qt::DisplayRole).toInt();
    int name_col = jumpsystems.whereIsThisColumn( "Name" );
    Coordinate2D<int> table_i = jumpsystems.searchFor( "S_ID", s_id );
    std::string js_name = jumpsystems( name_col, table_i.y ).getString();
    edit_JumpSystems_dialog *edit = new edit_JumpSystems_dialog();
    edit->setData( s_id, js_name );
    edit->setWindowTitle( QString( "Edit Jump System #%1" ).arg( s_id ) );
    connect( edit, SIGNAL(saveUpdates(int,std::string) ), this, SLOT(updateDb(int,std::string)) );
    edit->setModal(true);
    edit->show();
  }
}

void add_JumpSystem_dialog::updateDb( int s_id, std::string name ) {
  Access *access = Access::getInstance();
  Db data = access->getDb();
  editTable::JumpSystems::edit( data, s_id, name );
  refreshList();
}
