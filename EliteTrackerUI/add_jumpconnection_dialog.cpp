#include "add_jumpconnection_dialog.hpp"
#include "ui_add_jumpconnection_dialog.h"
#include "QMessageBox"
#include "../../EliteTracker/src/core/database/tables/JumpConnections.hpp"

add_JumpConnection_dialog::add_JumpConnection_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::add_JumpConnection_dialog)
{
  ui->setupUi(this);
  Access *access = Access::getInstance();
  Db data = access->getDb();
  ct = new CoordinateTool( data, "JumpConnections", "S_ID_A", "S_ID_B", "Distance", "JumpSystems", "S_ID", "X_Coordinate", "Y_Coordinate", "Z_Coordinate" );
  this->newdata = true;
  Table systems { };
  data.sqlQueryPull_typed( "SELECT S_ID, Name FROM JumpSystems", systems );
  for( int i = 0; i < systems.getRowCount(); i++ ) {
    QString str = QString::fromStdString( systems(1,i).getString() );
    ui->a_comboBox->addItem(str, systems(0,i).getInt() );
    ui->b_comboBox->addItem(str, systems(0,i).getInt() );
  }
  ui->a_comboBox->setCurrentIndex(0);
  ui->b_comboBox->setCurrentIndex(0);
  ui->doubleSpinBox->clear();
  checkJCStatus();
}

add_JumpConnection_dialog::~add_JumpConnection_dialog()
{
  delete ui;
}

void add_JumpConnection_dialog::on_close_button_clicked()
{
  this->close();
}

void add_JumpConnection_dialog::on_ok_button_clicked()
{
  double val = ui->doubleSpinBox->value();
  if( val > 0 ) {
    Access *access = Access::getInstance();
    Db data = access->getDb();
    int a = ui->a_comboBox->itemData(ui->a_comboBox->currentIndex()).toInt();
    int b = ui->b_comboBox->itemData(ui->b_comboBox->currentIndex()).toInt();
    if( newdata ) {
       editTable::JumpConnections::add( data, a, b, val );
       ct->generateCoordinatesFrom( data, std::to_string( a ) );
       ct->generateCoordinatesFrom( data, std::to_string( b ) );
       this->close();
    }
  } else {
    QMessageBox errorBox;
    errorBox.critical(0,"Error","Distance cannot be 0.");
  }
}

void add_JumpConnection_dialog::on_b_comboBox_activated(int index)
{
  checkJCStatus();
}

void add_JumpConnection_dialog::on_a_comboBox_activated(int index)
{
  checkJCStatus();
}

void add_JumpConnection_dialog::checkJCStatus() {
  ui->doubleSpinBox->clear();
  int a = ui->a_comboBox->itemData(ui->a_comboBox->currentIndex()).toInt();
  int b = ui->b_comboBox->itemData(ui->b_comboBox->currentIndex()).toInt();
  if( a == b ) {
    ui->ok_button->setEnabled(false);
    ui->doubleSpinBox->setEnabled(false);
  } else {
    Access *access = Access::getInstance();
    Db data = access->getDb();
    double dist = editTable::JumpConnections::getDistance( data, a, b );
    if( dist < 0 ) { //no recorded distance found
      ui->ok_button->setEnabled(true);
      ui->doubleSpinBox->setEnabled(true);
      this->newdata = true;
      ui->ok_button->setText( QString( "Add" ) );
    } else {
      ui->ok_button->setEnabled(false);
      ui->doubleSpinBox->setEnabled(false);
      this->newdata = false;
      ui->ok_button->setText( QString( "Update" ) );
      ui->doubleSpinBox->setValue(dist);
    }
  }
}
