#ifndef ADDTRAVEL_HPP
#define ADDTRAVEL_HPP

#include <QDialog>

namespace Ui {
  class addTravel;
}

class addTravel : public QDialog
{
    Q_OBJECT

  public:
    explicit addTravel(QWidget *parent = 0);
    ~addTravel();

  private:
    Ui::addTravel *ui;
};

#endif // ADDTRAVEL_HPP
