#ifndef ACCESS_HPP
#define ACCESS_HPP

#include "../../EliteTracker/src/core/logger/Log.hpp"
#include "../../EliteTracker/src/core/database/Db.hpp"
#include "../../EliteTracker/src/core/fileops/Settings.hpp"

class Access
{
  public:
    ~Access();
    static Access* getInstance();
    fileIO::Settings & getConfig();
    Db & getDb();
  private:
    Access() : data( Db { "elitedata.db" } ), config( fileIO::Settings( "settings.cfg" ) ) {
      Log *log = Log::getInstance();
      config.readFile();
    }
    static bool instanceFlag;
    static Access *access;
    Db data;
    fileIO::Settings config;
};

#endif // ACCESS_HPP
