#ifndef EDIT_JUMPSYSTEMS_DIALOG_HPP
#define EDIT_JUMPSYSTEMS_DIALOG_HPP

#include <QDialog>
#include <QMessageBox>

namespace Ui {
  class edit_JumpSystems_dialog;
}

class edit_JumpSystems_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit edit_JumpSystems_dialog(QWidget *parent = 0);
    ~edit_JumpSystems_dialog();
    void setData( int s_id, std::string name );

  signals:
    void saveUpdates( int s_id, std::string name );

  private slots:
    void on_buttonBox_accepted();

  private:
    Ui::edit_JumpSystems_dialog *ui;
    int s_id;
};

#endif // EDIT_JUMPSYSTEMS_DIALOG_HPP
