#ifndef ADD_PLAYERSHIP_DIALOG_HPP
#define ADD_PLAYERSHIP_DIALOG_HPP

#include <QDialog>
#include <QtCore>
#include <QtGUI>
#include <QMessageBox>
#include "access.hpp"
#include "../../EliteTracker/src/core/database/Db.hpp"
#include "../../EliteTracker/src/core/containers/Table.hpp"

namespace Ui {
  class add_playership_dialog;
}

class add_playership_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit add_playership_dialog(QWidget *parent = 0);
    ~add_playership_dialog();
    void refresh();
  signals:
    void hasAdded();
  private slots:
    void on_cancel_clicked();
    void on_add_clicked();
    void on_ships_comboBox_activated(int index);

  private:
    Ui::add_playership_dialog *ui;
    Table ships;
    QStandardItemModel *ship_model;
    int selected_ship;
};

#endif // ADD_PLAYERSHIP_DIALOG_HPP
