#ifndef ADD_SHIPLOG_DIALOG_HPP
#define ADD_SHIPLOG_DIALOG_HPP

#include <QDialog>
#include <QMessageBox>
#include "access.hpp"
#include "../../EliteTracker/src/core/database/tables/ShipLog.hpp"

namespace Ui {
  class add_shiplog_dialog;
}

class add_shiplog_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit add_shiplog_dialog(QWidget *parent = 0);
    ~add_shiplog_dialog();
    void setData( int &origin );
  signals:
    void hasValidated();
  private slots:
    void on_buttonBox_accepted();

  private:
    Ui::add_shiplog_dialog *ui;
};

#endif // ADD_SHIPLOG_DIALOG_HPP
