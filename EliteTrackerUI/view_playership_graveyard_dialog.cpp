#include "view_playership_graveyard_dialog.hpp"
#include "ui_view_playership_graveyard_dialog.h"
#include "../../EliteTracker/src/core/database/tables/PlayerShips.hpp"

view_playership_graveyard_dialog::view_playership_graveyard_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::view_playership_graveyard_dialog)
{
  ui->setupUi(this);
  ui->specs_tableView->setSelectionMode(QAbstractItemView::NoSelection);
  ui->specs_tableView->horizontalHeader()->hide();
  ui->specs_tableView->verticalHeader()->setSelectionMode(QAbstractItemView::NoSelection);
  refreshList();
}

view_playership_graveyard_dialog::~view_playership_graveyard_dialog()
{
  delete ui;
}

void view_playership_graveyard_dialog::refreshList() {
  Access *access = Access::getInstance();
  Db data = access->getDb();
  ui->graveyard_listWidget->clear();
  if( editTable::PlayerShips::getPlayerShips_graveyard( data, graveyard ) && graveyard.getRowCount() > 0 ) {
    for( int i = 0; i < graveyard.getRowCount(); i++ ) {
      QString str = "[" + QString::fromStdString( graveyard( 0, i ).getString() ) + "] " + QString::fromStdString( graveyard( 1, i ).getString() );
      ui->graveyard_listWidget->addItem( str );
    }
    ui->graveyard_listWidget->repaint();
  }
}

void view_playership_graveyard_dialog::showSpecs( int row ) {
  this->ship_model = new QStandardItemModel(graveyard.getColumnCount(), 1, this);
  for( int col = 0; col < graveyard.getColumnCount(); col++) {
    this->ship_model->setVerticalHeaderItem( col, new QStandardItem( QString::fromStdString( graveyard.getHeading( col )) ));
  }
  for( int col = 0; col < graveyard.getColumnCount(); col++ ) {
    QStandardItem *item = new QStandardItem( QString::fromStdString( graveyard(col,row).getString()) );
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ship_model->setItem(col,row,item);
  }
  ui->specs_tableView->setModel(ship_model);
  ui->specs_tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
  ui->specs_tableView->clearSelection();
}

void view_playership_graveyard_dialog::on_resurect_clicked()
{
  Access *access = Access::getInstance();
  Db data = access->getDb();
  int i = ui->graveyard_listWidget->selectionModel()->currentIndex().row();
  if( i >= 0 ) {
    int id = graveyard(0,i).getInt();
    editTable::PlayerShips::edit( data, id, false );
    ui->graveyard_listWidget->clearSelection();
    ship_model->clear();
    refreshList();
  }
}

void view_playership_graveyard_dialog::on_close_clicked()
{
    emit hasClosed();
    this->close();
}

void view_playership_graveyard_dialog::on_graveyard_listWidget_itemClicked(QListWidgetItem *item)
{
  int row = ui->graveyard_listWidget->selectionModel()->currentIndex().row();
  showSpecs( row );
}
