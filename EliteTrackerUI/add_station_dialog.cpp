#include "add_station_dialog.hpp"
#include "ui_add_station_dialog.h"

add_Station_dialog::add_Station_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::add_Station_dialog)
{
  Access *access = Access::getInstance();
  Db data = access->getDb();
  ui->setupUi(this);
  Table systems { };
  data.sqlQueryPull_typed( "SELECT S_ID, Name FROM JumpSystems", systems );
  for( int i = 0; i < systems.getRowCount(); i++ ) {
    QString str = QString::fromStdString( systems(1,i).getString() );
    ui->system_comboBox->addItem(str, systems(0,i).getInt() );
  }
  ui->system_comboBox->setCurrentIndex(0);
}

add_Station_dialog::~add_Station_dialog()
{
  delete ui;
}

void add_Station_dialog::on_close_button_clicked()
{
  this->close();
}

void add_Station_dialog::writeToDb( int id, std::string name ) {
  Access *access = Access::getInstance();
  Db data = access->getDb();
  editTable::Stations::add( data, id, name );
  this->close();
}

void add_Station_dialog::on_add_button_clicked()
{
  std::string name = (ui->name_lineEdit->text()).toStdString();
  if( name.size() > 3 ) {
    int system_id = ui->system_comboBox->itemData(ui->system_comboBox->currentIndex()).toInt();
    QString system_name = ui->system_comboBox->currentText();
    QString str = "'<i>" + QString::fromStdString( name ) + "</i>' station in the '<i>" + system_name + "</i>' system.";
    QMessageBox::StandardButton reply;
    reply = QMessageBox::question(this, "Write to database?", str,
                                  QMessageBox::Yes|QMessageBox::No);
    if (reply == QMessageBox::Yes) {
      writeToDb( system_id, name );
    }
  } else {
    QMessageBox errorBox;
    errorBox.critical(0,"Error","Name must be more than 3 characters long.");
  }
}
