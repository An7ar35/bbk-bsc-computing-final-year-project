#ifndef ADD_JUMPSYSTEM_DIALOG_HPP
#define ADD_JUMPSYSTEM_DIALOG_HPP

#include <QDialog>
#include <QtCore>
#include <QtGUI>
#include "access.hpp"
#include "../../EliteTracker/src/core/containers/Table.hpp"
#include "../../EliteTracker/src/satellites/coordinate/coordinate_tool.hpp"

namespace Ui {
  class add_JumpSystem_dialog;
}

class add_JumpSystem_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit add_JumpSystem_dialog(QWidget *parent = 0);
    ~add_JumpSystem_dialog();

  private slots:
    void updateDb( int s_id, std::string name );
    void on_close_button_clicked();
    void refreshList();
    void on_add_button_clicked();
    void on_edit_button_clicked();

  private:
    Ui::add_JumpSystem_dialog *ui;
    Table jumpsystems;
    QStandardItemModel *model;
    CoordinateTool *ct;

};

#endif // ADD_JUMPSYSTEM_DIALOG_HPP
