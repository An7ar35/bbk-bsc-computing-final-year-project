#ifndef ADD_JUMPCONNECTION_DIALOG_HPP
#define ADD_JUMPCONNECTION_DIALOG_HPP

#include <QDialog>
#include <QtCore>
#include <QtGUI>
#include "access.hpp"
#include "../../EliteTracker/src/core/containers/Table.hpp"
#include "../../EliteTracker/src/satellites/coordinate/coordinate_tool.hpp"

namespace Ui {
  class add_JumpConnection_dialog;
}

class add_JumpConnection_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit add_JumpConnection_dialog(QWidget *parent = 0);
    ~add_JumpConnection_dialog();

  private slots:
    void on_close_button_clicked();
    void on_ok_button_clicked();
    void on_b_comboBox_activated(int index);
    void on_a_comboBox_activated(int index);
    void checkJCStatus();

  private:
    Ui::add_JumpConnection_dialog *ui;
    Table jumpconnection;
    bool newdata;
    CoordinateTool *ct;
};

#endif // ADD_JUMPCONNECTION_DIALOG_HPP
