#include "shortestsetpath_dialog.hpp"
#include "ui_shortestsetpath_dialog.h"

shortestsetpath_dialog::shortestsetpath_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::shortestsetpath_dialog)
{
  ui->setupUi(this);
  Access *access = Access::getInstance();
  Db data = access->getDb();
  fileIO::Settings config = access->getConfig();
  pf = new Pathfinder( "elitedata.db", config );
  ui->pathnotfound_label->setVisible(false);
  ui->results_tableView->setSelectionMode(QAbstractItemView::NoSelection);
  ui->results_tableView->verticalHeader()->hide();
  ui->results_tableView->horizontalHeader()->setSelectionMode(QAbstractItemView::NoSelection);
  Table systems { };
  data.sqlQueryPull_typed( "SELECT S_ID, Name FROM JumpSystems", systems );
  for( int i = 0; i < systems.getRowCount(); i++ ) {
    QString str = QString::fromStdString( systems(1,i).getString() );
    ui->from_comboBox->addItem(str, systems(0,i).getInt() );
    ui->to_comboBox->addItem(str, systems(0,i).getInt() );
  }
  ui->from_comboBox->setCurrentIndex(0);
  ui->to_comboBox->setCurrentIndex(0);
}

shortestsetpath_dialog::~shortestsetpath_dialog()
{
  delete ssp_table_model;
  delete ssp_list_model;
  delete pf;
  delete ui;
}

void shortestsetpath_dialog::refresh_table() {
  int from_id = ui->from_comboBox->itemData(ui->from_comboBox->currentIndex()).toInt();
  bool gotpath = pf->findShortestPath( from_id, set );
  ui->pathnotfound_label->setText( "Couldn't find a path.." );
  if( gotpath ) {
    ui->pathnotfound_label->setVisible(false);
    ssp_results = pf->getPathInfo();
    this->ssp_table_model = new QStandardItemModel( ssp_results.getRowCount(), ssp_results.getColumnCount(), this);
    for( int col = 0; col < ssp_results.getColumnCount(); col++) {
      this->ssp_table_model->setHorizontalHeaderItem( col, new QStandardItem( QString::fromStdString( ssp_results.getHeading( col )) ));
    }
    for( int row = 0; row < ssp_results.getRowCount(); row++ ) {
      for( int col = 0; col < ssp_results.getColumnCount(); col++ ) {
          QStandardItem *item = new QStandardItem( QString::fromStdString( ssp_results(col,row).getString()) );
          item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          ssp_table_model->setItem(row,col,item);
      }
    }
    ui->results_tableView->setModel(ssp_table_model);
    ui->results_tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->results_tableView->clearSelection();
  } else {
    if( ssp_results.getColumnCount() > 0 ) {
      while( ssp_table_model->rowCount() > 0 )
      {
        ssp_table_model->removeRow(0);
      }
    }
    ui->pathnotfound_label->setVisible(true);
  }
}


void shortestsetpath_dialog::on_clea_list_clicked()
{
  ui->stops_listWidget->clear();
  std::vector<int>().swap( set );
  ui->to_comboBox->setCurrentIndex(0);
  if( ssp_results.getColumnCount() > 0 ) {
    while( ssp_table_model->rowCount() > 0 )
    {
      ssp_table_model->removeRow(0);
    }
  }
}

void shortestsetpath_dialog::on_go_clicked()
{
  if( set.size() > 0 ) {
    if( set.size() >= 10 ) {
      if( ssp_results.getColumnCount() > 0 ) {
        while( ssp_table_model->rowCount() > 0 )
        {
          ssp_table_model->removeRow(0);
        }
      }
      ui->pathnotfound_label->setText( "Too many destinations set. Must be less than 10 Jump Systems." );
      ui->pathnotfound_label->setVisible(true);
    } else {
      refresh_table();
    }
  } else {
    if( ssp_results.getColumnCount() > 0 ) {
      while( ssp_table_model->rowCount() > 0 )
      {
        ssp_table_model->removeRow(0);
      }
    }
    ui->pathnotfound_label->setText( "No destination(s) set." );
    ui->pathnotfound_label->setVisible(true);
  }
}

void shortestsetpath_dialog::on_add_clicked()
{
  int to_id = ui->to_comboBox->itemData(ui->to_comboBox->currentIndex()).toInt();
  set.push_back( to_id );
  QString str = "(" + ui->to_comboBox->itemData(ui->to_comboBox->currentIndex()).toString() + ") " + ui->to_comboBox->currentText();
  ui->stops_listWidget->addItem( str );
  ui->stops_listWidget->repaint();
}

void shortestsetpath_dialog::on_close_clicked()
{
    this->close();
}

void shortestsetpath_dialog::on_from_comboBox_activated(int index)
{
  ui->stops_listWidget->clear();
  std::vector<int>().swap( set );
}
