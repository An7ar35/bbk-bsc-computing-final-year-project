#ifndef EDIT_PLAYERSHIP_DIALOG_HPP
#define EDIT_PLAYERSHIP_DIALOG_HPP

#include <QDialog>
#include <QtCore>
#include <QtGUI>
#include <QMessageBox>
#include "access.hpp"
#include "../../EliteTracker/src/core/database/Db.hpp"
#include "../../EliteTracker/src/core/containers/Table.hpp"

namespace Ui {
  class edit_playership_dialog;
}

class edit_playership_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit edit_playership_dialog(QWidget *parent = 0);
    ~edit_playership_dialog();

  public slots:
    void refreshPlayerShips();

  signals:
    void hasUpdated();

  private slots:
    void on_playerships_comboBox_currentIndexChanged(int index);
    void on_playerships_comboBox_activated(int index);
    void on_buttonBox_accepted();
    void on_show_graveyard_clicked();
    void on_destroy_playership_clicked();
    void on_add_playership_clicked();

    void on_edit_playership_clicked();

  private:
    Ui::edit_playership_dialog *ui;
    Table playerships;
    Table ship_specs;
    QStandardItemModel *ship_model;
    int selected_ship;
};

#endif // EDIT_PLAYERSHIP_DIALOG_HPP
