#include "edit_jumpsystems_dialog.hpp"
#include "ui_edit_jumpsystems_dialog.h"

edit_JumpSystems_dialog::edit_JumpSystems_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::edit_JumpSystems_dialog)
{
  ui->setupUi(this);
}

edit_JumpSystems_dialog::~edit_JumpSystems_dialog()
{
  delete ui;
}

void edit_JumpSystems_dialog::setData( int id, std::string name ) {
  this->s_id = id;
  ui->plainTextEdit->insertPlainText( QString::fromStdString( name ) );
}

void edit_JumpSystems_dialog::on_buttonBox_accepted()
{
  std::string name = (ui->plainTextEdit->toPlainText()).toStdString();
  if( name.size() > 3 ) {
      emit saveUpdates( s_id, name );
  } else {
    QMessageBox errorBox;
    errorBox.critical(0,"Error","Name must be more than 3 characters long.");
  }
}
