#include "log_dialog.hpp"
#include "ui_log_dialog.h"

log_dialog::log_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::log_dialog)
{
  ui->setupUi(this);
  log = Log::getInstance();
  ui->core->setChecked( log->getSwitchState( Location::Core ) );
  ui->satellite->setChecked( log->getSwitchState( Location::Satellite ) );
  ui->sqlite->setChecked( log->getSwitchState( Location::SQLite ) );
  ui->io->setChecked( log->getSwitchState( Location::IO ) );
  ui->gui->setChecked( log->getSwitchState( Location::GUI ) );
  ui->os->setChecked( log->getSwitchState( Location::OS ) );
  ui->events->setChecked( log->getSwitchState( EventType::Event ) );
  ui->errors->setChecked( log->getSwitchState( EventType::Error ) );
  ui->trace->setChecked( log->getSwitchState( EventType::Trace ) );
  ui->debug->setChecked( log->getSwitchState( EventType::Debug ) );
}

log_dialog::~log_dialog()
{
  delete ui;
}

void log_dialog::on_core_clicked(bool checked)
{
  log->switch_Location( Location::Core, checked );
}

void log_dialog::on_satellite_clicked(bool checked)
{
  log->switch_Location( Location::Satellite, checked );
}

void log_dialog::on_io_clicked(bool checked)
{
  log->switch_Location( Location::IO, checked );
}

void log_dialog::on_sqlite_clicked(bool checked)
{
  log->switch_Location( Location::SQLite, checked );
}

void log_dialog::on_gui_clicked(bool checked)
{
  log->switch_Location( Location::GUI, checked );
}

void log_dialog::on_os_clicked(bool checked)
{
  log->switch_Location( Location::OS, checked );
}

void log_dialog::on_errors_clicked(bool checked)
{
  log->switch_EventType( EventType::Error, checked );
}

void log_dialog::on_events_clicked(bool checked)
{
  log->switch_EventType( EventType::Event, checked );
}

void log_dialog::on_trace_clicked(bool checked)
{
  log->switch_EventType( EventType::Trace, checked );
}

void log_dialog::on_debug_clicked(bool checked)
{
  log->switch_EventType( EventType::Debug, checked );
}
