#ifndef MODEL_READONLY_HPP
#define MODEL_READONLY_HPP
#include <QTGUI>
#include <QTCore>

class Model_readonly : public QStandardItemModel
{
  public:
    Model_readonly();
    ~Model_readonly();
    virtual Qt::ItemFlags flags(const QModelIndex& index) const override;
};

#endif // MODEL_READONLY_HPP
