#include "add_manufacturer_dialog.hpp"
#include "ui_add_manufacturer_dialog.h"

add_manufacturer_dialog::add_manufacturer_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::add_manufacturer_dialog)
{
  ui->setupUi(this);
}

add_manufacturer_dialog::~add_manufacturer_dialog()
{
  delete ui;
}

void add_manufacturer_dialog::on_close_button_clicked()
{
  this->close();
}
