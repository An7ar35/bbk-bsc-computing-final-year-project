#include "edit_playership_dialog.hpp"
#include "ui_edit_playership_dialog.h"
#include "view_playership_graveyard_dialog.hpp"
#include "add_playership_dialog.hpp"
#include "../../EliteTracker/src/core/database/tables/PlayerShips.hpp"

edit_playership_dialog::edit_playership_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::edit_playership_dialog)
{
  ui->setupUi(this);
  Access *access = Access::getInstance();
  fileIO::Settings config = access->getConfig();
  selected_ship = config.getValueOfKey<int>("CurrentPlayerShip", -1);
  ui->shipspecs_tableView->setSelectionMode(QAbstractItemView::NoSelection);
  ui->shipspecs_tableView->horizontalHeader()->hide();
  ui->shipspecs_tableView->verticalHeader()->setSelectionMode(QAbstractItemView::NoSelection);
  refreshPlayerShips();
}

edit_playership_dialog::~edit_playership_dialog()
{
  delete ui;
}

void edit_playership_dialog::refreshPlayerShips() {
  Access *access = Access::getInstance();
  Db data = access->getDb();
  ui->playerships_comboBox->clear();
  if( editTable::PlayerShips::getPlayerShips_current( data, playerships ) ) {
    for( int i = 0; i < playerships.getRowCount(); i++ ) {
      QString str = "[" + QString::fromStdString( playerships(0,i).getString() ) + "] " + QString::fromStdString( playerships(1,i).getString() );
      ui->playerships_comboBox->addItem(str, playerships(0,i).getInt() );
    }
    int s_index = ui->playerships_comboBox->findData(selected_ship);
    if ( s_index != -1 ) {
       ui->playerships_comboBox->setCurrentIndex(s_index);
    }
    bool gotInfo = editTable::PlayerShips::getPlayerShipInfo( data, selected_ship, ship_specs );
    if( gotInfo && ship_specs.getRowCount() > 0 ) {
      this->ship_model = new QStandardItemModel(ship_specs.getColumnCount(), ship_specs.getRowCount(), this);
      for( int col = 0; col < ship_specs.getColumnCount(); col++) {
        this->ship_model->setVerticalHeaderItem( col, new QStandardItem( QString::fromStdString( ship_specs.getHeading( col )) ));
      }
      for( int row = 0; row < ship_specs.getRowCount(); row++ ) {
        for( int col = 0; col < ship_specs.getColumnCount(); col++ ) {
          QStandardItem *item = new QStandardItem( QString::fromStdString( ship_specs(col,row).getString()) );
          item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          ship_model->setItem(col,row,item);
        }
      }
    }
    std::string fuelscoop = ship_specs(4,0).getString();
    if( fuelscoop == "TRUE" ) {
        ui->edit_playership->setText( "Remove fuel scoop" );
    } else {
        ui->edit_playership->setText( "Add fuel scoop" );
    }
    ui->shipspecs_tableView->setModel(ship_model);
    ui->shipspecs_tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
    ui->shipspecs_tableView->clearSelection();
  }
}

void edit_playership_dialog::on_playerships_comboBox_activated(int index)
{
  selected_ship = ui->playerships_comboBox->itemData(index).toInt();
  refreshPlayerShips();
}

void edit_playership_dialog::on_playerships_comboBox_currentIndexChanged(int index)
{

}

void edit_playership_dialog::on_buttonBox_accepted()
{
  Access *access = Access::getInstance();
  fileIO::Settings config = access->getConfig();
  config.writeConfig<int>("CurrentPlayerShip", selected_ship );
  emit hasUpdated();
}

void edit_playership_dialog::on_show_graveyard_clicked()
{
  view_playership_graveyard_dialog *graveyard = new view_playership_graveyard_dialog();
  graveyard->setModal(true);
  connect( graveyard, SIGNAL( hasClosed() ), this, SLOT( refreshPlayerShips() ) );
  graveyard->show();
}

void edit_playership_dialog::on_destroy_playership_clicked()
{
  Access *access = Access::getInstance();
  Db data = access->getDb();
  if( selected_ship != 1 ) {
    editTable::PlayerShips::edit( data, selected_ship, true );
    selected_ship = 1;
  } else {
    QMessageBox::information(NULL, "Handy tip", "The first and default ship cannot be destroyed.");
  }
  refreshPlayerShips();
}

void edit_playership_dialog::on_add_playership_clicked()
{
  add_playership_dialog *addship = new add_playership_dialog();
  addship->setModal(true);
  connect( addship, SIGNAL( hasAdded() ), this, SLOT( refreshPlayerShips() ) );
  addship->show();
}

void edit_playership_dialog::on_edit_playership_clicked()
{
  Access *access = Access::getInstance();
  Db data = access->getDb();
  std::string fuelscoop = ship_specs(4,0).getString();
  int id = ship_specs( 0, 0 ).getInt();
  if( fuelscoop == "TRUE" ) {
    editTable::PlayerShips::edit( data, id, false, false );
  } else {
    editTable::PlayerShips::edit( data, id, true, false );
  }
  refreshPlayerShips();
}
