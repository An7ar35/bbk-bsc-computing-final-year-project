#include "edit_shiplog_dialog.hpp"
#include "ui_edit_shiplog_dialog.h"

Edit_ShipLog_Dialog::Edit_ShipLog_Dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::Edit_ShipLog_Dialog)
{
  ui->setupUi(this);
  this->logEntry_number = -1;
  Access *access = Access::getInstance();
  Db data = access->getDb();
  Table systems_table { };
  std::string shiplog_query = "SELECT S_ID, Name FROM JumpSystems;";
  data.sqlQueryPull_typed( shiplog_query, systems_table );
  for( int i = 0; i < systems_table.getRowCount(); i++ ) {
    ui->originList->addItem(QString::fromStdString( systems_table(1,i).getString() ), systems_table(0,i).getInt() );
    ui->destinationList->addItem(QString::fromStdString( systems_table(1,i).getString() ), systems_table(0,i).getInt() );
  }
}

Edit_ShipLog_Dialog::~Edit_ShipLog_Dialog()
{
  delete ui;
}

void Edit_ShipLog_Dialog::setData( int &id, std::string &date, std::string &time, int &origin, int &destination, QString &desc ) {
  this->logEntry_number = id;
  ui->entry_numberEdit->setText( QString::fromStdString(std::to_string(id)) );
  QTime qtime = QTime::fromString(QString::fromStdString(time),QString("hh:mm:ss"));
  QDate qdate = QDate::fromString(QString::fromStdString(date),QString("dd/MM/yyyy"));
  ui->timeEdit->setTime( qtime );
  ui->dateEdit->setDate( qdate );
  int o_index = ui->originList->findData(origin);
  int d_index = ui->destinationList->findData(destination);
  if ( o_index != -1 ) {
     ui->originList->setCurrentIndex(o_index);
  }
  if ( d_index != -1 ) {
     ui->destinationList->setCurrentIndex(d_index);
  }
  ui->descriptionEdit->setText( desc );
}

void Edit_ShipLog_Dialog::on_buttonBox_accepted()
{
  Log *log = Log::getInstance();
  Access *access = Access::getInstance();
  Db data = access->getDb();
  if( logEntry_number >= 0 ) {
    std::string str_date = (ui->dateEdit->text()).toStdString();
    std::string str_time = (ui->timeEdit->text()).toStdString();
    int from = ui->originList->itemData(ui->originList->currentIndex()).toInt();
    int to = ui->destinationList->itemData(ui->destinationList->currentIndex()).toInt();
    std::string str_desc = (ui->descriptionEdit->toPlainText()).toStdString();
    log->logEvent( Location::GUI, EventType::Trace, "ship log edit data" );
    editTable::ShipLog::edit( data, this->logEntry_number, str_date, str_time, from, to, str_desc );
    emit hasUpdated();
  }
}
