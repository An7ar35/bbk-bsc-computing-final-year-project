#include "model_readonly.hpp"

Model_readonly::Model_readonly() {
  using QStandardItemModel::QStandardItemModel;
}

Model_readonly::~Model_readonly() {
  using QStandardItemModel::~QStandardItemModel;
}

virtual Qt::ItemFlags Model_readonly::flags(const QModelIndex& index) const override
{
  Qt::ItemFlags result = QStandardItemModel::flags(index);
  if (index.column() >= 0)
  {
    result &= ~Qt::ItemIsEditable;
  }
  return result;
}
