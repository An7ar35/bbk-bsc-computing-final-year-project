#include "shortestpath_dialog.hpp"
#include "ui_shortestpath_dialog.h"
#include "../../EliteTracker/src/core/database/tables/JumpSystems.hpp"

shortestpath_dialog::shortestpath_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::shortestpath_dialog)
{
  ui->setupUi(this);
  Access *access = Access::getInstance();
  Db data = access->getDb();
  fileIO::Settings config = access->getConfig();
  pf = new Pathfinder( "elitedata.db", config );
  ui->pathnotfound_label->setVisible(false);
  ui->results_tableView->setSelectionMode(QAbstractItemView::NoSelection);
  ui->results_tableView->verticalHeader()->hide();
  ui->results_tableView->horizontalHeader()->setSelectionMode(QAbstractItemView::NoSelection);
  Table systems { };
  data.sqlQueryPull_typed( "SELECT S_ID, Name FROM JumpSystems", systems );
  for( int i = 0; i < systems.getRowCount(); i++ ) {
    QString str = QString::fromStdString( systems(1,i).getString() );
    ui->from_comboBox->addItem(str, systems(0,i).getInt() );
    ui->to_comboBox->addItem(str, systems(0,i).getInt() );
  }
  ui->from_comboBox->setCurrentIndex(0);
  ui->to_comboBox->setCurrentIndex(0);
}

shortestpath_dialog::~shortestpath_dialog()
{
  delete ui;
  delete sp_model;
  delete pf;
}

void shortestpath_dialog::refresh() {
  int from_id = ui->from_comboBox->itemData(ui->from_comboBox->currentIndex()).toInt();
  int to_id = ui->to_comboBox->itemData(ui->to_comboBox->currentIndex()).toInt();
  bool gotpath = pf->findShortestPath( from_id, to_id );
  if( gotpath ) {
    ui->pathnotfound_label->setVisible(false);
    sp_results = pf->getPathInfo();
    this->sp_model = new QStandardItemModel( sp_results.getRowCount(), sp_results.getColumnCount(), this);
    for( int col = 0; col < sp_results.getColumnCount(); col++) {
      this->sp_model->setHorizontalHeaderItem( col, new QStandardItem( QString::fromStdString( sp_results.getHeading( col )) ));
    }
    for( int row = 0; row < sp_results.getRowCount(); row++ ) {
      for( int col = 0; col < sp_results.getColumnCount(); col++ ) {
          QStandardItem *item = new QStandardItem( QString::fromStdString( sp_results(col,row).getString()) );
          item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
          sp_model->setItem(row,col,item);
      }
    }
    ui->results_tableView->setModel(sp_model);
    ui->results_tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->results_tableView->clearSelection();
  } else {
    if( sp_results.getColumnCount() > 0 ) {
      while( sp_model->rowCount() > 0 )
      {
        sp_model->removeRow(0);
      }
    }
    ui->pathnotfound_label->setVisible(true);
  }
}

void shortestpath_dialog::on_close_clicked()
{
    this->close();
}

void shortestpath_dialog::on_go_clicked()
{
    refresh();
}
