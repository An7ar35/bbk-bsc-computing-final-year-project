#include "access.hpp"

bool Access::instanceFlag { false };
Access* Access::access = NULL;

Access::~Access()
{
  data.close();
  config.writeFile();
  this->instanceFlag = false;
  Log *log = Log::getInstance();
  log->logEvent( Location::GUI, EventType::Debug, "(Access::~Access) Access killed off.");
}

Access * Access::getInstance() {
  Log *log = Log::getInstance();
  log->logEvent( Location::GUI, EventType::Debug, "(Access::getInstance) Access requested.");
  if( !instanceFlag ) {
          access = new Access { };
          instanceFlag = true;
          return access;
  } else {
          return access;
  }
}

Db & Access::getDb() {
  Log *log = Log::getInstance();
  std::ostringstream msg { };
  if( data.open() ) {
    msg << "(Access::getDb) access ok.";
    log->newEvent( Location::GUI, EventType::Trace, msg );
  } else {
    msg << "(Access::getDb) access denied";
    log->newEvent( Location::GUI, EventType::Error, msg );
  }
  return data;
}

fileIO::Settings & Access::getConfig() {
  Log *log = Log::getInstance();
  log->logEvent( Location::GUI, EventType::Trace, "(Access::getConfig) Getting settings access.");
  return config;
}
