#include "add_playership_dialog.hpp"
#include "ui_add_playership_dialog.h"
#include "../../EliteTracker/src/core/database/tables/Ships.hpp"
#include "../../EliteTracker/src/core/database/tables/PlayerShips.hpp"
#include "../../EliteTracker/src/core/containers/Coordinate2D.hpp"

add_playership_dialog::add_playership_dialog(QWidget *parent) :
  QDialog(parent),
  ui(new Ui::add_playership_dialog)
{
  ui->setupUi(this);
  Access *access = Access::getInstance();
  Db data = access->getDb();
  selected_ship = -1;
  ui->specs_tableView->setSelectionMode(QAbstractItemView::NoSelection);
  ui->specs_tableView->horizontalHeader()->hide();
  ui->specs_tableView->verticalHeader()->setSelectionMode(QAbstractItemView::NoSelection);
  editTable::Ships::getShipsInfo( data, ships );
  for( int i = 0; i < ships.getRowCount(); i++ ) {
    QString str = QString::fromStdString( ships(1,i).getString() );
    ui->ships_comboBox->addItem(str, ships(0,i).getInt() );
  }
  ui->ships_comboBox->setCurrentIndex(0);
  selected_ship = 1;
  refresh();
}

add_playership_dialog::~add_playership_dialog()
{
  delete ui;
}

void add_playership_dialog::refresh() {
  this->ship_model = new QStandardItemModel(ships.getColumnCount(), 1, this);
  for( int col = 0; col < ships.getColumnCount(); col++) {
    this->ship_model->setVerticalHeaderItem( col, new QStandardItem( QString::fromStdString( ships.getHeading( col )) ));
  }
  Coordinate2D<int> loc = ships.searchFor( "ID", Item( selected_ship ) );
  int row = loc.y;
  for( int col = 0; col < ships.getColumnCount(); col++ ) {
    QStandardItem *item = new QStandardItem( QString::fromStdString( ships(col,row).getString()) );
    item->setFlags(Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    ship_model->setItem(col,0,item);
  }
  ui->specs_tableView->setModel(ship_model);
  ui->specs_tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::Stretch);
  ui->specs_tableView->clearSelection();
}

void add_playership_dialog::on_ships_comboBox_activated(int index)
{
  selected_ship = ui->ships_comboBox->itemData(index).toInt();
  refresh();
}

void add_playership_dialog::on_cancel_clicked()
{
  this->close();
}

void add_playership_dialog::on_add_clicked()
{
  Access *access = Access::getInstance();
  Db data = access->getDb();
  bool fscoop = ui->fuelScoop_check->isChecked();
  int id = ui->ships_comboBox->itemData(ui->ships_comboBox->currentIndex()).toInt();
  editTable::PlayerShips::add( data, id, fscoop );
  emit hasAdded();
  this->close();
}


