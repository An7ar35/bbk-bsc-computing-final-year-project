#ifndef LOG_DIALOG_HPP
#define LOG_DIALOG_HPP

#include <QDialog>
#include "../../EliteTracker/src/core/logger/Log.hpp"

namespace Ui {
  class log_dialog;
}

class log_dialog : public QDialog
{
    Q_OBJECT

  public:
    explicit log_dialog(QWidget *parent = 0);
    ~log_dialog();

  private slots:
    void on_core_clicked(bool checked);
    void on_satellite_clicked(bool checked);
    void on_io_clicked(bool checked);
    void on_sqlite_clicked(bool checked);
    void on_gui_clicked(bool checked);
    void on_os_clicked(bool checked);
    void on_errors_clicked(bool checked);
    void on_events_clicked(bool checked);

    void on_trace_clicked(bool checked);

    void on_debug_clicked(bool checked);

  private:
    Ui::log_dialog *ui;
    Log *log;
};

#endif // LOG_DIALOG_HPP
